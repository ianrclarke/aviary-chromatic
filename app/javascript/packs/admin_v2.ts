import "react-hot-loader";

import ReactRailsUJS from "react_ujs";

import "@styles/entry-points/patient.scss";

const componentRequireContext = require.context("admin", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
