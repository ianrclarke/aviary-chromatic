import ReactRailsUJS from "react_ujs";

import "@styles/framework.scss";

const componentRequireContext = require.context("@legacyComponents/customGraphiql", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
