import "react-hot-loader";

import ReactRailsUJS from "react_ujs";

import "@styles/entry-points/wholesaleCheckout.scss";

const componentRequireContext = require.context("@checkout/wholesale", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
