import "@styles/entry-points/oauth.scss";
import ReactRailsUJS from "react_ujs";

const componentRequireContext = require.context("oauth", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
