import ReactRailsUJS from "react_ujs";

import "@styles/entry-points/practitioner.scss";

const componentRequireContext = require.context("clinic/clerk", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
