import ReactRailsUJS from "react_ujs";

import "@styles/entry-points/unsubscribe.scss";

const componentRequireContext = require.context("unsubscribe", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
