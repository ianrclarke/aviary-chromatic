import ReactRailsUJS from "react_ujs";

import "@styles/entry-points/openApi.scss";

const componentRequireContext = require.context("@api/openApi", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
