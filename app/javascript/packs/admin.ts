import ReactRailsUJS from "react_ujs";

import "@styles/entry-points/admin.scss";

const componentRequireContext = require.context("@legacyComponents/admin", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
