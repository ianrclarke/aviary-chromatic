import ReactRailsUJS from "react_ujs";

const componentRequireContext = require.context("@legacyComponents/patient", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
