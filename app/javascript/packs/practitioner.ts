import * as hotLoader from "react-hot-loader";
import ReactRailsUJS from "react_ujs";

import "@styles/entry-points/practitioner.scss";

hotLoader.setConfig({ ErrorOverlay: () => null });

const componentRequireContext = require.context("clinic/practitioner", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
