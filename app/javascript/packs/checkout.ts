import ReactRailsUJS from "react_ujs";

import "@styles/entry-points/checkout.scss";

const componentRequireContext = require.context("@checkout/patient", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
