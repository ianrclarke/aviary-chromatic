import ReactRailsUJS from "react_ujs";

const componentRequireContext = require.context("@legacyComponents/login", false);
// eslint-disable-next-line react-hooks/rules-of-hooks
ReactRailsUJS.useContext(componentRequireContext);
