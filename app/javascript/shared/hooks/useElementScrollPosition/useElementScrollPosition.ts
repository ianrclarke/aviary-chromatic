import { useCallback, useRef, useState } from "react";

const useElementScrollPosition = () => {
  const ref = useRef<HTMLDivElement>(null);
  const [atTop, setAtTop] = useState(true);
  const [atBottom, setAtBottom] = useState(true);

  const checkScrollPosition = useCallback(() => {
    if (!ref.current) return;
    if (ref.current.scrollTop === 0) {
      setAtTop(true);
    } else if (atTop) {
      setAtTop(false);
    }
    if (ref.current.scrollHeight - ref.current.scrollTop === ref.current.clientHeight) {
      setAtBottom(true);
    } else if (atBottom) {
      setAtBottom(false);
    }
  }, []);

  // React will call the ref callback with the DOM element when the component mounts, and call it with null when it unmounts
  const setRef = useCallback((node: HTMLDivElement) => {
    if (ref.current) {
      ref.current.removeEventListener("scroll", checkScrollPosition);
    }
    ref.current = node;
    if (node) {
      checkScrollPosition();
      node.addEventListener("scroll", checkScrollPosition);
    }
  }, []);

  return { atTop, atBottom, checkScrollPosition, setRef };
};

export { useElementScrollPosition };
