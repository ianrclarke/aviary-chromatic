import { useEffect } from "react";

const useKeyPressed = (key: string, onKeyDownEvent: (e: KeyboardEvent) => void) => {
  useEffect(() => {
    const handleKeyDownEvent = (e: KeyboardEvent) => {
      if (e.key === key) {
        onKeyDownEvent(e);
      }
    };

    document.addEventListener("keydown", handleKeyDownEvent);
    return () => {
      document.removeEventListener("keydown", handleKeyDownEvent);
    };
  }, [onKeyDownEvent]);
};

export { useKeyPressed };
