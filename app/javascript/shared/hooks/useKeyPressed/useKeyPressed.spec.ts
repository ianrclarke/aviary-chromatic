import { hookWrapper, testHook } from "@testing/testHook";

import { useKeyPressed } from "./useKeyPressed";

describe("useKeyPressed", () => {
  let eventFn;
  let addEventListener;
  let removeEventListener;
  let onKeyPressed;
  const call = () => testHook(() => useKeyPressed("Escape", onKeyPressed));
  beforeEach(() => {
    addEventListener = jest.fn((e, fn) => e === "keydown" && (eventFn = fn));
    removeEventListener = jest.fn();
    // eslint-disable-next-line @typescript-eslint/unbound-method
    document.addEventListener = addEventListener;
    // eslint-disable-next-line @typescript-eslint/unbound-method
    document.removeEventListener = removeEventListener;
    onKeyPressed = jest.fn();
    call();
  });

  it("add event listener on render", () => {
    expect(addEventListener).toHaveBeenCalled();
  });

  it("does nothing if keyDown on different key", () => {
    call();
    eventFn({ key: "Space" });
    expect(onKeyPressed).not.toHaveBeenCalled();
  });

  it("calls eventFn if click and keyDown", () => {
    eventFn({ key: "Escape" });
    expect(onKeyPressed).toHaveBeenCalled();
  });

  it("updates event listeners whenever onKeyDownEvent is updated", () => {
    expect(addEventListener).toHaveBeenCalledTimes(1);
    onKeyPressed = jest.fn();
    hookWrapper.setProps();

    expect(removeEventListener).toHaveBeenCalledTimes(1);
    expect(addEventListener).toHaveBeenCalledTimes(2);
  });
});
