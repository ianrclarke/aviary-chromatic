import { act } from "react-dom/test-utils";

import { testHook } from "@testing/testHook";

import { useControlledState } from "./useControlledState";

let mockValue;
let mockSetFn;

describe("useControlledState", () => {
  const call = (mockControlValue, mockControlledOptions?) => {
    const useHook = () => {
      [mockValue, mockSetFn] = useControlledState(mockControlValue, mockControlledOptions);
    };
    testHook(useHook);
  };

  describe("useControlledState", () => {
    it("When controlledOptions is absent, mockValue is unchanged", () => {
      call(false);
      expect(mockValue).toBe(false);
    });
    it("When controlledOptions is absent, mockValue will be updated to the value that's passed into setState", () => {
      call(false);

      act(() => {
        mockSetFn(true);
      });
      expect(mockValue).toBe(true);
    });

    it("When controlledOptions is present, hook will take the value from controlledOptions", () => {
      const mockFn = jest.fn();
      call(false, { value: true, setValue: mockFn });
      expect(mockValue).toBe(true);
    });

    it("When controlledOptions is present, hook will take the value function from controlledOptions", () => {
      const mockFn = jest.fn();
      call(false, { value: () => true, setValue: mockFn });
      expect(mockValue).toBe(true);
    });

    it("When controlledOption is present, hook's controlledOptions setState will be called", () => {
      const mockFn = jest.fn();
      call(false, { value: false, setValue: mockFn });
      act(() => {
        mockSetFn(true);
      });

      expect(mockFn).toHaveBeenCalledWith(true);
      expect(mockValue).toBe(false);
    });
  });
});
