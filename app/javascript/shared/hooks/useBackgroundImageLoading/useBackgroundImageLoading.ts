import { useEffect, useState } from "react";

const useBackgroundImageLoading = src => {
  const [isImageLoading, setIsImageLoading] = useState(true);

  useEffect(() => {
    if (src) {
      const img = new Image();
      img.onload = () => setIsImageLoading(false);
      img.onerror = () => setIsImageLoading(false);
      img.src = src;
    }
  }, []);

  return { isImageLoading };
};

export { useBackgroundImageLoading };
