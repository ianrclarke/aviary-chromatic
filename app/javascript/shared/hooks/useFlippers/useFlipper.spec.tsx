import { useFlippers } from "./useFlippers";

let mockAvailableFlippers;

jest.mock("react", () => ({
  useContext: () => mockAvailableFlippers,
  createContext: () => {
    return null;
  },
}));

describe("useFlippers", () => {
  const flipper1 = "practitioner_v2_recommendations_page_trevor";
  const flipper2 = "practitioner_v2_wholesale_aidan";

  describe("when no available flippers are present", () => {
    beforeEach(() => {
      mockAvailableFlippers = [];
    });

    it("returns false", () => {
      expect(useFlippers(flipper1)).toEqual([false]);
    });
  });

  describe("when available flippers are present", () => {
    beforeEach(() => {
      mockAvailableFlippers = [flipper1];
    });

    describe("when the passed flipper is present", () => {
      it("returns true", () => {
        expect(useFlippers(flipper1)).toEqual([true]);
      });
    });

    describe("when multiple flippers are checked", () => {
      it("returns a value for each flipper", () => {
        expect(useFlippers(flipper1, flipper2)).toEqual([true, false]);
      });
    });
  });
});
