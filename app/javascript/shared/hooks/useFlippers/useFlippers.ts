import { useContext } from "react";

import { FlippersContext } from "@shared/context";
import { Flipper } from "@shared/types/flipper";

const useFlippers = (...args: Flipper[]): boolean[] => {
  const availableFlippers = useContext(FlippersContext);
  return args.map(flipper => availableFlippers.includes(flipper));
};

export { useFlippers };
