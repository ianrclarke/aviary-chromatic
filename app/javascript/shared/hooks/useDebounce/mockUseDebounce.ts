const mockDebounceProperties = {
  cancel: jest.fn(),
  callback: undefined,
  debounce: jest.fn(),
};

const initUseDebounce = () => {
  mockDebounceProperties.cancel = jest.fn();
  mockDebounceProperties.callback = undefined;
  mockDebounceProperties.debounce = jest.fn();
};

const mockUseDebounce = cb => {
  (mockDebounceProperties.debounce as any).cancel = mockDebounceProperties.cancel;
  mockDebounceProperties.debounce.mockImplementation((...args) => {
    mockDebounceProperties.callback = () => cb(...args);
  });
  return mockDebounceProperties.debounce;
};

export { initUseDebounce, mockUseDebounce, mockDebounceProperties };
