import { testHook } from "@testing/testHook";

import { useDebounce } from "./useDebounce";

describe("useDebounce", () => {
  let debouncedFn;
  const call = (fn: (...args: any) => any, delay: number) => {
    const useHook = () => {
      debouncedFn = useDebounce(fn, delay);
    };
    testHook(useHook);
  };

  it("should only invoke the last function call", () => {
    const delay = 100;
    const dummyFn = jest.fn();
    call(dummyFn, delay);
    debouncedFn(1);
    debouncedFn(2);
    debouncedFn(3);
    expect(dummyFn).toBeCalledTimes(0);
    setTimeout(() => {
      expect(dummyFn).toBeCalledTimes(1);
    }, delay + 50);
  });
});
