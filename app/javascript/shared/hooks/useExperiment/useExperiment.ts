import { useContext } from "react";

import { hashCode } from "@helpers/hashCode";
import { ExperimentContext } from "@shared/context";
import { useExperimentCreateMutation } from "@shared/data/mutations";

const cache = {};

const shouldCall = (experiment: string, group: string, subjectId: string) => {
  return (
    !cache[experiment] ||
    !cache[experiment][group] ||
    !cache[experiment][group][subjectId] ||
    !cache[experiment][group][subjectId]
  );
};

const called = (experiment: string, group: string, subjectId: string) => {
  if (!cache[experiment]) cache[experiment] = {};
  if (!cache[experiment][group]) cache[experiment][group] = {};
  if (!cache[experiment][group][subjectId]) cache[experiment][group][subjectId] = {};
  cache[experiment][group][subjectId] = true;
};

const logOnce = (
  logFn: (experiment: string, owner: string, group: string, subjectId: string) => void,
  experiment: string,
  owner: string,
  group: string,
  subjectId: string
) => {
  if (shouldCall(experiment, group, subjectId)) {
    logFn(experiment, owner, group, subjectId);
    called(experiment, group, subjectId);
  }
};

interface ExperimentOptions {
  subjectId?: string;
  subjectType?: "user" | "role" | "store";
  experiment: string;
  owner: string;
  groups: string[];
  e2eGroup?: string;
}

const useExperiment = ({
  subjectId,
  subjectType = "user",
  experiment,
  owner,
  groups,
  e2eGroup,
}: ExperimentOptions) => {
  const [logMutation] = useExperimentCreateMutation();
  const { roleId, userId, storeId, e2e } = useContext(ExperimentContext);

  if (e2e) {
    return e2eGroup ?? groups[0];
  }

  const getSubjectId = () => {
    if (subjectId) return subjectId;
    switch (subjectType) {
      case "role":
        return roleId;
      case "store":
        return storeId;
      case "user":
      default:
        return userId;
    }
  };

  const log = (experimentName: string, ownerName: string, groupName: string, id: string) => {
    logMutation({
      variables: {
        input: { experiment: experimentName, owner: ownerName, group: groupName, subjectId: id },
      },
    });
  };

  const mod = (value: number, by: number) => ((value % by) + by) % by;
  const selectGroup = () => {
    const count = groups.length;
    const value = hashCode(`${experiment}${getSubjectId()}`);
    const index = mod(value, count);

    return [...groups].sort()[index];
  };

  const selectedGroup = selectGroup();
  logOnce(log, experiment, owner, selectedGroup, getSubjectId());

  return selectedGroup;
};

export { useExperiment };
