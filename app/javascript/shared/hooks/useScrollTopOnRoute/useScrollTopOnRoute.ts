import { useEffect } from "react";
import { useHistory } from "react-router-dom";

const useScrollTopOnRoute = () => {
  const history = useHistory();
  useEffect(() => {
    if (history.location.state !== "PRESERVE_SCROLL_POSITION") {
      /*
      Why this condition?
      There is a custom hook that is defined at /shared/hooks/useScrollPosition/useScrollPosition.tsx
      It's used to preserve the scrolling position of pages on demand.
      When the useScrollPosition is used, we set the state of the history.location to "PRESERVE_SCROLL_POSITION"
      For this hook, we only want to scroll to the very top if the hook is not used (aka) the history.location.state is not "PRESERVE_SCROLL_POSITION"
      */
      window.scrollTo(0, 0);
    }
  }, [history.location.pathname]);
};

export { useScrollTopOnRoute };
