import { useEffect, useState } from "react";

import { breakpoints } from "@styles/emotion-styles/dimensions";

type Breakpoint = keyof typeof breakpoints;
interface BreakpointDetails {
  min: number;
  max: number;
  active: boolean;
  greaterThan: boolean;
  lessThan: boolean;
}
type BreakpointsActiveMap = Record<Breakpoint, BreakpointDetails>;

const useBreakpoints = () => {
  const getInnerWidth = () => {
    return window ? window.innerWidth : breakpoints.fullHD.max;
  };

  const [width, setWidth] = useState(getInnerWidth());

  const handleResize = () => {
    setWidth(getInnerWidth());
  };

  useEffect(() => {
    if (!window) return;
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const breakpointLessThan = (breakpoint: Breakpoint) => width < breakpoints[breakpoint].min;
  const breakpointGreaterThan = (breakpoint: Breakpoint) => width > breakpoints[breakpoint].max;
  const breakpointActive = (breakpoint: Breakpoint) => {
    return !breakpointLessThan(breakpoint) && !breakpointGreaterThan(breakpoint);
  };

  const breakpointDetails = (breakpoint: Breakpoint): BreakpointDetails => ({
    min: breakpoints[breakpoint].min,
    max: breakpoints[breakpoint].max,

    active: breakpointActive(breakpoint),
    greaterThan: breakpointGreaterThan(breakpoint),
    lessThan: breakpointLessThan(breakpoint),
  });

  const breakpointsActiveMap = Object.keys(breakpoints).reduce((map, breakpoint: Breakpoint) => {
    map[breakpoint] = breakpointDetails(breakpoint);
    return map;
  }, {});

  return {
    width,
    ...(breakpointsActiveMap as BreakpointsActiveMap),
  };
};

export { useBreakpoints };
