import { breakpoints } from "@styles/emotion-styles/dimensions";
import { testHook } from "@testing/testHook";

import { useBreakpoints } from "./useBreakpoints";
type Breakpoint = keyof typeof breakpoints;

describe("useBreakpoints", () => {
  let breakpointResults: ReturnType<typeof useBreakpoints>;
  const call = () => testHook(() => (breakpointResults = useBreakpoints()));
  const setWidth = width => ((window as any).innerWidth = width);

  beforeEach(() => {
    setWidth(300);
  });

  it("returns the width of the window", () => {
    call();
    expect(breakpointResults.width).toEqual(300);
  });

  describe.each(Object.keys(breakpoints) as Breakpoint[])("breakpoint: %s", breakpoint => {
    it("returns the min and max", () => {
      call();
      expect(breakpointResults[breakpoint].min).toEqual(breakpoints[breakpoint].min);
      expect(breakpointResults[breakpoint].max).toEqual(breakpoints[breakpoint].max);
    });

    it("returns true for active if width is min", () => {
      setWidth(breakpoints[breakpoint].min);
      call();
      expect(breakpointResults[breakpoint].lessThan).toEqual(false);
      expect(breakpointResults[breakpoint].greaterThan).toEqual(false);
      expect(breakpointResults[breakpoint].active).toEqual(true);
    });

    it("returns true for lessThan if width < min", () => {
      setWidth(breakpoints[breakpoint].min - 1);
      call();
      expect(breakpointResults[breakpoint].lessThan).toEqual(true);
      expect(breakpointResults[breakpoint].greaterThan).toEqual(false);
      expect(breakpointResults[breakpoint].active).toEqual(false);
    });

    it("returns true for active if width is min + 1", () => {
      setWidth(breakpoints[breakpoint].min + 1);
      call();
      expect(breakpointResults[breakpoint].lessThan).toEqual(false);
      expect(breakpointResults[breakpoint].greaterThan).toEqual(false);
      expect(breakpointResults[breakpoint].active).toEqual(true);
    });

    it("returns true for active if width is max", () => {
      setWidth(breakpoints[breakpoint].max);
      call();
      expect(breakpointResults[breakpoint].lessThan).toEqual(false);
      expect(breakpointResults[breakpoint].greaterThan).toEqual(false);
      expect(breakpointResults[breakpoint].active).toEqual(true);
    });

    if (breakpoints[breakpoint].max !== Number.MAX_SAFE_INTEGER) {
      it("returns true for greaterThan if width is max + 1", () => {
        setWidth(breakpoints[breakpoint].max + 1);
        call();
        expect(breakpointResults[breakpoint].lessThan).toEqual(false);
        expect(breakpointResults[breakpoint].greaterThan).toEqual(true);
        expect(breakpointResults[breakpoint].active).toEqual(false);
      });
    }
  });
});
