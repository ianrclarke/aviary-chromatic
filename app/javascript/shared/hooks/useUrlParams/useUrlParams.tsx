/* eslint-disable prefer-arrow/prefer-arrow-functions */

import { useHistory } from "react-router-dom";

interface Options {
  pathname?: string;
  replace?: boolean;
}

const ARRAY_SUFFIX_INDICATOR = "_array";

function getParamsString<T>(paramObject: T): string {
  const urlParams = new URLSearchParams();
  const filteredParamsEntries = Object.entries(paramObject).filter(entry => entry[1] !== undefined);

  filteredParamsEntries.forEach(([key, value]) => {
    if (Array.isArray(value)) {
      value.forEach(subValue => {
        urlParams.append(key + ARRAY_SUFFIX_INDICATOR, JSON.stringify(subValue));
      });
    } else {
      urlParams.append(key, JSON.stringify(value));
    }
  });

  return urlParams.toString();
}

function parseParamsString<T>(paramString: string): T {
  const urlParams = new URLSearchParams(paramString);
  const paramObject = {};

  const isArrayParam = (key: string) => key.endsWith(ARRAY_SUFFIX_INDICATOR);
  const removeArrayIndicator = (key: string) =>
    key.slice(0, key.length - ARRAY_SUFFIX_INDICATOR.length);

  urlParams.forEach((value, key) => {
    if (isArrayParam(key)) {
      const parsedKey = removeArrayIndicator(key);
      const currentArray = paramObject[parsedKey] || [];

      paramObject[parsedKey] = [...currentArray, JSON.parse(value)];
    } else if (key in paramObject) {
      paramObject[key] = [...paramObject[key], JSON.parse(value)];
    } else {
      paramObject[key] = JSON.parse(value);
    }
  });

  return paramObject as T;
}

function useUrlParams<T extends Record<string, any>>(): [
  T,
  (params: T, options?: Options) => void
] {
  const history = useHistory();
  const searchParams = history.location.search;

  const setUrlParams = (params: T, options?: Options): void => {
    const defaultOptions = {
      pathname: history.location.pathname,
      replace: false,
    };

    const { pathname, replace } = { ...defaultOptions, ...options };

    const location = {
      pathname,
      search: getParamsString<T>(params),
    };

    if (replace) {
      history.replace(location);
    } else {
      history.push(location);
    }
  };

  return [parseParamsString<T>(searchParams), setUrlParams];
}

export { useUrlParams, parseParamsString, getParamsString, Options as ParamsOptions };
