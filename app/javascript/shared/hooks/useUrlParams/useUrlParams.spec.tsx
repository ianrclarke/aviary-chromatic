import { useUrlParams, getParamsString, parseParamsString } from "./useUrlParams";

let mockHistory;
let mockSearchString;
let mockPush;
let mockReplace;
let mockPathname;

jest.mock("react-router-dom", () => ({
  useHistory: () => mockHistory,
}));

describe("useUrlParams", () => {
  let urlParams;
  let setUrlParams;

  beforeEach(() => {
    mockPush = jest.fn();
    mockReplace = jest.fn();
    mockSearchString = "?testParam=1234";
    mockHistory = {
      location: { pathname: "test-pathname", search: mockSearchString },
      push: mockPush,
      replace: mockReplace,
    };
    mockPathname = "pathname";
    [urlParams, setUrlParams] = useUrlParams();
  });

  it("returns the current URL params", () => {
    expect(urlParams).toEqual({ testParam: 1234 });
  });

  it("updates the URL params when setOrderParams is used", () => {
    setUrlParams({ testParam: 5678 });
    expect(mockPush).toHaveBeenCalledWith({
      pathname: mockHistory.location.pathname,
      search: "testParam=5678",
    });
  });

  it("skips adding undefined params to the URL", () => {
    setUrlParams({ testParam: 5678, isUndefined: undefined });
    expect(mockPush).toHaveBeenCalledWith({
      pathname: mockHistory.location.pathname,
      search: "testParam=5678",
    });
  });

  describe("options", () => {
    it("if a replace option is true, it will replace in history instead of push", () => {
      setUrlParams({ testParam: 5678 }, { replace: true });
      expect(mockReplace).toHaveBeenCalledWith({
        pathname: mockHistory.location.pathname,
        search: "testParam=5678",
      });
    });

    it("if a pathname is provided, it will use that", () => {
      setUrlParams({ testParam: 5678 }, { pathname: mockPathname });
      expect(mockPush).toHaveBeenCalledWith({
        pathname: mockPathname,
        search: "testParam=5678",
      });
    });
  });

  describe("can correctly interpret the query param when it is", () => {
    it("a boolean", () => {
      mockSearchString = getParamsString({ isBoolean: false });
      mockHistory = {
        location: { pathname: "test-pathname", search: mockSearchString },
        push: mockPush,
      };
      [urlParams, setUrlParams] = useUrlParams();

      expect(urlParams).toEqual({ isBoolean: false });
    });
    it("a number", () => {
      mockSearchString = getParamsString({ isNumber: 44 });
      mockHistory = {
        location: { pathname: "test-pathname", search: mockSearchString },
        push: mockPush,
      };
      [urlParams, setUrlParams] = useUrlParams();

      expect(urlParams).toEqual({ isNumber: 44 });
    });

    it("a falsy number", () => {
      mockSearchString = getParamsString({ isNumber: 0 });
      mockHistory = {
        location: { pathname: "test-pathname", search: mockSearchString },
        push: mockPush,
      };
      [urlParams, setUrlParams] = useUrlParams();

      expect(urlParams).toEqual({ isNumber: 0 });
    });
    it("a string", () => {
      mockSearchString = getParamsString({ isString: "1234" });
      mockHistory = {
        location: { pathname: "test-pathname", search: mockSearchString },
        push: mockPush,
      };
      [urlParams, setUrlParams] = useUrlParams();

      expect(urlParams).toEqual({ isString: "1234" });
    });

    it("an empty string", () => {
      mockSearchString = getParamsString({ isString: "" });
      mockHistory = {
        location: { pathname: "test-pathname", search: mockSearchString },
        push: mockPush,
      };
      [urlParams, setUrlParams] = useUrlParams();

      expect(urlParams).toEqual({ isString: "" });
    });

    it("a string of unpleasant characters", () => {
      mockSearchString = getParamsString({ isString: '"" < > # % { } | \\ ^ ~ [ ] ``' });
      mockHistory = {
        location: { pathname: "test-pathname", search: mockSearchString },
        push: mockPush,
      };
      [urlParams, setUrlParams] = useUrlParams();

      expect(urlParams).toEqual({ isString: '"" < > # % { } | \\ ^ ~ [ ] ``' });
    });
    it("an array", () => {
      mockSearchString = getParamsString({ isArray: ["zero", 1, true] });
      mockHistory = {
        location: { pathname: "test-pathname", search: mockSearchString },
        push: mockPush,
      };
      [urlParams, setUrlParams] = useUrlParams();

      expect(urlParams).toEqual({ isArray: ["zero", 1, true] });
    });

    it("an array containing a single string", () => {
      mockSearchString = getParamsString({ isArray: ["813"] });
      mockHistory = {
        location: { pathname: "test-pathname", search: mockSearchString },
        push: mockPush,
      };
      [urlParams, setUrlParams] = useUrlParams();

      expect(urlParams).toEqual({ isArray: ["813"] });
    });

    it("an array containing two strings", () => {
      mockSearchString = getParamsString({ isArray: ["zero", "1"] });
      mockHistory = {
        location: { pathname: "test-pathname", search: mockSearchString },
        push: mockPush,
      };
      [urlParams, setUrlParams] = useUrlParams();

      expect(urlParams).toEqual({ isArray: ["zero", "1"] });
    });

    it("an object", () => {
      mockSearchString = getParamsString({ isObject: { 0: "zero", one: 1, two: true } });
      mockHistory = {
        location: { pathname: "test-pathname", search: mockSearchString },
        push: mockPush,
      };
      [urlParams, setUrlParams] = useUrlParams();

      expect(urlParams).toEqual({ isObject: { 0: "zero", one: 1, two: true } });
    });

    it("an array of objects", () => {
      mockSearchString = getParamsString({
        isObjectArray: [
          { 0: "zero" },
          { one: 1 },
          { two: true },
          { nestedObject: { 0: "zero", one: 1, two: true } },
        ],
      });
      mockHistory = {
        location: { pathname: "test-pathname", search: "?" + mockSearchString },
        push: mockPush,
      };
      [urlParams, setUrlParams] = useUrlParams();

      expect(urlParams).toEqual({
        isObjectArray: [
          { 0: "zero" },
          { one: 1 },
          { two: true },
          { nestedObject: { 0: "zero", one: 1, two: true } },
        ],
      });
    });
  });

  describe("getParamsString", () => {
    describe("when passed an object with a string", () => {
      const testObject = { testString: '"" < > # % { } | \\ ^ ~ [ ] ``' };

      it("returns a URL encoded string", () => {
        expect(getParamsString(testObject)).toEqual(
          "testString=%22%5C%22%5C%22+%3C+%3E+%23+%25+%7B+%7D+%7C+%5C%5C+%5E+%7E+%5B+%5D+%60%60%22"
        );
      });

      it("returns something that can be parsed by parseParamsString", () => {
        const testString = getParamsString(testObject);
        expect(parseParamsString(testString)).toEqual(testObject);
      });
    });

    describe("when passed an object with a boolean", () => {
      const testObject = { testString: true };

      it("returns a URL string", () => {
        expect(getParamsString(testObject)).toEqual("testString=true");
      });

      it("returns something that can be parsed by parseParamsString", () => {
        const testString = getParamsString(testObject);
        expect(parseParamsString(testString)).toEqual(testObject);
      });
    });

    describe("when passed an object with a number", () => {
      const testObject = { testNumber: 0 };

      it("returns a URL encoded string", () => {
        expect(getParamsString(testObject)).toEqual("testNumber=0");
      });

      it("returns something that can be parsed by parseParamsString", () => {
        const testString = getParamsString(testObject);
        expect(parseParamsString(testString)).toEqual(testObject);
      });
    });

    describe("when passed an object with an object", () => {
      const testObject = { simpleObject: { 0: "zero", one: 1, two: true } };

      it("returns a URL encoded string", () => {
        expect(getParamsString(testObject)).toEqual(
          "simpleObject=%7B%220%22%3A%22zero%22%2C%22one%22%3A1%2C%22two%22%3Atrue%7D"
        );
      });

      it("returns something that can be parsed by parseParamsString", () => {
        const testString = getParamsString(testObject);
        expect(parseParamsString(testString)).toEqual(testObject);
      });
    });

    describe("when passed an object with an array", () => {
      const testObject = { testArray: [0, "1", true] };

      it("returns a URL encoded string", () => {
        expect(getParamsString(testObject)).toEqual(
          "testArray_array=0&testArray_array=%221%22&testArray_array=true"
        );
      });

      it("returns something that can be parsed by parseParamsString", () => {
        const testString = getParamsString(testObject);
        expect(parseParamsString(testString)).toEqual(testObject);
      });
    });
  });

  describe("parseParamsString", () => {
    it("takes a url param string and parses it into an object", () => {
      const testString =
        "0=%22a%22&a=%221%22&b=0&d=%22true%22&e_array=%22one%22&e_array=2&e_array=false";
      const expectedObject = { a: "1", b: 0, 0: "a", d: "true", e: ["one", 2, false] };

      expect(parseParamsString(testString)).toEqual(expectedObject);
    });
  });
});
