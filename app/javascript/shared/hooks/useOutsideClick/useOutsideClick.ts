import { MutableRefObject, useLayoutEffect } from "react";

const useOutsideClick = <T extends HTMLElement>(
  ref: MutableRefObject<T>,
  onClick: (e: MouseEvent) => void
) => {
  useLayoutEffect(() => {
    const handleClick = (e: MouseEvent) => {
      if (ref && ref.current && !ref.current.contains(e.target as any)) {
        onClick(e);
      }
    };
    document.addEventListener("click", handleClick);
    return () => {
      document.removeEventListener("click", handleClick);
    };
  }, [onClick]);
};

export { useOutsideClick };
