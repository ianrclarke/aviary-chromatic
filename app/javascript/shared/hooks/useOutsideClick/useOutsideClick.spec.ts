import { createRef } from "react";

import { testHook } from "@testing/testHook";

import { useOutsideClick } from "./useOutsideClick";

describe("useOutsideClick", () => {
  let eventFn;
  let addEventListener;
  let removeEventListener;
  let ref;
  let onClick;
  let children;
  const call = () => testHook(() => useOutsideClick(ref, onClick));
  beforeEach(() => {
    addEventListener = jest.fn((e, fn) => e === "click" && (eventFn = fn));
    removeEventListener = jest.fn();
    // eslint-disable-next-line @typescript-eslint/unbound-method
    document.addEventListener = addEventListener;
    // eslint-disable-next-line @typescript-eslint/unbound-method
    document.removeEventListener = removeEventListener;

    ref = createRef();
    children = ["a", "b"];
    ref.current = {
      contains: value => children.includes(value),
    };
    onClick = jest.fn();
    call();
  });

  it("add event listener on render", () => {
    expect(addEventListener).toHaveBeenCalled();
  });

  it("does nothing if click and no ref exists", () => {
    ref = null;
    call();
    eventFn({ target: "c" });
    expect(onClick).not.toHaveBeenCalled();
  });

  it("does nothing if click is within the ref", () => {
    eventFn({ target: children[0] });
    expect(onClick).not.toHaveBeenCalled();
  });

  it("calls eventFn if click is not within the ref", () => {
    eventFn({ target: "c" });
    expect(onClick).toHaveBeenCalled();
  });
});
