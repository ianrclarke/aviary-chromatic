import { act } from "react-dom/test-utils";

import { testHook } from "@testing/testHook";

import { useBooleanAggregator } from "./useBooleanAggregator";

describe("useBooleanAggregator", () => {
  let mockActive;
  let mockIncrement;
  let mockDecrement;
  const call = () => {
    const useHook = () => {
      const [active, increment, decrement] = useBooleanAggregator();
      mockIncrement = () => {
        act(() => {
          increment();
        });
      };
      mockDecrement = () => {
        act(() => {
          decrement();
        });
      };
      mockActive = active;
    };
    testHook(useHook);
  };

  it("should return FALSE on initial state", () => {
    call();
    expect(mockActive).toBe(false);
  });

  it("should return TRUE when still active", () => {
    call();
    mockIncrement();
    expect(mockActive).toBe(true);
  });

  it("should return FALSE when not active", () => {
    call();
    mockIncrement();
    mockDecrement();
    expect(mockActive).toBe(false);
  });

  it("should return TRUE after caling increment twice but decrement only once", () => {
    call();
    mockIncrement();
    mockIncrement();
    mockDecrement();
    expect(mockActive).toBe(true);
  });

  it("should set active as FALSE after caling increment and decrement twice", () => {
    call();
    mockIncrement();
    mockIncrement();
    mockDecrement();
    mockDecrement();
    expect(mockActive).toBe(false);
  });
});
