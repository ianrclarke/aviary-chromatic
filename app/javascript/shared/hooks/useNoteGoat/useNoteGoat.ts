import { useContext } from "react";

import { NoteGoatContext, NoteGoatData } from "@shared/context";

const useNoteGoat = (): NoteGoatData => {
  return useContext(NoteGoatContext);
};

export { useNoteGoat };
