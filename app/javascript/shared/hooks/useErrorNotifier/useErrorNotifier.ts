import { useContext } from "react";

import { ErrorNotifierContext, ErrorNotifierData } from "@shared/context";

const useErrorNotifier = (): ErrorNotifierData => {
  return useContext(ErrorNotifierContext);
};

export { useErrorNotifier };
