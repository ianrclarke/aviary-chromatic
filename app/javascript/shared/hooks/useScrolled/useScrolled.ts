import { useEffect } from "react";

const useScrolled = ({
  onScrollDown,
  onScrollUp,
}: {
  onScrollDown: (e: Event) => void;
  onScrollUp: (e: Event) => void;
}) => {
  let ticking = false;
  let lastScrollPosition = 0;

  const handleScroll = (e: Event) => {
    if (!ticking) {
      window.requestAnimationFrame(() => {
        if (lastScrollPosition < window.pageYOffset) {
          onScrollDown(e);
        } else {
          onScrollUp(e);
        }

        ticking = false;
        lastScrollPosition = window.pageYOffset;
      });

      ticking = true;
    }
  };

  useEffect(() => {
    document.addEventListener("scroll", handleScroll);

    return () => {
      document.removeEventListener("scroll", handleScroll);
    };
  }, [onScrollDown]);
};

export { useScrolled };
