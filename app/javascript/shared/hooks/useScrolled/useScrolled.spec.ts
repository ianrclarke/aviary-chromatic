import { testHook } from "@testing/testHook";

import { useScrolled } from "./useScrolled";

describe("useScrolled", () => {
  let mockEventListener;
  let onScrollDown;
  let onScrollUp;
  let eventCallback;

  const mount = () => testHook(() => useScrolled({ onScrollDown, onScrollUp }));

  const mockRequestAnimationFrame = () => {
    window.requestAnimationFrame = (callback: FrameRequestCallback) => {
      callback(0);
      return 0;
    };
  };

  const mockAddEventListener = () => {
    mockEventListener = jest.fn((_, callBack) => {
      eventCallback = callBack;
    });

    // eslint-disable-next-line @typescript-eslint/unbound-method
    document.addEventListener = mockEventListener;
  };

  const mockpageYOffset = value => {
    // eslint-disable-next-line no-global-assign
    window = Object.create(window);
    Object.defineProperty(window, "pageYOffset", {
      value,
    });
  };

  beforeEach(() => {
    onScrollDown = jest.fn();
    onScrollUp = jest.fn();
    mockRequestAnimationFrame();
    mockAddEventListener();
    mount();
  });

  it("adds an event listener", () => {
    expect(mockEventListener).toHaveBeenCalled();
  });

  it("calls onScrollDown when scrolling downwards", () => {
    mockpageYOffset(200);
    mount();
    eventCallback();
    expect(onScrollDown).toHaveBeenCalled();
  });

  it("calls onScrollUp when scrolling upwards", () => {
    mockpageYOffset(-100);
    mount();
    eventCallback();
    expect(onScrollUp).toHaveBeenCalled();
  });
});
