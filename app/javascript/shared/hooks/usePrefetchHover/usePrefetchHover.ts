import { useEffect, useState } from "react";

import { useInterval } from "@shared/hooks/useInterval";

const usePrefetchHover = (onPrefetch?: () => void, mouseOverTime?: number) => {
  const [prefetch, setPrefetch] = useState(false);
  const [mouseOver, setMouseOver] = useState(false);

  useInterval(() => setPrefetch(true), mouseOver && mouseOverTime);
  useEffect(() => {
    if (prefetch && onPrefetch) {
      onPrefetch();
    }
  }, [prefetch, onPrefetch]);

  const handleMouseEnter = () => {
    if (mouseOverTime) {
      setMouseOver(true);
    } else {
      setPrefetch(true);
    }
  };

  const handleMouseLeave = () => {
    setMouseOver(false);
  };

  return { prefetch, handleMouseEnter, handleMouseLeave };
};

export { usePrefetchHover };
