import { act } from "react-dom/test-utils";

import { testHook } from "@testing/testHook";

import { usePrefetchHover } from "./usePrefetchHover";

describe("usePrefetchHover", () => {
  let prefetch;
  let handleMouseEnter;
  let handleMouseLeave;
  const call = (onPrefetch?: () => void, mouseOverTime?: number) => {
    const useHook = () => {
      const output = usePrefetchHover(onPrefetch, mouseOverTime);
      handleMouseEnter = () => {
        act(() => {
          output.handleMouseEnter();
        });
      };
      handleMouseLeave = () => {
        act(() => {
          output.handleMouseLeave();
        });
      };
      prefetch = output.prefetch;
    };
    testHook(useHook);
  };

  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.clearAllTimers();
  });

  it("prefetch is false by default", () => {
    call();
    expect(prefetch).toEqual(false);
  });

  it("prefetch is true if mouseEnter with no mouseOverTime is specified", () => {
    call();
    handleMouseEnter(null);
    expect(prefetch).toEqual(true);
  });

  it("prefetch is true if mouseEnter and mouseLeave with no mouseOverTime is specified", () => {
    call();
    handleMouseEnter(null);
    handleMouseLeave(null);
    expect(prefetch).toEqual(true);
  });

  it("does not call onPrefetch when prefetch is not triggered", () => {
    const onPrefetch = jest.fn();
    call(onPrefetch);
    expect(onPrefetch).not.toHaveBeenCalled();
  });

  it("calls onPrefetch when prefetch is triggered", () => {
    const onPrefetch = jest.fn();
    call(onPrefetch);
    handleMouseEnter(null);
    expect(onPrefetch).toHaveBeenCalled();
  });

  it("does not trigger prefetch if Mouse is over for less then specified time", () => {
    call(null, 300);
    handleMouseEnter(null);
    expect(prefetch).toEqual(false);
  });

  it("does not trigger prefetch if Mouse is over for less then specified time then mouse leave", () => {
    call(null, 300);
    handleMouseEnter(null);
    jest.advanceTimersByTime(100);
    handleMouseLeave(null);
    jest.advanceTimersByTime(200);

    expect(prefetch).toEqual(false);
  });

  it("trigger prefetch if Mouse is over the specified time", () => {
    call(null, 300);
    handleMouseEnter(null);
    act(() => {
      jest.advanceTimersByTime(300);
    });

    expect(prefetch).toEqual(true);
  });
});
