import { MutableRefObject, useLayoutEffect } from "react";

// This is used only for the wholesale StickyNav
// We need to determine the height of the nav so that the treatment plan drawer can stick to it
// Once the treatment plan drawer is removed from the app, we can get rid of this
const useHeightEvent = <T extends HTMLElement>(
  eventName: string,
  ref: MutableRefObject<T>,
  deps = []
) => {
  useLayoutEffect(() => {
    if (ref) {
      const height = ref.current ? ref.current.offsetHeight : 0;
      const event = new CustomEvent(eventName, { detail: { height } });
      document.dispatchEvent(event);
    }
  }, deps);
};

export { useHeightEvent };
