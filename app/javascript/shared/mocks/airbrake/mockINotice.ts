import { INotice } from "@airbrake/browser";

import { createINoticeError } from "./mockINoticeError";

export const createINotice = (id: string, options: any = {}): INotice => {
  return {
    id,
    context: options.context || {},
    environment: options.environment || {},
    errors: options.errors || [createINoticeError()],
    params: options.params || {},
    session: options.session || {},
  };
};
