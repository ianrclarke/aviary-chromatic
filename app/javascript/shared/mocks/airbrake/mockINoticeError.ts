import { INoticeError } from "@airbrake/browser/dist/notice";

export const createINoticeError = (options: any = {}): INoticeError => {
  return {
    type: options.type || "Error",
    message: options.message || "error message",
    backtrace: options.backtrace || [],
  };
};
