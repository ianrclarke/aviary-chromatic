import React, { FC, ReactNode } from "react";
import { MemoryRouter } from "react-router-dom";

import { mockMatch } from "./RouteComponentProps";

interface Props {
  children: JSX.Element | ReactNode;
  initialEntries?: string[];
}

const MockRouter: FC<Props> = props => {
  return (
    <MemoryRouter initialEntries={props.initialEntries || [mockMatch.url]} keyLength={0}>
      {props.children}
    </MemoryRouter>
  );
};

export { MockRouter };
