import { History, Location } from "history";
import { match, RouteComponentProps } from "react-router-dom";

const path = "/localhost/v2/fake/url";

export const mockLocation: Location = {
  pathname: "",
  search: "",
  state: {},
  hash: "",
  key: "",
};

export const mockHistory: History = {
  length: 2,
  action: "POP",
  location: mockLocation,
  push: jest.fn(),
  replace: jest.fn(),
  go: jest.fn(),
  goBack: jest.fn(),
  goForward: jest.fn(),
  block: jest.fn(),
  createHref: jest.fn(),
  listen: jest.fn(),
};

export const mockMatch: match = {
  params: {},
  url: path,
  isExact: false,
  path,
};

export const mockRouteComponentProps: RouteComponentProps = {
  match: mockMatch,
  location: mockLocation,
  history: mockHistory,
};
