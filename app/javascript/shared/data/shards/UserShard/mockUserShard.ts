import { RoleableFragment } from "@shared/data/fragments";
import { createRoleableFragment, createUserFragment } from "@shared/data/fragments/mocks";

import { UserShard } from "./UserShard";

const DEFAULT_VALUES = { rolesCount: 1 };
export const createUserShard = (id: string, options = {}): UserShard => {
  const values = { ...DEFAULT_VALUES, ...options };

  const userFragment = createUserFragment(`${id}-user-fragment`);

  const roles: RoleableFragment[] = [];

  for (let i = 0; i < values.rolesCount; i++) {
    const role = createRoleableFragment(`${id}-role-fragment-${0}`);
    roles.push(role);
  }

  return {
    ...userFragment,
    roles,
  };
};
