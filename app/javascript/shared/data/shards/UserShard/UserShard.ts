import gql from "graphql-tag";

import {
  RoleableFragment,
  ROLEABLE_FRAGMENT,
  UserFragment,
  USER_FRAGMENT,
} from "@shared/data/fragments";

interface UserShard extends UserFragment {
  roles: RoleableFragment[];
}

const USER_SHARD = gql`
  fragment UserShard on User {
    ...UserFragment
    roles {
      ...RoleableFragment
    }
  }
  ${USER_FRAGMENT}
  ${ROLEABLE_FRAGMENT}
`;

export { UserShard, USER_SHARD };
