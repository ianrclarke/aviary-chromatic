import gql from "graphql-tag";

import {
  PaymentFragment,
  PAYMENT_FRAGMENT,
  SourceFragment,
  SOURCE_FRAGMENT,
} from "@shared/data/fragments";

interface PaymentShard extends PaymentFragment {
  source: SourceFragment;
}

const PAYMENT_SHARD = gql`
  fragment PaymentShard on Payment {
    ...PaymentFragment
    source {
      ...SourceFragment
    }
  }

  ${PAYMENT_FRAGMENT}
  ${SOURCE_FRAGMENT}
`;

export { PaymentShard, PAYMENT_SHARD };
