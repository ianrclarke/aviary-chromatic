import { createPaymentFragment, createSourceFragment } from "@shared/data/fragments/mocks";

import { PaymentShard } from "./PaymentShard";

export const createPaymentShard = (id: number): PaymentShard => {
  const payment = createPaymentFragment(id);
  const source = createSourceFragment(id);

  return { ...payment, source };
};
