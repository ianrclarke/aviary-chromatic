import gql from "graphql-tag";

import {
  TreatmentPlanTemplateFragment,
  TREATMENT_PLAN_FRAGMENT,
  TreatmentPlanFragment,
  TREATMENT_PLAN_TEMPLATE_FRAGMENT,
} from "@shared/data/fragments";

interface TreatmentPlanTemplateShard extends TreatmentPlanTemplateFragment {
  treatmentPlan: TreatmentPlanFragment;
}

const TREATMENT_PLAN_TEMPLATE_SHARD = gql`
  fragment TreatmentPlanTemplateShard on TreatmentPlanTemplate {
    ...TreatmentPlanTemplateFragment
    treatmentPlan {
      ...TreatmentPlanFragment
    }
  }
  ${TREATMENT_PLAN_TEMPLATE_FRAGMENT}
  ${TREATMENT_PLAN_FRAGMENT}
`;

export { TreatmentPlanTemplateShard, TREATMENT_PLAN_TEMPLATE_SHARD };
