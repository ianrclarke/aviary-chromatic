import {
  createTreatmentPlanFragment,
  createTreatmentPlanTemplateFragment,
} from "@shared/data/fragments/mocks";

import { TreatmentPlanTemplateShard } from "./TreatmentPlanTemplateShard";

const createTreatmentPlanTemplateShard = (id: string): TreatmentPlanTemplateShard => {
  const treatmentPlanTemplateFragment = createTreatmentPlanTemplateFragment(
    `${id}-treatment-plan-template-fragment`
  );

  const treatmentPlan = createTreatmentPlanFragment(`${id}-treatment-plan-fragment`);

  return { ...treatmentPlanTemplateFragment, treatmentPlan };
};

export { createTreatmentPlanTemplateShard };
