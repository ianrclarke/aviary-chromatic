import gql from "graphql-tag";

import {
  PatientPromotionFragment,
  PATIENT_PROMOTION_FRAGMENT,
  StoresPatientPromotionFragment,
  STORES_PATIENT_PROMOTION_FRAGMENT,
} from "@shared/data/fragments";

interface StoresPatientPromotionShard extends StoresPatientPromotionFragment {
  patientPromotion: PatientPromotionFragment;
}

const STORES_PATIENT_PROMOTION_SHARD = gql`
  fragment StoresPatientPromotionShard on StoresPatientPromotion {
    ...StoresPatientPromotionFragment
    patientPromotion {
      ...PatientPromotionFragment
    }
  }
  ${STORES_PATIENT_PROMOTION_FRAGMENT}
  ${PATIENT_PROMOTION_FRAGMENT}
`;

export { StoresPatientPromotionShard, STORES_PATIENT_PROMOTION_SHARD };
