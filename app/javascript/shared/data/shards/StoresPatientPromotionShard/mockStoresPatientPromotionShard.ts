import { PatientPromotionFragment, StoresPatientPromotionFragment } from "@shared/data/fragments";
import {
  createPatientPromotionFragment,
  createStoresPatientPromotionFragment,
} from "@shared/data/fragments/mocks";

import { StoresPatientPromotionShard } from "./StoresPatientPromotionShard";

export const createStoresPatientPromotionShard = (
  id: string,
  storesPromoOptions: Partial<StoresPatientPromotionFragment> = {},
  patientPromoOptions: Partial<PatientPromotionFragment> = {}
): StoresPatientPromotionShard => {
  const patientPromotionFragment = createPatientPromotionFragment(
    patientPromoOptions.id,
    patientPromoOptions
  );
  const storesPatientPromotionFragment = createStoresPatientPromotionFragment(
    id,
    storesPromoOptions
  );

  return { ...storesPatientPromotionFragment, patientPromotion: patientPromotionFragment };
};
