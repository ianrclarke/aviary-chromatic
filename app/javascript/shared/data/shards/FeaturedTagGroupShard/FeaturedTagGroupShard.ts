import gql from "graphql-tag";

import {
  FeaturedTagGroupFragment,
  TagFragment,
  FEATURED_TAG_GROUP_FRAGMENT,
  TAG_FRAGMENT,
} from "@shared/data/fragments";

interface FeaturedTagGroupShard extends FeaturedTagGroupFragment {
  tags: {
    nodes: TagFragment[];
  };
}

const FEATURED_TAG_GROUP_SHARD = gql`
  fragment FeaturedTagGroupShard on FeaturedTagGroup {
    ...FeaturedTagGroupFragment
    tags {
      nodes {
        ...TagFragment
      }
    }
  }

  ${FEATURED_TAG_GROUP_FRAGMENT}
  ${TAG_FRAGMENT}
`;

export { FeaturedTagGroupShard, FEATURED_TAG_GROUP_SHARD };
