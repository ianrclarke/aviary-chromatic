import { createTagFragment, createFeaturedTagGroupFragment } from "@shared/data/fragments/mocks";

import { FeaturedTagGroupShard } from "./FeaturedTagGroupShard";

export const createFeaturedTagGroupShard = (
  id: string,
  options: any = {}
): FeaturedTagGroupShard => {
  const featuredTagGroupFragment = createFeaturedTagGroupFragment(`${id}-featuredTagGroupFragment`);
  const tagFragment = createTagFragment(`${id}-tagFragment`);

  return {
    ...featuredTagGroupFragment,
    tags: {
      nodes: [tagFragment],
    },
    ...options,
  };
};
