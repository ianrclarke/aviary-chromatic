import { createVariantNotificationFragment } from "@shared/data/fragments/mocks";
import { createVariantShard } from "@shared/data/shards/mocks";

import { VariantNotificationShard } from "./VariantNotificationShard";

const createVariantNotificationShard = (
  id: string,
  options: Partial<VariantNotificationShard> = {}
): VariantNotificationShard => {
  const variantNotificationFragment = createVariantNotificationFragment(
    `${id}-VariantNotificationShard`,
    options
  );
  const variant = createVariantShard(`${id}-VariantNotificationShard`, options.variant);
  return {
    ...variantNotificationFragment,
    variant,
  };
};

export { createVariantNotificationShard };
