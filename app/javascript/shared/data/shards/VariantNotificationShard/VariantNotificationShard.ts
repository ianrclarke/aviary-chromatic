import gql from "graphql-tag";

import {
  VariantNotificationFragment,
  VARIANT_NOTIFICATION_FRAGMENT,
} from "@shared/data/fragments/VariantNotificationFragment";
import { VariantShard, VARIANT_SHARD } from "@shared/data/shards/VariantShard";

const VARIANT_NOTIFICATION_SHARD = gql`
  fragment VariantNotificationShard on VariantNotification {
    ...VariantNotificationFragment
    variant {
      ...VariantShard
    }
  }
  ${VARIANT_NOTIFICATION_FRAGMENT}
  ${VARIANT_SHARD}
`;

interface VariantNotificationShard extends VariantNotificationFragment {
  variant: VariantShard;
}

export { VARIANT_NOTIFICATION_SHARD, VariantNotificationShard };
