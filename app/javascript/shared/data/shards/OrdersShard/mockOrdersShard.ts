import { createOrderFragment } from "@shared/data/fragments/mocks";
import { createLineItemShard } from "@shared/data/shards/mocks";

import { OrderShard } from "./OrdersShard";

const DEFAULT_OPTIONS = {
  lineItemCount: 1,
  lineItemShardOptions: [{}],
  shipmentFragmentOptions: [{}],
};
export const createOrderShard = (id: string, options: any = {}): OrderShard => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  const orderFragment = createOrderFragment(`${id}-order-fragment`, values.orderFragmentOptions);

  const lineItemShards = [];
  for (let i = 0; i < values.lineItemCount; i++) {
    const lineItemShard = createLineItemShard(
      `${id}-lineItemShard-${i}`,
      values.lineItemShardOptions[i]
    );
    lineItemShards.push(lineItemShard);
  }

  const storesPatientPromotion = options.storesPatientPromotion || null;

  return {
    ...orderFragment,
    lineItems: lineItemShards,
    storesPatientPromotion,
  };
};
