import gql from "graphql-tag";

import { OrderFragment, ORDER_FRAGMENT } from "@shared/data/fragments";

import {
  StoresPatientPromotionShard,
  STORES_PATIENT_PROMOTION_SHARD,
} from "../StoresPatientPromotionShard";

interface LineItem {
  id;
}

interface OrderShard extends OrderFragment {
  lineItems: LineItem[];
  storesPatientPromotion: StoresPatientPromotionShard;
}

const ORDER_SHARD = gql`
  fragment OrderShard on Order {
    ...OrderFragment
    lineItems {
      id
    }
    storesPatientPromotion {
      ...StoresPatientPromotionShard
    }
  }
  ${ORDER_FRAGMENT}
  ${STORES_PATIENT_PROMOTION_SHARD}
`;

export { OrderShard, ORDER_SHARD };
