import gql from "graphql-tag";

import {
  BrandFragment,
  BRAND_FRAGMENT,
  TopBrandFragment,
  TOP_BRAND_FRAGMENT,
} from "@shared/data/fragments";

interface BrandShard extends BrandFragment {
  topBrand: TopBrandFragment;
}

const BRAND_SHARD = gql`
  fragment BrandShard on Brand {
    ...BrandFragment
    topBrand {
      ...TopBrandFragment
    }
  }
  ${BRAND_FRAGMENT}
  ${TOP_BRAND_FRAGMENT}
`;

export { BrandShard, BRAND_SHARD };
