import { createBrandFragment, createTopBrandFragment } from "@shared/data/fragments/mocks";

import { BrandShard } from "./BrandShard";

export const createBrandShard = (id: string, options: any = {}): BrandShard => {
  const brandFragment = createBrandFragment(`${id}-brandFragment`);
  const topBrandFragment = createTopBrandFragment(
    `${id}-topBrandFragment`,
    options.topBrandFragmentOptions
  );
  return {
    ...brandFragment,
    topBrand: topBrandFragment,
  };
};
