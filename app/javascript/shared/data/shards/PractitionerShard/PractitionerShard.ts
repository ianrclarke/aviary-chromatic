import gql from "graphql-tag";

import {
  AvatarFragment,
  AVATAR_FRAGMENT,
  PractitionerFragment,
  PractitionerTypeFragment,
  PRACTITIONER_FRAGMENT,
  PRACTITIONER_TYPE_FRAGMENT,
} from "@shared/data/fragments";

interface PractitionerShard extends PractitionerFragment {
  avatar: AvatarFragment;
  practitionerType: PractitionerTypeFragment;
}

const PRACTITIONER_SHARD = gql`
  fragment PractitionerShard on Practitioner {
    ...PractitionerFragment
    avatar {
      ...AvatarFragment
    }
    practitionerType {
      ...PractitionerTypeFragment
    }
  }

  ${PRACTITIONER_FRAGMENT}
  ${AVATAR_FRAGMENT}
  ${PRACTITIONER_TYPE_FRAGMENT}
`;

export { PractitionerShard, PRACTITIONER_SHARD };
