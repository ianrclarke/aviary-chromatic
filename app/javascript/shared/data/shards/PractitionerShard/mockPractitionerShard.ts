import {
  createAvatarFragment,
  createPractitionerFragment,
  createPractitionerTypeFragment,
} from "@shared/data/fragments/mocks";

import { PractitionerShard } from "./PractitionerShard";

export const createPractitionerShard = (id: string): PractitionerShard => {
  const practitionerFragment = createPractitionerFragment(`${id}-practitionerFragment`);
  const avatarFragment = createAvatarFragment(`${id}-avatarFragment`);
  const practitionerTypeFragment = createPractitionerTypeFragment(`${id}-practitionerTypeFragment`);

  return {
    ...practitionerFragment,
    avatar: avatarFragment,
    practitionerType: practitionerTypeFragment,
  };
};
