import {
  createTreatmentPlanFragment,
  createPractitionerFragment,
} from "@shared/data/fragments/mocks";

import { TreatmentPlanShard } from "./TreatmentPlanShard";

const DEFAULT_OPTIONS = {
  recommendationShardCount: 1,
  recommendationShardOptions: [{}],
  documentFragmentCount: 1,
  documentFragmentOptions: [{}],
};
const createTreatmentPlanShard = (id: string, options: any = {}): TreatmentPlanShard => {
  const values = { ...DEFAULT_OPTIONS, ...options };

  const treatmentPlanFragment = createTreatmentPlanFragment(`${id}-treatment-plan-fragment`);

  const recommendations: { id: string }[] = [];
  for (let i = 0; i < values.recommendationShardCount; i++) {
    recommendations.push({ id: `${id}-recommendation-shard-${i}` });
  }

  const practitioner = createPractitionerFragment(`${id}-practitioner-fragment`);

  return { ...treatmentPlanFragment, recommendations, practitioner };
};

export { createTreatmentPlanShard };
