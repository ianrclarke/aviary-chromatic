import gql from "graphql-tag";

import {
  TreatmentPlanFragment,
  TREATMENT_PLAN_FRAGMENT,
  PractitionerFragment,
  PRACTITIONER_FRAGMENT,
} from "@shared/data/fragments";

interface TreatmentPlanShard extends TreatmentPlanFragment {
  recommendations: { id: string }[];
  practitioner: PractitionerFragment;
}

const TREATMENT_PLAN_SHARD = gql`
  fragment TreatmentPlanShard on TreatmentPlan {
    ...TreatmentPlanFragment
    recommendations(filterObject: { state: [ACTIVE] }) {
      id
    }
    practitioner {
      ...PractitionerFragment
    }
  }
  ${TREATMENT_PLAN_FRAGMENT}
  ${PRACTITIONER_FRAGMENT}
`;

export { TreatmentPlanShard, TREATMENT_PLAN_SHARD };
