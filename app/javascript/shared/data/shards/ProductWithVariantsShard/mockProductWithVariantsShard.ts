import { VariantFragment } from "@shared/data/fragments";
import {
  createBrandFragment,
  createProductFragment,
  createVariantFragment,
} from "@shared/data/fragments/mocks";

import { ProductWithVariantsShard } from "./ProductWithVariantsShard";

const DEFAULT_OPTIONS = { variantCount: 2 };

export const createProductWithVariantsShard = (
  id: string,
  options: any = {}
): ProductWithVariantsShard => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  const productFragment = createProductFragment(`${id}-productFragment`);
  const brandFragment = createBrandFragment(`${id}-brandFragment`);
  const variantShards: VariantFragment[] = [];
  for (let i = 0; i < values.variantCount; i++) {
    const variantShard = createVariantFragment(
      `${id}-variantFragment-${i}`,
      values.variantWithVariantNotificationsShard
    );
    variantShards.push(variantShard);
  }
  return {
    ...productFragment,
    brand: brandFragment,
    variants: variantShards,
  };
};
