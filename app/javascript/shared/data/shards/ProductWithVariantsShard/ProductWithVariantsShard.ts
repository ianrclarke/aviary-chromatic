import gql from "graphql-tag";

import {
  BrandFragment,
  BRAND_FRAGMENT,
  ProductFragment,
  PRODUCT_FRAGMENT,
  VariantFragment,
  VARIANT_FRAGMENT,
} from "@shared/data/fragments";

interface ProductWithVariantsShard extends ProductFragment {
  brand: BrandFragment;
  variants: VariantFragment[];
}

const PRODUCT_WITH_VARIANTS_SHARD = gql`
  fragment ProductWithVariantsShard on Product {
    ...ProductFragment
    brand {
      ...BrandFragment
    }
    variants {
      ...VariantFragment
    }
  }

  ${PRODUCT_FRAGMENT}
  ${BRAND_FRAGMENT}
  ${VARIANT_FRAGMENT}
`;

export { ProductWithVariantsShard, PRODUCT_WITH_VARIANTS_SHARD };
