export { PRODUCT_WITH_VARIANTS_SHARD } from "./ProductWithVariantsShard";

export type { ProductWithVariantsShard } from "./ProductWithVariantsShard";
