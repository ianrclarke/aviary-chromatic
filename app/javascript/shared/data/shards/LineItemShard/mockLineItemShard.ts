import { createLineItemFragment } from "@shared/data/fragments/mocks";
import { createVariantShard } from "@shared/data/shards/mocks";

import { LineItemShard } from "./LineItemShard";

export const createLineItemShard = (id: string, options: any = {}): LineItemShard => {
  const variantShard = createVariantShard(`${id}-VariantShard`, options.VariantShardOptions);
  const lineItemFragment = createLineItemFragment(
    `${id}-lineItemFragment`,
    options.lineItemFragmentOptions
  );

  return {
    ...lineItemFragment,
    variant: variantShard,
  };
};
