import gql from "graphql-tag";

import { LineItemFragment, LINE_ITEM_FRAGMENT } from "@shared/data/fragments";
import { VariantShard, VARIANT_SHARD } from "@shared/data/shards/VariantShard";

interface LineItemShard extends LineItemFragment {
  variant: VariantShard;
}

const LINE_ITEM_SHARD = gql`
  fragment LineItemShard on LineItem {
    ...LineItemFragment
    variant {
      ...VariantShard
    }
  }
  ${LINE_ITEM_FRAGMENT}
  ${VARIANT_SHARD}
`;

export { LineItemShard, LINE_ITEM_SHARD };
