import {
  createBrandFragment,
  createProductFragment,
  createVariantFragment,
} from "@shared/data/fragments/mocks";

import { VariantShard } from "./VariantShard";

export const createVariantShard = (id: string, options: any = {}): VariantShard => {
  const brandFragment = createBrandFragment(`${id}-brandFragment`, options.brandFragmentOptions);
  const productFragment = createProductFragment(
    `${id}-productFragment`,
    options.productFragmentOptions
  );
  const variantFragment = createVariantFragment(
    `${id}-variantFragment`,
    options.variantFragmentOptions
  );

  return {
    ...variantFragment,
    brand: brandFragment,
    product: productFragment,
  };
};
