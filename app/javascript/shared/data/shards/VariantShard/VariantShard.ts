import gql from "graphql-tag";

import {
  BrandFragment,
  BRAND_FRAGMENT,
  ProductFragment,
  PRODUCT_FRAGMENT,
  VariantFragment,
  VARIANT_FRAGMENT,
} from "@shared/data/fragments";

interface VariantShard extends VariantFragment {
  brand: BrandFragment;
  product: ProductFragment;
}

const VARIANT_SHARD = gql`
  fragment VariantShard on Variant {
    ...VariantFragment
    brand {
      ...BrandFragment
    }
    product {
      ...ProductFragment
    }
  }
  ${VARIANT_FRAGMENT}
  ${BRAND_FRAGMENT}
  ${PRODUCT_FRAGMENT}
`;

export { VariantShard, VARIANT_SHARD };
