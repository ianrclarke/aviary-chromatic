export { DOCUMENT_ATTACHMENT_WITH_UPLOADABLE_SHARD } from "./DocumentAttachmentWithUploadableShard";
import { DocumentAttachmentWithUploadableShard as DocumentAttachmentWithUploadableShardType } from "./DocumentAttachmentWithUploadableShard";
export type DocumentAttachmentWithUploadableShard = DocumentAttachmentWithUploadableShardType;
