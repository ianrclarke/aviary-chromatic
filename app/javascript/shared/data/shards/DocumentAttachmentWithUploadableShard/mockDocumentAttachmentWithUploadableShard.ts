import {
  createDocumentAttachmentFragment,
  createUploadableFragment,
} from "@shared/data/fragments/mocks";

import { DocumentAttachmentWithUploadableShard } from "./DocumentAttachmentWithUploadableShard";

const createDocumentAttachmentWithUploadableShard = (
  id: string,
  options: any = {}
): DocumentAttachmentWithUploadableShard => {
  const documentAttachment = createDocumentAttachmentFragment(
    `${id}-documentAttachmentWithUploadableShard`,
    options
  );
  const uploadable = createUploadableFragment(
    `${id}-documentAttachmentWithUploadableShard`,
    options.uploadable
  );
  return {
    ...documentAttachment,
    uploadable,
  };
};

export { createDocumentAttachmentWithUploadableShard };
