import gql from "graphql-tag";

import {
  DOCUMENT_ATTACHMENT_FRAGMENT,
  DocumentAttachmentFragment,
  UPLOADABLE_FRAGMENT,
  UploadableFragment,
} from "@shared/data/fragments";

interface DocumentAttachmentWithUploadableShard extends DocumentAttachmentFragment {
  uploadable: UploadableFragment;
}

const DOCUMENT_ATTACHMENT_WITH_UPLOADABLE_SHARD = gql`
  fragment DocumentAttachmentWithUploadableShard on DocumentAttachment {
    ...DocumentAttachmentFragment
    uploadable {
      ...UploadableFragment
    }
  }
  ${DOCUMENT_ATTACHMENT_FRAGMENT}
  ${UPLOADABLE_FRAGMENT}
`;

export { DocumentAttachmentWithUploadableShard, DOCUMENT_ATTACHMENT_WITH_UPLOADABLE_SHARD };
