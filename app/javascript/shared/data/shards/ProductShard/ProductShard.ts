import gql from "graphql-tag";

import {
  BrandFragment,
  BRAND_FRAGMENT,
  ProductFragment,
  PRODUCT_FRAGMENT,
  VariantFragment,
  VARIANT_FRAGMENT,
} from "@shared/data/fragments";

interface ProductShard extends ProductFragment {
  brand: BrandFragment;
  masterVariant: VariantFragment;
}

const PRODUCT_SHARD = gql`
  fragment ProductShard on Product {
    ...ProductFragment
    brand {
      ...BrandFragment
    }
    masterVariant {
      ...VariantFragment
    }
  }

  ${PRODUCT_FRAGMENT}
  ${BRAND_FRAGMENT}
  ${VARIANT_FRAGMENT}
`;

export { ProductShard, PRODUCT_SHARD };
