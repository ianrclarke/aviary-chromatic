import { createBrandFragment, createProductFragment } from "@shared/data/fragments/mocks";
import { createVariantShard } from "@shared/data/shards/VariantShard/mockVariantShard";

import { ProductShard } from "./ProductShard";

export const createProductShard = (id: string, options: any = {}): ProductShard => {
  const productFragment = createProductFragment(`${id}-productFragment`);
  const brandFragment = createBrandFragment(`${id}-brandFragment`);

  const variantShard = createVariantShard(`${id}-variantShard`, {
    variantFragmentOptions: options.variantShard,
  });

  return {
    ...productFragment,
    brand: brandFragment,
    masterVariant: variantShard,
  };
};
