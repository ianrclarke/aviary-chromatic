import gql from "graphql-tag";

import {
  AvatarWithFilterFragment,
  AVATAR_FRAGMENT_WITH_FILTER,
  UserFragment,
  USER_FRAGMENT,
} from "@shared/data/fragments";

interface UserWithAvatarShard extends UserFragment {
  avatar: AvatarWithFilterFragment;
}

const USER_WITH_AVATAR_SHARD = gql`
  fragment UserWithAvatarShard on User {
    ...UserFragment
    avatar {
      ...AvatarWithFilterFragment
    }
  }

  ${AVATAR_FRAGMENT_WITH_FILTER}
  ${USER_FRAGMENT}
`;

export { UserWithAvatarShard, USER_WITH_AVATAR_SHARD };
