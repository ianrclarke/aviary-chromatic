import { UserFragment } from "@shared/data/fragments";
import { UserWithAvatarShard } from "@shared/data/shards/UserWithAvatarShard";

const DEFAULT_OPTIONS = {
  id: "1111",
  email: "example@fullscript.com",
  avatar: {
    id: "id",
    photoUrl: "https://test.com",
  },
};

export const createUserWithAvatarShard = (
  id: string,
  photoUrl: string,
  options: any = {}
): UserWithAvatarShard => {
  const values = { ...DEFAULT_OPTIONS, id, photoUrl, ...options };

  const user: UserFragment = {
    id: values.id,
    email: values.email,
    createdAt: Date.now().toString(),
    updatedAt: Date.now().toString(),
  };

  return {
    ...user,
    avatar: {
      id: "id",
      photoUrl,
    },
  };
};
