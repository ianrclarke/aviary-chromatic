import gql from "graphql-tag";

import { ProductShard, PRODUCT_SHARD } from "@shared/data/shards/ProductShard";
import {
  ProductWithVariantsShard,
  PRODUCT_WITH_VARIANTS_SHARD,
} from "@shared/data/shards/ProductWithVariantsShard";

interface ProductWithSimilarProductsShard extends ProductWithVariantsShard {
  similarProducts: {
    totalCount: number;
    products: ProductShard[];
  };
}

const PRODUCT_WITH_SIMILAR_PRODUCTS_SHARD = gql`
  fragment ProductWithSimilarProductsShard on Product {
    ... on Product {
      ...ProductWithVariantsShard
      similarProducts {
        totalCount
        products: nodes {
          ...ProductShard
        }
      }
    }
  }
  ${PRODUCT_WITH_VARIANTS_SHARD}
  ${PRODUCT_SHARD}
`;

export { ProductWithSimilarProductsShard, PRODUCT_WITH_SIMILAR_PRODUCTS_SHARD };
