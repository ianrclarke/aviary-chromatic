import { ProductShard } from "@shared/data/shards";
import { createProductShard, createProductWithVariantsShard } from "@shared/data/shards/mocks";

import { ProductWithSimilarProductsShard } from "./ProductWithSimilarProductsShard";

const DEFAULT_OPTIONS = { totalCount: 2 };

export const createProductWithSimilarProductsShard = (
  id: string,
  options: any = {}
): ProductWithSimilarProductsShard => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  const productShard = createProductWithVariantsShard(`${id}-productShard`);
  const similarProductsList: ProductShard[] = [];
  for (let i = 0; i < values.totalCount; i++) {
    const similarProductShard = createProductShard(`${id}-similarProductShard-${i}`);
    similarProductsList.push(similarProductShard);
  }

  return {
    ...productShard,
    similarProducts: {
      totalCount: values.totalCount,
      products: similarProductsList,
    },
  };
};
