export { PRODUCT_WITH_SIMILAR_PRODUCTS_SHARD } from "./ProductWithSimilarProductsShard";

export type { ProductWithSimilarProductsShard } from "./ProductWithSimilarProductsShard";
