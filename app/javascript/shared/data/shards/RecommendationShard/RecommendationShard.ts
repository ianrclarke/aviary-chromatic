import gql from "graphql-tag";

import { RecommendationFragment, RECOMMENDATION_FRAGMENT } from "@shared/data/fragments";
import { VariantShard, VARIANT_SHARD } from "@shared/data/shards/VariantShard";

interface RecommendationShard extends RecommendationFragment {
  variant: VariantShard;
  originalSource?: { id: string };
}

const RECOMMENDATION_SHARD = gql`
  fragment RecommendationShard on Recommendation {
    ...RecommendationFragment
    variant {
      ...VariantShard
    }
    originalSource {
      id
    }
  }
  ${VARIANT_SHARD}
  ${RECOMMENDATION_FRAGMENT}
`;

export { RecommendationShard, RECOMMENDATION_SHARD };
