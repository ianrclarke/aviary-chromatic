import { createRecommendationFragment } from "@shared/data/fragments/mocks";
import { createVariantShard } from "@shared/data/shards/mocks";

import { RecommendationShard } from "./RecommendationShard";

const createRecommendationShard = (id: string, options: any = {}): RecommendationShard => {
  const recommendationFragment = createRecommendationFragment(
    `${id}-recommendation-fragment`,
    options.recommendationFragment
  );
  const originalSource = {
    id: `${id}-originalSource`,
  };

  const variant = createVariantShard(`${id}-variantShard`, options.variantShard);
  return {
    ...recommendationFragment,
    originalSource,
    variant,
  };
};

export { createRecommendationShard };
