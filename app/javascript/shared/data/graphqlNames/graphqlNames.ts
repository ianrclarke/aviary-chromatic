const PREFIX = "Shared";
const QUERY = "Query";
const MUTATION = "Mutation";

const queryName = (name: string): string => `${PREFIX}_${QUERY}_${name}`;
const mutationName = (name: string): string => `${PREFIX}_${MUTATION}_${name}`;

export { mutationName, queryName };
