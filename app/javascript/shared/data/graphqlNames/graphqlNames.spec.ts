import { mutationName, queryName } from "./graphqlNames";

describe("mutationName", () => {
  it("returns a mutation name", () => {
    expect(mutationName("lineItem")).toEqual("Shared_Mutation_lineItem");
  });
});

describe("queryName", () => {
  it("returns a query name", () => {
    expect(queryName("lineItem")).toEqual("Shared_Query_lineItem");
  });
});
