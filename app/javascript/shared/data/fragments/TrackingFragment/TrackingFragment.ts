import gql from "graphql-tag";

interface TrackingFragment {
  id: string;
  trackingNumber: string;
  trackingUrl: string;
}

const TRACKING_FRAGMENT = gql`
  fragment TrackingFragment on Tracking {
    id
    trackingNumber
    trackingUrl
  }
`;

export { TrackingFragment, TRACKING_FRAGMENT };
