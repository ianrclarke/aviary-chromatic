import { TrackingFragment } from "./TrackingFragment";

export const createTrackingFragment = (id: string): TrackingFragment => {
  return {
    id,
    trackingNumber: `${id}-trackingNumber`,
    trackingUrl: `${id}-trackingUrl`,
  };
};
