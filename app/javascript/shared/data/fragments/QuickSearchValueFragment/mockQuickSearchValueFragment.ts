import { QuickSearchValueFragment } from "./QuickSearchValueFragment";

const createQuickSearchValueFragment = (id: string): QuickSearchValueFragment => {
  return {
    id,

    name: `${id}-name`,
  };
};

export { createQuickSearchValueFragment };
