import gql from "graphql-tag";

interface QuickSearchValueFragment {
  id: string;
  name: string;
}

const QUICK_SEARCH_VALUE_FRAGMENT = gql`
  fragment QuickSearchValueFragment on QuickSearchValue {
    id
    name
  }
`;

export { QuickSearchValueFragment, QUICK_SEARCH_VALUE_FRAGMENT };
