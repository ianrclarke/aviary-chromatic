import { defaultValues } from "@shared/mocks/defaultValues";

import { ShipmentFragment } from "./ShipmentFragment";

export const createShipmentFragment = (id: string): ShipmentFragment => {
  return {
    id,
    createdAt: defaultValues.date,
    number: `${id}-number`,
    estimatedShippingDate: `${id}-estimateddate`,
    shippedAt: defaultValues.date,
    state: `${id}-state`,
    tracking: `${id}-tracking`,
    trackingUrl: `${id}-tracking_url`,
    updatedAt: defaultValues.date,
  };
};
