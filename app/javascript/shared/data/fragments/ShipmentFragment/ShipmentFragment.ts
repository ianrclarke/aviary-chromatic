import gql from "graphql-tag";

interface ShipmentFragment {
  id: string;

  createdAt: string;
  number: string;
  estimatedShippingDate: string;
  shippedAt: string;
  state: string;
  tracking: string;
  trackingUrl: string;
  updatedAt: string;
}

const SHIPMENT_FRAGMENT = gql`
  fragment ShipmentFragment on Shipment {
    id

    createdAt
    number
    estimatedShippingDate
    shippedAt
    state
    tracking
    trackingUrl
    updatedAt
  }
`;

export { ShipmentFragment, SHIPMENT_FRAGMENT };
