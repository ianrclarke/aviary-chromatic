export { TREATMENT_PLAN_TEMPLATE_FRAGMENT } from "./TreatmentPlanTemplateFragment";

export type { TreatmentPlanTemplateFragment } from "./TreatmentPlanTemplateFragment";
