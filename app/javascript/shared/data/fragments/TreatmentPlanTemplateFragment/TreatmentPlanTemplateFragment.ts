import gql from "graphql-tag";

interface TreatmentPlanTemplateFragment {
  id: string;

  _legacyId: number;
  createdAt: string;
  currentState: string;
  name: string;
  shareable: boolean;
  treatmentPlanId: number;
  updatedAt: string;
  hasPrescriptionOnlyProducts: boolean;
  shareableWithPatients: "none" | "my_patients" | "store_patients";
}

const TREATMENT_PLAN_TEMPLATE_FRAGMENT = gql`
  fragment TreatmentPlanTemplateFragment on TreatmentPlanTemplate {
    id

    _legacyId
    createdAt
    currentState
    name
    shareable
    treatmentPlanId
    updatedAt
    hasPrescriptionOnlyProducts
    shareableWithPatients
  }
`;

export { TreatmentPlanTemplateFragment, TREATMENT_PLAN_TEMPLATE_FRAGMENT };
