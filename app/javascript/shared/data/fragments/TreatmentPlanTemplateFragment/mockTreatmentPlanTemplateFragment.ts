/* eslint-disable no-underscore-dangle */
import { defaultValues } from "@shared/mocks/defaultValues";

import { TreatmentPlanTemplateFragment } from "./TreatmentPlanTemplateFragment";

const createTreatmentPlanTemplateFragment = (
  id: string,
  options: any = {}
): TreatmentPlanTemplateFragment => {
  return {
    id,
    _legacyId: options._legacyId || 0,
    createdAt: defaultValues.date,
    currentState: "template",
    name: `${id}-name`,
    shareable: options.shareable || true,
    treatmentPlanId: options.treatmentPlanId || 0,
    updatedAt: defaultValues.date,
    hasPrescriptionOnlyProducts: false,
    shareableWithPatients: "none",
  };
};

export { createTreatmentPlanTemplateFragment };
