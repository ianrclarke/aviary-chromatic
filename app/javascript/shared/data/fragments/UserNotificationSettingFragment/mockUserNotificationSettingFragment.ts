import { defaultValues } from "@shared/mocks/defaultValues";

import { UserNotificationSettingFragment } from "./UserNotificationSettingFragment";

const createUserNotificationSettingFragment = (
  id: string,
  options: Partial<UserNotificationSettingFragment> = {}
): UserNotificationSettingFragment => ({
  id,
  createdAt: options.createdAt || defaultValues.date,
  updatedAt: options.updatedAt || defaultValues.date,
  subscribed: options.subscribed || true,
});

export { createUserNotificationSettingFragment };
