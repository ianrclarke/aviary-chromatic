import gql from "graphql-tag";

interface UserNotificationSettingFragment {
  id: string;

  createdAt: string;
  updatedAt: string;
  subscribed: boolean;
}

const USER_NOTIFICATION_SETTING_FRAGMENT = gql`
  fragment UserNotificationSettingFragment on UserNotificationSetting {
    id

    createdAt
    updatedAt
    subscribed
  }
`;

export { UserNotificationSettingFragment, USER_NOTIFICATION_SETTING_FRAGMENT };
