import { PractitionerPropertiesFragment } from "@shared/data/fragments";

const createPractitionerPropertiesFragment = (
  id: string,
  options: any = {}
): PractitionerPropertiesFragment => ({
  id: `${id}-practitionerPropertiesFragment`,
  canView1099kBanner: options.canView1099kBanner || false,
  hasAtLeastOneApprovedCertification: options.hasAtLeastOneApprovedCertification || false,
  hasCertificationsUnderReview: options.hasCertificationsUnderReview || false,
  hasGlobalPatients: options.hasGlobalPatients || false,
  isApprovedToSendOrdersToDistributor: options.isApprovedToSendOrdersToDistributor || false,
  isEligibleForTaxExemption: options.isEligibleForTaxExemption || false,
  isGlobalAlertVisible: options.isGlobalAlertVisible || false,
  isNeverMarginStore: options.isNeverMarginStore || false,
  isStoreOwner: options.isStoreOwner || false,
  isStudent: options.isStudent || false,
  isStudentGraduationDateInTheFuture: options.isStudentGraduationDateInTheFuture || false,
});

export { createPractitionerPropertiesFragment };
