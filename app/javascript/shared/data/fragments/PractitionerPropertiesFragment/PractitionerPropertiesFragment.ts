import gql from "graphql-tag";

const PRACTITIONER_PROPERTIES_FRAGMENT = gql`
  fragment PractitionerPropertiesFragment on PractitionerProperties {
    id

    canView1099kBanner
    hasAtLeastOneApprovedCertification
    hasCertificationsUnderReview
    hasGlobalPatients
    isApprovedToSendOrdersToDistributor
    isEligibleForTaxExemption
    isGlobalAlertVisible
    isNeverMarginStore
    isStoreOwner
    isStudent
    isStudentGraduationDateInTheFuture
  }
`;

interface PractitionerPropertiesFragment {
  id: string;

  canView1099kBanner: boolean;
  hasAtLeastOneApprovedCertification: boolean;
  hasCertificationsUnderReview: boolean;
  hasGlobalPatients: boolean;
  isApprovedToSendOrdersToDistributor: boolean;
  isEligibleForTaxExemption: boolean;
  isGlobalAlertVisible: boolean;
  isNeverMarginStore: boolean;
  isStoreOwner: boolean;
  isStudent: boolean;
  isStudentGraduationDateInTheFuture: boolean;
}

export { PractitionerPropertiesFragment, PRACTITIONER_PROPERTIES_FRAGMENT };
