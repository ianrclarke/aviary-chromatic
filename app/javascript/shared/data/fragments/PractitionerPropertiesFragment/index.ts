export { PRACTITIONER_PROPERTIES_FRAGMENT } from "./PractitionerPropertiesFragment";

export type { PractitionerPropertiesFragment } from "./PractitionerPropertiesFragment";
