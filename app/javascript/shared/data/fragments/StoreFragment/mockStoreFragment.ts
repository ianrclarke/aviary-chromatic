import { StoreFragment } from "@shared/data/fragments";
import { defaultValues } from "@shared/mocks/defaultValues";
import { MarginType } from "@shared/types/maginTypes.d";

const DEFAULT_VALUES = {
  currentSetupPage: "complete",
};

const createStoreFragment = (id: string, options: Partial<StoreFragment> = {}): StoreFragment => {
  const values = { ...DEFAULT_VALUES, ...options };
  return {
    id,
    createdAt: values.createdAt || defaultValues.date,
    updatedAt: values.updatedAt || defaultValues.date,
    discount: values.discount === undefined ? 10 : values.discount,
    marginType: values.marginType || MarginType.variable,
    freePriorityShipping: values.freePriorityShipping || false,
    name: values.name || `${id}-name`,
    slug: values.name || `${id}-slug`,
    currentSetupPage: values.currentSetupPage,
    referralLink: "http://referralLink",
  };
};

export { createStoreFragment };
