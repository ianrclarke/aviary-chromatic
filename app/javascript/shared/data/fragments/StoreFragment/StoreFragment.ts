import gql from "graphql-tag";

const STORE_FRAGMENT = gql`
  fragment StoreFragment on Store {
    id

    createdAt
    updatedAt
    discount
    marginType
    freePriorityShipping
    name
    slug
    currentSetupPage
    referralLink
  }
`;

interface StoreFragment {
  id: string;
  createdAt: string;
  updatedAt: string;
  discount: number;
  marginType: string;
  freePriorityShipping: boolean;
  name: string;
  slug: string;
  currentSetupPage: string;
  referralLink: string;
}

export { StoreFragment, STORE_FRAGMENT };
