import gql from "graphql-tag";

interface PractitionerFragment {
  id: string;
  createdAt: string;
  fullName: string;
  title: string;
  firstName: string;
  lastName: string;
  email: string;
  name: string;
  storeId: string;
  storeName: string;
  isStoreOwner: boolean;
  updatedAt: string;
}

const PRACTITIONER_FRAGMENT = gql`
  fragment PractitionerFragment on Practitioner {
    id
    createdAt
    fullName
    title
    firstName
    lastName
    email
    name
    storeName
    isStoreOwner
    updatedAt
  }
`;

export { PractitionerFragment, PRACTITIONER_FRAGMENT };
