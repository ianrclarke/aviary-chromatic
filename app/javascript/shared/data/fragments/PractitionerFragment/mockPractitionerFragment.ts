import { defaultValues } from "@shared/mocks/defaultValues";

import { PractitionerFragment } from "./PractitionerFragment";

export const createPractitionerFragment = (id: string): PractitionerFragment => {
  return {
    id,
    createdAt: defaultValues.date,
    fullName: `${id}-fullName`,
    title: `${id}-title`,
    firstName: `${id}-firstName`,
    lastName: `${id}-lastName`,
    email: `${id}-email`,
    name: `${id}-name`,
    storeId: `${id}-storeId`,
    storeName: `${id}-storeName`,
    isStoreOwner: true,
    updatedAt: defaultValues.date,
  };
};
