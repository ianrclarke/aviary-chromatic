export { DOCUMENT_ATTACHMENT_FRAGMENT } from "./DocumentAttachmentFragment";
import { DocumentAttachmentFragment as DocumentAttachmentFragmentType } from "./DocumentAttachmentFragment";
export type DocumentAttachmentFragment = DocumentAttachmentFragmentType;
