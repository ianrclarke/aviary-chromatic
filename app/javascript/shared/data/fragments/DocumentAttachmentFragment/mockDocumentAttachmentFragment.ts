import { defaultValues } from "@shared/mocks/defaultValues";

import { DocumentAttachmentFragment } from "./DocumentAttachmentFragment";

const createDocumentAttachmentFragment = (
  id: string,
  options: any = {}
): DocumentAttachmentFragment => ({
  id,
  createdAt: options.createdAt || defaultValues.date,
  updatedAt: options.updatedAt || defaultValues.date,
  deletedAt: options.deletedAt || null,
});

export { createDocumentAttachmentFragment };
