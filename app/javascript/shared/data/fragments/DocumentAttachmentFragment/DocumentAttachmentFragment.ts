import gql from "graphql-tag";

interface DocumentAttachmentFragment {
  id: string;

  createdAt: string;
  updatedAt: string;
  deletedAt: string;
}

const DOCUMENT_ATTACHMENT_FRAGMENT = gql`
  fragment DocumentAttachmentFragment on DocumentAttachment {
    id
    createdAt
    updatedAt
    deletedAt
  }
`;

export { DOCUMENT_ATTACHMENT_FRAGMENT, DocumentAttachmentFragment };
