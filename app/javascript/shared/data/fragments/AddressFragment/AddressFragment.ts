import gql from "graphql-tag";

interface AddressFragment {
  id: string;

  address1: string;
  address2: string;
  city: string;
  country: string;
  createdAt: string;
  firstname: string;
  lastname: string;
  phone: string;
  state: string;
  updatedAt: string;
  zipcode: string;
}

const ADDRESS_FRAGMENT = gql`
  fragment AddressFragment on Address {
    id

    address1
    address2
    city
    country
    createdAt
    firstname
    lastname
    phone
    state
    updatedAt
    zipcode
  }
`;

export { AddressFragment, ADDRESS_FRAGMENT };
