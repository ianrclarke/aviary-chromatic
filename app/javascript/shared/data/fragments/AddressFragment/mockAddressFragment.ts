import { defaultValues } from "@shared/mocks/defaultValues";

import { AddressFragment } from "./AddressFragment";

export const createAddressFragment = (id: string, options: any = {}): AddressFragment => {
  const DEFAULT_OPTIONS = {
    address1: `${id}-address1`,
    address2: `${id}-address2`,
    city: `${id}-city`,
    country: `${id}-country`,
    firstname: `${id}-firstname`,
    lastname: `${id}-lastname`,
    phone: `${id}-phone`,
    state: `${id}-state`,
    zipcode: `${id}-zipcode`,
  };

  const values = { ...DEFAULT_OPTIONS, ...options };

  return {
    id,
    createdAt: defaultValues.date,
    updatedAt: defaultValues.date,
    address1: values.address1,
    address2: values.address2,
    city: values.city,
    country: values.country,
    firstname: values.firstname,
    lastname: values.lastname,
    phone: values.phone,
    state: values.state,
    zipcode: values.zipcode,
  };
};
