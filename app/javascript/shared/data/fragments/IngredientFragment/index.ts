export { INGREDIENT_FRAGMENT } from "./IngredientFragment";

export type { IngredientFragment } from "./IngredientFragment";
