import gql from "graphql-tag";

interface IngredientFragment {
  id: string;
  name: string;
}

const INGREDIENT_FRAGMENT = gql`
  fragment IngredientFragment on Ingredient {
    id
    name
  }
`;

export { IngredientFragment, INGREDIENT_FRAGMENT };
