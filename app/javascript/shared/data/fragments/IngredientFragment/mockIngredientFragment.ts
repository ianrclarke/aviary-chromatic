import { IngredientFragment } from "./IngredientFragment";

const createIngredientFragment = (id: string): IngredientFragment => {
  return {
    id,
    name: `${id}-name`,
  };
};

export { createIngredientFragment };
