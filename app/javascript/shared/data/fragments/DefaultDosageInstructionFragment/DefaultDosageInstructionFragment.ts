import gql from "graphql-tag";

const DEFAULT_DOSAGE_INSTRUCTION_FRAGMENT = gql`
  fragment DefaultDosageInstructionFragment on DefaultDosageInstruction {
    id

    createdAt
    updatedAt
    dosageAmount
    dosageFormat
    dosageDuration
    dosageQuantity
    dosageFrequency
    dosagePermutations
  }
`;

interface DefaultDosageInstructionFragment {
  id: string;
  createdAt: string;
  updatedAt: string;
  dosageAmount: number;
  dosageFormat: string;
  dosageDuration: string;
  dosageQuantity: number;
  dosageFrequency: string;
  dosagePermutations: string;
}

export { DefaultDosageInstructionFragment, DEFAULT_DOSAGE_INSTRUCTION_FRAGMENT };
