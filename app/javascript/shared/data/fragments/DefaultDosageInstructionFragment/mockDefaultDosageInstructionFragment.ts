import { DefaultDosageInstructionFragment } from "@shared/data/fragments";
import { defaultValues } from "@shared/mocks/defaultValues";

export const createDefaultDosageInstructionFragment = (
  id: string,
  options: any = {}
): DefaultDosageInstructionFragment => {
  const values = { ...options };
  return {
    id,
    createdAt: values.createdAt || defaultValues.date,
    updatedAt: values.updatedAt || defaultValues.date,
    dosageAmount: values.dosageAmount || `${id}-dosage-amount`,
    dosageFormat: values.dosageFormat || `${id}-dosage-format`,
    dosageDuration: values.dosageDuration || `${id}-dosage-duration`,
    dosageQuantity: values.dosageQuantity || 23,
    dosageFrequency: `${id}-frequency`,
    dosagePermutations: `${id}-dosage-permutations`,
  };
};
