import gql from "graphql-tag";

interface State {
  id: string;
  name: string;
}

interface PatientAddressFragment {
  id: string;

  address1: string;
  address2: string;
  city: string;
  country: string;
  createdAt: string;
  state: State;
  updatedAt: string;
  zipcode: string;
  addressType: string;
  shippingAndBilling: boolean;
}

const PATIENT_ADDRESS_FRAGMENT = gql`
  fragment PatientAddressFragment on PatientAddress {
    id

    address1
    address2
    city
    country
    createdAt
    state {
      id
      name
    }
    updatedAt
    zipcode
    addressType
    shippingAndBilling
  }
`;

export { PatientAddressFragment, PATIENT_ADDRESS_FRAGMENT };
