import { defaultValues } from "@shared/mocks/defaultValues";

import { createStateFragment } from "../StateFragment/mockStateFragment";

import { PatientAddressFragment } from "./PatientAddressFragment";

const DEFAULT_OPTIONS = {
  address1: "123 Fake St",
  address2: "unit 1",
  city: "O-Town",
  country: "Canada",
  zipcode: "90210",
  addressType: "",
  shippingAndBilling: true,
};
export const createPatientAddressFragment = (
  id: string,
  options: any = {}
): PatientAddressFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    createdAt: defaultValues.date,
    updatedAt: defaultValues.date,
    address1: values.address1,
    address2: values.address2,
    city: values.city,
    country: values.country,
    state: createStateFragment(`${id}-state`),
    zipcode: values.zipcode,
    addressType: values.addressType,
    shippingAndBilling: values.shippingAndBilling,
  };
};
