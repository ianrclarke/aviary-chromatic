import { GlobalPromotionFragment } from "@shared/data/fragments";

const createGlobalPromotionFragment = (
  id: string,
  options: Partial<GlobalPromotionFragment> = {}
): GlobalPromotionFragment => {
  const values = { ...options };
  return {
    id,
    targetedDiscount: values.targetedDiscount || 10,
    optIn: values.optIn || false,
  };
};

export { createGlobalPromotionFragment };
