import gql from "graphql-tag";

const GLOBAL_PROMOTION_FRAGMENT = gql`
  fragment GlobalPromotionFragment on GlobalPatientPromotion {
    id

    targetedDiscount
    optIn
  }
`;

interface GlobalPromotionFragment {
  id: string;
  targetedDiscount: number;
  optIn: boolean;
}

export { GlobalPromotionFragment, GLOBAL_PROMOTION_FRAGMENT };
