import gql from "graphql-tag";

interface LandingPageBackgroundFragment {
  id: string;
  purpose: string;
  name: string;
  url: string;
}

const LANDING_PAGE_BACKGROUND_FRAGMENT = gql`
  fragment LandingPageBackgroundFragment on StockPhoto {
    id
    purpose
    name
    url
  }
`;

export { LandingPageBackgroundFragment, LANDING_PAGE_BACKGROUND_FRAGMENT };
