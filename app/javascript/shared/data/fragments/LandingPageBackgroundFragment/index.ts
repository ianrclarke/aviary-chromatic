export { LANDING_PAGE_BACKGROUND_FRAGMENT } from "./LandingPageBackgroundFragment";

export type { LandingPageBackgroundFragment } from "./LandingPageBackgroundFragment";
