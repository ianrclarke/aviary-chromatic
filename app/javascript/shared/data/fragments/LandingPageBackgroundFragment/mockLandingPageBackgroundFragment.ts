import { LandingPageBackgroundFragment } from "./LandingPageBackgroundFragment";

const DEFAULT_OPTIONS = {
  purpose: "practitioner_homepage",
  name: "mountains.jpg",
  url:
    "https://s3.amazonaws.com/assets.healthwave.co/stock-photos/practitioner_homepage/mountains.jpg",
};
export const createLandingPageBackgroundFragment = (
  id: string,
  options: any = {}
): LandingPageBackgroundFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    purpose: values.purpose,
    name: values.name,
    url: values.url,
  };
};
