export { PRACTITIONER_TYPE_FRAGMENT } from "./PractitionerTypeFragment";

export type { PractitionerTypeFragment } from "./PractitionerTypeFragment";
