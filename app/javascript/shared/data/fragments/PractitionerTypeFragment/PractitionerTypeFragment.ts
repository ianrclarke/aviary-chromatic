import gql from "graphql-tag";

interface PractitionerTypeFragment {
  id: string;

  description: string;
  code: string;
  core: boolean;
}

const PRACTITIONER_TYPE_FRAGMENT = gql`
  fragment PractitionerTypeFragment on PractitionerType {
    id

    description
    code
    core
  }
`;

export { PractitionerTypeFragment, PRACTITIONER_TYPE_FRAGMENT };
