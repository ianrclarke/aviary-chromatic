import { PractitionerTypeFragment } from "./PractitionerTypeFragment";

export const createPractitionerTypeFragment = (
  practitionerType: string
): PractitionerTypeFragment => ({
  id: `${practitionerType}-id`,
  description: `${practitionerType}`,
  core: false,
  code: `${practitionerType}-code`,
});
