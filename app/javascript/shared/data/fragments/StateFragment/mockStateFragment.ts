import { StateFragment } from "./StateFragment";

export const createStateFragment = (id: string, options: any = {}): StateFragment => {
  return {
    id,
    name: options.name || `${id}-name`,
    abbr: options.abbr || `${id}-abbr`,
  };
};
