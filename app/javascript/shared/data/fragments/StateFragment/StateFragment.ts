import gql from "graphql-tag";

interface StateFragment {
  id: string;
  name: string;
  abbr: string;
}

const STATE_FRAGMENT = gql`
  fragment StateFragment on State {
    id
    name
    abbr
  }
`;

export { StateFragment, STATE_FRAGMENT };
