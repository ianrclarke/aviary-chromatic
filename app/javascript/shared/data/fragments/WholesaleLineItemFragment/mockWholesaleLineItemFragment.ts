import { defaultValues } from "@shared/mocks/defaultValues";

import { WholesaleLineItemFragment } from "./WholesaleLineItemFragment";

const DEFAULT_OPTIONS = { quantity: 1, amount: 1.1 };
export const createWholesaleLineItemFragment = (
  id: string,
  options: any = {}
): WholesaleLineItemFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,

    taxTotal: values.taxTotal,
    adjustmentTotal: values.adjustmentTotal,
    amount: values.amount,

    costPrice: values.costPrice,
    createdAt: defaultValues.date,

    price: values.price,
    promoTotal: values.promoTotal,
    quantity: values.quantity,
    updatedAt: defaultValues.date,
  };
};
