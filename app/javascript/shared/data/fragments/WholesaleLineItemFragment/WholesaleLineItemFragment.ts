import gql from "graphql-tag";

interface WholesaleLineItemFragment {
  id: string;

  taxTotal: number;
  adjustmentTotal: number;
  amount: number;

  costPrice: number;
  createdAt: string;

  price: number;
  promoTotal: number;
  quantity: number;
  updatedAt: string;
}

const WHOLESALE_LINE_ITEM_FRAGMENT = gql`
  fragment WholesaleLineItemFragment on WholesaleLineItem {
    id

    taxTotal
    adjustmentTotal
    amount

    costPrice
    createdAt

    price
    promoTotal
    quantity
    updatedAt
  }
`;

export { WholesaleLineItemFragment, WHOLESALE_LINE_ITEM_FRAGMENT };
