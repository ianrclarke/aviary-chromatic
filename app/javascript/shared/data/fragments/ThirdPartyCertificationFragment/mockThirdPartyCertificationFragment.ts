import { ThirdPartyCertificationFragment } from "./ThirdPartyCertificationFragment";

const createThirdPartyCertificationFragment = (id: string): ThirdPartyCertificationFragment => {
  return {
    id,
    name: `${id}-name`,
  };
};

export { createThirdPartyCertificationFragment };
