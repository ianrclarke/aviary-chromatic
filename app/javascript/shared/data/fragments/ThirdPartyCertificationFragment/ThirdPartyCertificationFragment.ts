import gql from "graphql-tag";

interface ThirdPartyCertificationFragment {
  id: string;
  name: string;
}

const THIRD_PARTY_CERTIFICATION_FRAGMENT = gql`
  fragment ThirdPartyCertificationFragment on ThirdPartyCertification {
    id
    name
  }
`;

export { ThirdPartyCertificationFragment, THIRD_PARTY_CERTIFICATION_FRAGMENT };
