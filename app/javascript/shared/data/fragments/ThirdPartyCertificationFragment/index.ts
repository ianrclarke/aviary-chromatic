export { THIRD_PARTY_CERTIFICATION_FRAGMENT } from "./ThirdPartyCertificationFragment";

export type { ThirdPartyCertificationFragment } from "./ThirdPartyCertificationFragment";
