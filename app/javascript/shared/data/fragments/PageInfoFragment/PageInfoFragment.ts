import gql from "graphql-tag";

// TODO(FIRE): Move to shared

interface PageInfoFragment {
  endCursor: string;
  hasNextPage: boolean;
  hasPreviousPage: boolean;
  startCursor: string;
}

const PAGE_INFO_FRAGMENT = gql`
  fragment PageInfoFragment on PageInfo {
    endCursor
    hasNextPage
    hasPreviousPage
    startCursor
  }
`;

export { PageInfoFragment, PAGE_INFO_FRAGMENT };
