import { PageInfoFragment } from "./PageInfoFragment";

const DEFAULT_OPTIONS = { hasNextPage: true, hasPreviousPage: false };
export const createPageInfoFragment = (id: string, options: any = {}): PageInfoFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    endCursor: `${id}-endCursor`,
    hasNextPage: values.hasNextPage,
    hasPreviousPage: values.hasPreviousPage,
    startCursor: `${id}-startCursor`,
  };
};
