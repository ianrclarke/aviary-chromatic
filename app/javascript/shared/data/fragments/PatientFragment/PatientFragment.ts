import gql from "graphql-tag";

interface PatientFragment {
  id: string;

  _legacyId: number;
  createdAt: string;
  fullName: string;
  email: string;
  name: string;
  discount: string;
  firstName: string;
  lastName: string;
  storeId: string;
  storeName: string;
  totalDiscount: number;
  type: string;
  updatedAt: string;
  isCurrentPatient: boolean;
  dateOfBirth: string;
  smsNumber: string;
  textMessageNotification: boolean;
  textMessageSubscription: boolean;
}

const PATIENT_FRAGMENT = gql`
  fragment PatientFragment on Patient {
    _legacyId
    id
    createdAt
    fullName
    email
    name
    discount
    dateOfBirth
    firstName
    lastName
    storeName
    totalDiscount
    type
    updatedAt
    isCurrentPatient
    smsNumber
    textMessageNotification
    textMessageSubscription
  }
`;

export { PatientFragment, PATIENT_FRAGMENT };
