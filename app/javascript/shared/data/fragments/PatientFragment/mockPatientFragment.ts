import { defaultValues } from "@shared/mocks/defaultValues";

import { PatientFragment } from "./PatientFragment";

const DEFAULT_OPTIONS = {
  isCurrentPatient: true,
  textMessageNotification: false,
  textMessageSubscription: false,
  _legacyId: 0,
};

const createPatientFragment = (id: string, options: any = {}): PatientFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    // eslint-disable-next-line no-underscore-dangle
    _legacyId: options._legacyId || 123,
    createdAt: defaultValues.date,
    fullName: `${id}-fullName`,
    email: `${id}-email`,
    dateOfBirth: "2019-10-10",
    name: `${id}-name`,
    discount: "20",
    firstName: `${id}-firstName`,
    lastName: `${id}-lastName`,
    storeId: `${id}-storeId`,
    storeName: `${id}-storeName`,
    totalDiscount: values.totalDiscount || null,
    type: `${id}-type`,
    updatedAt: defaultValues.date,
    isCurrentPatient: values.isCurrentPatient,
    smsNumber: `${id}-smsNumber`,
    textMessageNotification: values.textMessageNotification,
    textMessageSubscription: values.textMessageSubscription,
  };
};

export { createPatientFragment };
