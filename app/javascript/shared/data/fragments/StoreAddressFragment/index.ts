export { STORE_ADDRESS_FRAGMENT } from "./StoreAddressFragment";

export type { StoreAddressFragment } from "./StoreAddressFragment";
