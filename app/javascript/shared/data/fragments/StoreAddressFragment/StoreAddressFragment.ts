import gql from "graphql-tag";

const APPROVAL_STATUSES = {
  cleared: "cleared",
  failed: "failed",
  held: "held",
  passed: "passed",
  pending: "pending",
  rejected: "rejected",
  skipped: "skipped",
};

type ApprovalStatus = keyof typeof APPROVAL_STATUSES;

interface StoreAddressFragment {
  id: string;

  firstname: string;
  lastname: string;
  address1: string;
  address2: string;
  approvalStatus: ApprovalStatus;
  city: string;
  country: string;
  createdAt: string;
  updatedAt: string;
  zipcode: string;
}

const STORE_ADDRESS_FRAGMENT = gql`
  fragment StoreAddressFragment on StoreAddress {
    id

    firstname
    lastname
    address1
    address2
    approvalStatus
    city
    country
    createdAt
    updatedAt
    zipcode
  }
`;

export { StoreAddressFragment, STORE_ADDRESS_FRAGMENT };
