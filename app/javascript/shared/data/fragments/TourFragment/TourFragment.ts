import gql from "graphql-tag";

interface TourFragment {
  id: string;
  action: string;
  controller: string;
  subAction: string;
  templateName: string;
}

const TOUR_FRAGMENT = gql`
  fragment TourFragment on Tour {
    id

    action
    controller
    subAction
    templateName
  }
`;

export { TourFragment, TOUR_FRAGMENT };
