import { TourFragment } from "./TourFragment";

export const createTourFragment = (
  id: string,
  controller?: string,
  action?: string
): TourFragment => {
  return {
    id,
    action: action || `${id}-action`,
    controller: controller || `${id}-controller`,
    subAction: `${id}-subAction`,
    templateName: `${id}-templateName`,
  };
};
