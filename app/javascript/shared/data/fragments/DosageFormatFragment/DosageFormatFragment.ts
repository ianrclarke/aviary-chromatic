import gql from "graphql-tag";

interface DosageFormatFragment {
  id: string;
  baseFormat: string;
  variation: string;
}

const DOSAGE_FORMAT_FRAGMENT = gql`
  fragment DosageFormatFragment on DosageFormat {
    id
    baseFormat
    variation
  }
`;

export { DosageFormatFragment, DOSAGE_FORMAT_FRAGMENT };
