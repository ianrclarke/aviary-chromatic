import { DosageFormatFragment } from "./DosageFormatFragment";

export const createDosageFormatFragment = (id: string): DosageFormatFragment => {
  return {
    id,
    baseFormat: `${id}-baseFormat`,
    variation: `${id}-variation`,
  };
};
