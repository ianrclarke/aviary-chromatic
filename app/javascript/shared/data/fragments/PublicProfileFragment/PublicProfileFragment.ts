import gql from "graphql-tag";

interface PublicProfileFragment {
  email: string;
  phone: string;
}

const PUBLIC_PROFILE_FRAGMENT = gql`
  fragment PublicProfileFragment on PublicProfile {
    id
    email
    phone
  }
`;

export { PublicProfileFragment, PUBLIC_PROFILE_FRAGMENT };
