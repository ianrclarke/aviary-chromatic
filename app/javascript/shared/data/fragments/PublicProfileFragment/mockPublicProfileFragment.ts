import { PublicProfileFragment } from "./PublicProfileFragment";

export const createPublicProfileFragment = (id: string): PublicProfileFragment => {
  return { email: `${id}-email`, phone: `${id}-phone` };
};
