export { TREATMENT_PLAN_MESSAGE_FRAGMENT } from "./TreatmentPlanMessageFragment";

export type { TreatmentPlanMessageFragment } from "./TreatmentPlanMessageFragment";
