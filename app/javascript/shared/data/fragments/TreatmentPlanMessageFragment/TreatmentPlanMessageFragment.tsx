import gql from "graphql-tag";

interface TreatmentPlanMessageFragment {
  id: string;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  currentState: string;
  contents: string;
}

const TREATMENT_PLAN_MESSAGE_FRAGMENT = gql`
  fragment TreatmentPlanMessageFragment on TreatmentPlanMessage {
    id

    createdAt
    updatedAt
    publishedAt
    currentState
    contents
  }
`;
export { TreatmentPlanMessageFragment, TREATMENT_PLAN_MESSAGE_FRAGMENT };
