import { defaultValues } from "@shared/mocks/defaultValues";

import { TreatmentPlanMessageFragment } from "./TreatmentPlanMessageFragment";

export const createTreatmentPlanMessageFragment = (id: string): TreatmentPlanMessageFragment => {
  return {
    id,

    createdAt: defaultValues.date,
    updatedAt: defaultValues.date,
    publishedAt: defaultValues.date,
    currentState: `${id}-state`,
    contents: `${id}-contents`,
  };
};
