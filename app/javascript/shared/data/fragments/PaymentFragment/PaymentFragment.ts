import gql from "graphql-tag";

interface PaymentFragment {
  id: number;
  state: string;
}

const PAYMENT_FRAGMENT = gql`
  fragment PaymentFragment on Payment {
    id
    state
  }
`;

export { PaymentFragment, PAYMENT_FRAGMENT };
