import { PaymentFragment } from "./PaymentFragment";

export const createPaymentFragment = (id: number): PaymentFragment => {
  return {
    id,
    state: `${id}-state`,
  };
};
