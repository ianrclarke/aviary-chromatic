import gql from "graphql-tag";

const CERTIFICATION_FRAGMENT = gql`
  fragment CertificationFragment on Certification {
    id

    name
    certificationUrl
    currentState
    createdAt
    updatedAt
  }
`;

interface CertificationFragment {
  id: string;
  name: string;
  certificationUrl: string;
  currentState: string;
  createdAt: string;
  updatedAt: string;
}

export { CERTIFICATION_FRAGMENT, CertificationFragment };
