import { CertificationFragment } from "@shared/data/fragments";
import { defaultValues } from "@shared/mocks/defaultValues";

const DEFAULT_OPTIONS = { certificationUrl: "http://example.org/certification" };
export const createCertificationFragment = (
  id: string,
  options: any = {}
): CertificationFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    name: values.name || `${id}-name`,
    certificationUrl: values.certificationUrl,
    currentState: values.currentState || `${id}-currentState`,
    createdAt: defaultValues.date,
    updatedAt: defaultValues.date,
  };
};
