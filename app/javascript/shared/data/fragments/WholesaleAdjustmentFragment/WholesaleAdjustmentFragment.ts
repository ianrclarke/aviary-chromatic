import gql from "graphql-tag";

interface WholesaleAdjustmentFragment {
  id: string;
  amount: number;
  code: string;
  createdAt: string;
  description: string;
  label: string;
  updatedAt: string;
}

const WHOLESALE_ADJUSTMENT_FRAGMENT = gql`
  fragment WholesaleAdjustmentFragment on WholesaleAdjustment {
    id
    amount
    code
    createdAt
    description
    label
    updatedAt
  }
`;

export { WholesaleAdjustmentFragment, WHOLESALE_ADJUSTMENT_FRAGMENT };
