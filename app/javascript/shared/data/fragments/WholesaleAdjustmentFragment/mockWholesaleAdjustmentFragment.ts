import { defaultValues } from "@shared/mocks/defaultValues";

import { WholesaleAdjustmentFragment } from "./WholesaleAdjustmentFragment";

const DEFAULT_OPTIONS = {
  amount: -20,
  code: "TestCode2020",
  createdAt: defaultValues.date,
  description: "This takes $20 off",
  label: "Promotion(Test Promo)",
  updatedAt: defaultValues.date,
};

export const createWholesaleAdjustmentFragment = (
  id: string,
  options: any = {}
): WholesaleAdjustmentFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    amount: values.amount,
    code: values.code,
    createdAt: values.createdAt,
    description: values.description,
    label: values.label,
    updatedAt: values.updatedAt,
  };
};
