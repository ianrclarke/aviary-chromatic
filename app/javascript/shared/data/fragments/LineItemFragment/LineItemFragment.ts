import gql from "graphql-tag";

interface LineItemFragment {
  id: string;

  createdAt: string;
  quantity: number;
  updatedAt: string;

  msrpPrice: number;
  msrpTotal: number;
  price: number;
  priceTotal: number;
}

const LINE_ITEM_FRAGMENT = gql`
  fragment LineItemFragment on LineItem {
    id

    createdAt
    quantity
    updatedAt

    msrpPrice
    msrpTotal
    price
    priceTotal
  }
`;

export { LineItemFragment, LINE_ITEM_FRAGMENT };
