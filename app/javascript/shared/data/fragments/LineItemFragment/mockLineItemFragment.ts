import { defaultValues } from "@shared/mocks/defaultValues";

import { LineItemFragment } from "./LineItemFragment";

const DEFAULT_OPTIONS = { quantity: 1 };
export const createLineItemFragment = (id: string, options: any = {}): LineItemFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    createdAt: defaultValues.date,
    quantity: values.quantity,
    updatedAt: defaultValues.date,

    msrpPrice: values.msrpPrice,
    msrpTotal: values.msrpTotal,
    price: values.price,
    priceTotal: values.priceTotal,
  };
};
