import { defaultValues } from "@shared/mocks/defaultValues";

import { SubscriptionFragment } from "./SubscriptionFragment";

const DEFAULT_OPTIONS = { active: true, subtotal: 1 };
export const createSubscriptionFragment = (id: string, options: any = {}): SubscriptionFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    createdAt: defaultValues.date,
    frequency: `${id}-frequency`,
    lastOrderedOn: values.lastOrderedOn || defaultValues.date,
    nextOrderDate: values.nextOrderDate || defaultValues.date,
    receiveByDate: values.receiveByDate || defaultValues.date,
    subtotal: values.subtotal,
    updatedAt: defaultValues.date,
  };
};
