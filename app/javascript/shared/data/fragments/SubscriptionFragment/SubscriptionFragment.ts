import gql from "graphql-tag";

interface SubscriptionFragment {
  id: string;

  createdAt: string;
  frequency: string;
  lastOrderedOn: string;
  nextOrderDate: string;
  receiveByDate: string;
  subtotal: number;
  updatedAt: string;
}

const SUBSCRIPTION_FRAGMENT = gql`
  fragment SubscriptionFragment on Subscription {
    id

    createdAt
    frequency
    lastOrderedOn
    nextOrderDate
    receiveByDate
    subtotal
    updatedAt
  }
`;

export { SubscriptionFragment, SUBSCRIPTION_FRAGMENT };
