import gql from "graphql-tag";

interface WholesalePaymentFragment {
  amount: number;
  createdAt: string;
  id: string;
  number: string;
  state: string;
  updatedAt: string;
}

const WHOLESALE_PAYMENT_FRAGMENT = gql`
  fragment WholesalePaymentFragment on WholesalePayment {
    amount
    createdAt
    id
    number
    state
    updatedAt
  }
`;

export { WholesalePaymentFragment, WHOLESALE_PAYMENT_FRAGMENT };
