export { WHOLESALE_PAYMENT_FRAGMENT } from "./WholesalePaymentFragment";

export type { WholesalePaymentFragment } from "./WholesalePaymentFragment";
