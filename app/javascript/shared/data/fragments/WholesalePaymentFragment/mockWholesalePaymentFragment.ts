import { defaultValues } from "@shared/mocks/defaultValues";

import { WholesalePaymentFragment } from "./WholesalePaymentFragment";

const DEFAULT_OPTIONS = {
  amount: 135,
  createdAt: defaultValues.date,
  state: "completed",
  updatedAt: defaultValues.date,
};

export const createWholesalePaymentFragment = (
  id: string,
  options: any = {}
): WholesalePaymentFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    amount: values.amount,
    createdAt: defaultValues.date,
    number: `${id}-orderNumber`,
    state: values.state,
    updatedAt: defaultValues.date,
  };
};
