import { ImagesFragment } from "./ImagesFragment";

export const createImagesFragment = (id: string, options: any = {}): ImagesFragment => {
  return {
    id,

    contentType: `${id}-content-type`,
    fileName: `${id}-file-name`,
    fileSize: `${id}-file-size`,
    alt: `${id}-alt-text`,
    width: `${id}-width`,
    height: `${id}-height`,
    position: `${id}-position`,
    urlMini: `${id}-url-mini`,
    urlSmall: `${id}-url-small`,
    urlProduct: `${id}-url-product`,
    urlLarge: `${id}-url-large`,
    urlOriginal: `${id}-url-original`,
    ...options,
  };
};
