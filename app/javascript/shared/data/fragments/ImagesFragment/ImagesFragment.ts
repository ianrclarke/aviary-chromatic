import gql from "graphql-tag";

interface ImagesFragment {
  id: string;

  contentType: string;
  fileName: string;
  fileSize: string;
  alt: string;
  width: string;
  height: string;
  position: string;
  urlMini: string;
  urlSmall: string;
  urlProduct: string;
  urlLarge: string;
  urlOriginal: string;
}

const IMAGES_FRAGMENT = gql`
  fragment ImagesFragment on Images {
    id
    contentType
    fileName
    fileSize
    alt
    width
    height
    position
    urlMini
    urlSmall
    urlProduct
    urlLarge
    urlOriginal
  }
`;

export { ImagesFragment, IMAGES_FRAGMENT };
