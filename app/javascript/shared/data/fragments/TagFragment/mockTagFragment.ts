import { defaultValues } from "@shared/mocks/defaultValues";

import { TagFragment } from "./TagFragment";

const DEFAULT_OPTIONS = {
  depth: 2,
  isStub: false,
  productsCount: 2,
  root: false,
};

export const createTagFragment = (id: string, options?: any): TagFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };

  return {
    id,
    name: `${id}-name`,
    createdAt: defaultValues.date,
    updatedAt: defaultValues.date,
    parentId: `${id}-parent`,
    ...values,
  };
};
