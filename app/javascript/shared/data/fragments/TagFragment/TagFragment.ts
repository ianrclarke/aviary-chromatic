import gql from "graphql-tag";

interface TagFragment {
  id: string;
  createdAt: string;
  depth: number;
  isStub: boolean;
  name: string;
  parentId: string;
  productsCount: number;
  root: boolean;
  updatedAt: string;
  grandParentId: string;
}

const TAG_FRAGMENT = gql`
  fragment TagFragment on Tag {
    id
    createdAt
    depth
    isStub
    name
    parentId
    productsCount
    root
    updatedAt
    grandParentId
  }
`;

export { TagFragment, TAG_FRAGMENT };
