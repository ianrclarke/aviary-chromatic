import gql from "graphql-tag";

interface TypeaheadFragment {
  id: string;
  entityType: "Brand" | "Ingredient" | "SupplementType";
  entityTypeId: string;
  highlightedName: string;
  name: string;
}

const TYPEAHEAD_FRAGMENT = gql`
  fragment TypeaheadFragment on Typeahead {
    id
    entityType
    entityTypeId
    highlightedName
    name
  }
`;

export { TypeaheadFragment, TYPEAHEAD_FRAGMENT };
