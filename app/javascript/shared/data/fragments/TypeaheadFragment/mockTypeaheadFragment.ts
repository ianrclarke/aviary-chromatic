import { TypeaheadFragment } from "./TypeaheadFragment";

const createTypeaheadFragment = (id: string, options: any = {}): TypeaheadFragment => {
  return {
    id,
    entityType: options.entityType || "someType",
    entityTypeId: options.entityTypeId || "someTypeId",
    highlightedName: options.highlightedName || "someHighlightName",
    name: `${id}-name`,
  };
};

export { createTypeaheadFragment };
