import gql from "graphql-tag";

interface RecommendationFragment {
  id: string;

  additionalInfo: string;
  alreadyRequestedTherapeuticSwitching: boolean;
  amount: string;
  availableAt: string;
  createdAt: string;
  currentState: string;
  duration: string;
  format: string;
  frequency: string;
  instructions: string;
  refillable: boolean;
  unitsToPurchase: number;
  updatedAt: string;
  isSupplierDosage: boolean;
  isTextOnly: boolean;
}

const RECOMMENDATION_FRAGMENT = gql`
  fragment RecommendationFragment on Recommendation {
    id

    additionalInfo
    alreadyRequestedTherapeuticSwitching
    amount
    availableAt
    createdAt
    currentState
    duration
    format
    frequency
    instructions
    refillable
    unitsToPurchase
    updatedAt
    isSupplierDosage
    isTextOnly
  }
`;

export { RecommendationFragment, RECOMMENDATION_FRAGMENT };
