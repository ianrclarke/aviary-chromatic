import { defaultValues } from "@shared/mocks/defaultValues";

import { RecommendationFragment } from "./RecommendationFragment";

const DEFAULT_OPTIONS = {
  refillable: true,
  unitsToPurchase: 1,
  alreadyRequestedTherapeuticSwitching: false,
};
export const createRecommendationFragment = (
  id: string,
  options: any = {}
): RecommendationFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    additionalInfo: `${id}-additionalInfo`,
    alreadyRequestedTherapeuticSwitching: values.alreadyRequestedTherapeuticSwitching,
    amount: `${id}-amount`,
    availableAt: defaultValues.date,
    createdAt: defaultValues.date,
    currentState: `${id}-currentState`,
    duration: `${id}-duration`,
    format: `${id}-format`,
    frequency: `${id}-frequency`,
    instructions: `${id}-instructions`,
    refillable: values.refillable,
    unitsToPurchase: values.unitsToPurchase,
    updatedAt: defaultValues.date,
    isSupplierDosage: false,
    isTextOnly: false,
  };
};
