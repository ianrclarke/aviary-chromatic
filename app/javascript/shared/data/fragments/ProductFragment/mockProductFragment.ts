import { defaultValues } from "@shared/mocks/defaultValues";

import { ProductFragment } from "./ProductFragment";

const DEFAULT_OPTIONS = { heatSensitive: true, prescriptionOnly: false };
export const createProductFragment = (id: string, options: any = {}): ProductFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,

    _legacyId: id,
    createdAt: defaultValues.date,
    description: `${id}-description`,
    heatSensitive: values.heatSensitive,
    inOffice: values.inOffice,
    proposition65: values.proposition65,
    prescriptionOnly: values.prescriptionOnly,
    name: `${id}-name`,
    updatedAt: defaultValues.date,
    dosageAmount: `${id}-dosageAmount`,
    dosageFrequency: `${id}-dosageFrequency`,
    dosageInstructions: `${id}-dosageInstructions`,
    dosageFormat: `${id}-dosageFormat`,
    dosagePermutations: `${id}-dosagePermutations`,
    isInStoreCategories: values.isInStoreCategories || false,
  };
};
