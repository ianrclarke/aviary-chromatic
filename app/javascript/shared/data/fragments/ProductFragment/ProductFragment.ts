import gql from "graphql-tag";

interface ProductFragment {
  id: string;

  _legacyId: string;
  createdAt: string;
  description: string;
  heatSensitive: boolean;
  proposition65: boolean;
  prescriptionOnly: boolean;
  inOffice: boolean;
  name: string;
  updatedAt: string;
  dosageAmount: string;
  dosageFrequency: string;
  dosageInstructions: string;
  dosageFormat: string;
  dosagePermutations: string;
  isInStoreCategories: boolean;
}

const PRODUCT_FRAGMENT = gql`
  fragment ProductFragment on Product {
    id

    _legacyId
    createdAt
    description
    heatSensitive
    proposition65
    prescriptionOnly
    inOffice
    name
    updatedAt
    dosageAmount
    dosageFrequency
    dosageInstructions
    dosageFormat
    dosagePermutations
    isInStoreCategories
  }
`;

export { ProductFragment, PRODUCT_FRAGMENT };
