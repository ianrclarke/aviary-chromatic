/* eslint-disable no-underscore-dangle */
import { defaultValues } from "@shared/mocks/defaultValues";

import { CurrentState, TreatmentPlanFragment } from "./TreatmentPlanFragment";

export const createTreatmentPlanFragment = (
  id: string,
  options: any = {}
): TreatmentPlanFragment => {
  return {
    id,
    _legacyId: options._legacyId || 0,
    associatedOrderCount: options.associatedOrderCount || 0,
    createdAt: defaultValues.date,
    availableAt: defaultValues.date,
    updatedAt: defaultValues.date,
    currentState: options.currentState || CurrentState.ACTIVE,
    slug: `${id}-slug`,
    message: `${id}-message`,
    treatmentPlanPdfUrl: `${id}-treatmentPlanPdfUrl`,
    totalDiscountedPrice: 22,
    totalPrice: 102,
    totalSavings: 2,
    isDirty: options.isDirty || false,
  };
};
