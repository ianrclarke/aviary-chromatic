import gql from "graphql-tag";

enum CurrentState {
  ACTIVE = "active",
  DRAFT = "draft",
  CANCELLED = "cancelled",
}

interface TreatmentPlanFragment {
  id: string;

  associatedOrderCount: number;
  createdAt: string;
  availableAt: string;
  updatedAt: string;
  currentState: CurrentState;
  message: string;
  slug: string;
  treatmentPlanPdfUrl: string;
  totalDiscountedPrice: number;
  totalPrice: number;
  totalSavings: number;
  _legacyId: number;
  isDirty: boolean;
}

const TREATMENT_PLAN_FRAGMENT = gql`
  fragment TreatmentPlanFragment on TreatmentPlan {
    id

    associatedOrderCount
    createdAt
    availableAt
    updatedAt
    currentState
    message
    slug
    treatmentPlanPdfUrl
    totalDiscountedPrice
    totalPrice
    totalSavings
    _legacyId
    isDirty
  }
`;

export { CurrentState, TreatmentPlanFragment, TREATMENT_PLAN_FRAGMENT };
