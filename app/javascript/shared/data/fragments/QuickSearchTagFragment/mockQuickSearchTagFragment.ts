import { QuickSearchTagFragment } from "./QuickSearchTagFragment";

const DEFAULT_OPTIONS = { depth: 0 };
const createQuickSearchTagFragment = (id: string, options: any = {}): QuickSearchTagFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    name: `${id}-name`,
    parentId: `${id}-parent-id`,
    depth: values.depth,
  };
};

export { createQuickSearchTagFragment };
