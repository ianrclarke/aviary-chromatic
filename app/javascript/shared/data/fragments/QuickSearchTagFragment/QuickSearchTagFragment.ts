import gql from "graphql-tag";

interface QuickSearchTagFragment {
  id: string;
  name: string;
  parentId: string;
  depth: number;
}

const QUICK_SEARCH_TAG_FRAGMENT = gql`
  fragment QuickSearchTagFragment on QuickSearchTag {
    id
    name
    parentId
    depth
  }
`;

export { QuickSearchTagFragment, QUICK_SEARCH_TAG_FRAGMENT };
