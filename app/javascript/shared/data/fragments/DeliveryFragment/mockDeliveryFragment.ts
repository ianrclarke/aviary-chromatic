/* eslint-disable @typescript-eslint/camelcase */
import { DeliveryFragment } from "./DeliveryFragment";

export const createDeliveryFragment = (id: number): DeliveryFragment => {
  return {
    cost: `${id}.00`,
    description: `${id}-ok-cool-description`,
    display_cost: `$${id}.00`,
    full_name: `${id}-name-of-option`,
    selected: false,
    shipment_id: id,
    estimated_arrival: `${id}-five-days`,
  };
};
