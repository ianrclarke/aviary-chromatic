import gql from "graphql-tag";

interface DeliveryFragment {
  cost: string;
  description: string;
  display_cost: string;
  full_name: string;
  selected: boolean;
  shipment_id: number;
  estimated_arrival: string;
}

const DELIVERY_FRAGMENT = gql`
  fragment DeliveryFragment on Delivery {
    cost
    description
    display_cost
    full_name
    selected
    shipment_id
    estimated_arrival
  }
`;

export { DeliveryFragment, DELIVERY_FRAGMENT };
