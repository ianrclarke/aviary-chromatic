import gql from "graphql-tag";

interface TokenFragment {
  jwt: string;
  currentUserRoleType: string;
  currentUserRolePresent: boolean;
}

const TOKEN_FRAGMENT = gql`
  fragment TokenFragment on Token {
    jwt
    currentUserRoleType
    currentUserRolePresent
  }
`;

export { TokenFragment, TOKEN_FRAGMENT };
