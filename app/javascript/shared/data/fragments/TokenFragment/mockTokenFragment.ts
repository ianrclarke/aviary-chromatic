import { TokenFragment } from "./TokenFragment";

export const createTokenFragment = (
  jwt: string,
  currentUserRoleType: string,
  currentUserRolePresent: boolean
): TokenFragment => {
  return {
    jwt,
    currentUserRoleType,
    currentUserRolePresent,
  };
};
