import gql from "graphql-tag";

const MERCHANT_DATA_FRAGMENT = gql`
  fragment MerchantDataFragment on MerchantData {
    id

    createdAt
    updatedAt

    accountType
    birthDay
    birthMonth
    birthYear
    businessName
    businessNumber
    city
    employerId
    firstName
    lastName
    lastVerifiedTaxInformationAt
    phone
    postalCode
    region
    streetAddress
    streetAddress2
  }
`;

interface MerchantDataFragment {
  id: string;

  createdAt: string;
  updatedAt: string;

  accountType: string;
  birthDay: string;
  birthMonth: string;
  birthYear: string;
  businessName: string;
  businessNumber: string;
  city: string;
  employerId: string;
  firstName: string;
  lastName: string;
  lastVerifiedTaxInformationAt: string;
  phone: string;
  postalCode: string;
  region: string;
  streetAddress: string;
  streetAddress2: string;
}

export { MerchantDataFragment, MERCHANT_DATA_FRAGMENT };
