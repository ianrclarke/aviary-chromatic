import { MerchantDataFragment } from "@shared/data/fragments/MerchantDataFragment";
import { defaultValues } from "@shared/mocks/defaultValues";

const createMerchantDataFragment = (
  id: string,
  options: Partial<MerchantDataFragment> = {}
): MerchantDataFragment => {
  return {
    id,
    createdAt: options.createdAt || defaultValues.date,
    updatedAt: options.updatedAt || defaultValues.date,
    lastVerifiedTaxInformationAt: options.lastVerifiedTaxInformationAt || defaultValues.date,
    accountType: options.accountType || `${id}-accountType`,
    birthDay: options.birthDay || `${id}-birthDay`,
    birthMonth: options.birthMonth || `${id}-birthMonth`,
    birthYear: options.birthYear || `${id}-birthYear`,
    businessName: options.businessName || `${id}-businessName`,
    businessNumber: options.businessNumber || `${id}-businessNumber`,
    employerId: options.employerId || `${id}-employerId`,
    city: options.city || `${id}-city`,
    postalCode: options.postalCode || `${id}-postalCode`,
    region: options.region || `${id}-region`,
    streetAddress: options.streetAddress || `${id}-streetAddress`,
    streetAddress2: options.streetAddress2 || `${id}-streetAddress2`,
    firstName: options.firstName || `${id}-firstName`,
    lastName: options.lastName || `${id}-lastName`,
    phone: options.phone || `${id}-phone`,
  };
};

export { createMerchantDataFragment };
