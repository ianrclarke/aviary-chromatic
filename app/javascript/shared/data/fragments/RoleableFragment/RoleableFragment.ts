import gql from "graphql-tag";

interface RoleableFragment {
  id: string;
  email: string;
  fullName: string;
  storeId: string;
  storeName: string;
  type: string;
}

const ROLEABLE_FRAGMENT = gql`
  fragment RoleableFragment on Roleable {
    id
    email
    fullName
    storeId
    storeName
    type
  }
`;

export { RoleableFragment, ROLEABLE_FRAGMENT };
