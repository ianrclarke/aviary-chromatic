import { RoleableFragment } from "./RoleableFragment";

export const createRoleableFragment = (id: string, options: any = {}): RoleableFragment => {
  return {
    id,
    email: `${id}-email`,
    fullName: `${id}-fullName`,
    storeId: `${id}-storeId`,
    storeName: `${id}-storeName`,
    type: options.type ? options.type : "Practitioner",
  };
};
