import gql from "graphql-tag";

interface OrderFragment {
  id: string;

  additionalTaxTotal: number;
  adjustmentTotal: number;
  completedAt: string;
  createdAt: string;
  isAutoship: boolean;
  msrpTotal: number;
  number: string;
  orderReceiptUrl: string;
  paymentState: string;
  shipmentState: string;
  shippingMethod: string;
  specialInstructions: string;
  shipmentTotal: number;
  savingsTotal: number;
  signatureRequired: boolean;
  subTotal: number;
  taxTotal: number;
  patientPromotionDiscountTotal: number;
  previousStoreDiscountTotal: number;
  total: number;
  state: string;
  updatedAt: string;
}

const ORDER_FRAGMENT = gql`
  fragment OrderFragment on Order {
    id

    additionalTaxTotal
    adjustmentTotal
    completedAt
    createdAt
    isAutoship
    msrpTotal
    number
    orderReceiptUrl
    paymentState
    shipmentState
    shipmentTotal
    savingsTotal
    shippingMethod
    state
    subTotal
    taxTotal
    patientPromotionDiscountTotal
    previousStoreDiscountTotal
    total
    updatedAt
  }
`;

export { OrderFragment, ORDER_FRAGMENT };
