import { defaultValues } from "@shared/mocks/defaultValues";

import { OrderFragment } from "./OrderFragment";

const DEFAULT_OPTIONS = { msrpTotal: 1 };
export const createOrderFragment = (id: string, options: any = {}): OrderFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,

    additionalTaxTotal: values.additionalTaxTotal,
    adjustmentTotal: values.adjustmentTotal,
    completedAt: defaultValues.date,
    createdAt: defaultValues.date,
    isAutoship: values.isAutoship,
    msrpTotal: values.msrpTotal,
    number: `${id}-number`,
    paymentState: `${id}-paymentState`,
    savingsTotal: values.savingsTotal,
    shipmentState: `${id}-shipmentState`,
    shippingMethod: `${id}-shippingMethod`,
    shipmentTotal: values.shipmentTotal,
    specialInstructions: `${id}-specialInstructions`,
    signatureRequired: values.signatureRequired,
    state: `${id}-state`,
    subTotal: values.subTotal,
    taxTotal: values.taxTotal,
    patientPromotionDiscountTotal: values.patientPromotionDiscountTotal,
    previousStoreDiscountTotal: values.previousStoreDiscountTotal,
    total: values.total,
    updatedAt: defaultValues.date,
    orderReceiptUrl: `orders/${id}?pdf=true`,
  };
};
