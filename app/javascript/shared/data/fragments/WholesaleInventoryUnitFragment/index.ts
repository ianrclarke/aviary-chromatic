export { WHOLESALE_INVENTORY_UNIT_FRAGMENT } from "./WholesaleInventoryUnitFragment";

export type { WholesaleInventoryUnitFragment } from "./WholesaleInventoryUnitFragment";
