import { defaultValues } from "@shared/mocks/defaultValues";

import { WholesaleInventoryUnitFragment } from "./WholesaleInventoryUnitFragment";

const DEFAULT_OPTIONS = { quantity: 1, state: "ready" };
export const createWholesaleInventoryUnitFragment = (
  id: string,
  options: any = {}
): WholesaleInventoryUnitFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    createdAt: defaultValues.date,
    updatedAt: defaultValues.date,

    quantity: values.quantity,
    state: values.state,
  };
};
