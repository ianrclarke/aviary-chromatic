import gql from "graphql-tag";

interface WholesaleInventoryUnitFragment {
  id: string;
  createdAt: string;
  updatedAt: string;

  state: string;
  quantity: number;
}

const WHOLESALE_INVENTORY_UNIT_FRAGMENT = gql`
  fragment WholesaleInventoryUnitFragment on WholesaleInventoryUnit {
    id
    createdAt
    updatedAt

    state
    quantity
  }
`;

export { WholesaleInventoryUnitFragment, WHOLESALE_INVENTORY_UNIT_FRAGMENT };
