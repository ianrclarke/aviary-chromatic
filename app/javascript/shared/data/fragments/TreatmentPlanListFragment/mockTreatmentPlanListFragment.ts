import { defaultValues } from "@shared/mocks/defaultValues";

import { TreatmentPlanListFragment } from "./TreatmentPlanListFragment";

export const createTreatmentPlanListFragment = (id: string): TreatmentPlanListFragment => {
  return {
    id,
    availableAt: defaultValues.date,
  };
};
