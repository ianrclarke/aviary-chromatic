import gql from "graphql-tag";

interface TreatmentPlanListFragment {
  id: string;
  availableAt: string;
}

const TREATMENT_PLAN_LIST_FRAGMENT = gql`
  fragment TreatmentPlanListFragment on TreatmentPlanList {
    id
    availableAt
  }
`;

export { TreatmentPlanListFragment, TREATMENT_PLAN_LIST_FRAGMENT };
