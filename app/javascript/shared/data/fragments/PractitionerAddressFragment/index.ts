export { PRACTITIONER_ADDRESS_FRAGMENT } from "./PractitionerAddressFragment";

export type { PractitionerAddressFragment } from "./PractitionerAddressFragment";
