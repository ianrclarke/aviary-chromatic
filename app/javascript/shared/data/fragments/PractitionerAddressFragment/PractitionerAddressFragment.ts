import gql from "graphql-tag";

interface PractitionerAddressFragment {
  id: string;

  firstname: string;
  lastname: string;
  address1: string;
  address2: string;
  city: string;
  country: string;
  createdAt: string;
  updatedAt: string;
  zipcode: string;
}

const PRACTITIONER_ADDRESS_FRAGMENT = gql`
  fragment PractitionerAddressFragment on PractitionerAddress {
    id

    firstname
    lastname
    address1
    address2
    city
    country
    createdAt
    updatedAt
    zipcode
  }
`;

export { PractitionerAddressFragment, PRACTITIONER_ADDRESS_FRAGMENT };
