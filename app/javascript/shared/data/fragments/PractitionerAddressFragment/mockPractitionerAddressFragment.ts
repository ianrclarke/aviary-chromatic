import { defaultValues } from "@shared/mocks/defaultValues";

import { PractitionerAddressFragment } from "./PractitionerAddressFragment";

export const createPractitionerAddressFragment = (
  id: string,
  options: any = {}
): PractitionerAddressFragment => {
  return {
    id,
    createdAt: defaultValues.date,
    updatedAt: defaultValues.date,
    firstname: options.firstname || `${id}-firstname`,
    lastname: options.lastname || `${id}-lastname`,
    address1: options.address1 || `${id}-address1`,
    address2: options.address2 || `${id}-address2`,
    city: options.city || `${id}-city`,
    country: options.country || `${id}-country`,
    zipcode: options.zipcode || `${id}-zipcode`,
  };
};
