import gql from "graphql-tag";

interface NotificationSettingTypeFragment {
  id: string;

  createdAt: string;
  updatedAt: string;
  code: string;
  title: string;
  description: string;
  dispensarySpecific: boolean;
}

const NOTIFICATION_SETTING_TYPE_FRAGMENT = gql`
  fragment NotificationSettingTypeFragment on NotificationSettingType {
    id

    createdAt
    updatedAt
    code
    title
    description
    dispensarySpecific
  }
`;

export { NotificationSettingTypeFragment, NOTIFICATION_SETTING_TYPE_FRAGMENT };
