import { defaultValues } from "@shared/mocks/defaultValues";

import { NotificationSettingTypeFragment } from "./NotificationSettingTypeFragment";

const createNotificationSettingTypeFragment = (id: string): NotificationSettingTypeFragment => ({
  id,
  createdAt: defaultValues.date,
  updatedAt: defaultValues.date,
  title: `${id}-title`,
  description: `${id}-description`,
  code: `${id}-code`,
  dispensarySpecific: true,
});

export { createNotificationSettingTypeFragment };
