import { StudentProfileFragment } from "@shared/data/fragments/StudentProfileFragment";
import { defaultValues } from "@shared/mocks/defaultValues";

const createStudentProfileFragment = (id: string, options: any = {}): StudentProfileFragment => {
  return {
    id,
    createdAt: defaultValues.date,
    updatedAt: defaultValues.date,
    graduationDate: options.graduationDate || defaultValues.date,
  };
};

export { createStudentProfileFragment };
