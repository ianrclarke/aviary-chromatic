import gql from "graphql-tag";

interface StudentProfileFragment {
  id: string;

  graduationDate: string;
  createdAt: string;
  updatedAt: string;
}
const STUDENT_PROFILE_FRAGMENT = gql`
  fragment StudentProfileFragment on StudentProfile {
    id

    createdAt
    updatedAt

    graduationDate
  }
`;

export { StudentProfileFragment, STUDENT_PROFILE_FRAGMENT };
