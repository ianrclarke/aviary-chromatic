export { STUDENT_PROFILE_FRAGMENT } from "./StudentProfileFragment";

export type { StudentProfileFragment } from "@shared/data/fragments/StudentProfileFragment/StudentProfileFragment";
