import gql from "graphql-tag";

interface CreditCardAttributesFragment {
  name: string;
  token: string;
  oneTimeUse: boolean;
}

const CREDIT_CARD_ATTRIBUTES_FRAGMENT = gql`
  fragment CreditCardAttributesFragment on CreditCardAttributes {
    name
    token
    oneTimeUse
  }
`;

export { CreditCardAttributesFragment, CREDIT_CARD_ATTRIBUTES_FRAGMENT };
