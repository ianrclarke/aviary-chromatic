import { CreditCardAttributesFragment } from "./CreditCardAttributesFragment";

const DEFAULT_OPTIONS = { ccType: "visa" };
export const createCreditCardAttributesFragment = (
  id: string,
  options: any = {}
): CreditCardAttributesFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    name: values.name || `${id}-name`,
    token: `${id}-token`,
    oneTimeUse: values.oneTimeUse,
  };
};
