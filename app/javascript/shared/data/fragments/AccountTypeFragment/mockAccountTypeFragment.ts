import { AccountTypeFragment } from "./AccountTypeFragment";

export const createAccountTypeFragment = (id: string): AccountTypeFragment => ({
  presentation: `${id}-presentation`,
  value: `${id}-value`,
});
