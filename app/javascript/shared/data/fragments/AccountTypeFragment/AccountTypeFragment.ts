import gql from "graphql-tag";

interface AccountTypeFragment {
  presentation: string;
  value: string;
}

const ACCOUNT_TYPE_FRAGMENT = gql`
  fragment AccountTypeFragment on AccountType {
    presentation
    value
  }
`;

export { AccountTypeFragment, ACCOUNT_TYPE_FRAGMENT };
