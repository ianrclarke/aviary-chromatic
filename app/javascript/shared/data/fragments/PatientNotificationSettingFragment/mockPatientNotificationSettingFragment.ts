import { defaultValues } from "@shared/mocks/defaultValues";

import { PatientNotificationSettingFragment } from "./PatientNotificationSettingFragment";

const createPatientNotificationSettingFragment = (
  id: string,
  options: Partial<PatientNotificationSettingFragment> = {}
): PatientNotificationSettingFragment => ({
  id,
  createdAt: options.createdAt || defaultValues.date,
  updatedAt: options.updatedAt || defaultValues.date,
  subscribed: options.subscribed || true,
});

export { createPatientNotificationSettingFragment };
