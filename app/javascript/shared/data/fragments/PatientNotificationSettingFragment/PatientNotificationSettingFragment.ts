import gql from "graphql-tag";

interface PatientNotificationSettingFragment {
  id: string;

  createdAt: string;
  updatedAt: string;
  subscribed: boolean;
}

const PATIENT_NOTIFICATION_SETTING_FRAGMENT = gql`
  fragment PatientNotificationSettingFragment on PatientNotificationSetting {
    id

    createdAt
    updatedAt
    subscribed
  }
`;

export { PatientNotificationSettingFragment, PATIENT_NOTIFICATION_SETTING_FRAGMENT };
