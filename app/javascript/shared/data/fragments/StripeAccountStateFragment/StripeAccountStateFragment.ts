import gql from "graphql-tag";

interface StripeAccountStateFragment {
  id: string;

  createdAt: string;
  updatedAt: string;

  chargesEnabled: boolean;
  legalEntityVerified: string;
  transfersEnabled: boolean;
  isFullSsnRequired: boolean;
}

const STRIPE_ACCOUNT_STATE_FRAGMENT = gql`
  fragment StripeAccountStateFragment on StripeAccountState {
    id

    createdAt
    updatedAt

    chargesEnabled
    legalEntityVerified
    transfersEnabled
    isFullSsnRequired
  }
`;

export { StripeAccountStateFragment, STRIPE_ACCOUNT_STATE_FRAGMENT };
