import { defaultValues } from "@shared/mocks/defaultValues";

import { StripeAccountStateFragment } from "./StripeAccountStateFragment";

const DEFAULT_OPTIONS = {
  chargesEnabled: true,
  legalEntityVerified: "legalEntityVerified",
  transfersEnabled: true,
  isFullSsnRequired: false,
};
export const createStripeAccountStateFragment = (
  id: string,
  options: any = {}
): StripeAccountStateFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    createdAt: defaultValues.date,
    updatedAt: defaultValues.date,
    chargesEnabled: values.chargesEnabled,
    legalEntityVerified: values.legalEntityVerified,
    transfersEnabled: values.transfersEnabled,
    isFullSsnRequired: values.isFullSsnRequired,
  };
};
