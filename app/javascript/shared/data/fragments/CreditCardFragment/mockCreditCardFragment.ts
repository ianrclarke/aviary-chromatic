import { defaultValues } from "@shared/mocks/defaultValues";

import { CreditCardFragment } from "./CreditCardFragment";

const DEFAULT_OPTIONS = { ccType: "visa" };
export const createCreditCardFragment = (id: string, options: any = {}): CreditCardFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    firstName: values.firstName || `${id}-firstName`,
    lastName: values.lastName || `${id}-lastName`,
    lastDigits: values.lastDigits || `${id}-lastDigits`,
    month: values.month || `${id}-month`,
    year: values.year || `${id}-year`,
    ccType: values.ccType,
    createdAt: defaultValues.date,
    updatedAt: defaultValues.date,
  };
};
