import gql from "graphql-tag";

interface CreditCardFragment {
  id: string;
  firstName: string;
  lastName: string;
  lastDigits: string;
  month: string;
  year: string;
  ccType: string;
  createdAt: string;
  updatedAt: string;
}

const CREDIT_CARD_FRAGMENT = gql`
  fragment CreditCardFragment on CreditCard {
    id
    firstName
    lastName
    lastDigits
    month
    year
    ccType
    createdAt
    updatedAt
  }
`;

export { CreditCardFragment, CREDIT_CARD_FRAGMENT };
