import { SourceFragment } from "./SourceFragment";

export const createSourceFragment = (id: number): SourceFragment => {
  return {
    id: `${id}-id`,
    ccType: `${id}-ccType`,
    createdAt: `${id}-createdAt`,
    updatedAt: `${id}-updatedAt`,
    firstName: `${id}-firstName`,
    lastDigits: `${id}-lastDigits`,
    lastName: `${id}-lastName`,
    month: `${id}-month`,
    year: `${id}-year`,
  };
};
