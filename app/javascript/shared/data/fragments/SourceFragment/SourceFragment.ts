import gql from "graphql-tag";

interface SourceFragment {
  id: string;
  ccType: string;
  createdAt: string;
  updatedAt: string;
  firstName: string;
  lastDigits: string;
  lastName: string;
  month: string;
  year: string;
}

const SOURCE_FRAGMENT = gql`
  fragment SourceFragment on Source {
    id
    ccType
    createdAt
    updatedAt
    firstName
    lastDigits
    lastName
    month
    year
  }
`;

export { SourceFragment, SOURCE_FRAGMENT };
