import { defaultValues } from "@shared/mocks/defaultValues";

import { WholesaleShipmentFragment } from "./WholesaleShipmentFragment";

const DEFAULT_OPTIONS = {
  taxTotal: 10,
  adjustmentTotal: 10,

  cost: 80,
  master: true,
  number: "testing",

  promoTotal: 22,
  state: "pending",
};

export const createWholesaleShipmentFragment = (
  id: string,
  options: any = {}
): WholesaleShipmentFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    taxTotal: values.taxTotal,
    adjustmentTotal: values.adjustmentTotal,

    cost: values.cost,
    createdAt: defaultValues.date,
    master: values.master,
    number: values.number,

    promoTotal: values.promoTotal,
    shippedAt: defaultValues.date,
    state: values.state,
    updatedAt: defaultValues.date,
  };
};
