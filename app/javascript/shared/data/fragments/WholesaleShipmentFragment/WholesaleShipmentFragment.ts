import gql from "graphql-tag";

interface WholesaleShipmentFragment {
  id: string;

  taxTotal: number;
  adjustmentTotal: number;

  cost: number;
  createdAt: string;
  master: boolean;
  number: string;

  promoTotal: number;
  shippedAt: string;
  state: "pending" | "canceled" | "shipped" | "partial" | "ready";
  updatedAt: string;
}

const WHOLESALE_SHIPMENT_FRAGMENT = gql`
  fragment WholesaleShipmentFragment on WholesaleShipment {
    id

    taxTotal
    adjustmentTotal

    cost
    createdAt
    master
    number

    promoTotal
    shippedAt
    state
    updatedAt
  }
`;

export { WholesaleShipmentFragment, WHOLESALE_SHIPMENT_FRAGMENT };
