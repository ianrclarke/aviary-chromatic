export { WHOLESALE_SHIPMENT_FRAGMENT } from "./WholesaleShipmentFragment";

export type { WholesaleShipmentFragment } from "./WholesaleShipmentFragment";
