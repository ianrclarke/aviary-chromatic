import { defaultValues } from "@shared/mocks/defaultValues";

import { AlertsBannerFragment } from "./AlertsBannerFragment";

const DEFAULT_OPTIONS = { urgency: "info" };

const createAlertsBannerFragment = (id: string, options: any = {}): AlertsBannerFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    message: `${id}-message`,
    endDate: defaultValues.date,
    startDate: defaultValues.date,
    title: `${id}-title`,
    urgency: values.urgency,
  };
};

export { createAlertsBannerFragment };
