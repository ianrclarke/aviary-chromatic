import gql from "graphql-tag";

interface AlertsBannerFragment {
  id: string;
  message: string;
  endDate: string;
  startDate: string;
  title: string;
  urgency: string;
}

const ALERTS_BANNER_FRAGMENT = gql`
  fragment AlertsBannerFragment on AlertsBanner {
    id
    message
    endDate
    startDate
    title
    urgency
  }
`;

export { AlertsBannerFragment, ALERTS_BANNER_FRAGMENT };
