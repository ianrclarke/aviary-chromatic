import { defaultValues } from "@shared/mocks/defaultValues";

import { TopBrandFragment } from "./TopBrandFragment";

const DEFAULT_OPTIONS = { position: 1 };
export const createTopBrandFragment = (id: string, options: any = {}): TopBrandFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    blurb: `${id}-blurb`,
    createdAt: defaultValues.date,
    position: values.position,
    updatedAt: defaultValues.date,
  };
};
