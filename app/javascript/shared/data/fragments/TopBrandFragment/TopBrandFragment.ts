import gql from "graphql-tag";

interface TopBrandFragment {
  id: string;

  blurb: string;
  createdAt: string;
  position: number;
  updatedAt: string;
}

const TOP_BRAND_FRAGMENT = gql`
  fragment TopBrandFragment on TopBrand {
    id

    blurb
    createdAt
    position
    updatedAt
  }
`;

export { TopBrandFragment, TOP_BRAND_FRAGMENT };
