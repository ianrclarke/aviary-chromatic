import { PatientPromotionFragment } from "@shared/data/fragments";
import { defaultValues } from "@shared/mocks/defaultValues";

const createPatientPromotionFragment = (
  id: string,
  options: Partial<PatientPromotionFragment> = {}
): PatientPromotionFragment => {
  const values = { ...options };
  return {
    id,
    targetedDiscount: values.targetedDiscount || 10,
    durationDays: values.durationDays || 5,
    name: values.name || `${id}-name`,
    description: values.description || `${id}-description`,
    startDate: values.startDate || defaultValues.date,
    endDate: values.endDate || defaultValues.date,
  };
};

export { createPatientPromotionFragment };
