import gql from "graphql-tag";

const PATIENT_PROMOTION_FRAGMENT = gql`
  fragment PatientPromotionFragment on PatientPromotion {
    id

    targetedDiscount
    durationDays
    name
    description
    startDate
    endDate
  }
`;

interface PatientPromotionFragment {
  id: string;
  targetedDiscount: number;
  durationDays: number;
  name: string;
  description: string;
  startDate: string;
  endDate: string;
}

export { PatientPromotionFragment, PATIENT_PROMOTION_FRAGMENT };
