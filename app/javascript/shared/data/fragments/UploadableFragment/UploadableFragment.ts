import gql from "graphql-tag";

interface UploadableFragment {
  id: string;
  fileName: string;
  attachment: string;
  type: string;
}

const UPLOADABLE_FRAGMENT = gql`
  fragment UploadableFragment on Uploadable {
    id
    fileName
    attachment
    type
  }
`;

export { UploadableFragment, UPLOADABLE_FRAGMENT };
