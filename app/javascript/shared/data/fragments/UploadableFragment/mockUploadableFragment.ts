import { UploadableFragment } from "./UploadableFragment";

const DEFAULT_OPTIONS = {
  attachment: "image.png",
  type: "DocumentAttachment",
};

export const createUploadableFragment = (id: string, options: any = {}): UploadableFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    fileName: `${id}-name`,
    attachment: values.attachment,
    type: values.type,
  };
};
