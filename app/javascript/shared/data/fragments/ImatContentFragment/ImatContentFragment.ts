import gql from "graphql-tag";

interface ImatContentFragment {
  id: string;
  createdAt: string;
  name: string;
  contentUrl: string;
  isAttached: boolean;
}

const IMAT_CONTENT_FRAGMENT = gql`
  fragment ImatContentFragment on ImatContent {
    id
    createdAt
    name
    contentUrl
    isAttached(treatmentPlanId: $treatmentPlanId)
  }
`;

export { ImatContentFragment, IMAT_CONTENT_FRAGMENT };
