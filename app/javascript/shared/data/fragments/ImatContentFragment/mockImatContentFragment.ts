import { ImatContentFragment } from "./ImatContentFragment";

const DEFAULT_OPTIONS = {
  createdAt: "2020-02-06T20:08:46Z",
  contentUrl: "image.png",
  isAttached: false,
};

export const createImatContentFragment = (id: string, options: any = {}): ImatContentFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    createdAt: values.createdAt,
    name: `${id}-name`,
    contentUrl: values.contentUrl,
    isAttached: values.isAttached,
  };
};
