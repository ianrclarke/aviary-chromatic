import gql from "graphql-tag";

interface VariantFragment {
  id: string;

  availabilityStatus: string;
  countOnHand: number;
  createdAt: string;
  descriptor: string;
  isAvailableForPurchase: boolean;
  isAvailableInCatalog: boolean;
  isBackorderNotifiable: boolean;
  isBackorderNotificationSubscribed: boolean;
  isMaster: boolean;
  image: string;
  msrp: number;
  name: string;
  retail: boolean;
  sellPrice: number;
  sku: string;
  upc: string;
  updatedAt: string;
  validForSale: boolean;
  wholesale: boolean;
  wholesalePrice: number;
}

const VARIANT_FRAGMENT = gql`
  fragment VariantFragment on Variant {
    id

    availabilityStatus
    countOnHand
    createdAt
    descriptor
    isAvailableForPurchase
    isAvailableInCatalog
    isBackorderNotifiable
    isBackorderNotificationSubscribed
    image
    isMaster
    msrp
    name
    retail
    sellPrice
    sku
    upc
    updatedAt
    validForSale
    wholesale
    wholesalePrice
  }
`;

export { VariantFragment, VARIANT_FRAGMENT };
