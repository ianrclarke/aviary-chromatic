import { defaultValues } from "@shared/mocks/defaultValues";

import { VariantFragment } from "./VariantFragment";

const DEFAULT_OPTIONS = {
  isAvailableForPurchase: true,
  isAvailableInCatalog: true,
  isBackorderNotifiable: false,
  isBackorderNotificationSubscribed: false,
  isMaster: true,
  msrp: 43,
  retail: true,
  sellPrice: 22,
  validForSale: true,
  countOnHand: 10,
  wholesale: true,
  wholesalePrice: 12.34,
};
export const createVariantFragment = (id: string, options: any = {}): VariantFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    availabilityStatus: values.availabilityStatus || `${id}-availabilityStatus`,
    countOnHand: values.countOnHand,
    createdAt: defaultValues.date,
    descriptor: `${id}-descriptor`,
    image: `${id}-image`,
    isAvailableForPurchase: values.isAvailableForPurchase,
    isAvailableInCatalog: values.isAvailableInCatalog,
    isBackorderNotifiable: values.isBackorderNotifiable,
    isBackorderNotificationSubscribed: values.isBackorderNotificationSubscribed,
    isMaster: values.isMaster,
    msrp: values.msrp,
    name: `${id}-name`,
    retail: values.retail,
    sellPrice: values.sellPrice,
    sku: `${id}-sku`,
    upc: `${id}-upc`,
    updatedAt: defaultValues.date,
    validForSale: values.validForSale,
    wholesale: values.wholesale,
    wholesalePrice: values.wholesalePrice,
  };
};
