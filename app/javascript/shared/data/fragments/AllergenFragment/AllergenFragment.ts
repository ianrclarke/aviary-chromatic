import gql from "graphql-tag";

interface AllergenFragment {
  id: string;
  name: string;
}

const ALLERGEN_FRAGMENT = gql`
  fragment AllergenFragment on Allergen {
    id
    name
  }
`;

export { AllergenFragment, ALLERGEN_FRAGMENT };
