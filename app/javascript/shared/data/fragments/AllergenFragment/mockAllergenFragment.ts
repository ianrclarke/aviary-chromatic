import { AllergenFragment } from "./AllergenFragment";

const createAllergenFragment = (id: string): AllergenFragment => {
  return {
    id,
    name: `${id}-name`,
  };
};

export { createAllergenFragment };
