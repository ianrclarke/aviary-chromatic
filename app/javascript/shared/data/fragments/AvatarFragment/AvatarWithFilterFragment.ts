import gql from "graphql-tag";

interface AvatarWithFilterFragment {
  id: string;
  photoUrl: string;
}

const AVATAR_FRAGMENT_WITH_FILTER = gql`
  fragment AvatarWithFilterFragment on Avatar {
    id
    photoUrl(filter: $filter)
  }
`;

export { AvatarWithFilterFragment, AVATAR_FRAGMENT_WITH_FILTER };
