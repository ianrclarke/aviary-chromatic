import gql from "graphql-tag";

interface AvatarFragment {
  id: string;
  photoUrl: string;
}

const AVATAR_FRAGMENT = gql`
  fragment AvatarFragment on Avatar {
    id
    photoUrl(filter: SMALL_SIZE)
  }
`;

export { AvatarFragment, AVATAR_FRAGMENT };
