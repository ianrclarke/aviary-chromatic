import { AvatarFragment } from "./AvatarFragment";

export const createAvatarFragment = (id: string): AvatarFragment => ({
  id,
  photoUrl: "string",
});
