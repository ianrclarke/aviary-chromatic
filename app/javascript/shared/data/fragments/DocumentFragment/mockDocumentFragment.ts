import { DocumentFragment } from "./DocumentFragment";

const DEFAULT_OPTIONS = {
  documentLibrary: false,
  createdAt: "2020-02-06T20:08:46Z",
  isAttached: false,
};
export const createDocumentFragment = (id: string, options: any = {}): DocumentFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    createdAt: values.createdAt,
    attachment: `${id}-attachment`,
    documentLibrary: values.documentLibrary,
    attachmentFileName: `${id}-title`,
    isAttached: values.isAttached,
  };
};
