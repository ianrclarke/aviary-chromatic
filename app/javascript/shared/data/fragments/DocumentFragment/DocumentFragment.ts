import gql from "graphql-tag";

interface DocumentFragment {
  id: string;
  createdAt: string;
  attachment: string;
  documentLibrary: boolean;
  attachmentFileName: string;
  isAttached: boolean;
}

const DOCUMENT_FRAGMENT = gql`
  fragment DocumentFragment on Document {
    id
    createdAt
    attachment
    documentLibrary
    attachmentFileName
    isAttached(treatmentPlanId: $treatmentPlanId)
  }
`;

export { DocumentFragment, DOCUMENT_FRAGMENT };
