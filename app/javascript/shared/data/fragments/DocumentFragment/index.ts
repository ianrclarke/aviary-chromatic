export { DOCUMENT_FRAGMENT } from "./DocumentFragment";

export type { DocumentFragment } from "./DocumentFragment";
