import { defaultValues } from "@shared/mocks/defaultValues";

import { VariantNotificationFragment } from "./VariantNotificationFragment";

const createVariantNotificationFragment = (
  id: string,
  options: Partial<VariantNotificationFragment> = {}
): VariantNotificationFragment => ({
  id,
  createdAt: options.createdAt || defaultValues.date,
  updatedAt: options.updatedAt || defaultValues.date,
  currentState: options.currentState || `${id}-currentState`,
  notificationSentAt: options.notificationSentAt || defaultValues.date,
});

export { createVariantNotificationFragment };
