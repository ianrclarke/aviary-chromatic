import gql from "graphql-tag";

const VARIANT_NOTIFICATION_FRAGMENT = gql`
  fragment VariantNotificationFragment on VariantNotification {
    id
    createdAt
    updatedAt

    currentState
    notificationSentAt
  }
`;

interface VariantNotificationFragment {
  id: string;
  createdAt: string;
  updatedAt: string;
  currentState: string;
  notificationSentAt: string;
}

export { VARIANT_NOTIFICATION_FRAGMENT, VariantNotificationFragment };
