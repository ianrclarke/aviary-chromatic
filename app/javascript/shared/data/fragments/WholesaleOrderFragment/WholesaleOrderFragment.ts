import gql from "graphql-tag";

interface WholesaleOrderFragment {
  id: string;

  taxTotal: number;
  savingsTotal: number; // The total value of adjustments excluding shipping adjustments

  completedAt: string;
  createdAt: string;
  itemTotal: number;

  number: string;
  paymentState: string;
  paymentTotal: number;
  shipmentState: string;
  shippingTotal: number; // The total value of all shipments’ costs including its respective shipping adjustments

  orderReceiptUrl: string;

  state: string;
  total: number;
  updatedAt: string;
}

const WHOLESALE_ORDER_FRAGMENT = gql`
  fragment WholesaleOrderFragment on WholesaleOrder {
    id

    taxTotal
    savingsTotal

    completedAt
    createdAt
    itemTotal

    number
    paymentState
    paymentTotal
    shipmentState
    shippingTotal

    orderReceiptUrl

    state
    total
    updatedAt
  }
`;

export { WholesaleOrderFragment, WHOLESALE_ORDER_FRAGMENT };
