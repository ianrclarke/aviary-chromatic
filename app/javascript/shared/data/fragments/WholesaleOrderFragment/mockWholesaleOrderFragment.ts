import { defaultValues } from "@shared/mocks/defaultValues";

import { WholesaleOrderFragment } from "./WholesaleOrderFragment";

const DEFAULT_OPTIONS = {
  taxTotal: 10,
  savingsTotal: 5,

  itemTotal: 100,

  number: "test slug",
  paymentState: "paid",
  paymentTotal: 120,
  shipmentState: "packing",
  shippingTotal: 15,

  orderReceiptUrl: "testing-url",

  state: "california",
  total: 135,
  updatedAt: defaultValues.date,
};
export const createWholesaleOrderFragment = (
  id: string,
  options: any = {}
): WholesaleOrderFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,

    taxTotal: values.taxTotal,
    savingsTotal: values.savingsTotal,

    completedAt: defaultValues.date,
    createdAt: defaultValues.date,
    itemTotal: values.itemTotal,

    number: values.number,
    paymentState: values.paymentState,
    paymentTotal: values.paymentTotal,

    shipmentState: values.shipmentState,
    shippingTotal: values.shippingTotal,

    orderReceiptUrl: values.orderReceiptUrl,

    state: values.state,
    total: values.total,
    updatedAt: defaultValues.date,
  };
};
