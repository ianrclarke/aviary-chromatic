import gql from "graphql-tag";

interface FeaturedTagGroupFragment {
  id: string;
  name: string;
  description: string;
  startDate: string;
  endDate: string;
}

const FEATURED_TAG_GROUP_FRAGMENT = gql`
  fragment FeaturedTagGroupFragment on FeaturedTagGroup {
    id
    name
    description
    startDate
    endDate
  }
`;

export { FeaturedTagGroupFragment, FEATURED_TAG_GROUP_FRAGMENT };
