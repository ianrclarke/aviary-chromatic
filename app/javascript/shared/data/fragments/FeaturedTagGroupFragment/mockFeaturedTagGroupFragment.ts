import { defaultValues } from "@shared/mocks/defaultValues";

import { FeaturedTagGroupFragment } from "./FeaturedTagGroupFragment";

export const createFeaturedTagGroupFragment = (
  id: string,
  options?: any
): FeaturedTagGroupFragment => {
  return {
    id,
    name: `${id}-nmae`,
    description: `${id}-description`,
    startDate: defaultValues.date,
    endDate: defaultValues.date,
    ...options,
  };
};
