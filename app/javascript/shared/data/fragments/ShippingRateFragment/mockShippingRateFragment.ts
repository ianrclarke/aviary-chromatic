import { defaultValues } from "@shared/mocks/defaultValues";

import { ShippingRateFragment } from "./ShippingRateFragment";

const DEFAULT_OPTIONS = {
  cost: 10.99,
  description: "4-7 business days",
  estimatedShippingDates: "2020-07-17",
  isShippingFree: false,
};

export const createShippingRateFragment = (id: string, options: any = {}): ShippingRateFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    cost: values.cost,
    isShippingFree: values.isShippingFree,
    createdAt: defaultValues.date,
    description: values.description,
    estimatedShippingDates: [values.estimatedShippingDates],
    selected: values.selected ? values.selected : false,
    shippingMethod: values.shippingMethod ? values.shippingMethod : `${id}-shippingMethod`,
    updatedAt: defaultValues.date,
  };
};
