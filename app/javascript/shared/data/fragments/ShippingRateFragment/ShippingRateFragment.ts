import gql from "graphql-tag";

interface ShippingRateFragment {
  id: string;

  cost: number;
  createdAt: string;
  description: string;

  estimatedShippingDates: string[];
  selected: boolean;
  shippingMethod: string;
  isShippingFree: boolean;

  updatedAt: string;
}

const SHIPPING_RATE_FRAGMENT = gql`
  fragment ShippingRateFragment on ShippingRate {
    id

    cost
    createdAt
    description
    estimatedShippingDates
    selected
    shippingMethod
    isShippingFree

    updatedAt
  }
`;

export { ShippingRateFragment, SHIPPING_RATE_FRAGMENT };
