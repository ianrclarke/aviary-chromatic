import gql from "graphql-tag";

interface CategoryFragment {
  id: string;
  categoryType: string;
  name: string;
}

const CATEGORY_FRAGMENT = gql`
  fragment CategoryFragment on Category {
    id
    categoryType
    name
  }
`;

export { CategoryFragment, CATEGORY_FRAGMENT };
