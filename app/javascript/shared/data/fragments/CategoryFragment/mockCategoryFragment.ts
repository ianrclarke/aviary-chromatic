import { CategoryFragment } from "./CategoryFragment";

export const createCategoryFragment = (id: string): CategoryFragment => {
  return {
    id,
    categoryType: `${id}-categoryType`,
    name: `${id}-name`,
  };
};
