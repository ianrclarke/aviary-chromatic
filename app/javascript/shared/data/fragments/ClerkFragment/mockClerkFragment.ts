import { defaultValues } from "@shared/mocks/defaultValues";

import { ClerkFragment } from "./ClerkFragment";

export const createClerkFragment = (id: string): ClerkFragment => {
  return {
    id,
    createdAt: defaultValues.date,
    fullName: `${id}-fullName`,
    firstName: `${id}-firstName`,
    lastName: `${id}-lastName`,
    email: `${id}-email`,
    storeId: `${id}-storeId`,
    storeName: `${id}-storeName`,
    updatedAt: defaultValues.date,
  };
};
