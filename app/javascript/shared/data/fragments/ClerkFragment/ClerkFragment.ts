import gql from "graphql-tag";

interface ClerkFragment {
  id: string;
  createdAt: string;
  fullName: string;
  email: string;
  storeId: string;
  storeName: string;
  updatedAt: string;
  firstName: string;
  lastName: string;
}

const CLERK_FRAGMENT = gql`
  fragment ClerkFragment on Clerk {
    id
    createdAt
    fullName
    firstName
    lastName
    email
    storeId
    storeName
    updatedAt
  }
`;

export { ClerkFragment, CLERK_FRAGMENT };
