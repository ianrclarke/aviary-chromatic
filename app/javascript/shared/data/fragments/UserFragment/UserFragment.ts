import gql from "graphql-tag";

interface UserFragment {
  id: string;
  createdAt: string;
  email: string;
  updatedAt: string;
}

const USER_FRAGMENT = gql`
  fragment UserFragment on User {
    id
    createdAt
    email
    updatedAt
  }
`;

export { UserFragment, USER_FRAGMENT };
