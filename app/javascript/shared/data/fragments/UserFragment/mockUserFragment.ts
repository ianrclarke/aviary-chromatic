import { defaultValues } from "@shared/mocks/defaultValues";

import { UserFragment } from "./UserFragment";

export const createUserFragment = (
  id: string,
  options: Partial<UserFragment> = {}
): UserFragment => {
  return {
    id,
    createdAt: options.createdAt || defaultValues.date,
    email: options.email || `${id}-email`,
    updatedAt: options.updatedAt || defaultValues.date,
  };
};
