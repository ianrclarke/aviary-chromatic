import gql from "graphql-tag";

interface BrandFragment {
  id: string;

  createdAt: string;
  name: string;
  updatedAt: string;
  logo: string;
  prescriptionOnly: boolean;
}

const BRAND_FRAGMENT = gql`
  fragment BrandFragment on Brand {
    id

    createdAt
    logo
    name
    updatedAt
    prescriptionOnly
  }
`;

export { BrandFragment, BRAND_FRAGMENT };
