import { defaultValues } from "@shared/mocks/defaultValues";

import { BrandFragment } from "./BrandFragment";

const DEFAULT_OPTIONS = { prescriptionOnly: false };
export const createBrandFragment = (id: string, options: any = {}): BrandFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,

    createdAt: defaultValues.date,
    name: `${id}-name`,
    logo: `${name}-logo`,
    updatedAt: defaultValues.date,
    prescriptionOnly: values.prescriptionOnly,
  };
};
