export { PRODUCT_SEARCH_FRAGMENT } from "./ProductSearchFragment";

export type { ProductSearchFragment } from "./ProductSearchFragment";
