import { defaultValues } from "@shared/mocks/defaultValues";

import { ProductSearchFragment } from "./ProductSearchFragment";

export const createProductSearchFragment = (id: string): ProductSearchFragment => {
  return {
    id,
    createdAt: defaultValues.date,
    updatedAt: defaultValues.date,
    aggregations: `${id}-aggregations`,
  };
};
