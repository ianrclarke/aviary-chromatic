import gql from "graphql-tag";

interface ProductSearchFragment {
  id: string;

  createdAt: string;
  updatedAt: string;
  aggregations: string;
}

const PRODUCT_SEARCH_FRAGMENT = gql`
  fragment ProductSearchFragment on ProductSearch {
    id

    createdAt
    updatedAt
    aggregations
  }
`;

export { ProductSearchFragment, PRODUCT_SEARCH_FRAGMENT };
