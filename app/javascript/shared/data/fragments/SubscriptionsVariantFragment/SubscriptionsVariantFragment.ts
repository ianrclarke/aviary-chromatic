import gql from "graphql-tag";

interface SubscriptionsVariantFragment {
  id: string;

  active: boolean;
  createdAt: string;
  quantity: number;
  updatedAt: string;
}

const SUBSCRIPTIONS_VARIANT_FRAGMENT = gql`
  fragment SubscriptionsVariantFragment on SubscriptionsVariant {
    id

    active
    createdAt
    quantity
    updatedAt
  }
`;

export { SubscriptionsVariantFragment, SUBSCRIPTIONS_VARIANT_FRAGMENT };
