import { defaultValues } from "@shared/mocks/defaultValues";

import { SubscriptionsVariantFragment } from "./SubscriptionsVariantFragment";

const DEFAULT_OPTIONS = {
  active: true,
  quantity: 1,
};
export const createSubscriptionsVariantFragment = (
  id: string,
  options: any = {}
): SubscriptionsVariantFragment => {
  const values = { ...DEFAULT_OPTIONS, ...options };
  return {
    id,
    active: values.active,
    createdAt: defaultValues.date,
    quantity: values.quantity,
    updatedAt: defaultValues.date,
  };
};
