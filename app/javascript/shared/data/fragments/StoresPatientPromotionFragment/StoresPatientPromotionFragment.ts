import gql from "graphql-tag";

const STORES_PATIENT_PROMOTION_FRAGMENT = gql`
  fragment StoresPatientPromotionFragment on StoresPatientPromotion {
    id

    targetedDiscount
    durationDays
    endDate
    previousDiscount
    optIn
    fromGlobalOptIn
  }
`;

interface StoresPatientPromotionFragment {
  id: string;
  targetedDiscount: number;
  durationDays: number;
  endDate: string;
  previousDiscount: number;
  optIn: boolean;
  fromGlobalOptIn: boolean;
}

export { StoresPatientPromotionFragment, STORES_PATIENT_PROMOTION_FRAGMENT };
