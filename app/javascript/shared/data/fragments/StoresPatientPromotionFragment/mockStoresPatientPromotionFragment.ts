import { StoresPatientPromotionFragment } from "@shared/data/fragments";

const createStoresPatientPromotionFragment = (
  id: string,
  options: Partial<StoresPatientPromotionFragment> = {}
): StoresPatientPromotionFragment => {
  const values = { ...options };
  return {
    id,
    targetedDiscount: values.targetedDiscount || 10,
    durationDays: values.durationDays || 5,
    endDate: values.endDate || null,
    previousDiscount: values.previousDiscount || 5,
    optIn: values.optIn || false,
    fromGlobalOptIn: values.fromGlobalOptIn || false,
  };
};

export { createStoresPatientPromotionFragment };
