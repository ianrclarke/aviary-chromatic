import { QueryResult } from "@apollo/react-common";
import { QueryHookOptions, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { StateFragment, STATE_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";

const name = queryName("States");
const GET_STATES = gql`
  query ${name} {
    viewer {
      states {
        ...StateFragment
      }
    }
  }
  ${STATE_FRAGMENT}
`;

interface Data {
  viewer: {
    states: StateFragment[];
  };
}

const useStatesQuery = (options?: QueryHookOptions<Data, {}>): QueryResult<Data, {}> => {
  return useQuery<Data, {}>(GET_STATES, options);
};

export { GET_STATES, useStatesQuery };
