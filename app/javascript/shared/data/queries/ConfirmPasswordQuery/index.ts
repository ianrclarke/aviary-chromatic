export {
  CONFIRM_PASSWORD_QUERY,
  useConfirmPasswordQuery,
  useConfirmPasswordLazyQuery,
} from "./ConfirmPasswordQuery";
