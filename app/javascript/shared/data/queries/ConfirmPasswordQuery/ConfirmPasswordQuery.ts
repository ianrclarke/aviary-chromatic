import { QueryHookOptions, useLazyQuery, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { UserFragment, USER_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";

const name = queryName("ConfirmPassword");
const CONFIRM_PASSWORD_QUERY = gql`
  query ${name}($password: String!) {
    viewer {
      user {
        ...UserFragment
        confirmPassword(password: $password)
      }
    }
  }
  ${USER_FRAGMENT}
`;

interface UserWithConfirmPassword extends UserFragment {
  confirmPassword: boolean;
}

interface Data {
  viewer: {
    user: UserWithConfirmPassword;
  };
}

interface Variables {
  password: string;
}

const useConfirmPasswordQuery = (options?: QueryHookOptions<Data, Variables>) =>
  useQuery<Data, Variables>(CONFIRM_PASSWORD_QUERY, options);

const useConfirmPasswordLazyQuery = (options?: QueryHookOptions<Data, Variables>) =>
  useLazyQuery<Data, Variables>(CONFIRM_PASSWORD_QUERY, options);

export { useConfirmPasswordQuery, useConfirmPasswordLazyQuery, CONFIRM_PASSWORD_QUERY };
