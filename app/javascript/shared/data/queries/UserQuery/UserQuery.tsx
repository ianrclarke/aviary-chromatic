import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { queryName } from "@shared/data/graphqlNames";
import { UserShard, USER_SHARD } from "@shared/data/shards";

const name = queryName("User");
const USER_QUERY = gql`
  query ${name} {
    viewer {
      user {
        ...UserShard
      }
    }
  }
  ${USER_SHARD}
`;

interface UserQueryData {
  viewer: {
    user: UserShard;
  };
}

const useUserQuery = () => useQuery<UserQueryData>(USER_QUERY);

export { useUserQuery, USER_QUERY };
