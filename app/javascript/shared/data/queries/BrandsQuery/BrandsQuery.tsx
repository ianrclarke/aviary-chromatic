import { QueryHookOptions, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { PageInfoFragment, PAGE_INFO_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";
import { BrandShard, BRAND_SHARD } from "@shared/data/shards";

const name = queryName("Brands");
const BRANDS_QUERY = gql`
  query ${name} ($filters: BrandFilterObject, $first: Int, $last: Int, $after: String, $before: String) {
    viewer {
      brands(filters: $filters, first: $first, last: $last, after: $after, before: $before) {
        nodes {
          ...BrandShard
        }
        pageInfo {
          ...PageInfoFragment
        }
        totalCount
      }
    }
  }
  ${BRAND_SHARD}
  ${PAGE_INFO_FRAGMENT}
`;

interface Data {
  viewer: {
    brands: {
      nodes: BrandShard[];
      pageInfo: PageInfoFragment;
      totalCount: number;
    };
  };
}

type Lists = "TOP_BRANDS" | "WITH_NEW_PRODUCTS";

interface Variables {
  filters?: {
    list?: Lists;
    active?: boolean;
  };
  first?: number;
  last?: number;
  after?: string;
  before?: string;
}

const defaultVariables: Variables = {
  first: 24,
};

const useBrandsQuery = (options?: QueryHookOptions<Data, Variables>) => {
  const { variables } = { variables: {}, ...options };
  return useQuery<Data, Variables>(BRANDS_QUERY, {
    ...options,
    variables: { ...defaultVariables, ...variables },
  });
};

export { useBrandsQuery, BRANDS_QUERY, Variables };
