export { useBrandsQuery, BRANDS_QUERY } from "./BrandsQuery";

export type { Variables } from "./BrandsQuery";
