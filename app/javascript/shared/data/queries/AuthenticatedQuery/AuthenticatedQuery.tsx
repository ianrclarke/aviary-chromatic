import { useQuery, QueryHookOptions } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { queryName } from "@shared/data/graphqlNames";

const name = queryName("Authenticated");
const AUTHENTICATED_QUERY = gql`
  query ${name} {
    viewer {
      user {
        id
      }
    }
  }
`;

interface AuthenticatedQueryData {
  viewer: {
    user: { id: string };
  };
}

const useAuthenticatedQuery = (options?: QueryHookOptions<AuthenticatedQueryData>) =>
  useQuery<AuthenticatedQueryData>(AUTHENTICATED_QUERY, options);

export { useAuthenticatedQuery, AUTHENTICATED_QUERY };
