import { OperationVariables } from "@apollo/react-common";
import { QueryComponentOptions } from "@apollo/react-components";

type QueryProps<TData, TVariables = OperationVariables> = Omit<
  QueryComponentOptions<TData, TVariables>,
  "query" | "variables"
>;

export { QueryProps };
