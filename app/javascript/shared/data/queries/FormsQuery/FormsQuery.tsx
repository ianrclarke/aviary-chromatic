import { QueryHookOptions, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { DosageFormatFragment, DOSAGE_FORMAT_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";

const name = queryName("getForms");
const FORMS_QUERY = gql`
    query ${name} ($sort: [DosageFormatSort!]) {
        viewer {
            forms(sort: $sort) {
                ...DosageFormatFragment
            }
        }
    }
    ${DOSAGE_FORMAT_FRAGMENT}
`;

interface Data {
  viewer: {
    forms: DosageFormatFragment[];
  };
}

type DIRECTION = "ASC" | "DESC";

interface Variables {
  sort: {
    direction: DIRECTION;
    field: "SUM_PRODUCTS_COUNT";
  };
}

const defaultVariables: Variables = {
  sort: {
    direction: "DESC",
    field: "SUM_PRODUCTS_COUNT",
  },
};

const useFormsQuery = (options?: QueryHookOptions<Data, Variables>) => {
  const { variables } = { variables: {}, ...options };
  return useQuery<Data, Variables>(FORMS_QUERY, {
    ...options,
    variables: { ...defaultVariables, ...variables },
  });
};

export { useFormsQuery, FORMS_QUERY };
