export { useAlertsBannerQuery, GET_ALERTS_BANNER } from "./AlertsBannerQuery";
export { useAuthenticatedQuery, AUTHENTICATED_QUERY } from "./AuthenticatedQuery";
export { useBrandsQuery, BRANDS_QUERY } from "./BrandsQuery";
export { useCategoryQuery, GET_CATEGORY } from "./CategoryQuery";
export { useOldBrandsQuery, GET_OLD_BRANDS } from "./OldBrandsQuery";
export {
  CONFIRM_PASSWORD_QUERY,
  useConfirmPasswordQuery,
  useConfirmPasswordLazyQuery,
} from "./ConfirmPasswordQuery";
export { usePractitionerTypesQuery, GET_PRACTITIONER_TYPES } from "./PractitionerTypesQuery";
export { QueryProps } from "./QueryProps";
export { useUserQuery, USER_QUERY } from "./UserQuery";
export { useUserToursQuery, GET_USER_TOURS } from "./UserToursQuery";
export { useStatesQuery, GET_STATES } from "./StatesQuery";
export { useStoreQuery, GET_STORE } from "./StoreQuery";
export {
  useStoreWithGlobalAndPatientPromotionsQuery,
  STORE_WITH_GLOBAL_AND_PATIENT_PROMOTIONS,
} from "./StoreWithGlobalAndPatientPromotionsQuery";

export { usePatientPromotionsQuery, PATIENT_PROMOTIONS } from "./PatientPromotionsQuery";
export { useUserAvatarQuery, USER_AVATAR_QUERY } from "./UserAvatarQuery";
export type { UserAvatarQueryData } from "./UserAvatarQuery";

export { useTreatmentPlanQuery, TREATMENT_PLAN_QUERY } from "./TreatmentPlanQuery";
export { useSimilarProductsQuery, SIMILAR_PRODUCTS_QUERY } from "./SimilarProductsQuery";
export { usePopularProductsQuery, GET_POPULAR_PRODUCTS } from "./PopularProductsQuery";
export { useFormsQuery, FORMS_QUERY } from "./FormsQuery";
