import { QueryHookOptions, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { QuickSearchValueFragment, QUICK_SEARCH_VALUE_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";
import { BrandShard, BRAND_SHARD } from "@shared/data/shards";

const name = queryName("oldAllBrands");
const GET_OLD_BRANDS = gql`
  query ${name} {
    viewer {
      quickSearch {
        brands(filters: {active: true}) {
          ...QuickSearchValueFragment
        }
      }
      topBrands: brands(filters: {list: TOP_BRANDS, active: true}, first: 24) {
        nodes {
          ...BrandShard
        }
      }
    }
  }
  ${QUICK_SEARCH_VALUE_FRAGMENT}
  ${BRAND_SHARD}
`;

interface Data {
  viewer: {
    quickSearch: {
      brands: QuickSearchValueFragment[];
    };
    topBrands: {
      nodes: BrandShard[];
    };
  };
}

const useOldBrandsQuery = (options?: QueryHookOptions<Data>) =>
  useQuery<Data>(GET_OLD_BRANDS, options);

export { useOldBrandsQuery, GET_OLD_BRANDS };
