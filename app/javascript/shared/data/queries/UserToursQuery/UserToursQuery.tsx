import { QueryHookOptions, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { TourFragment, TOUR_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";

const name = queryName("UserTours");
const GET_USER_TOURS = gql`
  query ${name}($controller: String, $action: String) {
    viewer {
      user {
        id
        tours(controller: $controller, action: $action) {
          ...TourFragment
        }
      }
    }
  }
  ${TOUR_FRAGMENT}
`;

interface Data {
  viewer: {
    user: {
      id: string;
      tours: TourFragment[];
    };
  };
}

interface Variables {
  controller: string;
  action: string;
}

const useUserToursQuery = (options: QueryHookOptions<Data, Variables>) => {
  return useQuery<Data, Variables>(GET_USER_TOURS, options);
};

export { useUserToursQuery, GET_USER_TOURS };
