import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { PageInfoFragment, PAGE_INFO_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";
import { ProductShard, PRODUCT_SHARD } from "@shared/data/shards";

const name = queryName("PopularProducts");
const GET_POPULAR_PRODUCTS = gql`
  query ${name}($after: String) {
    viewer {
      products(first: 4, after: $after, filters: { list: POPULAR_BY_WEEK }) {
        nodes {
          ...ProductShard
        }
        pageInfo {
          ...PageInfoFragment
        }
      }
    }
  }
  ${PRODUCT_SHARD}
  ${PAGE_INFO_FRAGMENT}
`;

interface PopularProductsData {
  viewer: {
    products: {
      nodes: ProductShard[];
      pageInfo: PageInfoFragment;
    };
  };
  after?: string;
}

const usePopularProductsQuery = () => useQuery<PopularProductsData>(GET_POPULAR_PRODUCTS);

export { usePopularProductsQuery, GET_POPULAR_PRODUCTS };
