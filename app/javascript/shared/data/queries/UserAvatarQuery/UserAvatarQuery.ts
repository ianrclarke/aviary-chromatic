import { QueryHookOptions, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { queryName } from "@shared/data/graphqlNames";
import { UserWithAvatarShard, USER_WITH_AVATAR_SHARD } from "@shared/data/shards";
import { AvatarSize } from "@shared/types/avatarSize.d";

const name = queryName("UserAvatar");
const USER_AVATAR_QUERY = gql`
  query ${name}($filter: PhotoSizeFilters!) {
    viewer {
      user {
        ...UserWithAvatarShard
      }
    }
  }

  ${USER_WITH_AVATAR_SHARD}
`;

interface UserAvatarQueryData {
  viewer: {
    user: UserWithAvatarShard;
  };
}

interface Variables {
  filter?: AvatarSize;
}

const useUserAvatarQuery = (
  options: QueryHookOptions<UserAvatarQueryData, Variables> = {
    variables: {
      filter: AvatarSize.SMALL,
    },
  }
) => useQuery<UserAvatarQueryData>(USER_AVATAR_QUERY, options);

export { useUserAvatarQuery, USER_AVATAR_QUERY, UserAvatarQueryData };
