export { useUserAvatarQuery, USER_AVATAR_QUERY } from "./UserAvatarQuery";

export type { UserAvatarQueryData } from "./UserAvatarQuery";
