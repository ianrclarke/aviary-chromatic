import { QueryHookOptions, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { queryName } from "@shared/data/graphqlNames";
import { ProductShard, PRODUCT_SHARD } from "@shared/data/shards";

const name = queryName("SimilarProducts");
const SIMILAR_PRODUCTS_QUERY = gql`
  query ${name}($productId: ID!, $first: Int) {
    viewer {
      product: node(id: $productId) {
        ... on Product {
          id
          similarProducts(first: $first) {
            nodes {
              ...ProductShard
            }
          }
        }
      }
    }
  }
  ${PRODUCT_SHARD}
`;

interface Data {
  viewer: {
    product: {
      id: string;
      similarProducts: {
        nodes: ProductShard[];
      };
    };
  };
}

interface Variables {
  productId: string;
  first?: number;
}

const useSimilarProductsQuery = (options: QueryHookOptions<Data, Variables>) =>
  useQuery<Data, Variables>(SIMILAR_PRODUCTS_QUERY, options);

export { useSimilarProductsQuery, SIMILAR_PRODUCTS_QUERY };
