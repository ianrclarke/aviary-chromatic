import { useQuery, QueryHookOptions } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { TREATMENT_PLAN_FRAGMENT, TreatmentPlanFragment } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";

const name = queryName("TreatmentPlan");
const TREATMENT_PLAN_QUERY = gql`
  query ${name}($treatmentplanId: ID!) {
    viewer {
      treatmentPlan: node(id: $treatmentplanId) {
        ... on TreatmentPlan {
          ...TreatmentPlanFragment
        }
      }
    }
  }
  ${TREATMENT_PLAN_FRAGMENT}
`;

interface Data {
  viewer: {
    treatmentPlan: TreatmentPlanFragment;
  };
}

interface Variables {
  treatmentplanId: string;
}

const useTreatmentPlanQuery = (options?: QueryHookOptions<Data, Variables>) => {
  return useQuery<Data, Variables>(TREATMENT_PLAN_QUERY, options);
};

export { useTreatmentPlanQuery, TREATMENT_PLAN_QUERY };
