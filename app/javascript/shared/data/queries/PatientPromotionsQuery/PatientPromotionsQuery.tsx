import { QueryHookOptions, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { PatientPromotionFragment, PATIENT_PROMOTION_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";

const name = queryName("PatientPromotions");
const PATIENT_PROMOTIONS = gql`
  query ${name} {
    viewer {
      patientPromotions {
        nodes {
            ...PatientPromotionFragment
        }
      }
    }
  }
  ${PATIENT_PROMOTION_FRAGMENT}
`;

interface Data {
  viewer: {
    patientPromotions: {
      nodes: PatientPromotionFragment[];
    };
  };
}

const usePatientPromotionsQuery = (options?: QueryHookOptions<Data>) =>
  useQuery<Data>(PATIENT_PROMOTIONS, options);

export { usePatientPromotionsQuery, PATIENT_PROMOTIONS };
