import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { StoreFragment, STORE_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";

const name = queryName("Store");
const GET_STORE = gql`
  query ${name} {
    currentStore {
      ...StoreFragment
    }
  }
  ${STORE_FRAGMENT}
`;

interface StoreQueryData {
  currentStore: StoreFragment;
}

const useStoreQuery = () => useQuery<StoreQueryData>(GET_STORE);

export { useStoreQuery, GET_STORE };
