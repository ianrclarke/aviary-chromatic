import { QueryHookOptions, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { AlertsBannerFragment, ALERTS_BANNER_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";

const name = queryName("getAlertsBanner");
const GET_ALERTS_BANNER = gql`
  query ${name} {
    viewer {
      alertsBanners {
        ...AlertsBannerFragment
      }
    }
  }
  ${ALERTS_BANNER_FRAGMENT}
`;

interface Data {
  viewer: {
    alertsBanners: AlertsBannerFragment[];
  };
}

const useAlertsBannerQuery = (options?: QueryHookOptions<Data>) => {
  return useQuery<Data>(GET_ALERTS_BANNER, options);
};

export { useAlertsBannerQuery, GET_ALERTS_BANNER };
