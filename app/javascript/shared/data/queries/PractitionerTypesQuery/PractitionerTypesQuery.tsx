import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { PractitionerTypeFragment, PRACTITIONER_TYPE_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";

const name = queryName("PractitionerTypes");
const GET_PRACTITIONER_TYPES = gql`
  query ${name} {
    practitionerTypes {
      ...PractitionerTypeFragment
    }
  }
  ${PRACTITIONER_TYPE_FRAGMENT}
`;

interface PractitionerTypesData {
  practitionerTypes: PractitionerTypeFragment[];
}

const usePractitionerTypesQuery = () => useQuery<PractitionerTypesData>(GET_PRACTITIONER_TYPES);

export { usePractitionerTypesQuery, GET_PRACTITIONER_TYPES };
