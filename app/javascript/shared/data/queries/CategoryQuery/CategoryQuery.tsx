import { QueryHookOptions, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { CategoryFragment, CATEGORY_FRAGMENT } from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";

const name = queryName("getCategory");
const GET_CATEGORY = gql`
  query ${name}($categoryId: ID!) {
    viewer {
      category: node(id: $categoryId) {
        ...CategoryFragment
      }
    }
  }

  ${CATEGORY_FRAGMENT}
`;

interface CategoryData {
  viewer: {
    category: CategoryFragment;
  };
}

interface Variables {
  categoryId: string;
}

const useCategoryQuery = (options?: QueryHookOptions<CategoryData, Variables>) =>
  useQuery<CategoryData, Variables>(GET_CATEGORY, options);

export { useCategoryQuery, GET_CATEGORY };
