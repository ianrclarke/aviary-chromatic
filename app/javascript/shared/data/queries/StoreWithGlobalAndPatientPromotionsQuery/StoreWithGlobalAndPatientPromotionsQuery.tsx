import { QueryHookOptions, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

import {
  GlobalPromotionFragment,
  GLOBAL_PROMOTION_FRAGMENT,
  PatientPromotionFragment,
  PATIENT_PROMOTION_FRAGMENT,
  StoreFragment,
  StoresPatientPromotionFragment,
  STORES_PATIENT_PROMOTION_FRAGMENT,
  STORE_FRAGMENT,
} from "@shared/data/fragments";
import { queryName } from "@shared/data/graphqlNames";

const name = queryName("StoreWithGlobalAndPatientPromotions");
const STORE_WITH_GLOBAL_AND_PATIENT_PROMOTIONS = gql`
  query ${name} {
    currentStore {
      ...StoreFragment
      globalPatientPromotion {
        ...GlobalPromotionFragment
      }
      storesPatientPromotions {
        nodes {
          ...StoresPatientPromotionFragment
          patientPromotion {
            ...PatientPromotionFragment
          }
        }
      }
    }
  }
  ${STORE_FRAGMENT}
  ${GLOBAL_PROMOTION_FRAGMENT}
  ${STORES_PATIENT_PROMOTION_FRAGMENT}
  ${PATIENT_PROMOTION_FRAGMENT}
`;

interface StoresPatientPromotionShard extends StoresPatientPromotionFragment {
  patientPromotion: PatientPromotionFragment;
}

interface CurrentStoreShard extends StoreFragment {
  globalPatientPromotion: GlobalPromotionFragment;
  storesPatientPromotions: {
    nodes: StoresPatientPromotionShard[];
  };
}

interface StoreWithGlobalAndPatientPromotionsQueryData {
  currentStore: CurrentStoreShard;
}

const useStoreWithGlobalAndPatientPromotionsQuery = (
  options?: QueryHookOptions<StoreWithGlobalAndPatientPromotionsQueryData>
) =>
  useQuery<StoreWithGlobalAndPatientPromotionsQueryData>(
    STORE_WITH_GLOBAL_AND_PATIENT_PROMOTIONS,
    options
  );

export {
  useStoreWithGlobalAndPatientPromotionsQuery,
  STORE_WITH_GLOBAL_AND_PATIENT_PROMOTIONS,
  StoreWithGlobalAndPatientPromotionsQueryData,
};
