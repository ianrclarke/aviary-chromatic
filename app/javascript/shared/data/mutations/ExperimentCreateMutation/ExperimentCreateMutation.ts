import { MutationHookOptions, useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { mutationName } from "@shared/data/graphqlNames";

const name = mutationName("ExperimentCreate");

const EXPERIMENT_CREATE_MUTATION = gql`
  mutation ${name}($input: ExperimentCreateInput!) {
    experimentCreate(input: $input) {
      clientMutationId
    }
  }
`;

interface Data {
  experimentMutation: {
    clientMutationId?: string;
  };
}

interface Variables {
  input: {
    experiment: string;
    owner: string;
    group: string;
    subjectId: string;
  };
}

const useExperimentCreateMutation = (options?: MutationHookOptions<Data, Variables>) =>
  useMutation<Data, Variables>(EXPERIMENT_CREATE_MUTATION, options);

export { useExperimentCreateMutation, EXPERIMENT_CREATE_MUTATION };
