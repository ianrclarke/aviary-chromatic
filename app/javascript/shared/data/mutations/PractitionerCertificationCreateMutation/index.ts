export {
  PRACTITIONER_CERTIFICATION_CREATE_MUTATION,
  usePractitionerCertificationCreateMutation,
} from "./PractitionerCertificationCreateMutation";
