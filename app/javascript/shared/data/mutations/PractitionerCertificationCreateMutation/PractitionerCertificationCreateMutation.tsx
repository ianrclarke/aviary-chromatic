import { MutationHookOptions, useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { CERTIFICATION_FRAGMENT } from "@shared/data/fragments";
import { CertificationFragment } from "@shared/data/fragments/CertificationFragment";
import { mutationName } from "@shared/data/graphqlNames";

const name = mutationName("PractitionerCertificationCreate");

const PRACTITIONER_CERTIFICATION_CREATE_MUTATION = gql`
  mutation ${name}($input: PractitionerCertificationCreateInput!) {
    practitionerCertificationCreate(input: $input) {
      certification {
        ...CertificationFragment
      }
      errors {
        message
        fields {
          certificate
        }
      }
    }
  }
  ${CERTIFICATION_FRAGMENT}
`;

interface Data {
  practitionerCertificationCreate: {
    certification: CertificationFragment;
    errors: {
      message: string;
      fields: {
        certificate: string[];
      };
    };
  };
}

interface Variables {
  input: {
    attributes: {
      certificate: File;
    };
  };
}

const usePractitionerCertificationCreateMutation = (
  options?: MutationHookOptions<Data, Variables>
) => useMutation<Data, Variables>(PRACTITIONER_CERTIFICATION_CREATE_MUTATION, options);

export { usePractitionerCertificationCreateMutation, PRACTITIONER_CERTIFICATION_CREATE_MUTATION };
