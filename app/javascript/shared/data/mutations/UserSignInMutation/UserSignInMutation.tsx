import { Mutation } from "@apollo/react-components";
import gql from "graphql-tag";
import React, { FC } from "react";

import { UserFragment, USER_FRAGMENT } from "@shared/data/fragments/UserFragment";
import { mutationName } from "@shared/data/graphqlNames";
import { MutationType } from "@shared/data/mutations/MutationType";

const name = mutationName("UserSignIn");
const USER_SIGN_IN = gql`
  mutation ${name}($input: userSignInInput!) {
    userSignIn(input: $input) {
      user {
        ...UserFragment
      }
      errors
    }
  }
  ${USER_FRAGMENT}
`;

interface Data {
  userSignIn: {
    user: UserFragment;
    errors: string[];
  };
}

interface Variables {
  input: {
    email: string;
    password: string;
    prioritizedRole: string;
  };
}

type Props = MutationType<Data, Variables>;

const UserSignInMutation: FC<Props> = ({ children, ...rest }) => {
  return (
    <Mutation<Data, Variables> mutation={USER_SIGN_IN} {...rest}>
      {children}
    </Mutation>
  );
};

export { UserSignInMutation, USER_SIGN_IN };
