import { OperationVariables } from "@apollo/react-common";
import { MutationComponentOptions } from "@apollo/react-components";

type MutationType<TData, TVariables = OperationVariables> = Omit<
  MutationComponentOptions<TData, TVariables>,
  "mutation" | "variables"
>;

export { MutationType };
