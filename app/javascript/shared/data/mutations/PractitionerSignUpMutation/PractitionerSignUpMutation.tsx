import { useMutation, MutationHookOptions } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { UserFragment, USER_FRAGMENT } from "@shared/data/fragments/UserFragment";
import { mutationName } from "@shared/data/graphqlNames";

const name = mutationName("PractitionerSignUp");
const PRACTITIONER_SIGN_UP = gql`
  mutation ${name}($input: practitionerSignUpInput!) {
    practitionerSignUp(input: $input) {
      user {
        ...UserFragment
      }
      errors
    }
  }
  ${USER_FRAGMENT}
`;

interface PractitionerSignUpData {
  practitionerSignUp: {
    user: UserFragment;
    errors: string[];
  };
}

interface Variables {
  input: {
    firstName: string;
    lastName: string;
    practitionerTypeId: string;
    email: string;
    password: string;
    phone?: string;
    signupSource: string;
  };
}

const usePractitionerSignUpMutation = (
  options?: MutationHookOptions<PractitionerSignUpData, Variables>
) => {
  return useMutation<PractitionerSignUpData, Variables>(PRACTITIONER_SIGN_UP, options);
};

export { usePractitionerSignUpMutation, PRACTITIONER_SIGN_UP, PractitionerSignUpData };
