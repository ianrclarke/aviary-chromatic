import { MutationHookOptions, useMutation } from "@apollo/react-hooks";
import { DataProxy } from "apollo-cache";
import { FetchResult } from "apollo-link";
import gql from "graphql-tag";

import { AvatarWithFilterFragment, AVATAR_FRAGMENT_WITH_FILTER } from "@shared/data/fragments";
import { mutationName } from "@shared/data/graphqlNames";
import { UserAvatarQueryData, USER_AVATAR_QUERY } from "@shared/data/queries/UserAvatarQuery";
import { AvatarErrorsType } from "@shared/types/CreateAvatarErrorTypes.d";
import { AvatarSize } from "@shared/types/avatarSize.d";

const name = mutationName("UserAvatarCreate");
const USER_AVATAR_CREATE_MUTATION = gql`
  mutation ${name}($input: UserAvatarCreateInput!, $filter: PhotoSizeFilters!) {
    userAvatarCreate(input: $input) {
      avatar {
        ...AvatarWithFilterFragment
      }
      errors {
        message
        fields {
          photo
        }
      }
    }
  }
  ${AVATAR_FRAGMENT_WITH_FILTER}
`;

interface UserAvatarCreateData {
  userAvatarCreate: {
    avatar: AvatarWithFilterFragment;
    errors: AvatarErrorsType;
  };
}

interface Variables {
  input?: {
    attributes: {
      photo: File;
    };
  };
  filter?: AvatarSize;
}

const updateUserAvatar = (cache: DataProxy, mutationResult: FetchResult<UserAvatarCreateData>) => {
  const { photoUrl: newAvatarUrl, id: newAvatarId } = mutationResult.data?.userAvatarCreate?.avatar;
  if (!newAvatarUrl) {
    return;
  }

  const data: UserAvatarQueryData = cache.readQuery({
    query: USER_AVATAR_QUERY,
    variables: { filter: AvatarSize.SMALL },
  });

  const idFromCache = data.viewer?.user?.avatar?.id;

  const avatarIdToWrite = idFromCache ?? newAvatarId;

  cache.writeQuery({
    query: USER_AVATAR_QUERY,
    variables: { filter: AvatarSize.SMALL },
    data: {
      ...data,
      viewer: {
        ...data.viewer,
        user: {
          ...data.viewer.user,
          avatar: {
            ...data.viewer.user.avatar,
            id: avatarIdToWrite,
            photoUrl: newAvatarUrl,
          },
        },
      },
    },
  });
};

const useUserAvatarCreateMutation = (
  options: MutationHookOptions<UserAvatarCreateData, Variables> = {
    variables: {
      filter: AvatarSize.LARGE,
    },
  }
) =>
  useMutation<UserAvatarCreateData, Variables>(USER_AVATAR_CREATE_MUTATION, {
    update: updateUserAvatar,
    ...options,
  });

export { useUserAvatarCreateMutation, USER_AVATAR_CREATE_MUTATION, UserAvatarCreateData };
