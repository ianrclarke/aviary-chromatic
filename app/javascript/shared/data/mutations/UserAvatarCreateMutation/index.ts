export {
  useUserAvatarCreateMutation,
  USER_AVATAR_CREATE_MUTATION,
} from "./UserAvatarCreateMutation";

export type { UserAvatarCreateData } from "./UserAvatarCreateMutation";
