export {
  useUserAvatarDeleteMutation,
  USER_AVATAR_DELETE_MUTATION,
} from "./UserAvatarDeleteMutation";
export type { UserAvatarDeleteData } from "./UserAvatarDeleteMutation";
