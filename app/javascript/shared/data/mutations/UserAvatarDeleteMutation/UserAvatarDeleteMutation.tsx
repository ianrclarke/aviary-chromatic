import { MutationHookOptions, useMutation } from "@apollo/react-hooks";
import { DataProxy } from "apollo-cache";
import { FetchResult } from "apollo-link";
import gql from "graphql-tag";

import { mutationName } from "@shared/data/graphqlNames";
import { UserAvatarQueryData, USER_AVATAR_QUERY } from "@shared/data/queries/UserAvatarQuery";
import { AvatarSize } from "@shared/types/avatarSize.d";

const name = mutationName("UserAvatarDelete");
const USER_AVATAR_DELETE_MUTATION = gql`
  mutation ${name}($input: UserAvatarDeleteInput!) {
    userAvatarDelete(input: $input) {
      errors {
        message
      }
    }
  }
`;

interface UserAvatarDeleteData {
  userAvatarDelete: {
    errors: {
      message: string;
      fields: {
        userAvatarId?: string[];
      };
    };
  };
}

interface Variables {
  input: {
    userAvatarId: string;
  };
}

const updateUserAvatar = (cache: DataProxy, mutationResult: FetchResult<UserAvatarDeleteData>) => {
  const { errors } = mutationResult?.data?.userAvatarDelete;
  if (errors.message || errors.fields) {
    return;
  }

  const data: UserAvatarQueryData = cache.readQuery({
    query: USER_AVATAR_QUERY,
    variables: { filter: AvatarSize.SMALL },
  });

  cache.writeQuery({
    query: USER_AVATAR_QUERY,
    variables: { filter: AvatarSize.SMALL },
    data: {
      ...data,
      viewer: {
        ...data.viewer,
        user: {
          ...data.viewer.user,
          avatar: {
            ...data.viewer.user.avatar,
            photoUrl: null,
          },
        },
      },
    },
  });
};

const useUserAvatarDeleteMutation = (
  options: MutationHookOptions<UserAvatarDeleteData, Variables>
) =>
  useMutation<UserAvatarDeleteData, Variables>(USER_AVATAR_DELETE_MUTATION, {
    ...options,
    update: updateUserAvatar,
  });

export { useUserAvatarDeleteMutation, USER_AVATAR_DELETE_MUTATION, UserAvatarDeleteData };
