const chunkArray = (arr: any[], chunkLength: number) => {
  const chunks = [];
  const unchunkedArray = [...arr];

  while (unchunkedArray.length) {
    chunks.push(unchunkedArray.splice(0, chunkLength));
  }

  return chunks;
};

export { chunkArray };
