import { chunkArray } from "./chunkArray";

describe("chunkArray helper", () => {
  it("returns an array of arrays, derived from a larger array and given 'chunk length' ", () => {
    const testChunkSize = 4;
    const testArray = [1, 2, 3, 4, 5, 6, 7, 8];

    expect(chunkArray(testArray, testChunkSize)).toEqual([
      [1, 2, 3, 4],
      [5, 6, 7, 8],
    ]);
  });

  it("will respond to remainders in chunk size with a smaller final chunk ", () => {
    const testChunkSize = 4;
    const testArray = [1, 2, 3, 4, 5, 6];

    expect(chunkArray(testArray, testChunkSize)).toEqual([
      [1, 2, 3, 4],
      [5, 6],
    ]);
  });

  it("will return an empty array when it is passed an empty array", () => {
    const testChunkSize = 4;
    const testArray = [];

    expect(chunkArray(testArray, testChunkSize)).toEqual([]);
  });

  it("will return an single chunk when it is passed an array with fewer elements than the chunk size", () => {
    const testChunkSize = 4;
    const testArray = [1, 2];

    expect(chunkArray(testArray, testChunkSize)).toEqual([[1, 2]]);
  });
});
