import { getCashValue } from "./getCashValue";

describe("getCashValue hook", () => {
  it("returns a number as a string with a dollar sign and and to two decimal places", () => {
    expect(getCashValue(100)).toEqual("$100.00");
  });

  it("returns a negative number as a string with a dollar sign and to two decimal places", () => {
    expect(getCashValue(-100)).toEqual("-$100.00");
  });

  it("returns $0.00 on null, undefined, NaN, 0", () => {
    expect(getCashValue(0)).toEqual("$0.00");
    expect(getCashValue(null)).toEqual("$0.00");
    expect(getCashValue(undefined)).toEqual("$0.00");
    expect(getCashValue(NaN)).toEqual("$0.00");
  });
});
