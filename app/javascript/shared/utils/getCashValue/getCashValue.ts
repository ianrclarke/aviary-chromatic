const getCashValue = (value: number): string => {
  if (value === 0 || isNaN(value) || value === null) {
    return "$0.00";
  } else if (value < 0) {
    return `-$${Math.abs(value).toFixed(2)}`;
  } else {
    return `$${value.toFixed(2)}`;
  }
};

export { getCashValue };
