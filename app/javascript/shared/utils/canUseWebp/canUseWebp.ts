export const canUseWebp = () => {
  const elem = document.createElement("canvas");
  return (
    elem.getContext &&
    elem.getContext("2d") &&
    elem.toDataURL("image/webp").startsWith("data:image/webp")
  );
};
