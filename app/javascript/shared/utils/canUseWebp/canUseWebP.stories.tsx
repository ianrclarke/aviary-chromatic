import React from "react";

import { canUseWebp } from "./canUseWebp";
import { Title } from "@shared/aviary";

export default {
  title: "Shared|CanUseWebp",
};

export const Default = () => (
  <div>
    <Title>Is WebP supported in this browser?</Title>
    <p>canUseWebp util says: {canUseWebp().toString()}</p>
  </div>
);
