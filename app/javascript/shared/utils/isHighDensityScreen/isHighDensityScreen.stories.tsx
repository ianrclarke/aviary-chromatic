import React from "react";

import { isHighDensityScreen } from "./isHighDensityScreen";
import { Title } from "@shared/aviary";

export default {
  title: "Shared|isHighDensityScreen",
};

export const Default = () => (
  <div>
    <Title>Is this screen an high density screen?</Title>
    <p>isHighDensityScreen util says: {isHighDensityScreen().toString()}</p>
  </div>
);
