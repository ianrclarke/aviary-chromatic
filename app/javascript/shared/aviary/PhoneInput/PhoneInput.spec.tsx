import { mount } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { FloatingLabelInput } from "@shared/aviary";
import { CountrySelector } from "@shared/aviary/PhoneInput/CountrySelector/CountrySelector";
import { baseTests } from "@testing/baseTests";

import { PhoneInput } from "./PhoneInput";

describe("PhoneInput", () => {
  let wrapper;
  let mockValue;
  let mockRequired;
  let mockPreferredCountry;
  const mockOnChange = jest.fn();

  const buildWrapper = () => {
    wrapper = mount(
      <PhoneInput
        id={"id-input"}
        label={"label"}
        value={mockValue}
        required={mockRequired}
        preferredCountryCode={mockPreferredCountry}
        onChange={mockOnChange}
      />
    );
  };

  beforeEach(() => {
    mockPreferredCountry = undefined;
    mockValue = null;
    mockRequired = null;
    buildWrapper();
  });

  baseTests(() => wrapper);

  it("shows empty string no numbers passed", () => {
    mockValue = "garbage string without numbers";
    buildWrapper();
    expect(wrapper.find("[id='id-input']").last().prop("value")).toEqual("");
  });

  it("shows right selected country code and phone number when value is +1", () => {
    mockValue = "1";
    buildWrapper();
    const selectedCode = wrapper.find(CountrySelector).prop("selectedCode");
    expect(selectedCode).toEqual("US");
    expect(wrapper.find("[id='id-input']").last().prop("value")).toEqual("");
  });

  it("shows (X when first number passed is from a double digit country code", () => {
    mockValue = "5";
    buildWrapper();
    const selectedCode = wrapper.find(CountrySelector).prop("selectedCode");
    expect(selectedCode).toEqual("US"); // Default is US so until we know more about the number it'll stay like that
    expect(wrapper.find("[id='id-input']").last().prop("value")).toEqual("(5");
  });

  it("shows (XXX) XXX XXXX and right country code when full US number passed", () => {
    mockValue = "16038796464";
    buildWrapper();
    const selectedCode = wrapper.find(CountrySelector).prop("selectedCode");
    expect(selectedCode).toEqual("US");
    expect(wrapper.find("[id='id-input']").last().prop("value")).toEqual("(603) 879 6464");
  });

  it("shows (XXX) XXX XXXX and Canada country code when preferred country is CA and right number passed number passed", () => {
    mockValue = "16038796464";
    mockPreferredCountry = "CA";
    buildWrapper();
    const selectedCode = wrapper.find(CountrySelector).prop("selectedCode");
    expect(selectedCode).toEqual("CA");
    expect(wrapper.find("[id='id-input']").last().prop("value")).toEqual("(603) 879 6464");
  });

  it("shows (XXX) XXX XXXX  and right country code when two digit country code passed", () => {
    mockValue = "536038796464";
    buildWrapper();
    const selectedCode = wrapper.find(CountrySelector).prop("selectedCode");
    expect(selectedCode).toEqual("CU");
    expect(wrapper.find("[id='id-input']").last().prop("value")).toEqual("(603) 879 6464");
  });

  it("shows (XXX) XXX XXXX and right country code when three digit country code passed", () => {
    mockValue = "303603879646";
    buildWrapper();
    const selectedCode = wrapper.find(CountrySelector).prop("selectedCode");
    expect(selectedCode).toEqual("GR");
    expect(wrapper.find("[id='id-input']").last().prop("value")).toEqual("(360) 387 9646");
  });

  it("Clips value of number if going over limit", () => {
    mockValue = "3036038796464454545";
    buildWrapper();
    const selectedCode = wrapper.find(CountrySelector).prop("selectedCode");
    expect(selectedCode).toEqual("GR");
    expect(wrapper.find("[id='id-input']").last().prop("value")).toEqual("(360) 387 96464");
  });

  it("removes not numbers", () => {
    mockValue = "dff+g1603asd^&-()8796sddff464";
    buildWrapper();
    const selectedCode = wrapper.find(CountrySelector).prop("selectedCode");
    expect(selectedCode).toEqual("US"); // Default is US so until we know more about the number it'll stay like that
    expect(wrapper.find("[id='id-input']").last().prop("value")).toEqual("(603) 879 6464");
  });

  it("removes not numbers and doesn't format if too many numbers", () => {
    mockValue = "dffg7603asd^&-()8796sdd345ff464-6";
    buildWrapper();
    const selectedCode = wrapper.find(CountrySelector).prop("selectedCode");
    expect(selectedCode).toEqual("KZ"); // Default is US so until we know more about the number it'll stay like that
    expect(wrapper.find("[id='id-input']").last().prop("value")).toEqual("(603) 879 63454");
  });

  describe("input field color", () => {
    it("when valid number", () => {
      mockValue = "13879634546";
      buildWrapper();
      expect(wrapper.find(FloatingLabelInput).props().isColor).toEqual("secondary");
    });

    it("when short number", () => {
      mockValue = "1384676";
      buildWrapper();
      expect(wrapper.find(FloatingLabelInput).props().isColor).toEqual("danger");
    });

    it("when not required and empty", () => {
      mockValue = "";
      buildWrapper();
      expect(wrapper.find(FloatingLabelInput).props().isColor).toEqual("secondary");
    });

    it("when not required and undefined", () => {
      mockValue = undefined;
      buildWrapper();
      expect(wrapper.find(FloatingLabelInput).props().isColor).toEqual("secondary");
    });

    it("when required and empty", () => {
      mockValue = " ";
      mockRequired = true;
      buildWrapper();
      expect(wrapper.find(FloatingLabelInput).props().isColor).toEqual("danger");
    });

    it("when required and undefined", () => {
      mockValue = undefined;
      mockRequired = true;
      buildWrapper();
      expect(wrapper.find(FloatingLabelInput).props().isColor).toEqual("secondary");
    });

    it("calls onChange when new country picked", () => {
      mockValue = "16038796464";
      mockPreferredCountry = "CA";
      buildWrapper();
      act(() => {
        wrapper.find(CountrySelector).props().onSelect("BR");
      });
      wrapper.update();

      expect(mockOnChange).toBeCalledWith("+556038796464"); // Replaces the old country code (1) with Brazil's (55)
    });

    it("calls onChange when switches from CA (preferred) to US", () => {
      mockValue = "16038796464";
      mockPreferredCountry = "CA";
      buildWrapper();
      act(() => {
        wrapper.find(CountrySelector).props().onSelect("US");
      });
      wrapper.update();

      expect(mockOnChange).toBeCalledWith("+16038796464");

      expect(wrapper.find(CountrySelector).props().selectedCode).toEqual("US");
    });

    it("calls onChange when new number entered", () => {
      mockValue = "16038796464";
      mockPreferredCountry = "CA";
      buildWrapper();
      act(() => {
        wrapper
          .find(FloatingLabelInput)
          .props()
          .handleChange({ target: { value: "888" } });
      });
      wrapper.update();

      expect(mockOnChange).toBeCalledWith("+1888"); // Keeps the old country code and replaces the rest with the new number
    });
  });
});
