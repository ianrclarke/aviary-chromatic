import React, { useEffect, useState } from "react";

import { boolean, select, text } from "@storybook/addon-knobs";

import { PhoneInput } from "@shared/aviary/PhoneInput/PhoneInput";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=383%3A628",
};

export default {
  title: "Aviary|PhoneInput",
  component: PhoneInput,
  parameters: { design: componentDesign },
};

export const Default = () => {
  const storyValue = text("Value", "+16138887766");
  const [value, setValue] = useState(storyValue);

  useEffect(() => {
    if (storyValue) {
      setValue(storyValue.valueOf());
    }
  }, [storyValue]);

  const handleChange = inputValue => {
    setValue(inputValue);
  };

  return (
    <PhoneInput
      id={"tel"}
      label={"Phone number"}
      onChange={handleChange}
      value={value}
      required={boolean("required", false)}
      preferredCountryCode={select("Preferred Country", ["CA", "US"], "CA")}
    />
  );
};
