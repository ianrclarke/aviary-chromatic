import React, { FC, useEffect, useState } from "react";

import { FloatingLabelInput } from "@shared/aviary/FloatingLabelInput";
import { COUNTRIES } from "@shared/aviary/PhoneInput/CountrySelector/Countries";
import { CountrySelector } from "@shared/aviary/PhoneInput/CountrySelector/CountrySelector";
import { getCleanNumber, isValidPhoneNumber } from "@shared/aviary/PhoneInput/validatePhoneNumber";
import { stylesHelper } from "@styles/emotion-styles/helpers";

import { PopperPositionType } from "../popperPositions.d";

import * as styles from "./PhoneInput.styles";

const MIN_NUMBERS_MASK = "(999) 999 99999";
const DEFAULT_COUNTRY_NAME = "US";

const getCountryFromCode = (countryCode: string) =>
  COUNTRIES.find(({ code }) => code === countryCode);

export const getPhoneWithoutCountryCode = value => {
  const cleanNumber = getCleanNumber(value);
  const country = getCountryFromNumber(value);
  if (country) {
    return cleanNumber.replace(country.dialCode, "");
  }
  return cleanNumber;
};

const getCountryFromNumber = (value, preferredCountryCode = DEFAULT_COUNTRY_NAME) => {
  const justDigits = getCleanNumber(value);
  if (justDigits === "") {
    return COUNTRIES.find(({ code }) => code === preferredCountryCode);
  }
  const countriesWithCode = COUNTRIES.filter(({ dialCode }) => justDigits.startsWith(dialCode));
  if (countriesWithCode.length > 1 && preferredCountryCode) {
    const preferredCountry = countriesWithCode.find(({ code }) => code === preferredCountryCode);
    if (preferredCountry) {
      return preferredCountry;
    }
  }
  return countriesWithCode[0];
};

export type preferredCountries = "CA" | "US";

interface Props {
  id: string;
  label: string;
  value: string;
  onChange: any;
  name?: string;
  required?: boolean;
  errors?: string[];
  preferredCountryCode?: preferredCountries; // Used when two countries have same number, which to use (e.g. "CA" or "US")
  dropdownPlacement?: PopperPositionType;
}

const defaultProps = {
  preferredCountryCode: DEFAULT_COUNTRY_NAME,
  dropdownPlacement: "bottom-start",
};

const PhoneInput: FC<Props> = ({
  id,
  label,
  value,
  onChange,
  errors,
  required,
  preferredCountryCode,
  dropdownPlacement,
  ...rest
}) => {
  const [selectedCountry, setSelectedCode] = useState(
    getCountryFromNumber(value, preferredCountryCode)
  );

  useEffect(() => {
    setSelectedCode(getCountryFromNumber(value, preferredCountryCode));
  }, [value, preferredCountryCode]);

  const phoneNumber = getPhoneWithoutCountryCode(value);

  const handleOnChange = e => {
    const cleanNum = getCleanNumber(e.target.value);
    onChange(`+${selectedCountry.dialCode}${cleanNum}`);
  };

  const handleSelectedCountry = countryCode => {
    const country = getCountryFromCode(countryCode);
    setSelectedCode(country);

    onChange(`+${country.dialCode}${phoneNumber}`);
  };

  const getColor = () => {
    if (required) {
      return !value || (phoneNumber.length > 0 && isValidPhoneNumber(phoneNumber))
        ? "secondary"
        : "danger";
    }
    return phoneNumber.length === 0 || isValidPhoneNumber(phoneNumber) ? "secondary" : "danger";
  };

  const conditionalColorStyles = () => {
    return stylesHelper([styles.label.danger, getColor() === "danger"]);
  };

  return (
    <div css={styles.phoneInputWrapper}>
      <label
        className="legacy-floatingLabelInput__label"
        css={[styles.label.base, conditionalColorStyles()]}
        htmlFor={id}
      >
        {label}
      </label>
      <div css={styles.phoneInput}>
        <div css={styles.countrySelector}>
          <CountrySelector
            selectedCode={selectedCountry?.code}
            onSelect={handleSelectedCountry}
            dropdownPlacement={dropdownPlacement}
          />
          <div css={styles.countrySelectorBottom} />
        </div>
        <FloatingLabelInput
          id={id}
          label={label}
          hideLabel={true}
          type={"tel"}
          mask={MIN_NUMBERS_MASK}
          maskChar={null}
          value={phoneNumber}
          handleChange={handleOnChange}
          isColor={getColor()}
          errors={errors}
          required={required}
          {...rest}
        />
      </div>
    </div>
  );
};
PhoneInput.defaultProps = defaultProps as Props;
export { PhoneInput };
