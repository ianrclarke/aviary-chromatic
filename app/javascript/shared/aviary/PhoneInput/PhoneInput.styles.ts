import { css } from "@emotion/core";

import { colors } from "@styles";

export const countrySelector = css`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 3.35rem;
`;

export const countrySelectorBottom = css`
  border-bottom: 1px solid ${colors.lightGreen};
  margin: 0 1rem 3px 0;
`;
export const phoneInput = css`
  display: flex;
`;

export const phoneInputWrapper = css`
  display: flex;
  flex-direction: column;
`;

export const label = {
  base: css`
    z-index: 2;
    position: absolute;
    font-size: 1rem;
    color: ${colors.blue};
    transform: scale(0.85);
    transform-origin: left top;
  `,
  danger: css`
    color: ${colors.danger};
  `,
};
