import { css } from "@emotion/core";

import { layers } from "@styles";

export const countrySelector = css`
  display: flex;
  margin-top: 0;
`;

export const countryFlag = css`
  flex-direction: column;
  margin-right: 9px;
`;

export const dropDownButton = css`
  padding-bottom: calc(0.35em - 1px);
  padding-bottom: calc(0.375rem - 1px);
  padding-left: 0;
  padding-right: 0;
  height: 2rem;
  align-self: flex-end;
  margin-bottom: 1px;
  margin-right: 1rem;
`;

export const dropDown = css`
  display: flex;
  height: 100%;
`;

export const dropDownMenu = css`
  top: 3.1rem;
  max-width: 26rem;
  z-index: ${layers.modalDropdownMax};
`;
