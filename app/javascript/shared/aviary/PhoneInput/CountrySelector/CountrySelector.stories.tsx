import React, { useState } from "react";

import { CountrySelector } from "./CountrySelector";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=383%3A628",
};

export default {
  title: "Aviary|PhoneInput/CountrySelector",
  component: CountrySelector,
  parameters: { design: componentDesign },
};

export const Default = () => {
  const [code, setCode] = useState("CA");

  const handleSelect = selectedCode => {
    setCode(selectedCode);
  };

  return <CountrySelector selectedCode={code} onSelect={handleSelect} />;
};
