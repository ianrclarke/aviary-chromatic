import React, { FC } from "react";

import { PopperPositionType } from "@shared/aviary/popperPositions.d";

import { Dropdown } from "../../Dropdown";
import { DropdownContent } from "../../Dropdown/DropdownContent";
import { DropdownItem } from "../../Dropdown/DropdownItem";
import { DropdownButton } from "../../Dropdown/DropdownTrigger/DropdownButton";

import { COUNTRIES } from "./Countries";

import * as styles from "./CountrySelector.styles";

interface Props {
  selectedCode: string;
  onSelect: (code: string) => void;
  dropdownPlacement?: PopperPositionType;
}

const defaultProps = {
  selectedCode: "US",
};

const CountrySelector: FC<Props> = ({ selectedCode, onSelect, dropdownPlacement }) => {
  const getCurrentValue = () => {
    const selectedValue = COUNTRIES.find(({ code }) => code === selectedCode);
    if (selectedValue) {
      return (
        <div css={styles.countrySelector}>
          <div css={styles.countryFlag}>{selectedValue.flag}</div>
          <div> +{selectedValue.dialCode}</div>
        </div>
      );
    }
    return null;
  };

  return (
    <Dropdown css={styles.dropDown} dropdownPlacement={dropdownPlacement}>
      <DropdownButton
        isColor={"primary"}
        isOutlined={false}
        isText={true}
        css={styles.dropDownButton}
      >
        {getCurrentValue()}
      </DropdownButton>
      <DropdownContent css={styles.dropDownMenu}>
        {COUNTRIES.map(({ flag, name, dialCode, code }) => (
          <DropdownItem key={code} value={code} onSelect={() => onSelect(code)}>
            {flag} {name} +{dialCode}
          </DropdownItem>
        ))}
      </DropdownContent>
    </Dropdown>
  );
};

CountrySelector.defaultProps = defaultProps;
export { CountrySelector };
