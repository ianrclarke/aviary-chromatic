import { mount } from "enzyme";
import React from "react";

import { DropdownButton, DropdownItem } from "@shared/aviary";
import { baseTests } from "@testing/baseTests";

import { CountrySelector } from "./CountrySelector";

describe("PhoneInput", () => {
  let wrapper;
  let mockSelectedCode;
  const mockOnSelectCallback = jest.fn();

  const buildWrapper = () => {
    wrapper = mount(
      <CountrySelector onSelect={mockOnSelectCallback} selectedCode={mockSelectedCode} />
    );
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => wrapper);

  it("defaults to US", () => {
    buildWrapper();
    expect(wrapper.find(DropdownButton).text()).toEqual("🇺🇸 +1");
  });

  it("shows right country flag and code when not default", () => {
    mockSelectedCode = "BR";
    buildWrapper();
    expect(wrapper.find(DropdownButton).text()).toEqual("🇧🇷 +55");
  });

  it("calls callback with right parameter", () => {
    buildWrapper();
    wrapper.find(DropdownButton).simulate("click");
    wrapper.find(DropdownItem).get(0).props.onSelect();
    expect(mockOnSelectCallback).toBeCalledWith("CA");
  });
});
