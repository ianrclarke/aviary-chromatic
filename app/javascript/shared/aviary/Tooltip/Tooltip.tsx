import { Global } from "@emotion/core";
import { Tooltip as MUITooltip } from "@material-ui/core";
import { withStyles, Theme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import React, { FC } from "react";

import { baseTheme } from "@shared/aviary/theme";
import { colors, helpers, layers } from "@styles";

import { PopperPositionType } from "../popperPositions.d";

import * as styles from "./Tooltip.styles";

interface Props {
  tooltipContent: React.ReactNode;
  textDirection?: "left" | "right" | "center";
  placement?: PopperPositionType;
  interactive?: boolean;
}
const Tooltip: FC<Props> = ({
  textDirection,
  tooltipContent,
  placement,
  interactive,
  children,
  ...rest
}) => {
  const tooltipStyles = [styles.wrapper, textDirection && styles.textDirection[textDirection]];

  const renderTooltipContent = () => {
    return <>{tooltipContent}</>;
  };

  const AviaryTooltip = withStyles((theme: Theme) => ({
    popper: {
      zIndex: layers.zIndexTopForeground,
    },
    arrow: {
      color: colors.dark,
    },
    tooltip: {
      backgroundColor: colors.dark,
      color: colors.white,
      fontSize: theme.typography.pxToRem(14),
      padding: "0.5rem 0.75rem",
      boxShadow: `${helpers.hexToRgba(colors.dark, 0.25)} 0 3px 8px 0`,
    },
  }))(MUITooltip);

  return (
    <>
      <Global styles={styles.tooltipOverrides} />
      <ThemeProvider theme={baseTheme}>
        <AviaryTooltip
          title={renderTooltipContent()}
          arrow={true}
          placement={placement}
          interactive={interactive}
          disableTouchListener={true}
        >
          <div css={tooltipStyles} tabIndex={0} {...rest}>
            {children}
          </div>
        </AviaryTooltip>
      </ThemeProvider>
    </>
  );
};

Tooltip.defaultProps = { placement: "top", textDirection: "right" };

export { Tooltip };
