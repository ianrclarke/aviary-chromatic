import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Tooltip } from "./Tooltip";

describe("Tooltip tests", () => {
  let tooltip;

  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    tooltip = shallow(<Tooltip tooltipContent="Hello" />);
  };

  baseTests(() => tooltip);
});
