import React from "react";

import { faCheckCircle } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { select } from "@storybook/addon-knobs";

import { PopperPositionType, POPPER_POSITIONS } from "../popperPositions.d";
import { Tooltip } from "./Tooltip";

export default {
  title: "Aviary|Tooltip",
  component: Tooltip,
};

const content = (
  <p>
    Look you found some content!{" "}
    <b>
      It's so bold and <em>stylish</em>!
    </b>
    <p>Tooltip so cool that I'm putting on my winter gloves.</p>
  </p>
);

export const Default = () => (
  <Tooltip
    tooltipContent={content}
    placement={select<PopperPositionType>("Tooltip placement", POPPER_POSITIONS, "top")}
    textDirection={select("Text Direction", ["right", "left"], "right")}
  >
    <p>Hover over to see more</p>
  </Tooltip>
);

export const Icon = () => (
  <Tooltip
    tooltipContent={content}
    placement={select<PopperPositionType>("Tooltip placement", POPPER_POSITIONS, "top")}
    textDirection={select("Text Direction", ["right", "left"], "right")}
  >
    <FontAwesomeIcon icon={faCheckCircle} />
  </Tooltip>
);
