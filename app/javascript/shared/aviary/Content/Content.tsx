import React, { FC, HTMLProps } from "react";

import * as styles from "./Content.styles";

interface ColumnsProps extends HTMLProps<HTMLDivElement> {
  isCentered?: boolean;
}

const Content: FC<ColumnsProps> = ({ isCentered, children, ...rest }) => {
  const contentStyles = [styles.content, isCentered && styles.centered];
  return (
    <div css={contentStyles} {...rest}>
      {children}
    </div>
  );
};

export { Content };
