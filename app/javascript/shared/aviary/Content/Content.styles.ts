import { css } from "@emotion/core";

export const content = css`
  li + li {
    margin-top: 0.25em;
  }

  p,
  dl,
  ol,
  ul,
  blockquote,
  pre,
  table {
    &:not(:last-child) {
      margin-bottom: 1em;
    }
  }

  ol {
    list-style-position: outside;
    margin-left: 2em;
    margin-top: 1em;
  }
  ul {
    list-style: disc outside;
    margin-left: 2em;
    margin-top: 1em;
    ul {
      list-style-type: circle;
      margin-top: 0.5em;
      ul {
        list-style-type: square;
      }
    }
  }
  dd {
    margin-left: 2em;
  }
  figure {
    margin-left: 2em;
    margin-right: 2em;
    text-align: center;
    &:not(:first-child) {
      margin-top: 2em;
    }
    &:not(:last-child) {
      margin-bottom: 2em;
    }
    img {
      display: inline-block;
    }
    figcaption {
      font-style: italic;
    }
  }
  pre {
    overflow-x: auto;
    padding: 1.25rem 1.5rem;
    white-space: pre;
    word-wrap: normal;
  }
  sup,
  sub {
    font-size: 75%;
  }
`;

export const centered = css`
  text-align: center;
`;
