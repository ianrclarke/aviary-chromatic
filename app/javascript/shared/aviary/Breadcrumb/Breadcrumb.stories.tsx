import React from "react";

import { Breadcrumb } from "./Breadcrumb";
import { BreadcrumbItem } from "./BreadcrumbItem";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=394%3A463",
};

export default {
  title: "Aviary|Breadcrumb",
  component: Breadcrumb,
  parameters: { design: componentDesign },
};

export const Default = () => (
  <Breadcrumb>
    <BreadcrumbItem>Item uno</BreadcrumbItem>
    <BreadcrumbItem>Item dos</BreadcrumbItem>
    <BreadcrumbItem>
      <a>Item linky</a>
    </BreadcrumbItem>
    <BreadcrumbItem isActive={true}>Item active</BreadcrumbItem>
  </Breadcrumb>
);
