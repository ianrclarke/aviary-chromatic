import React, { FC, HTMLProps } from "react";
import { useTranslation } from "react-i18next";

import {
  Dropdown,
  DropdownContent,
  DropdownItem,
  DropdownLink,
  DropdownTrigger,
  OpenAction,
} from "@shared/aviary/Dropdown";
import { useBreakpoints } from "@shared/hooks";

import { BreadcrumbItem } from "./BreadcrumbItem";

import * as styles from "./Breadcrumb.styles";

const BreadcrumbItemType = (<BreadcrumbItem />).type;

const Breadcrumb: FC<HTMLProps<HTMLUListElement>> = ({ children, ...rest }) => {
  const { tablet } = useBreakpoints();
  const { t } = useTranslation("aviary");
  const childCount = React.Children.toArray(children).length;
  const css = [styles.breadcrumb];

  const doMobileDisplay = () => {
    return (
      <>
        {getFirstItem()}
        <BreadcrumbItem>
          <Dropdown
            closeOnExternalClick={true}
            closeOnItemSelect={true}
            openAction={"click" as OpenAction}
            aria-label={t("collapsedBreadcrumb")}
          >
            <DropdownTrigger>...</DropdownTrigger>
            <DropdownContent>{makeDropdownItems()}</DropdownContent>
          </Dropdown>
        </BreadcrumbItem>
        {getLastItem()}
      </>
    );
  };

  const getFirstItem = () => {
    return React.Children.toArray(children)[0];
  };

  const getLastItem = () => {
    return React.Children.toArray(children)[childCount - 1];
  };

  const makeLinkItem = child => {
    if (child.props.children.props.to) {
      const text = child.props.children.props.children;
      const to = child.props.children.props.to;
      return <DropdownLink to={to}>{text}</DropdownLink>;
    }
    return <DropdownItem>{child.props.children.props.children}</DropdownItem>;
  };

  const makeDropdownItems = () => {
    return React.Children.map(children, (child, i) => {
      if (React.isValidElement(child) && i !== 0 && i !== childCount - 1) {
        if (child.type === BreadcrumbItemType) {
          if (child.props.children.props) {
            return makeLinkItem(child);
          }
          return <DropdownItem>{child.props.children}</DropdownItem>;
        }
      }
    });
  };

  const renderChildren = () => {
    if (tablet.lessThan && childCount > 2) {
      return doMobileDisplay();
    } else {
      return children;
    }
  };
  return (
    <ul css={css} {...rest}>
      {renderChildren()}
    </ul>
  );
};

export { Breadcrumb };
