import { css } from "@emotion/core";

import { colors, helpers } from "@styles";

export const breadcrumb = css`
  ${helpers.fontSizeNormalizer(18)}
  color: ${colors.grey};
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  justify-content: flex-start;
`;
