import { shallow } from "enzyme";
import React from "react";
import { Link } from "react-router-dom";

import { baseTests } from "@testing/baseTests";

import { Breadcrumb } from "./Breadcrumb";
import { BreadcrumbItem } from "./BreadcrumbItem";

describe("Breadcrumb renders correctly", () => {
  let wrapper;
  const setWidth = width => ((window as any).innerWidth = width);

  const buildWrapper = () => {
    wrapper = shallow(
      <Breadcrumb>
        <BreadcrumbItem>
          <Link to="/somewhere">Hello friendo</Link>
        </BreadcrumbItem>
        <BreadcrumbItem>
          <Link to="/somewhere">Nest 2</Link>
        </BreadcrumbItem>
        <BreadcrumbItem>
          <Link to="/somewhere">Nest 3</Link>
        </BreadcrumbItem>
      </Breadcrumb>
    );
  };

  beforeEach(() => {
    setWidth(1024);
    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("breakpoint tests", () => {
    it("it renders normally at desktop size", () => {
      expect(wrapper.find("BreadcrumbItem").length).toEqual(3);
    });
    it("it renders normally at mobile sizes if there is less than three breadcrumbs", () => {
      setWidth(768);
      wrapper = shallow(
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to="/somewhere">Hello friendo</Link>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <Link to="/somewhere">Nest 2</Link>
          </BreadcrumbItem>
        </Breadcrumb>
      );
      expect(wrapper.exists("Dropdown")).toBeFalsy();
    });
    it("it renders a dropdown when on mobile and there is more than 2 breadcrumbs", () => {
      setWidth(768);
      buildWrapper();
      expect(wrapper.exists("Dropdown")).toBeTruthy();
    });
  });
});
