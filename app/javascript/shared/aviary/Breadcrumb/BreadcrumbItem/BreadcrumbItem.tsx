import React, { FC } from "react";

import * as styles from "./BreadcrumbItem.styles";

interface Props {
  /**
   * Provides additional styling. To be used on the element that represents the active page
   *
   * @default false
   */
  isActive?: boolean;
}

const BreadcrumbItem: FC<Props> = ({ isActive, children, ...rest }) => {
  const css = [styles.item, isActive && styles.activeItem];
  return (
    <li css={css} {...rest}>
      <span css={styles.span}>{children}</span>
    </li>
  );
};

export { BreadcrumbItem };
