import { css } from "@emotion/core";

import { colors } from "@styles";

export const item = css`
  display: flex;
  align-items: center;
  color: ${colors.grey};
  font-weight: 500;

  a {
    color: ${colors.grey};
    font-weight: 500;
  }

  a:hover {
    color: ${colors.primary};
  }

  &:before {
    color: ${colors.grey};
    content: "/";
    display: none;
    margin-right: 0.5rem;
  }

  &:not(:first-of-type):before {
    display: block;
  }
`;

export const span = css`
  padding-right: 0.5rem;
  user-select: text;
`;

export const activeItem = css`
  font-weight: 600;
  color: ${colors.dark};

  a,
  a:hover {
    font-weight: 600;
    color: ${colors.dark};
    cursor: default;
  }
`;
