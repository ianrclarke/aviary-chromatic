import { SerializedStyles } from "@emotion/core";
import React, { FC } from "react";
import { NavLink } from "react-router-dom";

import { Box, BoxSection } from "@shared/aviary";

import * as styles from "./BoxLink.styles";

interface Props {
  href: string;
  boxCss?: SerializedStyles;
  anchor?: boolean;
}

// todo add the dashboard page and conditional logic to render the welcome page instead.
const BoxLink: FC<Props> = ({ href, boxCss, children, anchor = true, ...rest }) => {
  const renderAnchor = () => (
    <a href={href} css={styles.boxLink} {...rest}>
      {renderBox()}
    </a>
  );

  const renderBox = () => (
    <Box className="legacy-boxLink__box" css={boxCss}>
      <BoxSection>{children}</BoxSection>
    </Box>
  );

  const renderNavLink = () => (
    <NavLink css={styles.boxLink} to={href} {...rest}>
      {renderBox()}
    </NavLink>
  );

  return anchor ? renderAnchor() : renderNavLink();
};

export { BoxLink };
