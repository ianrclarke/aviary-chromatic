import { css } from "@emotion/core";

import { animations } from "@styles";

export const boxLink = css`
  &:hover,
  &:focus {
    .box {
      ${animations.hoverBoxShadow}
    }
  }
`;
