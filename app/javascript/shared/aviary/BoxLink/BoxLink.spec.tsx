import { mount } from "enzyme";
import React from "react";

import { BoxLink } from "@shared/aviary";
import { MockRouter } from "@shared/mocks/react-router";
import { baseTests } from "@testing/baseTests";

describe("BoxLink component", () => {
  let wrapper;
  let href;
  let anchor;

  const buildWrapper = () => {
    wrapper = mount(
      <MockRouter>
        <BoxLink href={href} anchor={anchor} />
      </MockRouter>
    );
  };

  beforeEach(() => {
    href = "http://example.org";
    anchor = undefined;

    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("conditional rendering", () => {
    it("renders a when default", () => {
      expect(wrapper.exists("a")).toBe(true);
      expect(wrapper.exists("NavLink")).toBe(false);
    });

    it("renders a when anchor is true", () => {
      anchor = true;
      buildWrapper();

      expect(wrapper.exists("a")).toBe(true);
      expect(wrapper.exists("NavLink")).toBe(false);
    });

    it("renders NavLink when anchor is false", () => {
      anchor = false;
      buildWrapper();

      expect(wrapper.exists("NavLink")).toBe(true);
    });
  });
});
