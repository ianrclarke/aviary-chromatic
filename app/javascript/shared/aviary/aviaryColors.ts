const AVIARY_COLORS = {
  primary: "primary",
  dark: "dark",
  info: "info",
  warning: "warning",
  danger: "danger",
  purple: "purple",
  light: "light",
};

type aviaryColors = keyof typeof AVIARY_COLORS;

interface AviaryColorProps {
  isColor?: aviaryColors;
}

export { AVIARY_COLORS, aviaryColors, AviaryColorProps };
