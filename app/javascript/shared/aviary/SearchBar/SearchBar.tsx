import { faSearch, faTimes } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC, HTMLProps, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";

import { useDebounce } from "@shared/hooks";
import { Size } from "@shared/types/sizes.d";

import { Button } from "../Button";

import * as styles from "./SearchBar.styles";

interface Props extends HTMLProps<HTMLInputElement> {
  isFocus?: boolean;
  isSize?: Size;
  incrementalSearch?: boolean;
  placeholder: string;
  value: string;
  alwaysShowSearchIcon?: boolean;
  alwaysShowTrailingIcon?: boolean;
  onBlur?: () => void;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onSearch: (term: string) => void;
  onTrailingIconClick: () => void;
  onFocus?: (e: React.FocusEvent<HTMLInputElement>) => void;
  onKeyDown?: (e: React.KeyboardEvent) => void;
}

const SearchBar: FC<Props> = ({
  onTrailingIconClick,
  onSearch,
  isFocus,
  isSize,
  incrementalSearch,
  alwaysShowSearchIcon,
  alwaysShowTrailingIcon,
  value,
  onKeyDown,
  ...rest
}) => {
  const debouncedSearch = useDebounce(term => onSearch(term), 400);
  const { t } = useTranslation("aviary:searchBar");
  const inputRef = useRef(null);

  useEffect(() => {
    if (isFocus) {
      inputRef.current.focus();
    }
  }, [isFocus]);

  useEffect(() => {
    if (incrementalSearch) {
      debouncedSearch(value);
      return () => debouncedSearch.cancel();
    }
  }, [value]);

  const handleKeyDown = (e: React.KeyboardEvent) => {
    // Enter key
    if (e.key === "Escape" && !incrementalSearch) {
      onSearch(value);
    }

    if (onKeyDown) {
      onKeyDown(e);
    }
  };

  const renderCloseButton = () => (
    <Button
      isSize={"small"}
      aria-label={t("aviary:searchBar.ClearSearch")}
      isText={true}
      isColor={"trio"}
      onClick={onTrailingIconClick}
    >
      <FontAwesomeIcon datatest-id={"close-icon"} icon={faTimes} />
    </Button>
  );

  const renderCloseButtonOrSearchIcon = () => {
    if (alwaysShowTrailingIcon) {
      return renderCloseButton();
    }

    if (incrementalSearch) {
      return value && renderCloseButton();
    }

    if (alwaysShowSearchIcon) {
      return (
        <Button
          isSize={"small"}
          aria-label={t("aviary:searchBar.Search")}
          isText={true}
          onClick={() => onSearch(value)}
        >
          <FontAwesomeIcon
            datatest-id={"search-icon-right"}
            css={styles.searchIcon}
            icon={faSearch}
          />
        </Button>
      );
    }
  };

  const containerBuilder = () => {
    return [styles.searchBar.base, styles.searchBar[isSize]];
  };

  return (
    <div css={containerBuilder()}>
      {incrementalSearch && (
        <FontAwesomeIcon datatest-id={"search-icon-left"} css={styles.searchIcon} icon={faSearch} />
      )}
      <input
        autoComplete="on"
        css={styles.inputStyles}
        type="text"
        value={value}
        onKeyDown={handleKeyDown}
        ref={inputRef}
        {...rest}
      />
      {renderCloseButtonOrSearchIcon()}
    </div>
  );
};

SearchBar.defaultProps = {
  placeholder: "...",
  isFocus: true,
  incrementalSearch: false,
  alwaysShowSearchIcon: true,
  isSize: Size.medium,
};

export { SearchBar };
