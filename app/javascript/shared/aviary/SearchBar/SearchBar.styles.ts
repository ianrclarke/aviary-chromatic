import { css } from "@emotion/core";

import { Size } from "@shared/types/sizes.d";
import { colors, typography, dimensions } from "@styles";
import { inputTextBoxMaker } from "@styles/emotion-styles/helpers";

export const searchBar = {
  base: css`
    ${inputTextBoxMaker};
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 100%;
    height: 3rem;

    @media only screen and (max-width: ${dimensions.phoneMax}) {
      padding-right: 0.5rem;
    }
  `,
  [Size.medium]: css`
    height: 3.5rem;
  `,
  [Size.small]: css`
    height: 2.75rem;
  `,
};

export const searchIcon = css`
  margin: 0 0.25rem;
  color: ${colors.primary};
`;

export const inputStyles = css`
  width: 100%;
  max-width: 100%;
  border: none;
  padding: 0.5rem;
  font-size: ${typography.paragraphLong};
  color: ${colors.grey};

  ::placeholder {
    color: ${colors.light};
  }

  &:focus {
    box-shadow: none;
    outline: none;
  }
`;
