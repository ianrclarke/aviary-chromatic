import React, { useState } from "react";

import { boolean, select, text } from "@storybook/addon-knobs";

import { Size } from "@shared/types/sizes.d";
import { SearchBar } from "./index";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=386%3A860",
};

export default {
  title: "Aviary|SearchBar",
  component: SearchBar,
  parameters: { design: componentDesign },
};

export const Default = () => {
  const [value, setValue] = useState<string>("");
  const [searchTerm, setSearchTerm] = useState<string>("");

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  const onSearch = (term: string) => {
    setSearchTerm(term);
  };

  const onTrailingIconClick = () => {
    setValue("");
  };

  return (
    <>
      <SearchBar
        placeholder={text("Placeholder", "Search 20,000 products...")}
        isSize={select("isSize", [null, Size.medium], null)}
        incrementalSearch={boolean("Incremental Search", false)}
        value={value}
        onChange={onChange}
        onSearch={onSearch}
        onTrailingIconClick={onTrailingIconClick}
      />
      {searchTerm}
    </>
  );
};
