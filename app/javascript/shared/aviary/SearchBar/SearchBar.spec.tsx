import { mount } from "enzyme";
import React from "react";

import * as mockDebounce from "@shared/hooks/useDebounce/mockUseDebounce";

import { SearchBar } from "./SearchBar";

jest.mock("@shared/hooks", () => ({
  useDebounce: cb => mockDebounce.mockUseDebounce(cb),
}));

const debounceSpy = jest.spyOn(mockDebounce, "mockUseDebounce");

describe("SearchBar tests", () => {
  let wrapper;

  let placeholder;
  let value;
  let onChange;
  let incrementalSearch;
  let onSearch;
  let onTrailingIconClick;

  let props = {
    incrementalSearch,
    onChange,
    onSearch,
    onTrailingIconClick,
    placeholder,
    value,
  };

  const buildWrapper = () => mount(<SearchBar {...props} />);
  const typeSomething = (typedValue: string) => {
    wrapper.find("input").simulate("change", {
      target: {
        value: typedValue,
      },
    });
  };

  beforeEach(() => {
    mockDebounce.initUseDebounce();

    // reset props to defined values
    incrementalSearch = false;
    onChange = jest.fn();
    onSearch = jest.fn();
    onTrailingIconClick = jest.fn();
    placeholder = "Placeholder...";
    value = "This is the first value";

    props = {
      ...props,
      incrementalSearch,
      onChange,
      onSearch,
      onTrailingIconClick,
      placeholder,
      value,
    };

    wrapper = buildWrapper();
  });

  it("calls onChange when text is typed in input field", () => {
    expect(wrapper.find("input")).toHaveLength(1);

    typeSomething("typed value");

    expect(props.onChange).toHaveBeenCalled();
    expect(props.onChange.mock.calls[0][0].target.value).toEqual("typed value");
  });

  it("renders the search icon on the right", () => {
    expect(wrapper.find('[datatest-id="search-icon-left"]')).toHaveLength(0);
    expect(wrapper.find('[datatest-id="search-icon-right"]')).toHaveLength(1);
  });

  it("calls onSearch when Enter is pressed", () => {
    wrapper.find("input").simulate("keydown", { key: "Escape" });

    expect(props.onSearch).toHaveBeenCalled();
    expect(props.onSearch).toHaveBeenCalledWith("This is the first value");
  });

  describe("when incrementalSearch is true", () => {
    beforeEach(() => {
      props = {
        ...props,
        incrementalSearch: true,
      };

      wrapper = buildWrapper();
      typeSomething("I am typing");
    });

    it("renders the close icon", () => {
      expect(wrapper.find("[datatest-id='close-icon']")).toHaveLength(1);
    });

    it("renders the search icon on the left", () => {
      expect(wrapper.find('[datatest-id="search-icon-right"]')).toHaveLength(0);
      expect(wrapper.find('[datatest-id="search-icon-left"]')).toHaveLength(1);
    });

    it("calls debounced onSearch", () => {
      mockDebounce.mockDebounceProperties.callback();

      expect(debounceSpy).toHaveBeenCalled();
      expect(onSearch).toHaveBeenCalled();
    });

    describe("when close icon is clicked", () => {
      it("calls the `trailingIconClick` prop", () => {
        wrapper.find("Button").simulate("click");

        expect(props.onTrailingIconClick).toHaveBeenCalled();
      });
    });
  });
});
