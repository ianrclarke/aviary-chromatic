import { ButtonProps } from "@shared/aviary/Button/Button";
import { useDropdownContext } from "@shared/aviary/Dropdown/DropdownContext";

const useDropdownButtonProps = (): Partial<ButtonProps> => {
  const {
    onTriggerClicked,
    setMouseOverTrigger,
    setTriggerFocused,
    isDisabled,
    isFullWidth,
    setTriggerElement,
  } = useDropdownContext();

  const onClick = e => {
    onTriggerClicked();
    if (e) setTriggerElement(e.currentTarget);
  };

  const onMouseEnter = () => {
    setMouseOverTrigger(true);
  };

  const onMouseLeave = () => {
    setMouseOverTrigger(false);
  };

  const onFocus = () => {
    setTriggerFocused(true);
  };

  const onBlur = () => {
    setTriggerFocused(false);
  };

  return {
    onClick,
    onMouseEnter,
    onMouseLeave,
    onFocus,
    onBlur,
    disabled: isDisabled,
    isFullWidth,
  };
};

export { useDropdownButtonProps };
