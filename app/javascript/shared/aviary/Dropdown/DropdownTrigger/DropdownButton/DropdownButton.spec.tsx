import { faCaretDown } from "@fortawesome/pro-solid-svg-icons";
import { shallow } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { DropdownInternals } from "../../types.d";

import { DropdownButton } from "./DropdownButton";

let mockInternalProps: DropdownInternals;

jest.mock("../../DropdownContext", () => ({
  useDropdownContext: () => mockInternalProps,
}));

describe("DropdownButton", () => {
  let wrapper;

  let mockIsDropdownOpen;
  let mockOnTriggerClicked;
  let mockSetMouseOverTrigger;
  let mockSetTriggerFocused;

  let mockHasIcon;
  let mockCustomIcon;

  const buildMockInternalProps = (options: Partial<DropdownInternals>) => {
    mockInternalProps = { ...options } as DropdownInternals;
  };

  const buildWrapper = () => {
    buildMockInternalProps({
      isDropdownOpen: mockIsDropdownOpen,
      onTriggerClicked: mockOnTriggerClicked,
      setMouseOverTrigger: mockSetMouseOverTrigger,
      setTriggerFocused: mockSetTriggerFocused,
    });

    wrapper = shallow(
      <DropdownButton hasIcon={mockHasIcon} customIcon={mockCustomIcon}>
        test
      </DropdownButton>
    );
  };

  beforeEach(() => {
    mockIsDropdownOpen = false;
    mockOnTriggerClicked = jest.fn();
    mockSetMouseOverTrigger = jest.fn();
    mockSetTriggerFocused = jest.fn();

    mockHasIcon = true;

    buildWrapper();
  });

  it("shows an icon when 'hasIcon' prop is true", () => {
    mockHasIcon = true;
    buildWrapper();

    expect(wrapper.exists("DropdownChevron")).toEqual(true);
  });

  it("does not show an icon when 'hasIcon' prop is false", () => {
    mockHasIcon = false;
    buildWrapper();

    expect(wrapper.exists("DropdownChevron")).toEqual(false);
  });

  it("shows the customIcon that is passed in as a prop", () => {
    mockHasIcon = true;
    mockCustomIcon = faCaretDown;
    buildWrapper();

    expect(wrapper.find("DropdownChevron").props().customIcon).toEqual(mockCustomIcon);
  });

  it("calls internal prop setMouseOverTrigger on mouse enter", () => {
    act(() => {
      wrapper.props().onMouseEnter();
    });
    wrapper.update();

    expect(mockSetMouseOverTrigger).toBeCalledWith(true);
  });

  it("calls internal prop setMouseOverTrigger on mouse leave", () => {
    act(() => {
      wrapper.props().onMouseLeave();
    });
    wrapper.update();

    expect(mockSetMouseOverTrigger).toBeCalledWith(false);
  });

  it("invokes the internal prop setTriggerFocused on focus", () => {
    act(() => {
      wrapper.props().onFocus();
    });

    expect(mockSetTriggerFocused).toBeCalledWith(true);
  });

  it("invokes the internal prop setTriggerFocused on blur", () => {
    act(() => {
      wrapper.props().onBlur();
    });

    expect(mockSetTriggerFocused).toBeCalledWith(false);
  });

  it("fires the callback provided when clicked", () => {
    act(() => {
      wrapper.props().onClick();
    });
    wrapper.update();

    expect(mockOnTriggerClicked).toBeCalled();
  });

  baseTests(() => wrapper);
});
