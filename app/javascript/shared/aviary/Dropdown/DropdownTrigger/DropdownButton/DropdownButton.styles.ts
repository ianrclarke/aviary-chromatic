import { css } from "@emotion/core";

export const dropdownButton = css`
  flex-direction: row;
  justify-content: space-between;
`;

export const text = css`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;
