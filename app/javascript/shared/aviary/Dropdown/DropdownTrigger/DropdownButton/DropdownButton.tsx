import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import React, { FC } from "react";

import { Button } from "@shared/aviary/Button";
import { ButtonProps } from "@shared/aviary/Button/Button";

import { DropdownChevron } from "../DropdownChevron";

import { useDropdownButtonProps } from "./useDropdownButtonProps";

import * as styles from "./DropdownButton.styles";

interface Props extends ButtonProps {
  /**
   * if true, button will have chevron icon, or custom icon
   *
   * @default true
   */
  hasIcon?: boolean;
  /**
   * optional custom icon that will be used in place of chevron
   */
  customIcon?: IconDefinition;
}

const defaultProps = {
  hasIcon: true,
};

const DropdownButton: FC<Props> = ({ children, customIcon, hasIcon, ...rest }) => {
  const dropdownProps = useDropdownButtonProps();

  const renderIcon = () => {
    if (hasIcon) {
      return <DropdownChevron customIcon={customIcon} />;
    }
  };

  return (
    <Button
      {...dropdownProps}
      css={styles.dropdownButton}
      isColor="specialty"
      isOutlined={true}
      {...rest}
    >
      <span css={styles.text}>{children}</span>
      {renderIcon()}
    </Button>
  );
};

DropdownButton.defaultProps = defaultProps;
export { DropdownButton };
