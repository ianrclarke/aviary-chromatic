import { shallow } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { DropdownInternals } from "../types.d";

import { DropdownTrigger } from "./DropdownTrigger";

let mockInternalProps: DropdownInternals;

jest.mock("../DropdownContext", () => ({
  useDropdownContext: () => mockInternalProps,
}));

describe("DropdownTrigger", () => {
  let wrapper;

  let mockOnTriggerClicked;
  let mockIsDropdownOpen;
  let mockSetMouseOverTrigger;
  let mockSetTriggerFocused;
  let mockSetTriggerElement;

  const buildMockInternalProps = (options: Partial<DropdownInternals>) => {
    mockInternalProps = { ...options } as DropdownInternals;
  };

  const buildWrapper = () => {
    buildMockInternalProps({
      isDropdownOpen: mockIsDropdownOpen,
      onTriggerClicked: mockOnTriggerClicked,
      setMouseOverTrigger: mockSetMouseOverTrigger,
      setTriggerFocused: mockSetTriggerFocused,
      setTriggerElement: mockSetTriggerElement,
    });

    wrapper = shallow(<DropdownTrigger>test</DropdownTrigger>);
  };

  beforeEach(() => {
    mockOnTriggerClicked = jest.fn();
    mockIsDropdownOpen = false;
    mockSetMouseOverTrigger = jest.fn();
    mockSetTriggerFocused = jest.fn();
    mockSetTriggerElement = jest.fn();

    buildWrapper();
  });

  it("fires a callback when trigger is clicked while dropdown is closed", () => {
    mockIsDropdownOpen = false;
    buildWrapper();

    act(() => {
      wrapper.props().onClick();
    });
    wrapper.update();

    expect(mockOnTriggerClicked).toHaveBeenCalledTimes(1);
  });

  it("fires a callback when trigger is clicked while dropdown is open", () => {
    mockIsDropdownOpen = true;
    buildWrapper();

    act(() => {
      wrapper.props().onClick();
    });
    wrapper.update();

    expect(mockOnTriggerClicked).toHaveBeenCalledTimes(1);
  });

  it("calls internal prop setMouseOverTrigger on mouse enter", () => {
    act(() => {
      wrapper.props().onMouseEnter();
    });
    wrapper.update();

    expect(mockSetMouseOverTrigger).toBeCalledWith(true);
  });

  it("calls internal prop setMouseOverTrigger on mouse leave", () => {
    act(() => {
      wrapper.props().onMouseLeave();
    });
    wrapper.update();

    expect(mockSetMouseOverTrigger).toBeCalledWith(false);
  });

  it("invokes setTriggerFocused on focus", () => {
    act(() => {
      wrapper.props().onFocus();
    });

    expect(mockSetTriggerFocused).toBeCalledWith(true);
  });

  it("invokes setTriggerFocused on blur", () => {
    act(() => {
      wrapper.props().onBlur();
    });

    expect(mockSetTriggerFocused).toBeCalledWith(false);
  });

  it("invokes onTriggerClicked on 'Enter'", () => {
    act(() => {
      wrapper.props().onKeyPress({ key: "Enter" });
    });

    expect(mockOnTriggerClicked).toHaveBeenCalledTimes(1);
  });

  baseTests(() => wrapper);
});
