import { faCaretUp, faChevronDown } from "@fortawesome/pro-regular-svg-icons";
import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { DropdownInternals } from "../../types.d";

import { DropdownChevron } from "./DropdownChevron";

let mockInternalProps: DropdownInternals;

jest.mock("../../DropdownContext", () => ({
  useDropdownContext: () => mockInternalProps,
}));

describe("DropdownChevron", () => {
  let wrapper;

  let mockIsDropdownOpen;

  let mockCustomIcon;

  const buildMockInternalProps = (options: Partial<DropdownInternals>) => {
    mockInternalProps = { ...options } as DropdownInternals;
  };

  const buildWrapper = () => {
    buildMockInternalProps({
      isDropdownOpen: mockIsDropdownOpen,
    });

    wrapper = shallow(<DropdownChevron customIcon={mockCustomIcon}>test</DropdownChevron>);
  };

  beforeEach(() => {
    mockIsDropdownOpen = false;

    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("conditional rendering", () => {
    it("shows the chevron down icon by default", () => {
      expect(wrapper.find("FontAwesomeIcon").props().icon).toEqual(faChevronDown);
    });

    it("shows the customIcon that is passed in as a prop", () => {
      mockCustomIcon = faCaretUp;
      buildWrapper();

      expect(wrapper.find("FontAwesomeIcon").props().icon).toEqual(mockCustomIcon);
    });
  });
});
