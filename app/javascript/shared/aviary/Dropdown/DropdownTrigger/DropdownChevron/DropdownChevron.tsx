import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { faChevronDown } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC } from "react";

import { ButtonProps } from "@shared/aviary/Button/Button";
import { stylesHelper } from "@styles/helpers";

import { useDropdownContext } from "../../DropdownContext";

import * as styles from "./DropdownChevron.styles";

interface Props extends ButtonProps {
  /**
   *  Optional custom icon to be used in place of chevron
   */
  customIcon?: IconDefinition;
}

const DropdownChevron: FC<Props> = ({ customIcon }) => {
  const { isDropdownOpen } = useDropdownContext();

  const conditionalIcon = () => {
    return stylesHelper(styles.icon, [styles.rotatedIcon, isDropdownOpen]);
  };

  return (
    <span>
      <FontAwesomeIcon size="sm" icon={customIcon || faChevronDown} css={conditionalIcon()} />
    </span>
  );
};

export { DropdownChevron };
