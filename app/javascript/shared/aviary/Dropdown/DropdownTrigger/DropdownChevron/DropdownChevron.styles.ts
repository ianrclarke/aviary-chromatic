import { css } from "@emotion/core";

import { colors } from "@styles";

export const icon = css`
  margin-left: 0.5rem;
  path:not(.fa-spinner-third) {
    color: ${colors.green} !important;
  }
`;

export const rotatedIcon = css`
  transform: rotate(180deg);
`;
