import { shallow } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { DropdownInternals } from "../../types.d";

import { SplitDropdownButton } from "./SplitDropdownButton";

let mockInternalProps: DropdownInternals;

jest.mock("../../DropdownContext", () => ({
  useDropdownContext: () => mockInternalProps,
}));

describe("SplitDropdownButton", () => {
  let wrapper;

  let mockIsDropdownOpen;
  let mockOnTriggerClicked;
  let mockSetMouseOverTrigger;
  let mockTriggerFocused;
  let mockIsPrimaryDisabled;
  let mockIsDisabled;

  let mockOnPrimarySelect;

  const buildMockInternalProps = (options: Partial<DropdownInternals>) => {
    mockInternalProps = { ...options } as DropdownInternals;
  };

  const buildWrapper = () => {
    buildMockInternalProps({
      isDropdownOpen: mockIsDropdownOpen,
      setMouseOverTrigger: mockSetMouseOverTrigger,
      onTriggerClicked: mockOnTriggerClicked,
      setTriggerFocused: mockTriggerFocused,
      isDisabled: mockIsDisabled,
    });

    wrapper = shallow(
      <SplitDropdownButton
        onPrimarySelect={mockOnPrimarySelect}
        isPrimaryDisabled={mockIsPrimaryDisabled}
      >
        test
      </SplitDropdownButton>
    );
  };

  beforeEach(() => {
    mockIsDropdownOpen = false;

    mockOnPrimarySelect = jest.fn();
    mockOnTriggerClicked = jest.fn();
    mockSetMouseOverTrigger = jest.fn();
    mockTriggerFocused = jest.fn();
    mockIsPrimaryDisabled = false;
    mockIsDisabled = false;

    buildWrapper();
  });

  it("fires the onPrimarySelect prop when the primary button is clicked", () => {
    act(() => {
      wrapper.find("Button").first().props().onClick();
    });

    expect(mockOnPrimarySelect).toBeCalled();
  });

  it("does not fire the onTriggerClicked internal prop when the primary button is clicked", () => {
    act(() => {
      wrapper.find("Button").first().props().onClick();
    });

    expect(mockOnTriggerClicked).not.toBeCalled();
  });

  it("does not fire the onPrimarySelect prop when the dropdown button is clicked", () => {
    act(() => {
      wrapper.find("Button").last().props().onClick();
    });

    expect(mockOnPrimarySelect).not.toBeCalled();
  });

  it("fires the onTriggerClicked internal prop when the dropdown button is clicked", () => {
    act(() => {
      wrapper.find("Button").last().props().onClick();
    });

    expect(mockOnTriggerClicked).toBeCalled();
  });

  it("calls internal prop setMouseOverTrigger on mouse enter of dropdown button", () => {
    act(() => {
      wrapper.find("Button").last().props().onMouseEnter();
    });
    wrapper.update();

    expect(mockSetMouseOverTrigger).toBeCalledWith(true);
  });

  it("calls internal prop setMouseOverTrigger on mouse leave of dropdown button", () => {
    act(() => {
      wrapper.find("Button").last().props().onMouseLeave();
    });
    wrapper.update();

    expect(mockSetMouseOverTrigger).toBeCalledWith(false);
  });

  it("invokes internal prop setTriggerFocused onFocus", () => {
    act(() => {
      wrapper.find("Button").last().props().onFocus();
    });

    expect(mockTriggerFocused).toHaveBeenCalledWith(true);
  });

  it("invokes internal prop setTriggerFocused onBlur", () => {
    act(() => {
      wrapper.find("Button").last().props().onBlur();
    });

    expect(mockTriggerFocused).toHaveBeenCalledWith(false);
  });

  it("Dropdown primary button is disabled", () => {
    mockIsPrimaryDisabled = true;
    buildWrapper();
    const disabled = wrapper.find("Button").first().props().disabled;

    expect(disabled).toBe(true);
  });

  it("Dropdown is disabled", () => {
    mockIsDisabled = true;
    buildWrapper();
    const disabled = wrapper.find("Button").last().props().disabled;

    expect(disabled).toBe(true);
  });

  baseTests(() => wrapper);
});
