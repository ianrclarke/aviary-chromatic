import { faChevronDown } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC } from "react";

import { ButtonGroup } from "@shared/aviary/Button";
import { Button, ButtonProps } from "@shared/aviary/Button/Button";
import { stylesHelper } from "@styles/helpers";

import { useDropdownContext } from "../../DropdownContext";

import * as styles from "./SplitDropdownButton.styles";

interface Props extends ButtonProps {
  /**
   * Callback that is fired whenever the primary button is selected
   */
  onPrimarySelect?: () => void;
  /**
   * If true, the primary button will be disabled.
   *
   * @default false
   */
  isPrimaryDisabled?: boolean;
  /**
   * If true, the dropdown button will be loading
   *
   * @default false
   */
  isDropdownLoading?: boolean;
  /**
   * To have an ability to set data-e2e to one of the buttons
   */
  dataE2eTextButton?: string;
  dataE2eSelectButton?: string;
}

const SplitDropdownButton: FC<Props> = ({
  children,
  onPrimarySelect,
  isPrimaryDisabled,
  isLoading,
  isDropdownLoading,
  to,
  href,
  dataE2eTextButton,
  dataE2eSelectButton,
  ...rest
}) => {
  const {
    isDisabled,
    isDropdownOpen,
    onTriggerClicked,
    setMouseOverTrigger,
    setTriggerFocused,
    setTriggerElement,
  } = useDropdownContext();

  const conditionalIcon = () => {
    return stylesHelper([styles.rotatedIcon, isDropdownOpen]);
  };

  const primaryClickHandler = () => {
    if (onPrimarySelect) {
      onPrimarySelect();
    }
  };

  const dropdownClickHandler = e => {
    onTriggerClicked();
    if (e) setTriggerElement(e.currentTarget);
  };

  const onMouseEnterHandler = () => {
    setMouseOverTrigger(true);
  };

  const onMouseLeaveHandler = () => {
    setMouseOverTrigger(false);
  };

  const onFocusHandler = () => {
    setTriggerFocused(true);
  };

  const onBlurHandler = () => {
    setTriggerFocused(false);
  };

  return (
    <ButtonGroup>
      <Button
        disabled={isPrimaryDisabled}
        css={styles.primaryActionButton}
        onClick={primaryClickHandler}
        to={to}
        href={href}
        isLoading={isLoading}
        {...rest}
        data-e2e={dataE2eTextButton}
      >
        {children}
      </Button>
      <Button
        disabled={isDisabled}
        css={styles.dropdownButton}
        onClick={dropdownClickHandler}
        onMouseEnter={onMouseEnterHandler}
        onMouseLeave={onMouseLeaveHandler}
        onFocus={onFocusHandler}
        onBlur={onBlurHandler}
        isLoading={isDropdownLoading}
        {...rest}
        data-e2e={dataE2eSelectButton}
      >
        <FontAwesomeIcon size="sm" icon={faChevronDown} css={conditionalIcon()} />
      </Button>
    </ButtonGroup>
  );
};

export { SplitDropdownButton };
