import { css } from "@emotion/core";

export const rotatedIcon = css`
  transform: rotate(180deg);
`;

export const primaryActionButton = css`
  padding-right: 0.625rem;
  padding-left: 0.625rem;
`;

export const dropdownButton = css`
  padding-right: 0.75rem;
  padding-left: 0.75rem;
`;
