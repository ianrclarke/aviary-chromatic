import { faChevronDown } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC } from "react";

import { IconButton } from "@shared/aviary";
import { ButtonProps } from "@shared/aviary/Button/Button";
import { useDropdownContext } from "@shared/aviary/Dropdown/DropdownContext";

import { useDropdownIconButtonProps } from "./useDropdownIconButtonProps";

import * as styles from "./DropdownIconButton.styles";

interface Props extends ButtonProps {
  ariaLabel: string;
}

const DropdownIconButton: FC<Props> = ({ children, ariaLabel, ...rest }) => {
  const dropdownProps = useDropdownIconButtonProps();
  const { isDropdownOpen } = useDropdownContext();

  const conditionalIcon = [styles.icon, isDropdownOpen && styles.rotatedIcon];

  return (
    <IconButton
      {...dropdownProps}
      css={styles.dropdownButton}
      isSize={"small"}
      aria-label={ariaLabel}
      isCircular={true}
      {...rest}
    >
      {children}
      <FontAwesomeIcon size="xs" icon={faChevronDown} css={conditionalIcon} />
    </IconButton>
  );
};

export { DropdownIconButton };
