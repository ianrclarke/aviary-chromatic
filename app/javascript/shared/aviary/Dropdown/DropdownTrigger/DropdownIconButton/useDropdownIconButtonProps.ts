import { ButtonProps } from "@shared/aviary/Button/Button";
import { useDropdownContext } from "@shared/aviary/Dropdown/DropdownContext";

const useDropdownIconButtonProps = (): Partial<ButtonProps> => {
  const {
    onTriggerClicked,
    setTriggerFocused,
    isDisabled,
    isFullWidth,
    setTriggerElement,
  } = useDropdownContext();

  const onClick = e => {
    onTriggerClicked();
    if (e) setTriggerElement(e.currentTarget);
  };

  const onFocus = () => {
    setTriggerFocused(true);
  };

  const onBlur = () => {
    setTriggerFocused(false);
  };

  return {
    onClick,
    onFocus,
    onBlur,
    disabled: isDisabled,
    isFullWidth,
  };
};

export { useDropdownIconButtonProps };
