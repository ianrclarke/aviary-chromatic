import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { DropdownInternals } from "../../types.d";

import { DropdownIconButton } from "./DropdownIconButton";

let mockInternalProps: DropdownInternals;

jest.mock("../../DropdownContext", () => ({
  useDropdownContext: () => mockInternalProps,
}));

describe("DropdownButton", () => {
  let wrapper;

  let mockIsDropdownOpen;
  let mockOnTriggerClicked;
  let mockSetTriggerFocused;
  let mockSetTriggerElement;

  const buildMockInternalProps = (options: Partial<DropdownInternals>) => {
    mockInternalProps = { ...options } as DropdownInternals;
  };

  const buildWrapper = () => {
    buildMockInternalProps({
      isDropdownOpen: mockIsDropdownOpen,
      onTriggerClicked: mockOnTriggerClicked,
      setTriggerFocused: mockSetTriggerFocused,
      setTriggerElement: mockSetTriggerElement,
    });

    wrapper = shallow(<DropdownIconButton ariaLabel={"test"}>test</DropdownIconButton>);
  };

  beforeEach(() => {
    mockIsDropdownOpen = false;
    mockOnTriggerClicked = jest.fn();
    mockSetTriggerFocused = jest.fn();
    mockSetTriggerElement = jest.fn();

    buildWrapper();
  });

  baseTests(() => wrapper);
});
