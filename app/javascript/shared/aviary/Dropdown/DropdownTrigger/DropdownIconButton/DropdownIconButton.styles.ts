import { css } from "@emotion/core";

export const dropdownButton = css`
  flex-direction: row;
  justify-content: center;
`;

export const icon = css`
  margin-left: 0.22rem;
  && {
    width: 0.4rem;
  }
`;

export const rotatedIcon = css`
  transform: rotate(180deg);
`;
