import { css } from "@emotion/core";

import { colors, dimensions, layers } from "@styles";
import { hexToRgba } from "@styles/helpers";

export const dropdownContent = css`
  margin-top: 1px;
  overflow-y: scroll;
  max-height: 10.25rem;
  background-color: ${colors.white};
  border-radius: ${dimensions.borderRadius};
  box-shadow: 0 1px 8px 0 ${hexToRgba(colors.grey, 0.25)};
  z-index: ${layers.indexDropdownContent};
`;

export const popper = css`
  z-index: ${layers.indexDropdownContent};
  max-width: calc(100vw - 0.5rem);
  position: absolute;
`;

export const isFullWidth = css`
  width: 100% !important;
`;

export const opensUp = css`
  bottom: 100%;
  margin-top: 0;
  margin-bottom: 1px;
`;
