import { mount } from "enzyme";
import { ReferenceObject } from "popper.js";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { DropdownCategory } from "../DropdownCategory/DropdownCategory";
import { DropdownItem } from "../DropdownItem";
import { DropdownInternals, OpenAction } from "../types.d";

import { DropdownContent } from "./DropdownContent";

let mockInternalProps: DropdownInternals;

const mockDefaultInternalProps: DropdownInternals = {
  activeCategoryId: "",
  setActiveCategoryId: () => null,
  isDropdownOpen: true,
  mouseOverContent: false,
  mouseOverTrigger: false,
  triggerFocused: false,
  triggerElement: null,
  setTriggerElement: () => null,
  itemFocused: false,
  onItemSelected: () => null,
  onTriggerClicked: () => null,
  openAction: OpenAction.click,
  setMouseOverContent: () => null,
  setMouseOverTrigger: () => null,
  setTriggerFocused: () => null,
  setItemFocused: () => null,
  isDisabled: false,
  isFullWidth: false,
  ellipsesPosition: "topRight",
  dropdownPlacement: "left",
};

jest.mock("../DropdownContext", () => ({
  useDropdownContext: () => ({ ...mockDefaultInternalProps, ...mockInternalProps }),
}));

describe("DropdownContent", () => {
  let wrapper;

  let mockIsDropdownOpen;
  let mockSetMouseOverContent;
  let mockActiveCategoryId;
  let mockSetActiveCategoryId;

  const buildMockInternalProps = (options: Partial<DropdownInternals>) => {
    mockInternalProps = { ...options } as DropdownInternals;
  };

  const buildWrapper = () => {
    buildMockInternalProps({
      isDropdownOpen: mockIsDropdownOpen,
      setMouseOverContent: mockSetMouseOverContent,
      triggerElement: { referenceNode: {} } as ReferenceObject,
      activeCategoryId: mockActiveCategoryId,
      setActiveCategoryId: mockSetActiveCategoryId,
    });

    wrapper = mount(
      <DropdownContent>
        <DropdownItem>Item 1</DropdownItem>
        <DropdownItem>Item 2</DropdownItem>
        <DropdownCategory text="subcategory" categoryId="subcategory">
          subcategory-content
        </DropdownCategory>
      </DropdownContent>
    );
  };

  beforeEach(() => {
    mockIsDropdownOpen = true;
    mockSetMouseOverContent = jest.fn();
    mockActiveCategoryId = undefined;
    mockSetActiveCategoryId = jest.fn();

    buildWrapper();
  });

  baseTests(() => wrapper);

  it("does not render content when internalProp 'isOpen' is false ", () => {
    mockIsDropdownOpen = false;
    buildWrapper();

    expect(wrapper.children().length).toEqual(0);
  });

  it("renders content when internalProp 'isOpen' is true ", () => {
    mockIsDropdownOpen = true;

    buildWrapper();

    expect(wrapper.exists("Popper")).toEqual(true);
    expect(wrapper.exists("DropdownItem")).toEqual(true);
  });

  it("calls internal prop setMouseOverContent on mouse enter", () => {
    act(() => {
      wrapper.find("[data-testid='dropdown-content']").props().onMouseEnter();
    });
    wrapper.update();

    expect(mockSetMouseOverContent).toBeCalledWith(true);
  });

  it("calls internal prop setMouseOverContent on mouse leave", () => {
    act(() => {
      wrapper.find("[data-testid='dropdown-content']").props().onMouseLeave();
    });
    wrapper.update();

    expect(mockSetMouseOverContent).toBeCalledWith(false);
  });

  describe("with categories", () => {
    it("renders the content with a matching category, if there is an active category defined", () => {
      mockActiveCategoryId = "subcategory";
      buildWrapper();

      expect(wrapper.exists("DropdownCategory")).toEqual(true);
      expect(wrapper.find("DropdownCategory").props().categoryId).toEqual("subcategory");
    });

    it("does not render the top-level items, if there is an active category defined", () => {
      mockActiveCategoryId = "subcategory";
      buildWrapper();

      expect(wrapper.text()).not.toContain("Item 1");
      expect(wrapper.text()).not.toContain("Item 2");
    });
  });
});
