import { Dropdown } from "./Dropdown";

const notes = `
#### Notes
DropdownSearch is functionally an input element, which means it is the developer's responsibility to decide what to do with the search input (including onSubmit actions, and whether that will affect the DropdownItems displayed). The [React docs on forms](https://reactjs.org/docs/forms.html) are a helpful resource.
`;

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=383%3A611",
};

export default {
  title: "Aviary|Dropdown",
  component: Dropdown,
  parameters: { design: componentDesign, notes },
};

export * from "./stories";
