import { createContext, useContext } from "react";

import { DropdownInternals } from "../types.d";

const DropdownContext = createContext<DropdownInternals>(null);

const useDropdownContext = (): DropdownInternals => useContext(DropdownContext);

export { useDropdownContext, DropdownContext };
