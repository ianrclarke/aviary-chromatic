import { css } from "@emotion/core";

import { typography } from "@styles";

export const headerItem = css`
  align-items: center;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`;

export const backIcon = css`
  margin-right: 1rem;
`;

export const strong = css`
  font-weight: ${typography.strong};
`;
