import { faAngleRight, faAngleLeft } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC } from "react";
import { useTranslation } from "react-i18next";

import { Text } from "@shared/aviary";

import { useDropdownContext } from "../DropdownContext";
import { DropdownDivider } from "../DropdownDivider";
import { DropdownItem } from "../DropdownItem";

import * as styles from "./DropdownCategory.styles";

interface Props {
  /**
   * The string passed here will be displayed on the dropdown item that leads into the category
   */
  text: string;
  /**
   * A unique key that is used to distinguish categories
   */
  categoryId: string;
  /**
   * Optional prop to override the return button text
   */
  backText?: string;
}

const DropdownCategory: FC<Props> = ({ children, text, backText, categoryId }) => {
  const { t } = useTranslation("aviary:dropdown");
  const { activeCategoryId, setActiveCategoryId } = useDropdownContext();

  const goBackHandler = () => {
    setActiveCategoryId();
  };

  const selectHandler = () => {
    setActiveCategoryId(categoryId);
  };

  if (activeCategoryId === categoryId) {
    return (
      <>
        <DropdownItem
          aria-label={t("aviary:dropdown.GoBack")}
          data-testid="back-button"
          onSelect={goBackHandler}
          closeOnItemSelect={false}
          css={styles.headerItem}
        >
          <FontAwesomeIcon icon={faAngleLeft} css={styles.backIcon} />
          <Text css={styles.strong}>{backText ? backText : text}</Text>
        </DropdownItem>
        <DropdownDivider />
        {children}
      </>
    );
  }

  return (
    <DropdownItem onSelect={selectHandler} closeOnItemSelect={false} icon={faAngleRight}>
      <Text>{text}</Text>
    </DropdownItem>
  );
};

export { DropdownCategory };
