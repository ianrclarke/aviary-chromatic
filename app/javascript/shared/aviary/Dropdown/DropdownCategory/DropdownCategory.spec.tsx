import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { DropdownCategory } from "./DropdownCategory";

let mockActiveCategoryId;
let mockSetActiveCategoryId;
jest.mock("../DropdownContext", () => ({
  useDropdownContext: () => ({
    activeCategoryId: mockActiveCategoryId,
    setActiveCategoryId: mockSetActiveCategoryId,
  }),
}));

describe("DropdownCategory", () => {
  let wrapper;
  let mockBackText;

  const buildWrapper = () => {
    wrapper = mount(
      <DropdownCategory text="test-button" categoryId="test" backText={mockBackText}>
        content
      </DropdownCategory>
    );
  };

  beforeEach(() => {
    mockActiveCategoryId = "test";
    mockSetActiveCategoryId = jest.fn();
    mockBackText = null;
    buildWrapper();
  });

  describe("not matching activeCategoryId", () => {
    it("renders the entry-button dropdown item", () => {
      mockActiveCategoryId = "no match";
      buildWrapper();

      expect(wrapper.text()).toContain("test-button");
    });

    it("does not render the content", () => {
      mockActiveCategoryId = "no match";
      buildWrapper();

      expect(wrapper.text()).not.toContain("content");
    });

    it("does not render the back button", () => {
      mockActiveCategoryId = "no match";
      buildWrapper();

      expect(wrapper.exists('[data-testid="back-button"]')).toEqual(false);
    });
  });

  describe("matching activeCategoryId", () => {
    it("renders the content", () => {
      mockActiveCategoryId = "test";
      buildWrapper();

      expect(wrapper.text()).toContain("content");
    });

    it("renders the back button", () => {
      mockActiveCategoryId = "test";
      buildWrapper();

      expect(wrapper.exists('[data-testid="back-button"]')).toEqual(true);
    });

    it("renders the custom back button text if it is present", () => {
      mockActiveCategoryId = "test";
      mockBackText = "Back";
      buildWrapper();

      expect(wrapper.find('[data-testid="back-button"]').contains(mockBackText)).toBeTruthy();
    });
  });

  baseTests(() => wrapper);
});
