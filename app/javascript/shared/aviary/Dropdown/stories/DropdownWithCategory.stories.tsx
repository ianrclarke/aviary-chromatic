import { boolean, select, text } from "@storybook/addon-knobs";
import React, { useState } from "react";

import { Dropdown } from "../Dropdown";
import { DropdownCategory } from "../DropdownCategory";
import { DropdownContent } from "../DropdownContent";
import { DropdownHeader } from "../DropdownHeader";
import { DropdownItem } from "../DropdownItem";
import { DropdownLink } from "../DropdownLink";
import { DropdownButton } from "../DropdownTrigger";
import { OpenAction } from "../types.d";

export const DropdownWithCategory = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Dropdown
      isOpen={isOpen}
      triggerCallback={setIsOpen}
      openAction={select<any>("openAction", Object.keys(OpenAction), OpenAction.click)}
      closeOnItemSelect={boolean("closeOnItemSelect", true)}
    >
      <DropdownButton>Click Here</DropdownButton>
      <DropdownContent>
        <DropdownHeader>Header</DropdownHeader>
        <DropdownItem>Regular Item 1</DropdownItem>
        <DropdownLink to="/some-link-somewhere">Link Item 2</DropdownLink>
        <DropdownCategory categoryId="test" text="Sub category 1" backText={text("backText", null)}>
          <DropdownItem>Sub Item 1.1</DropdownItem>
          <DropdownItem>Sub Item 1.2</DropdownItem>
          <DropdownItem>Sub Item 1.3</DropdownItem>
          <DropdownItem>Sub Item 1.4</DropdownItem>
          <DropdownItem>Sub Item 1.5</DropdownItem>
          <DropdownItem>Sub Item 1.6</DropdownItem>
          <DropdownItem>Sub Item 1.7</DropdownItem>
          <DropdownItem>Sub Item 1.8</DropdownItem>
          <DropdownItem>Sub Item 1.9</DropdownItem>
        </DropdownCategory>
        <DropdownCategory categoryId="test2" text="Sub category 2">
          <DropdownItem>Sub Item 2.1</DropdownItem>
          <DropdownItem>Sub Item 2.2</DropdownItem>
          <DropdownItem>Sub Item 2.3</DropdownItem>
          <DropdownItem>Sub Item 2.4</DropdownItem>
          <DropdownItem>Sub Item 2.5</DropdownItem>
          <DropdownItem>Sub Item 2.6</DropdownItem>
          <DropdownItem>Sub Item 2.7</DropdownItem>
          <DropdownItem>Sub Item 2.8</DropdownItem>
          <DropdownItem>Sub Item 2.9</DropdownItem>
        </DropdownCategory>
      </DropdownContent>
    </Dropdown>
  );
};
