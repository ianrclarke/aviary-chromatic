import React from "react";

import { action } from "@storybook/addon-actions";
import { boolean, select, text } from "@storybook/addon-knobs";

import { Dropdown } from "../Dropdown";
import { DropdownContent } from "../DropdownContent";
import { DropdownDivider } from "../DropdownDivider";
import { DropdownHeader } from "../DropdownHeader";
import { DropdownItem } from "../DropdownItem";
import { SplitDropdownButton } from "../DropdownTrigger";
import { OpenAction } from "../types.d";

export const WithSplitDropdownButton = () => {
  return (
    <Dropdown
      closeOnExternalClick={boolean("closeOnExternalClick", true)}
      closeOnItemSelect={boolean("closeOnItemSelect", true)}
      openAction={select<any>("openAction", OpenAction, OpenAction.click)}
      disabled={boolean("disabled", false)}
    >
      <SplitDropdownButton
        isPrimaryDisabled={boolean("isPrimaryDisabled", false)}
        onPrimarySelect={action("onPrimarySelect prop fired")}
      >
        {text("Button Text", "Click Here")}
      </SplitDropdownButton>
      <DropdownContent>
        <DropdownHeader>{text("Header Text", "Header")}</DropdownHeader>
        <DropdownItem onSelect={action("onSelect for DropdownItem1")}>DropdownItem</DropdownItem>
        <DropdownItem onSelect={action("onSelect for DropdownItem2")}>DropdownItem</DropdownItem>
        <DropdownDivider />
        <DropdownItem onSelect={action("onSelect for DropdownItem3")}>DropdownItem</DropdownItem>
      </DropdownContent>
    </Dropdown>
  );
};
