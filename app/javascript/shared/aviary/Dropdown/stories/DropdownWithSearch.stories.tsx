import React, { ChangeEvent, useState } from "react";

import { action } from "@storybook/addon-actions";
import { text } from "@storybook/addon-knobs";

import { Dropdown } from "../Dropdown";
import { DropdownContent } from "../DropdownContent";
import { DropdownSearch } from "../DropdownHeader/DropdownSearch";
import { DropdownItem } from "../DropdownItem";
import { DropdownButton } from "../DropdownTrigger";

export const DropdownWithSearch = () => {
  const [value, setValue] = useState("");

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
    action("onChange");
  };

  return (
    <Dropdown>
      <DropdownButton>Click Here</DropdownButton>
      <DropdownContent>
        <DropdownSearch
          placeholder={text("placeholder", "Quick filter")}
          onChange={onChange}
          value={value}
          onCancel={onChange}
        />
        <DropdownItem>Item1</DropdownItem>
        <DropdownItem>Item2</DropdownItem>
      </DropdownContent>
    </Dropdown>
  );
};
