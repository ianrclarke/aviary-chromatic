import React from "react";

import { action } from "@storybook/addon-actions";
import { boolean, select, text } from "@storybook/addon-knobs";

import { Dropdown } from "../Dropdown";
import { DropdownContent } from "../DropdownContent";
import { DropdownDivider } from "../DropdownDivider";
import { DropdownHeader } from "../DropdownHeader";
import { DropdownItem } from "../DropdownItem";
import { OpenAction } from "../types.d";
import { POPPER_POSITIONS, PopperPositionType } from "@shared/aviary/popperPositions.d";
import { DropdownIconButton } from "@shared/aviary/Dropdown";
import { faFilePlus } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const IconTriggerDropdown = () => {
  return (
    <Dropdown
      closeOnExternalClick={boolean("closeOnExternalClick", true)}
      closeOnItemSelect={boolean("closeOnItemSelect", true)}
      openAction={select<any>("openAction", Object.keys(OpenAction), OpenAction.click)}
      disabled={boolean("disabled", false)}
      isFullWidth={boolean("isFullWidth", false)}
      dropdownPlacement={select<PopperPositionType>("Placement", POPPER_POSITIONS as any, "bottom")}
    >
      <DropdownIconButton ariaLabel={"some text"}>
        <FontAwesomeIcon size="1x" icon={faFilePlus} />
      </DropdownIconButton>
      <DropdownContent>
        <DropdownHeader>{text("Header Text", "Header")}</DropdownHeader>
        <DropdownItem onSelect={action("onSelect for DropdownItem1")}>DropdownItem</DropdownItem>
        <DropdownItem isColor="danger" onSelect={action("onSelect for DropdownItem2")}>
          Danger!
        </DropdownItem>
        <DropdownItem onSelect={action("onSelect for DropdownItem2")} disabled={true}>
          This is item is disabled
        </DropdownItem>
        <DropdownDivider />
        <DropdownItem noStyle={true}>Plain Item1</DropdownItem>
        <DropdownItem noStyle={true}>Plain Item2</DropdownItem>
      </DropdownContent>
    </Dropdown>
  );
};
