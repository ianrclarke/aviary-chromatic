import React from "react";

import { action } from "@storybook/addon-actions";
import { boolean, select, text } from "@storybook/addon-knobs";

import { Dropdown } from "../Dropdown";
import { DropdownContent } from "../DropdownContent";
import { DropdownDivider } from "../DropdownDivider";
import { DropdownHeader } from "../DropdownHeader";
import { DropdownItem } from "../DropdownItem";
import { DropdownButton } from "../DropdownTrigger/DropdownButton";
import { OpenAction } from "../types.d";
import { POPPER_POSITIONS, PopperPositionType } from "@shared/aviary/popperPositions.d";

export const SimpleDropdown = () => {
  return (
    <Dropdown
      closeOnExternalClick={boolean("closeOnExternalClick", true)}
      closeOnItemSelect={boolean("closeOnItemSelect", true)}
      openAction={select<any>("openAction", Object.keys(OpenAction), OpenAction.click)}
      disabled={boolean("disabled", false)}
      isFullWidth={boolean("isFullWidth", false)}
      dropdownPlacement={select<PopperPositionType>("Placement", POPPER_POSITIONS as any, "bottom")}
    >
      <DropdownButton>{text("Button Text", "Click Here")}</DropdownButton>
      <DropdownContent>
        <DropdownHeader>{text("Header Text", "DropdownHeader")}</DropdownHeader>
        <DropdownItem onSelect={action("onSelect for DropdownItem1")}>DropdownItem</DropdownItem>
        <DropdownItem isColor="danger" onSelect={action("onSelect for DropdownItem2")}>
          Danger!
        </DropdownItem>
        <DropdownItem onSelect={action("onSelect for DropdownItem2")} disabled={true}>
          This is item is disabled
        </DropdownItem>
        <DropdownItem>There's a DropdownDivider between this item and the next!</DropdownItem>
        <DropdownDivider />
        <DropdownItem>Neat!</DropdownItem>
      </DropdownContent>
    </Dropdown>
  );
};
