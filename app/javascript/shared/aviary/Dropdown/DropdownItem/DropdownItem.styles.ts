import { css } from "@emotion/core";

import { colors } from "@styles";

export const dropdownItem = css`
  border: none;
  cursor: pointer;
  display: flex;
  justify-content: space-between;
  padding: 0.25rem 1rem;
  white-space: nowrap;
  width: 100%;
`;

export const color = {
  default: css`
    background: ${colors.white};
    color: ${colors.grey};

    &:hover {
      background-color: ${colors.backgroundExtraLight};
      color: ${colors.primary};
    }

    &:active,
    &:focus {
      background: ${colors.lightGrey};
    }
  `,
  danger: css`
    background: ${colors.white};
    color: ${colors.danger};

    &:hover {
      background-color: ${colors.danger};
      color: ${colors.white};
    }

    &:active,
    &:focus {
      color: ${colors.danger};
      background: ${colors.white};
    }
  `,
};

export const disabled = css`
  cursor: not-allowed;
  color: ${colors.greyLight};

  &:hover {
    background-color: inherit;
    color: ${colors.greyLight};
  }
`;

export const textDirection = {
  left: css`
    text-align: left;
  `,
  right: css`
    flex-direction: row-reverse;
    text-align: right;
  `,
};

export const spacer = css`
  align-items: center;
  display: flex;
  min-width: 2rem;
  justify-content: flex-end;
`;
