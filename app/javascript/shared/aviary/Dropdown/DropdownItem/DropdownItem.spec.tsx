import { faChevronDown } from "@fortawesome/pro-regular-svg-icons";
import { shallow } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { DropdownInternals } from "../types.d";

import { DropdownItem } from "./DropdownItem";

const mockEnterEvent = { key: "Enter" };

let mockInternalProps;
jest.mock("../DropdownContext", () => ({
  useDropdownContext: () => mockInternalProps,
}));

describe("DropdownItem", () => {
  let wrapper;

  let mockOnItemSelected;
  let mockSetItemFocused;

  let mockOnSelect;
  let mockDisabled;

  let mockIcon;

  const buildMockInternalProps = (options: Partial<DropdownInternals>) => {
    mockInternalProps = { ...options } as DropdownInternals;
  };

  const buildWrapper = () => {
    buildMockInternalProps({
      isDropdownOpen: true,
      onItemSelected: mockOnItemSelected,
      setItemFocused: mockSetItemFocused,
    });

    wrapper = shallow(
      <DropdownItem onSelect={mockOnSelect} disabled={mockDisabled} icon={mockIcon}>
        test
      </DropdownItem>
    );
  };

  beforeEach(() => {
    mockOnItemSelected = jest.fn();
    mockSetItemFocused = jest.fn();

    mockOnSelect = jest.fn();
    mockDisabled = false;

    mockIcon = null;

    buildWrapper();
  });

  baseTests(() => wrapper);

  it("fires the onItemSelected internalProp on click, passing the onSelect prop as callback", () => {
    act(() => {
      wrapper.props().onClick();
    });

    wrapper.update();

    expect(mockOnItemSelected).toHaveBeenCalledWith(mockOnSelect, { closeOnItemSelect: undefined });
  });

  it("fires the onItemSelected internalProp on enter, passing the onSelect prop as callback", () => {
    act(() => {
      wrapper.props().onKeyPress(mockEnterEvent);
    });

    wrapper.update();
    expect(mockOnItemSelected).toHaveBeenCalledWith(mockOnSelect, {});
  });

  it("fires setItemFocused on focus", () => {
    act(() => {
      wrapper.props().onFocus();
    });

    expect(mockSetItemFocused).toHaveBeenCalledWith(true);
  });

  it("fires setItemFocused on blur", () => {
    act(() => {
      wrapper.props().onBlur();
    });

    expect(mockSetItemFocused).toHaveBeenCalledWith(false);
  });

  describe("when disabled", () => {
    beforeEach(() => {
      mockDisabled = true;

      buildWrapper();
    });

    it("does not permit actions when clicked", () => {
      act(() => {
        wrapper.props().onClick();
      });

      wrapper.update();

      expect(mockOnSelect).not.toBeCalled();
      expect(mockOnItemSelected).not.toBeCalled();
    });

    it("does not permit actions when enter is pressed", () => {
      act(() => {
        wrapper.props().onKeyPress(mockEnterEvent);
      });

      wrapper.update();

      expect(mockOnSelect).not.toBeCalled();
      expect(mockOnItemSelected).not.toBeCalled();
    });
  });

  describe("Icon tests", () => {
    it("Doesn't render a FontAwesomeIcon if no icon passed", () => {
      expect(wrapper.exists("FontAwesomeIcon")).toBeFalsy();
    });
    it("Renders a fontAwesomeIcon when an icon is passed", () => {
      mockIcon = faChevronDown;
      buildWrapper();
      expect(wrapper.exists("FontAwesomeIcon")).toBeTruthy();
    });
  });
});
