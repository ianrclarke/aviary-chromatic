import { IconDefinition } from "@fortawesome/fontawesome-common-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC, HTMLProps } from "react";

import { useDropdownContext } from "../DropdownContext";

import * as styles from "./DropdownItem.styles";

interface Props extends HTMLProps<HTMLDivElement> {
  /**
   * Sets implementation of boilerplate styles
   */
  noStyle?: boolean;
  /**
   * Callback fires when component is selected
   */
  onSelect?: () => void;
  /**
   *  If true, the Dropdown will close when this item is selected.
   *  If defined, will override Dropdown component's closeOnItemSelect prop
   */
  closeOnItemSelect?: boolean;
  /**
   * Sets text justification
   *
   * @default right
   */
  textDirection?: "left" | "right";
  /**
   * sets if item can be selected
   */
  disabled?: boolean;
  /**
   * sets color theme
   *
   * @default "default"
   */
  isColor?: "default" | "danger";
  /**
   * An icon to be rendered at the end of the dropdown item
   */
  icon?: IconDefinition;
}

const DropdownItem: FC<Props> = ({
  children,
  closeOnItemSelect,
  disabled,
  isColor,
  noStyle,
  onSelect,
  textDirection,
  icon,
  ...rest
}) => {
  const { onItemSelected, setItemFocused } = useDropdownContext();

  const getTabIndex = () => (disabled ? -1 : 0);

  const clickHandler = () => {
    selectHandler();
  };

  const keyPressHandler = (event: React.KeyboardEvent<HTMLElement>) => {
    if (event.key === "Enter") {
      selectHandler();
    }
  };

  const getOptions = () => ({
    closeOnItemSelect,
  });

  const selectHandler = () => {
    if (!disabled) {
      onItemSelected(onSelect, getOptions());
    }
  };

  const handleFocus = () => {
    setItemFocused(true);
  };

  const handleBlur = () => {
    setItemFocused(false);
  };

  // This is a nasty hack to avoid the component taking outline/focus ring when user clicks on it
  const handleMouseDown = e => {
    e.preventDefault();
    if (rest.onMouseDown) {
      rest.onMouseDown(e);
    }
  };

  const conditionalStyles = () =>
    !noStyle && [
      styles.dropdownItem,
      styles.color[isColor],
      styles.textDirection[textDirection],
      disabled && styles.disabled,
    ];

  return (
    <div
      css={conditionalStyles()}
      tabIndex={getTabIndex()}
      onKeyPress={keyPressHandler}
      onClick={clickHandler}
      onMouseDown={handleMouseDown}
      onFocus={handleFocus}
      onBlur={handleBlur}
      role="button"
      {...rest}
    >
      {children}
      <div css={styles.spacer}>{icon && <FontAwesomeIcon icon={icon} />}</div>
    </div>
  );
};

DropdownItem.defaultProps = {
  isColor: "default",
  textDirection: "left",
};

export { DropdownItem };
