import React, { FC } from "react";
import { NavLink } from "react-router-dom";

import { stylesHelper } from "@styles/helpers";

import { DropdownItem } from "../DropdownItem";

import * as styles from "./DropdownLink.styles";

interface Props {
  /**
   * Sets boilerplate styling for component
   *
   * @default false
   */
  noStyle?: boolean;
  /**
   * sets use of active styling associated with matching url
   *
   *
   */
  noActiveStyling?: boolean;
  /**
   * path prop, use with links within react app
   */
  to?: string;
  /**
   * path prop, use with links leaving current react app
   */
  href?: string;
  /**
   * HTML standard
   */
  target?: string;
  /**
   * HTML standard
   */
  download?: boolean;
  /**
   * Callback fired when component selected
   */
  onSelect?: () => void;
  /**
   * data-e2e for signout link
   */
  singOutDataE2e?: string;
}

const DropdownLink: FC<Props> = ({
  children,
  to,
  noActiveStyling,
  noStyle,
  download,
  target,
  href,
  singOutDataE2e,
  ...rest
}) => {
  const conditionalStyles = () => stylesHelper([styles.dropdownLink, !noStyle]);

  const conditionalNavStyles = () => stylesHelper([styles.navLink, !noStyle]);

  if (href) {
    return (
      <DropdownItem css={conditionalStyles()} tabIndex={-1} noStyle={true} {...rest}>
        <a
          css={styles.navLink}
          tabIndex={0}
          href={href}
          target={target}
          download={download}
          data-e2e={singOutDataE2e}
        >
          {children}
        </a>
      </DropdownItem>
    );
  }

  return (
    <DropdownItem css={conditionalStyles()} tabIndex={-1} noStyle={true} {...rest}>
      <NavLink
        to={to}
        css={conditionalNavStyles()}
        exact={true}
        activeClassName={noActiveStyling ? "" : "active"}
        target={target}
        download={download}
      >
        {children}
      </NavLink>
    </DropdownItem>
  );
};

export { DropdownLink };
