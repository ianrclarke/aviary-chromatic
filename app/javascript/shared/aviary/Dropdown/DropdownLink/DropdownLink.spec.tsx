import { shallow } from "enzyme";
import React from "react";
import { NavLink } from "react-router-dom";

import { baseTests } from "@testing/baseTests";

import { DropdownLink } from "./DropdownLink";

describe("DropdownLink", () => {
  let wrapper;

  let noActiveStyling;

  const buildWrapper = () => {
    wrapper = shallow(
      <DropdownLink to={"/p/stores"} noActiveStyling={noActiveStyling}>
        test
      </DropdownLink>
    );
  };

  beforeEach(() => {
    noActiveStyling = true;

    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("functional", () => {
    it("assigns an active class name when noActiveClass is fallse", () => {
      noActiveStyling = false;
      buildWrapper();
      expect(wrapper.find(NavLink).props().activeClassName).toEqual("active");
    });

    it("does not assign an active class name when noActiveClass is true", () => {
      expect(wrapper.find(NavLink).props().activeClassName).toEqual("");
    });
  });
});
