import { css } from "@emotion/core";

import { animations, colors, typography } from "@styles";

export const dropdownLink = css`
  ${animations.transition()}
  transform-style: unset;

  cursor: pointer;
  font-size: ${typography.paragraphLong};
  width: 100%;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;

  &&& {
    color: ${colors.dark};

    &:hover,
    &:focus,
    &:focus-within {
      color: ${colors.green};
    }
  }
`;

export const navLink = css`
  display: block;
  padding: 0.5rem 1rem;
  color: ${colors.grey};
  width: 100%;
  line-height: 1.125rem;
  ${animations.transition()}
  transform-style: unset;

  &:hover,
  &:focus {
    color: ${colors.green};
    background-color: ${colors.backgroundExtraLight};
  }

  &.active {
    color: ${colors.green};
  }
`;
