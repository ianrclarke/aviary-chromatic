import { ReferenceObject } from "popper.js";
import { Dispatch, SetStateAction } from "react";

import { PopperPositionType } from "../popperPositions.d";

enum OpenAction {
  click = "click",
  hover = "hover",
}

enum EllipsesPosition {
  topRight = "topRight",
  bottomRight = "bottomRight",
  bottomLeft = "bottomLeft",
  topLeft = "topLeft",
  default = "default",
}

interface ItemSelectOptions {
  closeOnItemSelect?: boolean;
}

interface DropdownInternals {
  activeCategoryId: string;
  setActiveCategoryId: (category?: string) => void;
  isDropdownOpen: boolean;
  triggerElement?: ReferenceObject;
  setTriggerElement?: Dispatch<SetStateAction<ReferenceObject>>;
  onItemSelected?: (callback: () => void, options?: ItemSelectOptions) => void;
  onTriggerClicked?: () => void;
  openAction: OpenAction;
  mouseOverContent: boolean;
  setMouseOverContent: Dispatch<SetStateAction<boolean>>;
  mouseOverTrigger: boolean;
  setMouseOverTrigger: Dispatch<SetStateAction<boolean>>;
  triggerFocused: boolean;
  setTriggerFocused: Dispatch<SetStateAction<boolean>>;
  itemFocused: boolean;
  setItemFocused: Dispatch<SetStateAction<boolean>>;
  isDisabled: boolean;
  isFullWidth: boolean;
  ellipsesPosition: keyof typeof EllipsesPosition;
  dropdownPlacement: PopperPositionType;
}

export { DropdownInternals, OpenAction, EllipsesPosition, ItemSelectOptions };
