import { ReferenceObject } from "popper.js";
import React, { FC, HTMLAttributes, useEffect, useRef, useState } from "react";

import { useControlledState, useOutsideClick } from "@shared/hooks";
import { stylesHelper } from "@styles/helpers";

import { PopperPositionType, POPPER_POSITIONS } from "../popperPositions.d";

import { DropdownContext } from "./DropdownContext";
import { DropdownInternals, OpenAction, EllipsesPosition, ItemSelectOptions } from "./types.d";

import * as styles from "./Dropdown.styles";

interface Props extends HTMLAttributes<HTMLDivElement> {
  /**
   * Optionally control the open state of Dropdown with this boolean. Use triggerCallback as the callback that closes the dropdown
   *
   */
  isOpen?: boolean;
  /**
   * Determines what cursor behaviour prompts the dropdown to open
   *
   * @default OpenAction.click
   */
  openAction?: OpenAction;
  /**
   * Function that is fired whenever the trigger is pressed
   */
  triggerCallback?: (value: any) => void;
  /**
   * If true, the Dropdown will close when the cursor clicks anywhere outside the dropdown
   *
   * @default true
   */
  closeOnExternalClick?: boolean;
  /**
   * If true, the Dropdown will close when an item is selected
   *
   * @default true
   */
  closeOnItemSelect?: boolean;
  /**
   * Disallows the dropdown from being opened or used
   *
   * @default false
   */
  disabled?: boolean;
  /**
   * Component will stretch to fill its container
   *
   * @default false
   */
  isFullWidth?: boolean;
  /**
   * Position the ellipse absolutely to parent container so that the icon aligns to the container while still maintaining a touch point of 36px.
   * IMPORTANT!! Parent container MUST be position: relative
   *
   * @default EllipsesPosition.default
   */
  ellipsesPosition?: "topRight" | "bottomRight" | "bottomLeft" | "topLeft" | "default";
  /**
   * Position direction that the content will appear
   *
   * @default PopperPositionEnum.bottomEnd
   */
  dropdownPlacement?: PopperPositionType;
}

const defaultProps = {
  closeOnExternalClick: true,
  closeOnItemSelect: true,
  disabled: false,
  isFullWidth: false,
  openAction: OpenAction.click,
  ellipsesPosition: EllipsesPosition.default,
  dropdownPlacement: POPPER_POSITIONS["bottom-end"] as PopperPositionType,
};

const Dropdown: FC<Props> = ({
  children,
  closeOnExternalClick,
  closeOnItemSelect,
  isOpen,
  openAction,
  triggerCallback,
  disabled,
  isFullWidth,
  ellipsesPosition,
  dropdownPlacement,
  ...rest
}) => {
  const [activeCategoryId, setActiveCategoryId] = useState<string>();
  const [mouseOverContent, setMouseOverContent] = useState<boolean>(false);
  const [mouseOverTrigger, setMouseOverTrigger] = useState<boolean>(false);
  const [triggerFocused, setTriggerFocused] = useState<boolean>(false);
  const [itemFocused, setItemFocused] = useState<boolean>(false);
  const [triggerElement, setTriggerElement] = useState<ReferenceObject>(null);
  const [hoverClosed, setHoverClosed] = useState<boolean>(false);
  const [isDropdownOpen, setIsDropdownOpen] = useControlledState(!!isOpen, {
    value: isOpen,
    setValue: triggerCallback,
  });
  const timeoutRef = useRef(null);
  const dropdownRef = useRef(null);

  useOutsideClick(dropdownRef, () => {
    if (closeOnExternalClick && isDropdownOpen) {
      setIsDropdownOpen(false);
    }
  });

  useEffect(() => {
    if (openAction === OpenAction.hover) {
      if (
        (mouseOverContent || mouseOverTrigger || triggerFocused || itemFocused) &&
        !disabled &&
        !hoverClosed
      ) {
        setIsDropdownOpen(true);
        clearTimeout(timeoutRef.current);
      } else if (isDropdownOpen && !mouseOverContent) {
        timeoutRef.current = setTimeout(() => {
          setIsDropdownOpen(false);
        }, 0);
      }
    }
  }, [mouseOverContent, mouseOverTrigger, triggerFocused, itemFocused]);

  useEffect(() => {
    if (hoverClosed && !mouseOverTrigger) {
      setHoverClosed(false);
    }
  }, [mouseOverTrigger]);

  useEffect(() => {
    if (disabled) {
      setIsDropdownOpen(false);
    }
  }, [disabled]);

  const onTriggerClicked = () => {
    if (!disabled) {
      if (openAction === OpenAction.hover && isDropdownOpen === true) {
        setTriggerFocused(false);
        setHoverClosed(true);
      }
      setIsDropdownOpen(!isDropdownOpen);
    }
  };

  const onItemSelected = (itemCallback?: () => void, options?: ItemSelectOptions) => {
    const shouldClose = options?.closeOnItemSelect ?? closeOnItemSelect;

    if (shouldClose) {
      setItemFocused(false);
      setIsDropdownOpen(false);
    }

    if (itemCallback) {
      itemCallback();
    }
  };

  const onKeyDownHandler = event => {
    if (event.key === "Escape" && isDropdownOpen) {
      setIsDropdownOpen(false);
    }
  };

  const dropdownContext: DropdownInternals = {
    activeCategoryId,
    setActiveCategoryId,
    isDropdownOpen,
    mouseOverContent,
    mouseOverTrigger,
    triggerFocused,
    triggerElement,
    setTriggerElement,
    itemFocused,
    onItemSelected,
    onTriggerClicked,
    openAction,
    setMouseOverContent,
    setMouseOverTrigger,
    setTriggerFocused,
    setItemFocused,
    isDisabled: disabled,
    isFullWidth,
    ellipsesPosition,
    dropdownPlacement,
  };

  const conditionalDropdownStyles = () =>
    stylesHelper(styles.dropdown, ellipsesPosition && styles.position.kebab[ellipsesPosition], [
      [styles.isFullWidth, isFullWidth],
    ]);

  return (
    <div onKeyDown={onKeyDownHandler} ref={dropdownRef} css={conditionalDropdownStyles()} {...rest}>
      <DropdownContext.Provider value={dropdownContext}>{children}</DropdownContext.Provider>
    </div>
  );
};

Dropdown.defaultProps = defaultProps;
export { Dropdown, Props as DropdownProps };
