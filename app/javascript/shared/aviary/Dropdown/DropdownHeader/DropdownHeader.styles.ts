import { css } from "@emotion/core";

import { colors, typography } from "@styles";

export const dropdownHeader = css`
  position: sticky;
  top: 0;
  background-color: ${colors.white};
  padding: 0.3rem 3rem 0.3rem 1rem;
  font-weight: ${typography.bold};
  font-size: 1rem;
  text-align: left;
  color: ${colors.grey};
  white-space: nowrap;
`;

export const dropdownText = css``;
