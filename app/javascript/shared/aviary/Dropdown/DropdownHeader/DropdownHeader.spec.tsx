import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { DropdownHeader } from "./DropdownHeader";

describe("DropdownHeader", () => {
  let wrapper;
  let mockText;
  const buildWrapper = () => {
    wrapper = mount(<DropdownHeader>{mockText}</DropdownHeader>);
  };

  beforeEach(() => {
    mockText = "test";

    buildWrapper();
  });

  it("renders text passed as child", () => {
    expect(wrapper.text()).toContain(mockText);
  });

  baseTests(() => wrapper);
});
