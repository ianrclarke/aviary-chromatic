import React, { FC } from "react";

import { stylesHelper } from "@styles/helpers";

import { DropdownDivider } from "../DropdownDivider";

import * as styles from "./DropdownHeader.styles";

interface Props {
  /**
   * Sets implementation of boilerplate styles
   *
   * @default false
   */
  noStyle?: boolean;
  /**
   * sets inclusion of divider between header and content
   *
   * @default: true
   */
  showDivider?: boolean;
}

const defaultProps = {
  noStyle: false,
  showDivider: true,
};

const DropdownHeader: FC<Props> = ({ children, noStyle, showDivider, ...rest }) => {
  const conditionalStyles = () => stylesHelper([styles.dropdownHeader, !noStyle]);

  return (
    <>
      <div css={conditionalStyles()} {...rest}>
        {children}
      </div>
      {showDivider && <DropdownDivider />}
    </>
  );
};

DropdownHeader.defaultProps = defaultProps;
export { DropdownHeader };
