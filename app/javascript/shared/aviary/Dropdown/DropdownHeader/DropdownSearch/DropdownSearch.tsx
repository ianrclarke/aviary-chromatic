import { faSearch, faTimes } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { ChangeEvent, FC, HTMLProps } from "react";

import { isMobileAgent } from "@helpers/userAgent";
import { BasicInput } from "@shared/aviary";
import { AviaryColorProps } from "@shared/aviary/aviaryColors";

import { IconButton } from "../../../Button/IconButton";
import { DropdownHeader } from "../DropdownHeader";

import * as styles from "./DropdownSearch.styles";

interface Props extends AviaryColorProps, HTMLProps<HTMLInputElement> {
  onCancel: (event: ChangeEvent<HTMLInputElement>) => void;
}

const DropdownSearch: FC<Props> = ({ value, onCancel, ...rest }) => {
  const handleCancel = () => {
    onCancel({ target: { value: "" } } as ChangeEvent<HTMLInputElement>);
  };

  const isAutofocused = () => {
    return !isMobileAgent();
  };

  const isEmpty = !value;

  const cancelIconStyle = [styles.cancelIcon.base, isEmpty && styles.cancelIcon.hidden];

  const dropDownSearchStyle = [styles.dropdownSearch.base, isEmpty && styles.dropdownSearch.empty];

  return (
    <DropdownHeader noStyle={true} {...rest} css={dropDownSearchStyle} showDivider={false}>
      <FontAwesomeIcon css={styles.searchIcon} icon={faSearch} />
      <BasicInput
        type="text"
        css={styles.input}
        value={value}
        autoFocus={isAutofocused()}
        {...rest}
      />
      <IconButton
        data-testid="cancel-button"
        isColor="trio"
        isIcon={true}
        aria-label={"cancel button"}
        isCircular={true}
        isText={true}
        css={cancelIconStyle}
        onClick={handleCancel}
      >
        <FontAwesomeIcon icon={faTimes} />
      </IconButton>
    </DropdownHeader>
  );
};

export { DropdownSearch };
