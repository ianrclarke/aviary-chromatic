import { css } from "@emotion/core";

import { animations, colors } from "@styles";

export const dropdownSearch = {
  base: css`
    display: flex;
    flex-direction: row;
    min-width: 17rem;
    align-items: center;
    padding-top: 0;
    padding-bottom: 0;
    padding-left: 1rem;
    background-color: ${colors.white};
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom: 0.0625rem solid transparent;

    &:focus-within {
      box-shadow: none;
      border-bottom: 0.0625rem solid ${colors.green};
    }
  `,
  empty: css`
    background-color: ${colors.lightGrey};
  `,
};

export const searchIcon = css`
  margin: 0 0.5rem 0 0;
  color: ${colors.primary};
`;

export const cancelIcon = {
  base: css`
    width: 3.75rem;
  `,
  hidden: css`
    visibility: hidden;
  `,
};

export const input = css`
  border: none;
  padding: 0 0.5rem;
  transition: color 0.2s ${animations.easeOutQuad};
  font-size: 0.875rem;
  background-color: transparent;

  ::placeholder {
    color: ${colors.dark};
  }

  &:active,
  & :focus {
    border: none;
    box-shadow: none;
  }
`;
