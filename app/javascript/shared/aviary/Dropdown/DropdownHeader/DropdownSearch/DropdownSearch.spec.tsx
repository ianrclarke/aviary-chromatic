import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { DropdownSearch } from "./DropdownSearch";

describe("DropdownSearch", () => {
  let wrapper;
  let value;
  let onCancelMock;

  const buildWrapper = () => {
    onCancelMock = jest.fn();
    wrapper = shallow(<DropdownSearch value={value} onCancel={onCancelMock} />);
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => wrapper);

  it("cancel button not visible by default", () => {
    const cancelButton = wrapper.find('[data-testid="cancel-button"]');
    expect(cancelButton.get(0).props.css[1].styles.indexOf("visibility:hidden")).toBeGreaterThan(
      -1
    );
  });

  it("cancel button visible when value;", () => {
    value = "something";
    buildWrapper();
    const cancelButton = wrapper.find('[data-testid="cancel-button"]');
    expect(cancelButton.get(0).props.css[1]).toEqual(false);
  });

  it("calls onCancel", () => {
    value = "something";
    buildWrapper();
    const cancelButton = wrapper.find('[data-testid="cancel-button"]');
    cancelButton.simulate("click");
    expect(onCancelMock).toHaveBeenCalledWith({ target: { value: "" } });
  });
});
