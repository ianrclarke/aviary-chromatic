import { mount } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { Dropdown } from "./Dropdown";
import { DropdownContent } from "./DropdownContent";
import { useDropdownContext } from "./DropdownContext";
import { DropdownItem } from "./DropdownItem";
import { DropdownTrigger } from "./DropdownTrigger";
import { OpenAction } from "./types.d";

jest.useFakeTimers();

let dropdownInternals;

// workaround to expose object passed to provider
const Dumb = () => {
  dropdownInternals = useDropdownContext();
  return null;
};

describe("Dropdown", () => {
  let wrapper;

  let mockCloseOnExternalClick;
  let mockCloseOnItemSelect;
  let mockIsOpen;
  let mockTriggerCallback;
  let mockOpenAction: OpenAction;
  let mockDisabled;

  const buildWrapper = () => {
    wrapper = mount(
      <Dropdown
        closeOnExternalClick={mockCloseOnExternalClick}
        isOpen={mockIsOpen}
        openAction={mockOpenAction}
        triggerCallback={mockTriggerCallback}
        closeOnItemSelect={mockCloseOnItemSelect}
        disabled={mockDisabled}
      >
        <Dumb />
        <DropdownTrigger>trigger</DropdownTrigger>
        <DropdownContent>
          <DropdownItem>1</DropdownItem>
          <DropdownItem>2</DropdownItem>
        </DropdownContent>
      </Dropdown>
    );
  };

  beforeEach(() => {
    mockCloseOnExternalClick = true;
    mockCloseOnItemSelect = true;
    mockIsOpen = false;
    mockTriggerCallback = jest.fn();
    mockOpenAction = OpenAction.click;
    mockDisabled = false;

    buildWrapper();
  });

  baseTests(() => wrapper);

  it("fires the external 'triggerCallback' function when trigger notifies it's been clicked and the openAction is 'click'", () => {
    act(() => {
      dropdownInternals.onTriggerClicked();
    });
    wrapper.update();

    expect(mockTriggerCallback).toBeCalledTimes(1);
  });

  it("does not open when disabled is true", () => {
    mockDisabled = true;
    buildWrapper();
    act(() => {
      dropdownInternals.onTriggerClicked();
    });
    expect(dropdownInternals.isDropdownOpen).toEqual(false);
  });

  describe("when openAction === 'hover'", () => {
    beforeEach(() => {
      mockOpenAction = OpenAction.hover;
      buildWrapper();
    });

    it("calls the triggerCallback with true when mouse is over trigger", () => {
      act(() => {
        dropdownInternals.setMouseOverTrigger(true);
        jest.runAllTimers();
      });
      wrapper.update();

      expect(mockTriggerCallback).toBeCalledWith(true);
    });

    it("calls the triggerCallback with true when mouse is over content", () => {
      act(() => {
        dropdownInternals.setMouseOverContent(true);
      });
      wrapper.update();

      expect(mockTriggerCallback).toBeCalledWith(true);
    });

    it("calls the triggerCallback when mouse is neither over the content nor the trigger", () => {
      mockIsOpen = true;
      jest.useFakeTimers();

      buildWrapper();

      act(() => {
        dropdownInternals.setMouseOverTrigger(false);

        dropdownInternals.setMouseOverContent(false);

        jest.runAllTimers();
      });
      wrapper.update();

      expect(mockTriggerCallback).toBeCalledWith(false);
    });

    it("does not open when isDisabled is true", () => {
      act(() => {
        mockDisabled = true;
        buildWrapper();

        dropdownInternals.setMouseOverTrigger();

        expect(dropdownInternals.isDropdownOpen).toEqual(false);
      });
    });
  });

  describe("Context", () => {
    it("closes the dropdown when onItemSelect is called and closeOnItemSelect is true", () => {
      mockIsOpen = true;
      mockCloseOnItemSelect = true;
      mockTriggerCallback = undefined;

      buildWrapper();

      act(() => {
        dropdownInternals.onItemSelected();
      });
      wrapper.update();

      expect(dropdownInternals.isDropdownOpen).toEqual(false);
    });

    it("does not close the dropdown when onItemSelect is called and closeOnItemSelect is false", () => {
      mockIsOpen = true;
      mockCloseOnItemSelect = false;
      mockTriggerCallback = undefined;

      buildWrapper();

      act(() => {
        dropdownInternals.onItemSelected();
      });
      wrapper.update();

      expect(dropdownInternals.isDropdownOpen).toEqual(true);
    });

    it("fires the onSelect callback received as an argument when onItemSelect is called", () => {
      mockIsOpen = true;
      mockCloseOnItemSelect = false;
      mockTriggerCallback = undefined;

      const mockOnSelectCallback = jest.fn();

      buildWrapper();

      act(() => {
        dropdownInternals.onItemSelected(mockOnSelectCallback);
      });
      wrapper.update();

      expect(mockOnSelectCallback).toBeCalled();
    });

    it("closes the dropdown when onItemSelect is called and closeOnItemSelect is true", () => {
      mockTriggerCallback = undefined; // useControlledState relies on external control of state if isOpen and the callback are both defined
      mockIsOpen = true;
      mockCloseOnItemSelect = true;

      buildWrapper();

      act(() => {
        dropdownInternals.onItemSelected();
      });
      wrapper.update();

      expect(dropdownInternals.isDropdownOpen).toEqual(false);
    });

    it("closes the dropdown when onItemSelect is called and closeOnItemSelect is false", () => {
      mockTriggerCallback = undefined; // useControlledState relies on external control of state if isOpen and the callback are both defined
      mockIsOpen = true;
      mockCloseOnItemSelect = false;

      buildWrapper();

      act(() => {
        dropdownInternals.onItemSelected();
      });
      wrapper.update();

      expect(dropdownInternals.isDropdownOpen).toEqual(true);
    });
  });

  describe("closeOnExternalClick prop", () => {
    let documentOnMouseDown;

    beforeEach(() => {
      mockTriggerCallback = undefined; // useControlledState relies on external control of state if isOpen and the callback are both defined
      mockIsOpen = true;

      // eslint-disable-next-line @typescript-eslint/unbound-method
      document.addEventListener = jest.fn((event, callback) => {
        documentOnMouseDown = callback;
      });

      buildWrapper();
    });

    it("closes the dropdown when closeOnExternalClick is true and somewhere outside the dropdown is clicked ", () => {
      mockCloseOnExternalClick = true;

      buildWrapper();

      act(() => {
        documentOnMouseDown({ target: null });
      });
      wrapper.update();

      expect(dropdownInternals.isDropdownOpen).toEqual(false);
    });

    it("does not close the dropdown when closeOnExternalClick is false and somewhere outside the dropdown is clicked ", () => {
      mockCloseOnExternalClick = false;
      buildWrapper();

      act(() => {
        documentOnMouseDown({ target: null });
      });
      wrapper.update();

      expect(dropdownInternals.isDropdownOpen).toEqual(true);
    });
  });

  it("closes when open and escape is clicked", () => {
    mockIsOpen = true;
    buildWrapper();

    act(() => {
      wrapper.find("div").first().props().onKeyDown({ key: "Escape" });
    });

    wrapper.update();

    expect(mockTriggerCallback).toBeCalledWith(false);
  });
});
