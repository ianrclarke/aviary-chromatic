import React, { FC } from "react";

import * as styles from "./DropdownDivider.styles";

const DropdownDivider: FC = () => {
  return <div css={styles.dropdownDivider} />;
};

export { DropdownDivider };
