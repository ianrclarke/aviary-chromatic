import { css } from "@emotion/core";

import { colors } from "@styles";

export const dropdownDivider = css`
  width: 100%;
  height: 0.125rem;
  background-color: ${colors.backgroundLight};
`;
