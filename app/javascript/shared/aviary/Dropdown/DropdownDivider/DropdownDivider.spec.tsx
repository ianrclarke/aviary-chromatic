import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { DropdownDivider } from "./DropdownDivider";

describe("DropdownDivider", () => {
  let wrapper;

  const buildWrapper = () => {
    wrapper = mount(<DropdownDivider />);
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => wrapper);
});
