import { css } from "@emotion/core";

export const control = css`
  box-sizing: border-box;
  clear: both;
  font-size: 1rem;
  position: relative;
  text-align: left;
`;
