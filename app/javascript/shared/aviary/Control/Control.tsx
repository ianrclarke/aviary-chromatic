import React, { FC } from "react";

import * as styles from "./Control.styles";

const Control: FC = ({ children, ...rest }) => {
  return (
    <div css={styles.control} {...rest}>
      {children}
    </div>
  );
};

export { Control };
