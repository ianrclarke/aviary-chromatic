import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Control } from "./Control";

describe("Control component", () => {
  let wrapper;
  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = mount(
      <Control>
        <p>There's some text in here</p>
      </Control>
    );
  };

  baseTests(() => wrapper);
});
