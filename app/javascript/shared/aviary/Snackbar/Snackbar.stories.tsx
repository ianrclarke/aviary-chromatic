import React from "react";

import { Text, Title } from "@shared/aviary";

import { Snackbar, SnackWrap } from "./index";
import { SnackbarStoryButtons } from "./SnackbarStoryButtons";

export default {
  title: "Aviary|Snackbar",
  component: Snackbar,
};

export const Default = () => (
  <SnackWrap>
    <Snackbar />
    <div>
      <Title>Welcome to snackbar</Title>
      <Text>
        Begin by using the buttons below, the snackbar will appear from the bottom left corner
      </Text>
      <SnackbarStoryButtons />
    </div>
  </SnackWrap>
);
