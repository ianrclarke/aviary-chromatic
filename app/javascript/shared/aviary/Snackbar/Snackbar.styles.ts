import { css } from "@emotion/core";

import { animations, colors, helpers, layers, timing } from "@styles";

export const base = css`
  ${animations.transition()};
  background-color: ${helpers.hexToRgba(colors.lightGrey, 0.9)};
  opacity: 0.9;
  padding: 0.5rem 1rem;
  color: ${helpers.hexToRgba(colors.dark, 0.8)};
  border-radius: 5px;
  position: fixed;
  z-index: ${layers.indexModalMax};
  box-shadow: 0 2px 4px 0 ${helpers.hexToRgba(colors.dark, 0.2)};
`;

export const position = {
  topLeft: css`
    top: 2rem;
    left: 2rem;
  `,
  topRight: css`
    top: 2rem;
    right: 2rem;
  `,
  bottomLeft: css`
    bottom: 2rem;
    left: 2rem;
  `,
  bottomRight: css`
    bottom: 2rem;
    right: 2rem;
  `,
};

export const status = {
  normal: css`
    background-color: ${helpers.hexToRgba(colors.lightGrey, 0.9)};
  `,
  loading: css`
    background-color: ${helpers.hexToRgba(colors.lightGrey, 0.9)};
  `,
  success: css`
    background-color: ${colors.greenExtraLight};
    color: ${colors.primary};
  `,
};

export const spinner = css`
  margin-right: 0.5rem;
  animation-duration: ${timing.slow};
`;
