import React, { FC, useRef, useState } from "react";

import { SnackbarContext, SnackType, SnackPosition } from "../SnackbarContext";

const SnackWrap: FC = ({ children }) => {
  const [isSnackbarOpen, setSnackbarIsOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [status, setStatus] = useState<SnackType>();
  const [position, setPosition] = useState<SnackPosition>("bottomLeft");

  const timeout = useRef<number>();

  const setSnackbarState = (
    newStatus: SnackType,
    newMessage: string,
    isOpen: boolean,
    newPosition?: SnackPosition
  ) => {
    setStatus(newStatus);
    setMessage(newMessage);
    setSnackbarIsOpen(isOpen);
    setPosition(newPosition || "bottomLeft");
  };

  const setSnackMessage = (
    newStatus: SnackType,
    newMessage: string,
    newPosition?: SnackPosition
  ) => {
    clearTimeout(timeout.current);
    setSnackbarState(newStatus, newMessage, true, newPosition);
  };

  const closeSnack = (newMessage: string, noDelay?: boolean) => {
    if (noDelay) {
      setSnackbarState("normal", newMessage, false, position);
    } else {
      setSnackbarState("normal", newMessage, true, position);
      timeout.current = window.setTimeout(() => {
        setSnackbarState("normal", newMessage, false, position);
      }, 600);
    }
  };

  return (
    <SnackbarContext.Provider
      value={{
        isSnackbarOpen,
        closeSnack,
        message,
        status,
        setSnackMessage,
        position,
      }}
    >
      {children}
    </SnackbarContext.Provider>
  );
};

export { SnackWrap };
