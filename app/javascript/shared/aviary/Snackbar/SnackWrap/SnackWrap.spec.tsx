import { mount } from "enzyme";
import React, { FC } from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { useSnackbar } from "../useSnackbar";

import { SnackWrap } from "./SnackWrap";

jest.useFakeTimers();

describe("SnackWrap", () => {
  let snackbarInternals;
  let wrapper;

  const FakeSnack: FC = () => {
    snackbarInternals = useSnackbar();
    return null;
  };

  const buildWrapper = () => {
    wrapper = mount(
      <SnackWrap>
        <FakeSnack />
      </SnackWrap>
    );
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("functional", () => {
    const message = "Saving";
    const status = "loading";

    describe("setSnackMessage", () => {
      beforeEach(() => {
        act(() => {
          snackbarInternals.setSnackMessage(status, message);
        });
      });

      it("opens the Snackbar", () => {
        expect(snackbarInternals.isSnackbarOpen).toEqual(true);
      });

      it("updates the snack message", () => {
        expect(snackbarInternals.message).toEqual(message);
      });

      it("updates the snack message status", () => {
        expect(snackbarInternals.status).toEqual(status);
      });
    });

    describe("closeSnack with timeout event", () => {
      beforeEach(() => {
        act(() => {
          snackbarInternals.closeSnack("Saved!");
        });
      });

      it("does not close the snackbar before 600ms has passed", () => {
        act(() => {
          jest.advanceTimersByTime(599);
        });
        wrapper.update();
        expect(snackbarInternals.isSnackbarOpen).toEqual(true);
      });

      it("closes the snackbar after the timer completes", () => {
        act(() => {
          jest.runAllTimers();
        });
        wrapper.update();
        expect(snackbarInternals.isSnackbarOpen).toEqual(false);
      });

      it("clears the snack message", () => {
        expect(snackbarInternals.message).toEqual("Saved!");
      });

      it("sets the status back to normal", () => {
        expect(snackbarInternals.status).toEqual("normal");
      });
    });
  });
});
