import React, { FC } from "react";

import { Button, ButtonGroup } from "@shared/aviary";

import { SnackPosition } from "./SnackbarContext";

import { useSnackbar } from "./index";

const SnackbarStoryButtons: FC = () => {
  const { closeSnack, setSnackMessage } = useSnackbar();

  const unwrapSnack = () => {
    setSnackMessage("loading", "Saving...");
  };

  const throwItOut = () => {
    setSnackMessage("success", "Saved");
    closeSnack("Saved");
  };

  const makePositionalSnackbar = (position: SnackPosition) => {
    setSnackMessage("success", "Saved", position);
  };

  return (
    <div>
      <ButtonGroup>
        <Button onClick={unwrapSnack}>Open snackbar</Button>
        <Button onClick={throwItOut}>Close snackbar</Button>
      </ButtonGroup>
      <br />
      <ButtonGroup>
        <Button onClick={() => makePositionalSnackbar("topLeft")}>Top left snackbar</Button>
        <Button onClick={() => makePositionalSnackbar("topRight")}>Top right snackbar</Button>
        <Button onClick={() => makePositionalSnackbar("bottomLeft")}>Bottom left snackbar</Button>
        <Button onClick={() => makePositionalSnackbar("bottomRight")}>Bottom right snackbar</Button>
      </ButtonGroup>
    </div>
  );
};

export { SnackbarStoryButtons };
