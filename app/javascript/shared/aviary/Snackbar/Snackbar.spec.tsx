import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Snackbar } from "./Snackbar";

let mockIsSnackbarOpen;
let mockSetSnackbarOpen;
let mockMessage;
let mockStatus;
let mockPosition;

jest.mock("./useSnackbar", () => ({
  useSnackbar: () => ({
    isSnackbarOpen: mockIsSnackbarOpen,
    setSnackbarIsOpen: mockSetSnackbarOpen,
    position: mockPosition,
    message: mockMessage,
    status: mockStatus,
  }),
}));

describe("Snackbar", () => {
  let wrapper;

  const buildWrapper = () => {
    wrapper = mount(<Snackbar />);
  };

  beforeEach(() => {
    mockSetSnackbarOpen = jest.fn();
    mockIsSnackbarOpen = true;
    mockMessage = "Default message";
    mockStatus = "success";
    mockPosition = "bottomLeft";
    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("functional", () => {
    describe("when Snackbar is closed", () => {
      beforeEach(() => {
        mockIsSnackbarOpen = false;
        buildWrapper();
      });
      it("does not render it is closed", () => {
        expect(wrapper.exists('[data-testid="snackbar-wrapper"]')).toEqual(false);
      });
    });

    describe("when Snackbar is open", () => {
      it("renders whenever if it open", () => {
        expect(wrapper.exists('[data-testid="snackbar-wrapper"]')).toEqual(true);
      });
    });
  });
});
