import { useContext } from "react";

import { SnackbarContext, SnackbarData } from "./SnackbarContext";

const useSnackbar = (): SnackbarData => {
  return useContext(SnackbarContext);
};

export { useSnackbar };
