import { faBell, faCheckCircle, faSpinnerThird } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC, HTMLProps } from "react";
import { animated, useTransition } from "react-spring";

import { useSnackbar } from "./useSnackbar";

import * as styles from "./Snackbar.styles";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Snackbar: FC<HTMLProps<HTMLDivElement>> = ({ children, ...rest }) => {
  const { isSnackbarOpen, status, message, position } = useSnackbar();

  const direction = position.includes("Left") ? "-80px" : "80px";

  const transitions = useTransition(isSnackbarOpen, null, {
    from: { transform: `translate3d(${direction}, 0,0)`, opacity: 0 },
    enter: { transform: "translate3d(0, 0,0)", opacity: 1 },
    leave: { transform: "translate3d(0, 0,0)", opacity: 0 },
  });

  const snackbarStyles = () => {
    return [styles.base, styles.status[status], styles.position[position]];
  };

  const renderAnimatedDiv = elements => {
    return transitions.map(({ item, key, props: animatedProps }) => {
      return (
        item && (
          <animated.div key={key} style={animatedProps} css={snackbarStyles()}>
            {elements}
          </animated.div>
        )
      );
    });
  };

  const renderIcon = () => {
    if (status === "normal") {
      return <FontAwesomeIcon size="sm" icon={faBell} css={styles.spinner} />;
    }
    if (status === "loading") {
      return <FontAwesomeIcon size="sm" spin={true} icon={faSpinnerThird} css={styles.spinner} />;
    }
    if (status === "success") {
      return <FontAwesomeIcon size="sm" icon={faCheckCircle} css={styles.spinner} />;
    }
  };

  return (
    <>
      {renderAnimatedDiv(
        <div {...rest} data-testid="snackbar-wrapper">
          {renderIcon()}
          {message}
        </div>
      )}
    </>
  );
};

export { Snackbar };
