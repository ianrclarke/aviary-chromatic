import React, { useState } from "react";

import { Size } from "@shared/types/sizes.d";
import { boolean, select, text } from "@storybook/addon-knobs";
import { Toggle } from "./Toggle";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=960%3A66",
};

export default {
  title: "Aviary|Toggle",
  component: Toggle,
  parameters: { design: componentDesign },
};

export const Default = () => {
  const [isOn, setIsOn] = useState(true);

  return (
    <Toggle
      ariaLabel="toggleForStorybook"
      isDisabled={boolean("Disabled", false)}
      isLoading={boolean("Loading", false)}
      isOn={isOn}
      isSize={select("Size", Size, Size.medium)}
      onLabel={text("On Label", "On")}
      offLabel={text("Off Label", "Off")}
      onClick={() => setIsOn(!isOn)}
    />
  );
};
