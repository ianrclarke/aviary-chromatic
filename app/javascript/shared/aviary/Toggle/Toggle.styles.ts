import { css } from "@emotion/core";

import { Size } from "@shared/types/sizes.d";
import { animations, baseStyles, colors, helpers } from "@styles";
import * as timing from "@styles/emotion-styles/timing";

const MEDIUM_TRACK_HEIGHT = "1.5rem";
const SMALL_TRACK_HEIGHT = "1.24rem";
const BALL_GAP_FROM_EDGE = "0.5rem";
const MEDIUM_WIDTH = "6rem";
const SMALL_WIDTH = "5rem";
const MEDIUM_HEIGHT = "1.625rem";
const SMALL_HEIGHT = "1.25rem";

export const track = {
  base: css`
    background-color: ${colors.primary};
    border-radius: 1rem;
    height: ${MEDIUM_TRACK_HEIGHT};
    width: 3rem;

    &:active,
    &:focus {
      background-color: ${colors.primary};
      box-shadow: 0 0 0 0.25rem ${helpers.hexToRgba(colors.primary, 0.15)};
    }
  `,
  isOff: css`
    background-color: ${colors.legacy.border};
    box-shadow: none;

    &:hover,
    &:active {
      background-color: ${colors.legacy.border};
    }
  `,
  [Size.small]: css`
    height: ${SMALL_TRACK_HEIGHT};
    width: 2.25rem;
  `,
  hasHoverStyle: css`
    &:hover {
      background-color: ${colors.primaryHover};
    }
  `,
};

export const ball = {
  base: css`
    width: ${MEDIUM_TRACK_HEIGHT};
    height: ${MEDIUM_TRACK_HEIGHT};
    box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14),
      0px 1px 3px 0px rgba(0, 0, 0, 0.12);
    border-radius: 50%;
    position: absolute;
    left: ${BALL_GAP_FROM_EDGE};
    background-color: ${colors.white};
    transition: left ${timing.ultraFast} ${animations.easeInOutQuad} 0ms,
      transform ${timing.ultraFast} ${animations.easeInOutQuad} 0ms;
  `,
  isOn: css`
    left: 50%;
  `,
  [Size.small]: css`
    width: ${SMALL_TRACK_HEIGHT};
    height: ${SMALL_TRACK_HEIGHT};
  `,
};

export const toggle = {
  base: css`
    ${baseStyles.controlBase};
    align-content: center;
    justify-content: center;
    height: ${MEDIUM_HEIGHT};
    width: 4.25rem;
    display: flex;
  `,
  [Size.small]: css`
    height: ${SMALL_HEIGHT};
    width: 3.5rem;
  `,
};

export const toggleWrapper = {
  base: css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: ${MEDIUM_WIDTH};
    cursor: pointer;
    color: ${colors.grey};

    &:focus,
    &:focus-within {
      outline: 1px dotted #212121; // Firefox, IE and Edge outline styles
      outline: 5px auto -webkit-focus-ring-color; // Everybody else outline style
    }
  `,
  isDisabled: css`
    cursor: not-allowed;
  `,
  isLoading: css`
    cursor: not-allowed;
  `,
  [Size.small]: css`
    width: ${SMALL_WIDTH};
  `,
};

export const labelStyle = css`
  align-self: center;
`;
