import { faSync } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC, HTMLProps } from "react";

import { Size } from "@shared/types/sizes.d";
import { stylesHelper } from "@styles/emotion-styles/helpers/stylesHelper";
import { visuallyHidden } from "@styles/emotion-styles/utilities";

import * as styles from "./Toggle.styles";

const defaultProps = {
  isSize: Size.medium,
  onLabel: "On",
  offLabel: "Off",
};

interface Props extends HTMLProps<HTMLInputElement> {
  isLoading?: boolean;
  isOn?: boolean;
  isDisabled?: boolean;
  isSize?: Size;
  onLabel?: string;
  offLabel?: string;
  ariaLabel: string;
}

const Toggle: FC<Props> = ({
  isLoading,
  isOn,
  isSize,
  isDisabled,
  onLabel,
  offLabel,
  onClick,
  ariaLabel,
  ...rest
}) => {
  const getTrackStyle = stylesHelper(
    styles.track.base,
    styles.track[isSize],
    [styles.track.isOff, !isOn || isDisabled],
    [styles.track.hasHoverStyle, !isLoading && !isDisabled && isOn]
  );

  const getBallStyle = stylesHelper(styles.ball.base, styles.ball[isSize], [
    styles.ball.isOn,
    isOn,
  ]);

  const getToggleStyle = stylesHelper(styles.toggle.base, styles.toggle[isSize]);

  const getToggleWrapperStyle = stylesHelper(
    styles.toggleWrapper.base,
    styles.toggleWrapper[isSize],
    [styles.toggleWrapper.isDisabled, isDisabled],
    [styles.toggleWrapper.isLoading, isLoading]
  );

  const getImageSize = isSize === Size.small ? "sm" : "1x";

  const renderLabel = () => {
    if (isLoading) {
      return (
        <div css={styles.labelStyle}>
          <FontAwesomeIcon size={getImageSize} icon={faSync} spin={true} />
        </div>
      );
    }
    return isOn ? onLabel : offLabel;
  };

  const handleClick = event => {
    if (!isLoading && !isDisabled) {
      onClick(event);
    }
  };

  return (
    <div css={getToggleWrapperStyle} onClick={handleClick}>
      <input
        aria-label={ariaLabel}
        css={visuallyHidden}
        type="checkbox"
        disabled={isDisabled}
        {...rest}
        aria-checked={isOn}
      />
      <div css={getToggleStyle}>
        <div css={getTrackStyle} />
        <div css={getBallStyle} />
      </div>
      {renderLabel()}
    </div>
  );
};

Toggle.defaultProps = defaultProps;

export { Toggle, Props as ButtonProps };
