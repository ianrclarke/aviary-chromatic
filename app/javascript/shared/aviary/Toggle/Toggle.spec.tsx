import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Toggle } from "./Toggle";

let toggle;

describe("Toggle", () => {
  const buildWrapper = props => {
    if (!toggle) {
      toggle = shallow(<Toggle {...props} />);
    }
    return toggle;
  };

  afterEach(() => {
    toggle = null;
  });

  baseTests(() => buildWrapper({}));

  describe("when on", () => {
    const propsToPass = { isOn: true };
    it("right label shown", () => {
      expect(buildWrapper(propsToPass).text()).toEqual("On");
    });

    it("input has checked value", () => {
      expect(buildWrapper(propsToPass).find("input").prop("aria-checked")).toEqual(true);
    });
  });

  describe("when off", () => {
    const propsToPass = { isOn: false };
    it("right label shown", () => {
      expect(buildWrapper(propsToPass).text()).toEqual("Off");
    });

    it("input has checked value", () => {
      expect(buildWrapper(propsToPass).find("input").prop("aria-checked")).toEqual(false);
    });
  });

  describe("when loading", () => {
    const propsToPass = { isLoading: true };
    it("loading icon shown", () => {
      expect(buildWrapper(propsToPass).find(FontAwesomeIcon)).toHaveLength(1);
    });
  });
});
