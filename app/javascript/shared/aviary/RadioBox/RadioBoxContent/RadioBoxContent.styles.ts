import { css } from "@emotion/core";

export const visible = css`
  margin-top: 0.5rem;
`;
