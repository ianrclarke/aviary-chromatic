import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { RadioBoxContent } from "./RadioBoxContent";

describe("RadioBoxContent", () => {
  let wrapper;
  let isCollapsible = false;
  let checked = false;

  const buildWrapper = () => {
    wrapper = mount(<RadioBoxContent isCollapsible={isCollapsible} checked={checked} />);
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("functionality", () => {
    it("renders content when !isCollapsible", () => {
      expect(wrapper.exists('[data-testid="radiobox-content"]')).toBeTruthy();
    });
    it("renders content when isCollapsible and checked", () => {
      isCollapsible = true;
      checked = true;
      buildWrapper();
      expect(wrapper.exists('[data-testid="radiobox-content"]')).toBeTruthy();
    });
    it("does not render content when isCollapsible and !checked", () => {
      isCollapsible = true;
      checked = false;
      buildWrapper();
      expect(wrapper.exists('[data-testid="radiobox-content"]')).toBeFalsy();
    });
  });
});
