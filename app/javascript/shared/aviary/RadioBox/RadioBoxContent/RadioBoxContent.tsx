import React, { FC } from "react";

import * as styles from "./RadioBoxContent.styles";

interface Props extends React.HTMLProps<HTMLInputElement> {
  isCollapsible?: boolean;
}

const RadioBoxContent: FC<Props> = ({ checked, isCollapsible, children }) => {
  const activeStyle = () => {
    if (isCollapsible && checked) {
      return styles.visible;
    }
    if (!isCollapsible) {
      return styles.visible;
    }
    return null;
  };

  const renderChildren = () => {
    if (isCollapsible && !checked) {
      return null;
    }
    return (
      <div css={activeStyle()} data-testid="radiobox-content">
        {children}
      </div>
    );
  };

  return <>{renderChildren()}</>;
};

export { RadioBoxContent };
