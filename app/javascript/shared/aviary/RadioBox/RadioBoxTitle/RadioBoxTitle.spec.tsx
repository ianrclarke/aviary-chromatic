import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { RadioBoxTitle } from "./RadioBoxTitle";

describe("RadioBoxTitle", () => {
  let wrapper;

  const buildWrapper = () => {
    wrapper = mount(<RadioBoxTitle />);
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => wrapper);
});
