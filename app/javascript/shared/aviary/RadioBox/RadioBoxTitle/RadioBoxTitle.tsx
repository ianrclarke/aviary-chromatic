import React, { FC } from "react";

import { Title } from "@shared/aviary";

import * as styles from "./RadioBoxTitle.styles";

const RadioBoxTitle: FC = ({ children }) => {
  return (
    <Title type="h5" css={styles.radioTitle}>
      {children}
    </Title>
  );
};

export { RadioBoxTitle };
