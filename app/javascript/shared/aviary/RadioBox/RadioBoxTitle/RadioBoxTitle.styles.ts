import { css } from "@emotion/core";

import { colors, typography } from "@styles";

export const radioTitle = css`
  &&& {
    color: ${colors.grey};
    margin: 0;
  }
  em {
    color: ${colors.primary};
    font-style: ${typography.regular};
  }
`;
