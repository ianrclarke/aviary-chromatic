import { css } from "@emotion/core";

import { colors, dimensions, helpers } from "@styles";

export const radio = {
  base: css`
    display: block;
    border: 1px solid ${colors.legacy.border};
    padding: 1rem;
    margin: 0.75rem 0 0 0;
    border-radius: ${dimensions.borderRadius};
    position: relative;
    cursor: pointer;
    .custom-radio {
      align-items: flex-start;
    }
    && label {
      display: flex;
    }
    && .radio > span {
      margin-top: 0.25rem;
      width: 2.25rem;
      height: 1rem;
    }
    p {
      margin-bottom: 0.5rem;
    }
    &:focus {
      outline: none;
      box-shadow: 0 0 0 0.25rem ${helpers.hexToRgba(colors.primary, 0.65)};
    }
  `,
  selected: css`
    border-color: ${colors.primary};
    box-shadow: 0px 2px 10px ${helpers.hexToRgba(colors.grey, 0.25)};
  `,
};
