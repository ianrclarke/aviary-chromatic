import React, { useState } from "react";

import { css } from "@emotion/core";

import { boolean } from "@storybook/addon-knobs";

import { RadioBox } from ".";
import { RadioBoxContent } from "./RadioBoxContent";
import { RadioBoxTitle } from "./RadioBoxTitle";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=383%3A628",
};

export default {
  title: "Aviary|RadioBox",
  component: RadioBox,
  parameters: { design: componentDesign },
};

export const Default = () => {
  const [value, setValue] = useState(1);
  const isCollapsible = boolean("isCollapsible", true);

  const renderNonCollapsibleContent = () =>
    isCollapsible && (
      <p
        css={css`
          margin-top: 0.5rem;
        `}
      >
        Some non-collapsible content
      </p>
    );

  return (
    <div>
      <RadioBox
        name="testname"
        onChange={() => setValue(1)}
        checked={value === 1}
        isCollapsible={isCollapsible}
      >
        <RadioBoxTitle>Title 1</RadioBoxTitle>
        {renderNonCollapsibleContent()}
        <RadioBoxContent>collapsed content</RadioBoxContent>
      </RadioBox>
      <RadioBox
        name="testname"
        onChange={() => setValue(2)}
        checked={value === 2}
        isCollapsible={isCollapsible}
      >
        <RadioBoxTitle>Title 2</RadioBoxTitle>
        {renderNonCollapsibleContent()}
        some other text based content
        <RadioBoxContent>collapsed content</RadioBoxContent>
      </RadioBox>
      value is {value}
    </div>
  );
};
