import { SerializedStyles } from "@emotion/core";
import React, { Children, cloneElement, FC } from "react";

import { Radio } from "@shared/aviary/Radio";
import { RadioBoxContent } from "@shared/aviary/RadioBox/RadioBoxContent";
import { RadioBoxTitle } from "@shared/aviary/RadioBox/RadioBoxTitle";

import * as styles from "./RadioBox.styles";

const RadioBoxContentType = (<RadioBoxContent />).type;
const RadioBoxTitleType = (<RadioBoxTitle />).type;

interface Props extends React.HTMLProps<HTMLInputElement> {
  onChange: (value) => void;
  css?: SerializedStyles;
  isCollapsible?: boolean;
}

const RadioBox: FC<Props> = ({
  checked,
  name,
  onChange,
  children,
  value,
  css,
  isCollapsible,
  ...rest
}) => {
  const onClick = () => {
    onChange(value);
  };

  const onKeyDown = e => {
    if (e.key === "Enter") {
      onChange(value);
    }
  };

  const childs = Children.map(children, child => {
    if (!React.isValidElement(child)) {
      return null;
    }
    if (child.type === RadioBoxContentType) {
      return cloneElement(child, { checked, isCollapsible, ...child.props });
    }
    if (child.type === RadioBoxTitleType) {
      return cloneElement(child, { checked, isCollapsible, ...child.props });
    }
    return child;
  });

  const radioBoxStyle = [styles.radio.base, checked && styles.radio.selected, css];

  return (
    <div
      className="legacy-radiobox"
      css={radioBoxStyle}
      onClick={onClick}
      onKeyDown={onKeyDown}
      tabIndex={0}
    >
      <Radio
        className="legacy-radio"
        name={name}
        checked={checked}
        onChange={e => onChange(e.currentTarget.value)}
        tabIndex={-1}
        {...rest}
      >
        {childs}
      </Radio>
    </div>
  );
};

export { RadioBox };
