import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { RadioBox } from "./RadioBox";
import { RadioBoxContent } from "./RadioBoxContent";

describe("RadioBox", () => {
  const onChange = jest.fn();

  baseTests(() => mount(<RadioBox label="test label" name="test" onChange={onChange} />));

  it("includes the checked prop on children that are of type RadioBoxContent", () => {
    const wrapper = mount(
      <RadioBox label="test label" name="test" onChange={onChange} checked={true}>
        <RadioBoxContent>blah</RadioBoxContent>
      </RadioBox>
    );
    expect(wrapper.find(RadioBoxContent).prop("checked")).toBeDefined();
  });
});
