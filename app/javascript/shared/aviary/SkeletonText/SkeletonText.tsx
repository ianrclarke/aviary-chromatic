import React from "react";

import * as styles from "./SkeletonText.styles";

interface Props {
  type?:
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "h5"
    | "h6"
    | "subtitle"
    | "paragraph"
    | "small"
    | "short"
    | "long";
  width?: "fullwidth" | "small" | "medium" | "wide" | "tag";
  margin?:
    | "bottom"
    | "bottomLarge"
    | "bottomButton"
    | "top"
    | "topLarge"
    | "topButton"
    | "topAndBottom";
}

const SkeletonText: React.FC<Props> = ({ type, width, margin, ...rest }) => {
  return (
    <div
      css={[
        styles.skeletonText.base,
        styles.skeletonText.type[type],
        styles.skeletonText.width[width],
        styles.skeletonText.margin[margin],
      ]}
      {...rest}
    />
  );
};

export { SkeletonText };
