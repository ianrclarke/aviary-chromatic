import { css } from "@emotion/core";

import { animations, colors, typography } from "@styles";

export const skeletonText = {
  base: css`
    ${animations.skeletonPulse};
    background-color: ${colors.grey};
    min-width: 5rem;
    width: 30%;
    border-radius: 3px;
  `,
  type: {
    h1: css`
      height: ${typography.h1};
    `,
    h2: css`
      height: ${typography.h2};
    `,
    h3: css`
      height: ${typography.h3};
    `,
    h4: css`
      height: ${typography.h4};
    `,
    h5: css`
      height: ${typography.h5};
    `,
    subtitle: css`
      height: ${typography.h5};
    `,
    paragraph: css`
      height: ${typography.paragraphLong};
    `,
    small: css`
      height: ${typography.small};
    `,
    short: css`
      height: ${typography.paragraphShort};
    `,
    long: css`
      height: ${typography.paragraphLong};
    `,
  },
  width: {
    fullwidth: css`
      width: 100%;
    `,
    small: css`
      width: 6rem;
    `,
    medium: css`
      min-width: 6rem;
      width: 40%;
    `,
    wide: css`
      min-width: 8rem;
      width: 60%;
    `,
    tag: css`
      width: 4rem;
    `,
  },
  margin: {
    bottom: css`
      margin-bottom: 0.5rem;
    `,
    bottomLarge: css`
      margin-bottom: 1rem;
    `,
    bottomButton: css`
      margin-bottom: 1.5rem;
    `,
    top: css`
      margin-top: 0.5rem;
    `,
    topLarge: css`
      margin-top: 1rem;
    `,
    topButton: css`
      margin-top: 1.5rem;
    `,
    topAndBottom: css`
      margin-bottom: 0.5rem;
      margin-top: 0.5rem;
    `,
  },
};
