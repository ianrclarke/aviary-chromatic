import { select } from "@storybook/addon-knobs";
import React from "react";

import { SkeletonText } from "./SkeletonText";

export default {
  title: "Aviary|SkeletonText",
  component: SkeletonText,
};

export const Default = () => (
  <SkeletonText
    type={select(
      "type",
      ["h1", "h2", "h3", "h4", "h5", "subtitle", "paragraph", "small", "short", "long"],
      "h1"
    )}
    width={select("width", ["fullwidth", "small", "medium", "wide"], "small")}
    margin={select("margin", ["bottom", "top", "topAndBottom"], "topAndBottom")}
  />
);
