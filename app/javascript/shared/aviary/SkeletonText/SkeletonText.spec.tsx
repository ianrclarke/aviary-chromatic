import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { SkeletonText } from "./SkeletonText";

describe("SkeletonText tests", () => {
  let textSkeleton;

  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    textSkeleton = mount(<SkeletonText />);
  };

  baseTests(() => textSkeleton);
});
