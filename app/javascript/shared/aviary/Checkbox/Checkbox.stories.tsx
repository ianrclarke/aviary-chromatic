import React from "react";

import { boolean, select, text } from "@storybook/addon-knobs";

import { bulmaColorProps } from "@shared/types/bulmaColors.d";
import { Checkbox } from "./Checkbox";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=960%3A66",
};

export default {
  title: "Aviary|Checkbox",
  component: Checkbox,
  parameters: { design: componentDesign },
};

export const Default = () => (
  <Checkbox
    isColor={select<any>("isColor", Object.keys(bulmaColorProps), bulmaColorProps.primary)}
    disabled={boolean("Disabled", false)}
  >
    {text("Label", "This is a label")}
  </Checkbox>
);
