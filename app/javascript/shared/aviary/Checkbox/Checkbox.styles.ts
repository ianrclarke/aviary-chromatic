import { css } from "@emotion/core";

import { animations, colors, dimensions } from "@styles";
import { ColorProfile } from "@styles/emotion-styles/colorProfiles/colorProfile";
import { hexToRgba } from "@styles/helpers";

export const checkbox = css`
  display: inline-flex;
  align-items: center;
  cursor: pointer;
  justify-content: flex-start;
`;

export const checkboxInput = {
  base: css`
    position: absolute;
    left: -9999px;

    &:checked + div {
      > svg {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
      }
    }
  `,
  color: (profile: ColorProfile) => css`
    &:focus + div,
    &:active + div {
      border-color: ${profile.activeColor};
      box-shadow: 0 0 0 0.15em ${hexToRgba(profile.activeColor, 0.25)};
    }

    &:checked + div {
      background-color: ${profile.baseColor};
    }
  `,
};

export const iconContainer = {
  base: css`
    ${animations.transition()};
    cursor: pointer;
    width: 1.125rem;
    height: 1.125rem;
    border-radius: ${dimensions.borderRadius};
    display: flex;
    justify-content: center;
    align-items: center;
    border: 1px solid;
  `,
  color: (profile: ColorProfile) => css`
    border-color: ${profile.baseColor};
  `,
  disabled: css`
    &&& {
      border-color: ${colors.background};
      background-color: ${colors.backgroundLight};
    }
  `,
};

export const checkboxContent = {
  base: css`
    margin-left: 0.875rem;
  `,
  disabled: css`
    color: ${colors.lightGreen};
  `,
};

export const icon = {
  base: css`
    ${animations.transition()};
    color: ${colors.white};
    height: 0.75rem;
    opacity: 0;
    -webkit-transform: scale(0);
    transform: scale(0);
  `,
  disabled: css`
    &&& {
      color: ${colors.background};
    }
  `,
};
