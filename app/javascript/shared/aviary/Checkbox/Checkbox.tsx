import { faCheck } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC, HTMLProps } from "react";

import { BulmaColorProps, bulmaColors } from "@shared/types/bulmaColors";
import { profiles } from "@styles";
import { stylesHelper } from "@styles/helpers";

import * as styles from "./Checkbox.styles";

interface BulmaCheckboxProps extends BulmaColorProps, HTMLProps<HTMLInputElement> {}

const defaultColor: bulmaColors = "primary";

const Checkbox: FC<BulmaCheckboxProps> = ({ isColor, children, disabled, id, ...rest }) => {
  const stringToProfile = {
    primary: profiles.primaryProfile,
    secondary: profiles.darkProfile,
    danger: profiles.dangerProfile,
  };

  const checkboxInputStyles = () => {
    return [styles.checkboxInput.base, styles.checkboxInput.color(stringToProfile[isColor])];
  };

  const iconContainerStyles = () => {
    return stylesHelper(
      styles.iconContainer.base,
      styles.iconContainer.color(stringToProfile[isColor]),
      [styles.iconContainer.disabled, disabled]
    );
  };

  const checkboxContentStyles = () => {
    return stylesHelper(styles.checkboxContent.base, [styles.checkboxContent.disabled, disabled]);
  };

  const iconStyles = () => {
    return stylesHelper(styles.icon.base, [styles.icon.disabled, disabled]);
  };

  return (
    <>
      <label htmlFor={id} css={styles.checkbox}>
        <input id={id} css={checkboxInputStyles()} type="checkbox" disabled={disabled} {...rest} />
        <div css={iconContainerStyles()}>
          <FontAwesomeIcon css={iconStyles()} icon={faCheck} />
        </div>
        <div css={checkboxContentStyles()}>{children}</div>
      </label>
    </>
  );
};

Checkbox.defaultProps = {
  isColor: defaultColor,
};

export { Checkbox };
