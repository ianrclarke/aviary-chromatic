import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Checkbox } from "./Checkbox";

describe("Checkbox", () => {
  let wrapper;
  let checked;
  let disabled;
  let onChange;

  const Child = () => <div />;

  const buildWrapper = () => {
    return mount(
      <Checkbox {...{ checked, onChange, disabled }}>
        <Child />
      </Checkbox>
    );
  };

  beforeEach(() => {
    checked = false;
    disabled = false;
    onChange = jest.fn();
    wrapper = buildWrapper();
  });

  baseTests(() => wrapper);

  describe("Functionality", () => {
    it("renders passed children", () => {
      expect(wrapper.exists(Child)).toEqual(true);
    });

    describe("when selected", () => {
      it("invokes onChange", () => {
        wrapper.find("input").simulate("change");
        expect(onChange).toHaveBeenCalled();
      });
    });

    describe("when disabled is passed", () => {
      it("is disabled", () => {
        disabled = true;
        wrapper = buildWrapper();
        expect(wrapper.find("input").prop("disabled")).toEqual(true);
      });
    });

    describe("when disabled is blank", () => {
      it("is not disabled", () => {
        expect(wrapper.find("input").prop("disabled")).toEqual(false);
      });
    });

    describe("when checked is passed", () => {
      it("is selected", () => {
        checked = true;
        wrapper = buildWrapper();
        expect(wrapper.find("input").prop("checked")).toEqual(true);
      });
    });

    describe("when checked is blank", () => {
      it("is not selected", () => {
        expect(wrapper.find("input").prop("checked")).toEqual(false);
      });
    });
  });
});
