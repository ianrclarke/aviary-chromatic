import { css } from "@emotion/core";

import { colors, baseStyles, dimensions } from "@styles";

export const datePicker = css`
  width: 100%;

  &:hover {
    & .MuiFormLabel-root {
      color: ${colors.dark};
    }
  }

  // This is the override for the Modal variant
  & .MuiBackdrop-root {
    background: ${colors.backgroundModal};
  }

  // This is the override for the Popover variant
  & .MuiPaper-root {
    ${baseStyles.modalStyles};
  }

  // This is the override for the Popover variant
  & .MuiDialog-paper {
    ${baseStyles.modalStyles};
    background-color: ${colors.white};
  }

  & .MuiPickersBasePicker-pickerView {
    @media only screen and (max-width: ${dimensions.phoneSmall}) {
      padding-right: 1rem;
    }
  }

  & .MuiPickersToolbarText-toolbarTxt,
  .MuiPickersDay-daySelected {
    color: ${colors.white};
  }

  & .MuiPickersToolbarText-toolbarBtnSelected {
    color: ${colors.white};
    text-decoration: underline;
  }

  & .MuiFormLabel-root {
    color: ${colors.grey};
    font-size: 1rem;
  }

  & .MuiFormControl-root.MuiTextField-root {
    width: 100%;
  }

  & .MuiFormLabel-root.MuiFormLabel-filled,
  .MuiFormLabel-root.Mui-focused {
    color: ${colors.blue};
    font-size: 1.2rem;
  }

  & .MuiInput-underline:after,
  .MuiInput-underline:before,
  .MuiInput-underline:hover:not(.Mui-disabled):before {
    border-bottom: 1px solid ${colors.lightGreen};
  }
`;
