import React, { useState } from "react";
import { faTimesCircle } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { css } from "@emotion/core";

import { select, text } from "@storybook/addon-knobs";

import { DatePicker } from "./DatePicker";
import { IconButton } from "../Button/IconButton";

export default {
  title: "Aviary|DatePicker",
  component: DatePicker,
};

export const Default = () => {
  const [date, setDate] = useState<Date>(new Date());

  return (
    <div
      css={css`
        display: flex;
        align-items: flex-end;
      `}
    >
      <DatePicker
        label={text("Label", "Date of birth")}
        value={date}
        variant={select("variant", ["inline", "dialog"], "inline")}
        onChange={date => setDate(date)}
      />
      {date && (
        <IconButton
          isColor={"specialty"}
          isCircular={true}
          isText={true}
          isSize={"small"}
          aria-label={"yes"}
          onClick={() => setDate(null)}
        >
          <FontAwesomeIcon icon={faTimesCircle} />
        </IconButton>
      )}
    </div>
  );
};
