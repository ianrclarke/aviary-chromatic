import DateFnsUtils from "@date-io/date-fns";
import { DatePicker as MuiDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { ThemeProvider } from "@material-ui/styles";
import React, { FC } from "react";

import { baseTheme } from "@shared/aviary/theme";

import * as styles from "./DatePicker.styles";

interface Props {
  label: string;
  value: Date;
  variant: "dialog" | "inline";
  maxDate?: Date;
  onChange: (date) => void;
}

const DatePicker: FC<Props> = ({ variant, ...rest }) => {
  const portalProps =
    variant === "inline"
      ? { PopoverProps: { disablePortal: true } }
      : { DialogProps: { disablePortal: true } };

  return (
    <ThemeProvider theme={baseTheme}>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <div css={styles.datePicker}>
          <MuiDatePicker
            autoOk={variant === "inline"}
            allowKeyboardControl={true}
            placeholder="yyyy-mm-dd"
            format="yyyy-MM-dd"
            openTo="year"
            views={["year", "month", "date"]}
            {...{
              variant,
              ...portalProps,
              ...rest,
            }}
          />
        </div>
      </MuiPickersUtilsProvider>
    </ThemeProvider>
  );
};

DatePicker.defaultProps = {
  maxDate: new Date(),
};

export { DatePicker };
