import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { DatePicker } from "./DatePicker";

describe("DatePicker component", () => {
  let wrapper;
  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = mount(
      <DatePicker label={"Date of X"} value={new Date()} variant={"inline"} onChange={jest.fn()} />
    );
  };

  baseTests(() => wrapper);
});
