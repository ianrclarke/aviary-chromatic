import { SerializedStyles } from "@emotion/core";
import React, { FC, HTMLProps, useState } from "react";
import InputMask from "react-input-mask";

import { BulmaColorProps } from "@shared/types/bulmaColors";
import { stylesHelper } from "@styles/emotion-styles/helpers/stylesHelper";

import * as styles from "./FloatingLabelInput.styles";

interface BulmaInputProps extends BulmaColorProps, HTMLProps<HTMLInputElement> {}

interface Props extends BulmaInputProps {
  formatChars?: object;
  id: string;
  label: string;
  hideLabel?: boolean;
  type: "text" | "email" | "password" | "tel" | "number";
  handleChange?: any;
  hasIcon?: boolean;
  mask?: string;
  onBlur?: () => void;
  errors?: string[];
  maskChar?: string;
  wrapperStyles?: SerializedStyles;
}

const FloatingLabelInput: FC<Props> = props => {
  const [isFocused, setIsFocused] = useState(false);
  const {
    value,
    disabled,
    isColor,
    id,
    label,
    hideLabel,
    type,
    handleChange,
    maskChar,
    name,
    onBlur,
    hasIcon,
    errors,
    mask,
    formatChars,
    wrapperStyles,
    ...rest
  } = props;

  const getColor = () => (errors && errors.length > 0 ? "danger" : isColor);

  const conditionalFillStyles = elementStyle => {
    return stylesHelper(elementStyle.base, [
      [elementStyle.filled, Boolean(value)],
      [elementStyle.filled, isFocused],
      [elementStyle.disabled, disabled],
      [elementStyle.hasIcon, hasIcon],
      [elementStyle.hidden, hideLabel],
    ]);
  };

  const conditionalColorStyles = () => {
    return stylesHelper(
      [styles.label.variations.primary, getColor() === "primary"],
      [styles.label.variations.danger, getColor() === "danger"],
      [styles.label.variations.warning, getColor() === "warning"],
      [styles.label.hidden, hideLabel]
    );
  };

  /*
   * Hoshi is the animated bar underneath the input
   * that animates when a user focuses/activates the input
   */
  const conditionalHoshiStyles = (hoshiLine, isHighlight) => {
    return stylesHelper(hoshiLine.base, [
      [hoshiLine.highlight, isHighlight],
      [hoshiLine.focused, isFocused],
      [hoshiLine.disabled, disabled],
      [hoshiLine.variations.primary, getColor() === "primary"],
      [hoshiLine.variations.danger, getColor() === "danger"],
      [hoshiLine.variations.warning, getColor() === "warning"],
      [hoshiLine.variations.purple, getColor() === "purple"],
      [hoshiLine.variations.info, getColor() === "info"],
    ]);
  };

  const handleBlur = () => {
    if (onBlur) {
      onBlur();
    }
    setIsFocused(false);
  };

  const handleFocus = () => {
    setIsFocused(true);
  };

  const inputDefault = () => {
    return (
      <input
        className="legacy-floatingLabelInput__input"
        css={conditionalFillStyles(styles.htmlInput)}
        id={id}
        value={value}
        type={type}
        onChange={handleChange}
        onBlur={handleBlur}
        onFocus={handleFocus}
        disabled={disabled}
        name={name}
        {...rest}
      />
    );
  };

  const inputMask = () => {
    return (
      <InputMask
        mask={mask}
        formatChars={formatChars}
        className="legacy-floatingLabelInput__input"
        css={conditionalFillStyles(styles.htmlInput)}
        id={id}
        value={value}
        type={type}
        onChange={handleChange}
        maskChar={maskChar}
        onBlur={handleBlur}
        onFocus={handleFocus}
        disabled={disabled}
        name={name}
        {...rest}
      />
    );
  };

  const renderInput = () => {
    if (mask) {
      return inputMask();
    }
    return inputDefault();
  };

  const renderErrorMessages = () => {
    if (!errors || errors.length === 0) {
      return null;
    }

    return errors.slice(0, 2).map((error, index) => (
      <p
        key={`error-item-${index}`}
        className="legacy-floatingLabelInput__errorMessage"
        css={styles.errorText}
        data-testid={`error-message-${index}`}
      >
        {error}
      </p>
    ));
  };

  return (
    <div
      className="legacy-floatingLabelInput__errorWrapper"
      css={styles.floatingLabelInputErrorWrapper}
    >
      <div
        className="legacy-floatingLabelInput__wrapper"
        css={[styles.floatingLabelWrapper, wrapperStyles]}
      >
        <label
          className="legacy-floatingLabelInput__label"
          css={[conditionalFillStyles(styles.label), conditionalColorStyles()]}
          htmlFor={id}
        >
          {label}
        </label>
        <div css={styles.hoshiContainer}>
          <div css={conditionalHoshiStyles(styles.hoshiBarNormal, false)} />
          <div css={conditionalHoshiStyles(styles.hoshiBarHighlight, true)} />
        </div>
        {renderInput()}
      </div>
      {renderErrorMessages()}
    </div>
  );
};

FloatingLabelInput.defaultProps = { disabled: false };

export { FloatingLabelInput };
