import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { FloatingLabelInput } from "./FloatingLabelInput";

describe("FloatingLabelInput tests", () => {
  let wrapper;
  let label;
  let id;
  let type;
  let mask;

  const buildWrapper = () => {
    wrapper = mount(<FloatingLabelInput type={type} label={label} id={id} mask={mask} />);
  };

  beforeEach(() => {
    label = "Hello";
    id = "test";
    type = "text";
    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("FloatingLabelInput functional", () => {
    it("renders inputmask if provided a mask", () => {
      mask = "999";
      buildWrapper();

      expect(wrapper.find("InputMask")).toBeTruthy();
    });
  });
});
