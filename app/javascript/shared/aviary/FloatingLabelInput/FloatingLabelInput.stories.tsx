import React, { useState } from "react";

import { array, boolean, select, text } from "@storybook/addon-knobs";

import { FloatingLabelInput } from "./FloatingLabelInput";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=383%3A628",
};

export default {
  title: "Aviary|FloatingLabelInput",
  component: FloatingLabelInput,
  parameters: { design: componentDesign },
};

export const Default = () => {
  const [value, setValue] = useState(null);
  return (
    <FloatingLabelInput
      id="lel"
      value={value}
      handleChange={e => setValue(e.target.value)}
      label={text("Label", "Name")}
      type="text"
      disabled={boolean("disabled", false)}
      isColor={select("color", ["primary", "warning", "danger", "purple"], "primary")}
      errors={array("errors (comma seperated array)", [])}
    />
  );
};
