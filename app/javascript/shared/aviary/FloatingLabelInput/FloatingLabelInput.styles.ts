import { css } from "@emotion/core";

import { animations, colors, helpers } from "@styles";

type colors = "primary" | "info" | "success" | "warning" | "danger" | "secondary" | "purple";

// const sizes = {
//   small: '2.25rem',
//   normal: '3.25rem',
//   medium: '3.75rem',
//   large: '4.25rem'
// }

export const controlBase = css`
  transition: all 0.2s ${animations.easeOutCubic};
  color: ${colors.dark};
  align-items: center;
  display: inline-flex;
  font-size: 1rem;
  height: 3.25rem;
  justify-content: flex-start;
  line-height: 1.5;
  padding-bottom: calc(0.35em - 1px);
  padding-left: 0;
  padding-right: 0;
  padding-top: calc(0.375em - 1px);
  position: relative;
  vertical-align: top;
  width: 100%;

  box-sizing: border-box;
  clear: both;
  text-align: left;

  /* States */
  &:focus,
  &.is-focused,
  &:active,
  &.is-active {
    outline: none;
  }

  &[disabled],
  fieldset[disabled] & {
    cursor: not-allowed;
  }
`;

export const floatingLabelInputErrorWrapper = css`
  width: 100%;
`;

export const floatingLabelWrapper = css`
  ${controlBase};
  overflow: hidden;
  background-color: ${colors.white};
  border-color: ${colors.dark};
  z-index: 1;
  border-radius: 4px;

  /* IE9, IE10, IE11 Selector */
  @media screen and (min-width: 0\0) {
    display: inline-block;
  }
`;

export const htmlInput = {
  base: css`
    ${controlBase};
    height: calc(3.25em - 3px);
    margin-top: -3px;
    z-index: 2;
    top: 0;
    appearance: none;
    border: 0 solid transparent;
    border-radius: 0;
    box-shadow: none;

    /* IE Fix */
    &::-ms-clear {
      display: none;
    }

    /* IE9, IE10, IE11 Selector */
    @media screen and (min-width: 0\0) {
      appearance: none;
      margin-top: -5px;
    }

    &[disabled],
    fieldset[disabled] & {
      background-color: ${colors.background};
      color: ${colors.lightGreen};
      height: calc(3.25em - 1px);
      padding-left: 0.5rem;
      padding-right: 0.5rem;
    }
  `,
  filled: css`
    padding-top: calc(calc(0.375em - 1px) + 0.6rem);
    padding-bottom: 0;

    /* IE9, IE10, IE11 Selector: Due to compound calc() not working in IE */
    @media screen and (min-width: 0\0) {
      padding-top: 0.75rem;
      padding-bottom: 0;
    }
  `,
  hasIcon: css`
    padding-right: 2.5rem;
  `,
};

export const label = {
  base: css`
    transition: transform 0.1s cubic-bezier(0.1, 0.55, 0.83, 0.67), color 0.3s ease-out;
    font-size: 1rem;
    position: absolute;
    transform-origin: top left;
    transform: translate(0, 1px) scale(1);
    color: ${colors.grey};
    cursor: text;
    z-index: 3;

    /* IE9, IE10, IE11 Selector */
    @media screen and (min-width: 0\0) {
      top: 16px;
    }

    &:hover {
      color: ${colors.dark};
    }
  `,
  hidden: css`
    visibility: hidden;
    display: none;
  `,
  filled: css`
    color: ${colors.blue};
    transform: translate(0, -12px) scale(0.85);

    /* IE9, IE10, IE11 Selector */
    @media screen and (min-width: 0\0) {
      transform: translate(0, -18px) scale(0.85);
    }

    &:hover {
      color: ${colors.blue};
    }
  `,
  disabled: css`
    color: ${colors.lightGreen};
    transform: translate(8px, -12px) scale(0.85);

    &:hover {
      color: ${colors.lightGreen};
    }
  `,
  hasIcon: css`
    padding-right: 2.5rem;
  `,
  variations: {
    primary: css`
      color: ${colors.primary};
    `,
    danger: css`
      color: ${colors.danger};
    `,
    warning: css`
      color: ${colors.warning};
    `,
  },
};

export const hoshiContainer = css`
  transition: transform 0.3s ease-out;
  display: block;
  padding: 1.6em 0;
  width: 100%;
  position: absolute;
  z-index: 1;
  top: 0;
`;

const hoshiBarBase = css`
  transform: translate(0, 0);
  content: "";
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border-bottom: 1px solid ${colors.lightGreen};
  display: block;
  z-index: 1;
`;

export const hoshiBarNormal = {
  base: css`
    ${hoshiBarBase};
  `,
  disabled: css`
    border-bottom: 1px solid ${colors.legacy.light};
  `,
  variations: {
    primary: css`
      border-bottom: 1px solid ${helpers.hexToRgba(colors.primary, 0.8)};
    `,
    danger: css`
      border-bottom: 1px solid ${helpers.hexToRgba(colors.danger, 0.8)};
    `,
    warning: css`
      border-bottom: 1px solid ${helpers.hexToRgba(colors.warning, 0.8)};
    `,
    purple: css`
      border-bottom: 1px solid ${helpers.hexToRgba(colors.purple, 0.8)};
    `,
    info: css`
      border-bottom: 1px solid ${helpers.hexToRgba(colors.info, 0.8)};
    `,
  },
};

export const hoshiBarHighlight = {
  base: css`
    ${hoshiBarBase}
  `,
  highlight: css`
    border-bottom: 2px solid ${colors.lightGreen};
    transform: translate(-100%, 0);
    transition: transform 0.3s;
  `,
  focused: css`
    transform: translate(0, 0);
  `,
  disabled: css`
    border-bottom: 1px solid ${colors.legacy.light};
  `,
  variations: {
    primary: css`
      border-bottom: 2px solid ${colors.primary};
    `,
    danger: css`
      border-bottom: 2px solid ${colors.danger};
    `,
    warning: css`
      border-bottom: 2px solid ${colors.warning};
    `,
    purple: css`
      border-bottom: 2px solid ${colors.purple};
    `,
    info: css`
      border-bottom: 2px solid ${colors.info};
    `,
  },
};

export const errorText = css`
  ${helpers.fontSizeNormalizer(14)};
  color: ${colors.danger};
  animation: ${animations.fadeIn} 0.25s ${animations.easeOutCirc};
  &:not(:first-of-type) {
    margin-bottom: 1rem;
  }
`;
