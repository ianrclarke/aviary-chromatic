import { css } from "@emotion/core";

import { colors, dimensions, helpers, typography } from "@styles";

export const button = css`
  border-radius: ${dimensions.pillBorderRadius};
  &:hover,
  &:active,
  &:focus {
    border-color: ${colors.primary};
    box-shadow: 0 1px 2px 0 ${helpers.hexToRgba(colors.dark, 0.25)};
  }

  @media only screen and (max-width: ${dimensions.phoneMax}) {
    font-size: ${typography.small};
    height: 1.5rem;
    padding-top: 0;
    padding-bottom: 0;
  }
`;

export const label = css`
  margin-right: 1rem;
  @media only screen and (max-width: ${dimensions.phoneMax}) {
    margin-right: 0.5rem;
  }
`;

export const icon = css`
  display: flex;
  margin-right: -0.5rem;
  svg {
    height: 1rem;
    @media only screen and (max-width: ${dimensions.phoneMax}) {
      height: 0.75rem;
    }
  }
`;
