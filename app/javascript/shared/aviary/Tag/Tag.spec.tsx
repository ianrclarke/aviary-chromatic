import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Tag } from "./Tag";

describe("Tag", () => {
  let wrapper;

  const buildWrapper = () => {
    wrapper = mount(<Tag label="Gluten free" />);
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => wrapper);
});
