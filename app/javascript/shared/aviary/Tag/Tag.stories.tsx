import React from "react";

import { text } from "@storybook/addon-knobs";

import { Tag } from "./Tag";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=960%3A66",
};

export default {
  title: "Aviary|Tag",
  component: Tag,
  parameters: { design: componentDesign },
};

export const Default = () => <Tag label={text("label", "Gluten free")} />;
