import { faTimes } from "@fortawesome/pro-light-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC } from "react";

import { Button } from "@shared/aviary/Button";

import * as styles from "./Tag.styles";

interface Props {
  label: string;
  handleClick?: () => void;
}

const Tag: FC<Props> = ({ label, handleClick, ...rest }) => {
  return (
    <Button
      isOutlined={true}
      isColor={"specialty"}
      isSize="small"
      css={styles.button}
      onClick={handleClick}
      {...rest}
    >
      <div css={styles.label}>{label}</div>
      <div css={styles.icon}>
        <FontAwesomeIcon size={"2x"} icon={faTimes} />
      </div>
    </Button>
  );
};

export { Tag };
