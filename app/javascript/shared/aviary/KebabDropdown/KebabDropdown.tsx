import React, { FC } from "react";

import { Dropdown, DropdownContent, DropdownProps } from "../Dropdown";

import { KebabButton } from "./KebabButton";

interface Props extends DropdownProps {
  label: string;
}

const KebabDropdown: FC<Props> = ({ label, children, ...rest }) => {
  return (
    <Dropdown {...rest}>
      <KebabButton label={label} />
      <DropdownContent>{children}</DropdownContent>
    </Dropdown>
  );
};

export { KebabDropdown };
