import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { KebabDropdown } from "./KebabDropdown";

describe("Kebab Menu tests", () => {
  let wrapper;

  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = shallow(<KebabDropdown label="i am aria" />);
  };

  baseTests(() => wrapper);
});
