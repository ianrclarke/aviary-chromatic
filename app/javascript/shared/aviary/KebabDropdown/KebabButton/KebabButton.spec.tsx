import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { KebabButton } from "./KebabButton";

jest.mock("@shared/aviary/Dropdown/DropdownContext", () => ({
  useDropdownContext: () => ({
    isDropdownOpen: true,
  }),
}));

describe("Kebab Button tests", () => {
  let wrapper;

  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = shallow(<KebabButton label="i am aria" />);
  };

  baseTests(() => wrapper);
});
