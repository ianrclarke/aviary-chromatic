import { faEllipsisV } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC, HTMLProps } from "react";

import { Button } from "@shared/aviary/Button";
import { useDropdownContext } from "@shared/aviary/Dropdown/DropdownContext";
import { useDropdownButtonProps } from "@shared/aviary/Dropdown/DropdownTrigger/DropdownButton";
import { stylesHelper } from "@styles/emotion-styles/helpers";

import * as styles from "./KebabButton.styles";

interface Props extends HTMLProps<HTMLButtonElement> {
  label: string;
}

const KebabButton: FC<Props> = ({ label, ...rest }) => {
  const dropdownProps = useDropdownButtonProps();
  const { isDropdownOpen, mouseOverTrigger } = useDropdownContext();

  const conditionalStyle = () =>
    stylesHelper(styles.kebabButton, [
      styles.kebabButtonPrimary,
      isDropdownOpen || mouseOverTrigger,
    ]);

  return (
    <Button noStyle={true} aria-label={label} css={conditionalStyle()} {...dropdownProps} {...rest}>
      <FontAwesomeIcon icon={faEllipsisV} size="2x" />
    </Button>
  );
};

export { KebabButton };
