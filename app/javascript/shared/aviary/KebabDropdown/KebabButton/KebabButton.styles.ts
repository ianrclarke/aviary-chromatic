import { css } from "@emotion/core";

import { colors, dimensions, utilities } from "@styles";

export const kebabButton = css`
  display: flex;
  align-items: center;
  justify-content: center;

  background-color: transparent;
  border-color: transparent;
  color: ${colors.grey};
  padding: 0;

  border-radius: ${dimensions.circleRadius};
  width: 2.25rem;
  height: 2.25rem;

  ${utilities.transition};
  cursor: pointer;
`;

export const kebabButtonPrimary = css`
  color: ${colors.primary};
`;
