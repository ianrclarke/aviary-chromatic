import React from "react";

import { text, select } from "@storybook/addon-knobs";
import { DropdownItem } from "../Dropdown";

import { KebabDropdown } from "./KebabDropdown";

const notes = `
#### Notes
Ensure to create a semantic aria-label for your particular kebab!

Hot tip: adding punctuation in your labels can enhance the screen-reader experience - especially for long labels.
`;

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=383%3A611",
};

export default {
  title: "Aviary|KebabDropdown",
  component: KebabDropdown,
  parameters: { notes, design: componentDesign },
};

export const Default = () => (
  <KebabDropdown
    label={text("label", "I am an aria-label.")}
    ellipsesPosition={select(
      "Align Position",
      ["topRight", "bottomRight", "bottomLeft", "topLeft", "default"],
      "default"
    )}
  >
    <DropdownItem>Hello</DropdownItem>
    <DropdownItem>I am Kebab</DropdownItem>
  </KebabDropdown>
);
