import React from "react";

import { text } from "@storybook/addon-knobs";

import { SearchInput } from "./SearchInput";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=386%3A860",
};

export default {
  title: "Aviary|SearchBar",
  component: SearchInput,
  parameters: { design: componentDesign },
};

export const Default = () => <SearchInput placeholder={text("Placeholder", "placeholder")} />;
