import { css } from "@emotion/core";

import { animations, colors, dimensions } from "@styles";

export const searchInput = css`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const searchIcon = css`
  margin: 0.25rem 0.25rem 0 0;
  color: ${colors.primary};

  @media only screen and (max-width: ${dimensions.phoneMax}) {
    margin: 0;
  }
`;

export const input = css`
  border: none;
  font-weight: 700;
  padding: 0 0.5rem;
  transition: color 0.2s ${animations.easeOutQuad};
  text-overflow: ellipsis;

  ::placeholder {
    color: ${colors.dark};
  }

  &:active {
    border: none;
    box-shadow: none;
  }

  &:focus {
    box-shadow: none;
    ::placeholder {
      color: ${colors.light};
    }
  }
`;
