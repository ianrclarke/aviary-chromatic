import { shallow } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { SearchInput } from "./SearchInput";

let searchInput;
let input;
let mockOnChange;
let mockOnKeyDown;

describe("SearchInput", () => {
  mockOnChange = jest.fn();
  mockOnKeyDown = jest.fn();

  const buildWrapper = () => {
    searchInput = shallow(
      <SearchInput
        value={"value"}
        placeholder={"hint value"}
        onChange={mockOnChange}
        onKeyDown={mockOnKeyDown}
      />
    );
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => searchInput);

  it("fires onChange when the input value is changed", () => {
    const mockValue = "test";
    buildWrapper();
    input = searchInput.find("BasicInput");

    act(() => {
      input.simulate("change", { target: { value: mockValue } });
    });

    expect(mockOnChange).toHaveBeenCalled();
  });

  it("renders FontAwsome icon", () => {
    expect(searchInput.find("FontAwesomeIcon").exists()).toBe(true);
  });
});
