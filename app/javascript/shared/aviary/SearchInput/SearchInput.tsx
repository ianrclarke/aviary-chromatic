import { faSearch } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC, HTMLProps } from "react";

import { BasicInput } from "@shared/aviary";
import { AviaryColorProps } from "@shared/aviary/aviaryColors";

import * as styles from "./SearchInput.styles";

interface BulmaInputProps extends AviaryColorProps, HTMLProps<HTMLInputElement> {}

const SearchInput: FC<BulmaInputProps> = ({ ...rest }) => {
  return (
    <div css={styles.searchInput}>
      <FontAwesomeIcon css={styles.searchIcon} icon={faSearch} />
      <BasicInput css={styles.input} type="text" {...rest} />
    </div>
  );
};

export { SearchInput };
