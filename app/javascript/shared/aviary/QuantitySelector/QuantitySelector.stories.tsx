import { boolean, number } from "@storybook/addon-knobs";
import React, { useState } from "react";

import { QuantitySelector } from "./QuantitySelector";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=383%3A655",
};

export default {
  title: "Aviary|QuantitySelector",
  component: QuantitySelector,
  parameters: { design: componentDesign },
};

export const Default = () => {
  const [value, setValue] = useState(1);
  return (
    <QuantitySelector
      value={value}
      disabled={boolean("disabled", false)}
      isSmall={boolean("isSmall", false)}
      handleSetValue={setValue}
      minValue={number("minValue", 1)}
      maxValue={number("maxValue", 10)}
    />
  );
};
