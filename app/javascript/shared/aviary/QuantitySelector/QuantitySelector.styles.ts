import { css } from "@emotion/core";

import { colors, dimensions, helpers, layers } from "@styles";

export const wrapper = {
  base: css`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 2.75rem;
    width: 6rem;
  `,
  isSmall: css`
    height: 2.25rem;
  `,
  hasMargin: css`
    margin-right: 0.5rem;
  `,
};

export const button = {
  wrapper: css`
    width: 2rem;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
  `,
  content: css`
    display: flex;
    justify-content: center;
    align-items: center;
    ${helpers.fontSizeNormalizer(16)}
    background-color: ${colors.white};
    color: ${colors.green};
    padding: 0;
    height: 100%;
    width: 100%;
    z-index: ${layers.indexDefault};
    border-top: 1px solid ${colors.light};
    border-bottom: 1px solid ${colors.light};
    &:enabled {
      &:hover {
        color: ${colors.white};
        background-color: ${colors.primary};
        cursor: pointer;
      }
    }

    &:disabled {
      cursor: not-allowed;
      box-shadow: none;
      color: ${colors.lightGreen};
      background-color: ${colors.lightGrey};
    }

    svg {
      max-height: 0.5rem;
    }
  `,
};

export const leftButton = css`
  ${button.content}
  border-left: 1px solid ${colors.light};
  border-right: none;
  border-radius: ${dimensions.borderRadius}  0 0 ${dimensions.borderRadius};
`;

export const rightButton = css`
  ${button.content}
  border-right: 1px solid ${colors.light};
  border-left: none;
  border-radius: 0 ${dimensions.borderRadius} ${dimensions.borderRadius} 0;
`;

export const input = {
  wrapper: css`
    width: 2rem;
    height: 100%;
  `,
  content: css`
    ${helpers.fontSizeNormalizer(16)}
    border-top: 1px solid ${colors.light};
    border-bottom: 1px solid ${colors.light};
    border-left: none;
    border-right: none;
    padding-bottom: 0.25rem;
    color: ${colors.grey};
    height: 100%;
    width: 100%;
    text-align: center;
    font-weight: 600;
    padding-left: 0;
    padding-right: 0;

    &::-webkit-outer-spin-button,
    &::-webkit-inner-spin-button {
      -webkit-appearance: none;
    }
    &[type="number"] {
      -moz-appearance: textfield; /* Firefox */
    }
    &[disabled] {
      background-color: ${colors.white};
      color: ${colors.grey};
    }
  `,
  // TODO: Remove this once practitioner is full React
  isLegacy: css`
    padding-bottom: 9px;
  `,
};
