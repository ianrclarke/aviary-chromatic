import { faMinus, faPlus } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC, HTMLProps, useEffect, useState } from "react";

import { stylesHelper } from "@styles/helpers";

import * as styles from "./QuantitySelector.styles";

interface Props extends HTMLProps<HTMLInputElement> {
  value: number;
  handleSetValue: (quantity?: number) => any;
  disabled?: boolean;
  isSmall?: boolean;
  minValue?: number;
  maxValue?: number;
  labels?: {
    wrapper?: string;
    input?: string;
    buttonIncrement?: string;
    buttonDecrement?: string;
  };
  hasMargin?: boolean;
  isLegacy?: boolean;
  incrementE2eData?: string;
}

const defaultProps = {
  disabled: false,
  isSmall: false,
  minValue: 1,
  maxValue: 10,
  labels: {
    main: "Item quantity widget",
    input: "Specify the item quantity",
    buttonIncrement: "Increment item quantity by 1",
    buttonDecrement: "Decrement item quantity by 1",
  },
};

const QuantitySelector: FC<Props> = ({
  value,
  handleSetValue,
  isSmall,
  minValue,
  maxValue,
  disabled,
  labels,
  hasMargin,
  isLegacy,
  incrementE2eData,
  ...rest
}) => {
  const [localValue, setLocalValue] = useState(value);

  useEffect(() => {
    setLocalValue(value);
  }, [value]);

  const handleIncrement = () => {
    if (!maxValue || value < maxValue || maxValue < minValue) {
      handleSetValue(value + 1);
    }
  };
  const handleDecrement = () => {
    if (value > minValue) {
      handleSetValue(value - 1);
    }
  };

  const handleKeyPress = ({ key }) => {
    switch (key) {
      case "Enter":
        handleBlur();
        break;
      default:
        break;
    }
  };

  const handleChange = ({ target }) => {
    const inputValue = target.value;
    const parsedValue = inputValue <= maxValue ? parseInt(inputValue, 10) : maxValue;
    if (value) {
      handleSetValue(parsedValue);
      setLocalValue(parsedValue);
    } else {
      handleSetValue(parsedValue);
      setLocalValue(inputValue);
    }
  };

  const resetLocalValue = (propValue: number, newValue: number) => {
    /* To force re-render when the prop.value is same as newValue */
    if (propValue === newValue) {
      setLocalValue(newValue);
    }
  };

  const handleBlur = () => {
    if (!localValue || localValue < minValue) {
      handleSetValue(minValue);
      resetLocalValue(value, minValue);
    } else if (maxValue && localValue > maxValue && maxValue > minValue) {
      handleSetValue(maxValue);
      resetLocalValue(value, maxValue);
    } else {
      handleSetValue(localValue);
    }
  };

  const getValue = () => {
    if (localValue) {
      return localValue;
    }
    return "";
  };

  const isIncrementDisabled = () => {
    if ((maxValue >= minValue && getValue() === maxValue) || disabled) return true;
    return false;
  };

  const buildWrapper = () => {
    return stylesHelper(styles.wrapper.base, [
      [styles.wrapper.isSmall, isSmall],
      [styles.wrapper.hasMargin, hasMargin],
    ]);
  };

  const buildInput = () => {
    return stylesHelper(styles.input.content, [[styles.input.isLegacy, isLegacy]]);
  };

  return (
    <div css={buildWrapper()} role="widget" aria-label={labels.wrapper}>
      <div css={styles.button.wrapper}>
        <button
          css={styles.leftButton}
          type="button"
          onClick={handleDecrement}
          disabled={getValue() === minValue || disabled}
          aria-label={labels.buttonDecrement}
          onBlur={handleBlur}
          data-testid="value-decrement"
        >
          <FontAwesomeIcon icon={faMinus} />
        </button>
      </div>
      <div css={styles.input.wrapper}>
        <input
          css={buildInput()}
          type="number"
          value={getValue()}
          onChange={handleChange}
          disabled={disabled}
          onBlur={handleBlur}
          onKeyPress={handleKeyPress}
          aria-label={labels.input}
          data-e2e="product-quantity-input"
          {...rest}
        />
      </div>
      <div css={styles.button.wrapper}>
        <button
          css={styles.rightButton}
          type={"button"}
          onClick={handleIncrement}
          disabled={isIncrementDisabled()}
          aria-label={labels.buttonIncrement}
          data-testid="value-increment"
          data-e2e={incrementE2eData || "value-increment"}
          onBlur={handleBlur}
        >
          <FontAwesomeIcon icon={faPlus} />
        </button>
      </div>
    </div>
  );
};

QuantitySelector.defaultProps = defaultProps;

export { QuantitySelector };
