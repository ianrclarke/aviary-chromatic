import { faMinus, faPlus } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { shallow } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { QuantitySelector } from "./QuantitySelector";

let wrapper;
let mockHandler;
const defaultValue = 2;

describe("QuantitySelector", () => {
  const buildWrapper = () => {
    mockHandler = jest.fn();
    wrapper = shallow(<QuantitySelector value={defaultValue} handleSetValue={mockHandler} />);
  };

  describe("base tests", () => {
    beforeEach(() => {
      buildWrapper();
    });

    baseTests(() => wrapper);

    it("renders FontAwsome icon", () => {
      expect(wrapper.contains(<FontAwesomeIcon icon={faMinus} />)).toBe(true);
      expect(wrapper.contains(<FontAwesomeIcon icon={faPlus} />)).toBe(true);
    });
  });

  describe("events", () => {
    describe("normal case", () => {
      beforeEach(() => {
        buildWrapper();
      });

      it("should update the input display when plus(+) button is clicked", () => {
        wrapper.find('[data-testid="value-increment"]').simulate("click");
        expect(mockHandler).toBeCalledWith(defaultValue + 1);
      });

      it("should update the input display when minus(-) button is clicked", () => {
        wrapper.find('[data-testid="value-decrement"]').simulate("click");
        expect(mockHandler).toBeCalledWith(defaultValue - 1);
      });

      it("should update the input display when new value is entered in the input field", () => {
        const newValue = 3;
        wrapper.find("input").simulate("change", { target: { value: newValue } });
        expect(wrapper.find("input").prop("value")).toBe(newValue);
      });
    });

    describe("abnormal case", () => {
      beforeEach(() => {
        mockHandler = jest.fn();
      });

      it("should not change the input display when current value is minimum and you click the minus(-) button", () => {
        const currentValue = 1;
        const quantitySelector = shallow(
          <QuantitySelector value={currentValue} handleSetValue={mockHandler} minValue={1} />
        );
        quantitySelector.find('[data-testid="value-decrement"]').simulate("click");
        expect(mockHandler).not.toBeCalled();
      });

      it("should not change the input display when current value is maximum and you click the plus(+) button", () => {
        const currentValue = 2;
        const quantitySelector = shallow(
          <QuantitySelector value={currentValue} handleSetValue={mockHandler} maxValue={2} />
        );
        quantitySelector.find('[data-testid="value-increment"]').simulate("click");
        expect(mockHandler).not.toHaveBeenCalled();
      });

      it("should not increment the value if the maximum value allowed is 1", () => {
        const quantitySelector = shallow(
          <QuantitySelector value={1} handleSetValue={mockHandler} maxValue={1} />
        );
        const incrementButton = quantitySelector.find('[data-testid="value-increment"]');
        expect(incrementButton.prop("disabled")).toBeTruthy();
        incrementButton.simulate("click");
        expect(mockHandler).not.toHaveBeenCalled();
      });

      it("should call handleSetValue with minimum value when there is no value set", () => {
        const minValue = 5;
        const currentValue = undefined;
        const quantitySelector = shallow(
          <QuantitySelector value={currentValue} handleSetValue={mockHandler} minValue={minValue} />
        );
        quantitySelector.find("input").simulate("blur");
        expect(mockHandler).toBeCalledWith(minValue);
      });

      it("should call handleSetValue with minimum when value is less than the required", () => {
        const minValue = 5;
        const quantitySelector = shallow(
          <QuantitySelector value={2} handleSetValue={mockHandler} minValue={minValue} />
        );
        quantitySelector.find("input").simulate("blur");
        expect(mockHandler).toBeCalledWith(minValue);
      });

      it("should call handleSetValue with maximum when value is more than the required", () => {
        const maxValue = 5;
        const quantitySelector = shallow(
          <QuantitySelector value={10} handleSetValue={mockHandler} maxValue={maxValue} />
        );
        quantitySelector.find("input").simulate("blur");
        expect(mockHandler).toBeCalledWith(maxValue);
      });
    });

    it("ignores max value if maxValue < minValue", () => {
      const maxValue = 1;
      const minValue = 2;

      const quantitySelector = shallow(
        <QuantitySelector
          value={minValue}
          handleSetValue={mockHandler}
          minValue={minValue}
          maxValue={maxValue}
        />
      );

      expect(quantitySelector.find("input").props().value).toEqual(minValue);

      act(() => {
        quantitySelector.find('[data-testid="value-increment"]').simulate("click");
      });
      wrapper.update();

      expect(mockHandler).toHaveBeenCalledWith(minValue + 1);
    });
  });
});
