import { css } from "@emotion/core";

import { baseStyles, animations, colors, dimensions, typography } from "@styles";

export const attachment = css`
  ${animations.transition("0.4s")};
  border: 1px solid #e2eaf2;
  border-radius: ${dimensions.borderRadius};
  padding: 0;
  width: auto;

  display: inline-flex;
  justify-content: flex-start;
  align-items: stretch;

  &:hover {
    ${baseStyles.popoverShadow}
  }
`;

export const iconArea = css`
  border-radius: ${dimensions.circleRadius};
  margin-right: 0.75rem;
  width: 2rem;
  height: 2rem;

  display: flex;
  justify-content: center;
  align-items: center;
`;

export const iconColor = {
  default: css`
    background-color: ${colors.darkExtraLight};

    path {
      fill: ${colors.dark};
    }
  `,
  pdf: css`
    background-color: ${colors.greenLight};

    path {
      fill: ${colors.green};
    }
  `,
  png: css`
    background-color: ${colors.blueExtraLight};

    path {
      fill: ${colors.blue};
    }
  `,
  jpg: css`
    background-color: ${colors.purpleExtraLight};

    path {
      fill: ${colors.purple};
    }
  `,
  jpeg: css`
    background-color: ${colors.orangeExtraLight};

    path {
      fill: ${colors.orange};
    }
  `,
};

export const attachmentArea = css`
  padding: 0.5rem 0.75rem;
  display: flex;
  align-items: center;
`;

export const documentInfo = css`
  text-align: left;
`;

export const docName = css`
  color: ${colors.dark};
  font-weight: ${typography.semiBold};
  line-height: 1rem;
  display: flex;
  align-items: baseline;
`;

export const docType = css`
  font-weight: ${typography.semiBold};
`;

export const removableArea = css`
  ${animations.transition("0.2s")};
  position: relative;
  padding: 0.5rem 0.75rem;
  height: auto;
  cursor: pointer;
  display: flex;
  align-items: center;

  appearance: none;
  border: none;

  path {
    fill: ${colors.grey};
  }

  &:active {
    background-color: ${colors.lightGrey};

    path {
      fill: ${colors.dark};
    }
  }

  &:before {
    content: "";
    background-color: #e2eaf2;
    position: absolute;
    height: 70%;
    left: -1px;
    width: 1px;
    top: 15%;
  }
`;

export const showMore = css`
  cursor: pointer;
`;

export const showMoreNumber = css`
  background-color: ${colors.greenExtraLight};
  color: ${colors.green};

  font-size: 1rem;
  font-weight: ${typography.semiBold};
`;
