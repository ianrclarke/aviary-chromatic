import { faTimes } from "@fortawesome/pro-light-svg-icons";
import { faFile } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC } from "react";
import { useTranslation } from "react-i18next";

import { Text, Button } from "@shared/aviary";

import * as styles from "./Attachment.styles";

const MAX_LABEL_LENGTH = 12;
const fileTypeColors = ["png", "pdf", "jpeg", "jpg"];

const iconColorMatch = (fileType: string) => {
  return fileTypeColors.includes(fileType) ? fileType : "default";
};

const getFileType = (fileName: string) => {
  const splitResult = fileName.split(".");
  return splitResult.length > 1 ? splitResult.pop() : "";
};

interface Props {
  /**
   * Name of the file
   */
  fileName: string;
  /**
   * Callback for the conditional remove button is clicked
   */
  handleRemove?: () => void;
  /**
   * URL which enables viewing image in a new tab
   */
  thumbnailUrl?: string;
}

const Attachment: FC<Props> = ({ fileName, handleRemove, thumbnailUrl, ...rest }) => {
  const { t } = useTranslation("aviary:attachment");

  const fileType = getFileType(fileName);
  const fileTypeStyles = [styles.iconArea, styles.iconColor[iconColorMatch(fileType)]];

  const truncatedFilename = () => {
    const splitResult = fileName.split(".");
    let justName = fileName;
    if (splitResult.length > 1) {
      // contains file extension
      const regexFileExtension = /\.[0-9a-z]+$/i;
      justName = fileName.replace(regexFileExtension, "");
    }
    return justName.length < MAX_LABEL_LENGTH
      ? justName
      : `${justName.substr(0, MAX_LABEL_LENGTH)}...`;
  };

  const renderFileInformation = () => (
    <>
      <div css={fileTypeStyles}>
        <FontAwesomeIcon icon={faFile} />
      </div>
      <div css={styles.documentInfo}>
        <Text css={styles.docName} isSize="small" data-testid="filename">
          {truncatedFilename()}
          {fileType ? `.${fileType}` : ""}
        </Text>
        <Text css={styles.docType} isSize="xsmall">
          {fileType.toUpperCase()}
        </Text>
      </div>
    </>
  );

  const renderFileArea = () => {
    if (thumbnailUrl) {
      return (
        <a
          css={styles.attachmentArea}
          href={thumbnailUrl}
          target="_blank"
          rel="noopener noreferrer"
          data-testid="document-link"
        >
          {renderFileInformation()}
        </a>
      );
    }
    return <div css={styles.attachmentArea}>{renderFileInformation()}</div>;
  };

  return (
    <div
      css={styles.attachment}
      aria-label={t(`aviary:attachment.attachmentName`, { name: fileName })}
      {...rest}
    >
      {renderFileArea()}
      {handleRemove && (
        <Button
          css={styles.removableArea}
          onClick={handleRemove}
          noStyle={true}
          aria-label={t("aviary:attachment.removeAttachment", { name: fileName })}
          data-testid="close-button"
        >
          <FontAwesomeIcon icon={faTimes} size="lg" />
        </Button>
      )}
    </div>
  );
};

export { Attachment, getFileType, iconColorMatch };
