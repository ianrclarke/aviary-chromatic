import { faChevronLeft } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC } from "react";
import { useTranslation } from "react-i18next";

import { Text, Button } from "@shared/aviary";

import * as styles from "./Attachment.styles";

interface Props {
  /**
   * Number of show more
   */
  numToShow: number;
  /**
   * Determines if you are showing everything
   */
  isShowingAll: boolean;
  /**
   * Callback for toggle of show/hide
   */
  onClick?: () => void;
}

const ShowMoreAttachment: FC<Props> = ({ numToShow, isShowingAll, onClick, ...rest }) => {
  const { t } = useTranslation("aviary:attachment");

  const getLabel = () => {
    if (isShowingAll) return t("aviary:attachment.showLess");
    return t("aviary:attachment.showMore");
  };

  const getActionArea = () => {
    if (isShowingAll) {
      return <FontAwesomeIcon icon={faChevronLeft} size="xs" />;
    }
    return <>+{numToShow}</>;
  };

  return (
    <Button
      css={[styles.attachment, styles.showMore]}
      aria-label={getLabel()}
      onClick={onClick}
      noStyle={true}
      {...rest}
    >
      <div css={styles.attachmentArea}>
        <div css={[styles.iconArea, styles.showMoreNumber]}>{getActionArea()}</div>
        <div css={styles.documentInfo}>
          <Text css={styles.docName} isSize="small" data-testid="showMore">
            {getLabel()}
          </Text>
        </div>
      </div>
    </Button>
  );
};

export { ShowMoreAttachment };
