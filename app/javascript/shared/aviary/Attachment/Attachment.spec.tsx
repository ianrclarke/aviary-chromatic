import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Attachment } from "./Attachment";

describe("Attachment component", () => {
  let wrapper;
  let mockFileName;
  let mockCallback;
  let mockUrl;

  beforeEach(() => {
    mockFileName = "testfile.pdf";
    mockUrl = null;
    mockCallback = null;
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = mount(
      <Attachment fileName={mockFileName} handleRemove={mockCallback} thumbnailUrl={mockUrl} />
    );
  };

  baseTests(() => wrapper);

  describe("Conditional rendering", () => {
    it("displays the full file name if it is less that 12 characters", () => {
      expect(wrapper.find('[data-testid="filename"]').at(0).text()).toEqual("testfile.pdf");
    });
    it("displays the truncated name if it is more that 12 characters", () => {
      mockFileName = "averylongboringname.pdf";
      buildWrapper();
      expect(wrapper.find('[data-testid="filename"]').at(0).text()).toEqual("averylongbor....pdf");
    });
    it("displays name if it has no extension", () => {
      mockFileName = "someName";
      buildWrapper();
      expect(wrapper.find('[data-testid="filename"]').at(0).text()).toEqual("someName");
    });

    it("displays long truncated name with no extension", () => {
      mockFileName = "someNamethatsverylongggggish";
      buildWrapper();
      expect(wrapper.find('[data-testid="filename"]').at(0).text()).toEqual("someNamethat...");
    });

    it("displays name correctly when multiple dots in name", () => {
      mockFileName = "some.name.pdf";
      buildWrapper();
      expect(wrapper.find('[data-testid="filename"]').at(0).text()).toEqual("some.name.pdf");
    });

    it("displays name correctly when long and multiple dots in name", () => {
      mockFileName = "some.n.thatsssssssvery.longggg.ame.pdf";
      buildWrapper();
      expect(wrapper.find('[data-testid="filename"]').at(0).text()).toEqual("some.n.thats....pdf");
    });

    it("properly renders if a non-mapped filetype is used", () => {
      mockFileName = "grumble.tif";
      buildWrapper();
      expect(wrapper.find('[data-testid="filename"]').at(0).text()).toEqual("grumble.tif");
    });
    it("properly renders a close button if a handleRemove callback is passed in", () => {
      mockCallback = jest.fn();
      buildWrapper();
      expect(wrapper.exists('[data-testid="close-button"]')).toBeTruthy();
    });
    it("renders a link if a thumbnailUrl is passed in", () => {
      mockUrl = "http://www.google.com";
      buildWrapper();
      expect(wrapper.find('[data-testid="document-link"]').prop("href")).toEqual(mockUrl);
    });
  });
  describe("Functional", () => {
    it("properly calls back when the close button is clicked", () => {
      mockCallback = jest.fn();
      buildWrapper();
      wrapper.find('[data-testid="close-button"]').simulate("click");
      expect(mockCallback).toHaveBeenCalled();
    });
  });
});
