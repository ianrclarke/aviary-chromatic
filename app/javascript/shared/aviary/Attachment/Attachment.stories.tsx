import React from "react";

import { text, boolean } from "@storybook/addon-knobs";
import { Attachment } from "./Attachment";
import { ShowMoreAttachment } from "./ShowMoreAttachment";

export default {
  title: "Aviary|Attachment",
  component: Attachment,
};

const fakeBoi = () => {
  alert("You have attempted to remove a document. Skynet dispatched");
};

export const Default = () => (
  <div>
    <div>
      <Attachment
        fileName={text("fileName", "verylongoldscreenshotname.pdf")}
        thumbnailUrl={text("thumbnailUrl", "www.google.com")}
        handleRemove={fakeBoi}
      />
    </div>
    <br />
    <div>
      <Attachment fileName="instructions.pdf" handleRemove={fakeBoi} />
    </div>
    <br />
    <div>
      <Attachment fileName="yup.png" />
    </div>
    <br />
    <div>
      <ShowMoreAttachment
        numToShow={3}
        onClick={fakeBoi}
        isShowingAll={boolean("isShowingAll", true)}
      />
    </div>
  </div>
);
