import React, { FC, HTMLProps } from "react";

import * as styles from "./Box.styles";

interface Props extends HTMLProps<HTMLDivElement> {
  /**
   * Removes gutters made by padding from the Container component. Uses negative margin.
   *
   * @default false
   */
  isGutterless?: boolean;
}

const Box: FC<Props> = ({ isGutterless, children, ...rest }: Props) => {
  const css = [styles.box, isGutterless && styles.gutterless];
  return (
    <section css={css} {...rest}>
      {children}
    </section>
  );
};

export { Box };
