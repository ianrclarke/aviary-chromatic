import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Box } from "./Box";
import { BoxSection } from "./BoxSection";

describe("BoxSection component", () => {
  let wrapper;
  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = mount(
      <Box>
        <BoxSection>
          <p>This is some BoxSection text!</p>
        </BoxSection>
      </Box>
    );
  };

  baseTests(() => wrapper);
});
