import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { BoxSection } from "./BoxSection";

describe("BoxSection component", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(
      <BoxSection>
        <p>This is some BoxSection text!</p>
      </BoxSection>
    );
  });

  baseTests(() => wrapper);
});
