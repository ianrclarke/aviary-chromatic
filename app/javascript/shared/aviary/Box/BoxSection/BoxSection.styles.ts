import { css } from "@emotion/core";

import { dimensions } from "@styles";

export const boxSection = css`
  padding: 2rem;

  @media screen and (max-width: ${dimensions.phoneLargeMax}) {
    padding: 1rem;
  }
`;

export const paddingless = css`
  padding: 0;

  @media screen and (max-width: ${dimensions.phoneLargeMax}) {
    padding: 0;
  }
`;

export const reducedPadding = css`
  padding: 0.75rem 2rem;

  @media screen and (max-width: ${dimensions.phoneLargeMax}) {
    padding: 0.75rem 1rem;
  }
`;
