import React, { FC, HTMLProps } from "react";

import * as styles from "./BoxSection.styles";

interface Props extends HTMLProps<HTMLDivElement> {
  /**
   * Removes pudding. No wait, that's me. This removes PADDING.
   *
   * @default false
   */
  isPaddingless?: boolean;
  /**
   * A reduced padding section for elements that are spaced closer
   *
   * @default false
   */
  isReducedPadding?: boolean;
}

const BoxSection: FC<Props> = ({ isPaddingless, isReducedPadding, children, ...rest }) => {
  const css = [
    styles.boxSection,
    isPaddingless && styles.paddingless,
    isReducedPadding && styles.reducedPadding,
  ];
  return (
    <div css={css} {...rest}>
      {children}
    </div>
  );
};

export { BoxSection };
