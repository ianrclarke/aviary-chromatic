import React from "react";

import { boolean, withKnobs } from "@storybook/addon-knobs";
import { Separator } from "@shared/aviary/Separator";
import { colors } from "@styles";
import { Box } from "./Box";
import { BoxSection } from "./BoxSection";

export default {
  title: "Aviary|Box",
  component: Box,
  subcomponents: BoxSection,
  decorators: [withKnobs],
};

export const Default = () => (
  <div style={{ backgroundColor: colors.background, padding: 50 }}>
    <Box isGutterless={boolean("isGutterless", false)}>
      <BoxSection>
        <h1>Box content</h1>
        <button>A button</button>
      </BoxSection>
      <Separator />
      <BoxSection>
        <h1>Box more content</h1>
        <button>A second button</button>
      </BoxSection>
      <BoxSection>
        <h1>More stuff not with Separator</h1>
        <button>A third button</button>
      </BoxSection>
    </Box>
  </div>
);
