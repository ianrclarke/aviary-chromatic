import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Title } from "./Title";

describe("Title tests", () => {
  let title;

  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    title = mount(<Title />);
  };

  baseTests(() => title);
});
