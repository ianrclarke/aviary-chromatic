import React, { FC, HTMLProps } from "react";

import * as styles from "./Title.styles";

interface Props extends HTMLProps<HTMLElement> {
  /**
   * Determines the heading size
   *
   * @default h2
   */
  type?: "h1" | "h2" | "h3" | "h4" | "h5" | "p";

  /**
   * An option to indent the title
   *
   * @default false
   */
  isIndented?: boolean;

  /**
   * An option to make the title less bold
   *
   * @default false
   */
  isLessBold?: boolean;

  /**
   * An option to have the title center the full width of the page
   *
   * @default false
   */
  isFullWidth?: boolean;

  /**
   * An option to have no margins for the title
   *
   * @default false
   */
  isMarginless?: boolean;
}

const Title: FC<Props> = ({
  type,
  isIndented,
  isLessBold,
  isFullWidth,
  isMarginless,
  children,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  ref,
  ...rest
}: Props) => {
  const styleBuilder = [
    styles.base,
    styles.sizes[type](isLessBold),
    isIndented && styles.indented,
    isFullWidth && styles.fullwidth,
    !isMarginless && styles.margin,
  ];
  switch (type) {
    case "h1":
      return (
        <h1 css={styleBuilder} {...rest}>
          {children}
        </h1>
      );
    case "h2":
      return (
        <h2 css={styleBuilder} {...rest}>
          {children}
        </h2>
      );
    case "h3":
      return (
        <h3 css={styleBuilder} {...rest}>
          {children}
        </h3>
      );
    case "h4":
      return (
        <h4 css={styleBuilder} {...rest}>
          {children}
        </h4>
      );
    case "h5":
      return (
        <h5 css={styleBuilder} {...rest}>
          {children}
        </h5>
      );
    case "p":
      return (
        <p css={styleBuilder} {...rest}>
          {children}
        </p>
      );
  }
};

Title.defaultProps = {
  type: "h2",
  isLessBold: false,
};

export { Title };
