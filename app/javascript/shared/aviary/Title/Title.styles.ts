import { css } from "@emotion/core";

import { colors, typography } from "@styles";

export const base = css`
  color: ${colors.dark};
  font-size: 1.625rem;
  font-weight: 500;
  line-height: 1.125;
  word-break: break-word;

  strong {
    color: ${colors.primary};
    font-weight: 600;
  }
`;

export const margin = css`
  &:not(:last-child) {
    margin-bottom: 1rem;
  }
`;

export const indented = css`
  margin-left: 1rem;
`;

export const fullwidth = css`
  width: 100%;
`;

const getBoldWeight = isLessBold => {
  if (isLessBold) {
    return css`
      font-weight: ${typography.semiBold};
    `;
  }
  return css`
    font-weight: ${typography.bold};
  `;
};

const getSemiBoldWeight = isLessBold => {
  if (isLessBold) {
    return css`
      font-weight: ${typography.regular};
    `;
  }
  return css`
    font-weight: ${typography.semiBold};
  `;
};

export const sizes = {
  h1: isLessBold => css`
    font-size: 2.75rem;
    ${getBoldWeight(isLessBold)}
  `,
  h2: isLessBold => css`
    font-size: 2.125rem;
    ${getSemiBoldWeight(isLessBold)}
  `,
  h3: isLessBold => css`
    font-size: 1.625rem;
    ${getSemiBoldWeight(isLessBold)}
  `,
  h4: () => css`
    font-size: 1.375rem;
    font-weight: ${typography.semiBold};
  `,
  h5: () => css`
    font-size: 1.125rem;
    font-weight: ${typography.semiBold};
  `,

  p: isLessBold => css`
    font-size: 1rem;
    line-height: 1.5rem;
    ${getSemiBoldWeight(isLessBold)}
    &:not(:last-child) {
      margin-bottom: 0;
    }
  `,
};
