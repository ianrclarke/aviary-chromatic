import React from "react";

import { select, boolean } from "@storybook/addon-knobs";

import { Title } from "./Title";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=1475%3A542",
};

export default {
  title: "Aviary|Title",
  component: Title,
  parameters: { design: componentDesign },
};

export const Default = () => (
  <div>
    <Title
      type={select("Title size", ["h1", "h2", "h3", "h4", "h5", "p"], "h2")}
      isIndented={boolean("isIndented", false)}
      isLessBold={boolean("isLessBold", false)}
      isFullWidth={boolean("isFullWidth", false)}
      isMarginless={boolean("isMarginless", false)}
    >
      Welcome to Microsoft Sam's computer voice
    </Title>
    <Title type="h1">H1 title</Title>
    <Title type="h2">H2 title</Title>
    <Title type="h3">H3 title</Title>
    <Title type="h4">H4 title</Title>
    <Title type="h5">H5 title</Title>
    <Title type="p">Paragraph title</Title>
  </div>
);
