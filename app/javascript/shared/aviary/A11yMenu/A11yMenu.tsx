import React, { FC } from "react";
import { useTranslation } from "react-i18next";

import { Button } from "../Button";

import * as styles from "./A11yMenu.styles";

const A11yMenu: FC = () => {
  const { t } = useTranslation("aviary");
  return (
    <nav aria-label={t("A11yMenu")} css={styles.a11yMenu}>
      <Button isText={true} href="#main-content">
        {t("SkipToMainContent")}
      </Button>
    </nav>
  );
};

export { A11yMenu };
