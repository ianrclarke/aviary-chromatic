import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { A11yMenu } from "./A11yMenu";

describe("A11yMenu", () => {
  let wrapper;
  let children;

  const buildWrapper = () => {
    wrapper = mount(<A11yMenu>{children}</A11yMenu>);
  };

  beforeEach(() => {
    children = "Testing";
    buildWrapper();
  });

  baseTests(() => wrapper);
});
