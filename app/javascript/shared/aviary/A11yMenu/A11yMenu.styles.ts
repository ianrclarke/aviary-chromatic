import { css } from "@emotion/core";

import { colors, layers, utilities } from "@styles";

export const a11yMenu = css`
  background-color: ${colors.white};
  position: absolute;
  top: 0;
  left: 0;
  padding: 1rem;

  z-index: ${layers.indexAccessibleTop};

  &:not(:focus-within) {
    ${utilities.visuallyHidden};
  }
`;
