import React, { FC } from "react";

import { Button, ButtonProps } from "@shared/aviary/Button";

import { useCarouselContext } from "../CarouselContext";

const CarouselNext: FC<ButtonProps> = ({ children, ...rest }) => {
  const {
    activeIndex,
    setActiveIndex,
    carouselItemsCount,
    loop,
    setDirection,
    itemsToShow,
    itemsToScroll,
  } = useCarouselContext();

  const nextOnClick = (): void => {
    const nextFirstItemIndex = activeIndex + itemsToScroll;
    if (nextFirstItemIndex < carouselItemsCount) setActiveIndex(nextFirstItemIndex);
    if (loop && nextFirstItemIndex >= carouselItemsCount) {
      setActiveIndex(nextFirstItemIndex - carouselItemsCount);
    }
    setDirection("right");
  };

  const isDisabled: boolean = !loop && activeIndex + itemsToShow >= carouselItemsCount;

  if (itemsToShow === carouselItemsCount) return null;
  return (
    <Button onClick={nextOnClick} disabled={isDisabled} {...rest}>
      {children}
    </Button>
  );
};

export { CarouselNext };
