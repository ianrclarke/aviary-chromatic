import { mount } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { Button } from "@shared/aviary";
import { baseTests } from "@testing/baseTests";

import { Carousel } from "../Carousel";
import { useCarouselContext } from "../CarouselContext";
import { CarouselItem } from "../CarouselItem";
import { CarouselItems } from "../CarouselItems";
import { CarouselInternals } from "../types";

import { CarouselNext } from "./CarouselNext";

let wrapper;
const carouselItemsCount = 2;
let carouselContextData: CarouselInternals;
let loopMock = false;

const DumbComponent = () => {
  carouselContextData = useCarouselContext();
  return null;
};

const buildWrapper = () => {
  wrapper = mount(
    <Carousel loop={loopMock}>
      <CarouselItems>
        {[...new Array(carouselItemsCount)].map((_, i) => (
          <CarouselItem key={i} data-testid={i} />
        ))}
      </CarouselItems>
      <CarouselNext>Next</CarouselNext>
      <DumbComponent />
    </Carousel>
  );
};

baseTests(() => {
  buildWrapper();
  return wrapper;
});

describe("CarouselNext", () => {
  beforeEach(() => {
    loopMock = false;
    buildWrapper();
  });

  it("switches to the next Carousel Item when clicked", () => {
    act(() => {
      wrapper.find(Button).props().onClick();
    });
    wrapper.update();
    expect(wrapper.find(CarouselItem).prop("data-testid")).toEqual(1);
    expect(carouselContextData.direction).toEqual("right");
  });

  it("should be disabled when the Carousel is at the last Carousel Item", () => {
    act(() => {
      carouselContextData.setActiveIndex(1);
    });
    wrapper.update();
    expect(wrapper.find(Button).prop("disabled")).toBeTruthy();
  });

  it("loops", () => {
    loopMock = true;
    buildWrapper();
    act(() => {
      carouselContextData.setActiveIndex(1);
    });
    wrapper.update();
    act(() => {
      wrapper.find(Button).props().onClick();
    });
    wrapper.update();
    expect(wrapper.find(CarouselItem).prop("data-testid")).toEqual(0);
  });
});
