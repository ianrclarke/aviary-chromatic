import React, { FC } from "react";

import { Button, ButtonProps } from "@shared/aviary/Button";

import { useCarouselContext } from "../CarouselContext";

const CarouselPrev: FC<ButtonProps> = ({ children, ...rest }) => {
  const {
    activeIndex,
    setActiveIndex,
    carouselItemsCount,
    loop,
    setDirection,
    itemsToShow,
    itemsToScroll,
  } = useCarouselContext();

  const prevOnClick = (): void => {
    const prevFirstItemIndex = activeIndex - itemsToScroll;
    if (prevFirstItemIndex >= 0) setActiveIndex(prevFirstItemIndex);
    if (loop && prevFirstItemIndex < 0) {
      setActiveIndex(prevFirstItemIndex + carouselItemsCount);
    }
    setDirection("left");
  };

  const isDisabled: boolean = !loop && activeIndex === 0;

  if (itemsToShow === carouselItemsCount) return null;
  return (
    <Button onClick={prevOnClick} disabled={isDisabled} {...rest}>
      {children}
    </Button>
  );
};

export { CarouselPrev };
