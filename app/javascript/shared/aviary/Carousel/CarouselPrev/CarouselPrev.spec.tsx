import { mount } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { Button } from "@shared/aviary";
import { baseTests } from "@testing/baseTests";

import { Carousel } from "../Carousel";
import { useCarouselContext } from "../CarouselContext";
import { CarouselItem } from "../CarouselItem";
import { CarouselItems } from "../CarouselItems";
import { CarouselInternals } from "../types";

import { CarouselPrev } from "./CarouselPrev";

let wrapper;
const carouselItemsCount = 2;
let carouselContextData: CarouselInternals;
let loop = false;

const DumbComponent = () => {
  carouselContextData = useCarouselContext();
  return null;
};

const buildWrapper = () => {
  wrapper = mount(
    <Carousel loop={loop}>
      <CarouselItems>
        {[...new Array(carouselItemsCount)].map((_, i) => (
          <CarouselItem key={i} data-testid={i} />
        ))}
      </CarouselItems>
      <CarouselPrev>Previous</CarouselPrev>
      <DumbComponent />
    </Carousel>
  );
};

baseTests(() => {
  buildWrapper();
  return wrapper;
});

describe("CarouselPrev", () => {
  beforeEach(() => {
    loop = false;
    buildWrapper();
  });

  it("should be disabled when the Carousel is at the first Item", () => {
    expect(wrapper.find(Button).prop("disabled")).toBeTruthy();
  });

  it("switches to the previous Carousel Item when clicked", () => {
    act(() => {
      carouselContextData.setActiveIndex(1);
    });
    wrapper.update();
    act(() => {
      wrapper.find(Button).props().onClick();
    });
    wrapper.update();
    expect(carouselContextData.direction).toEqual("left");
    expect(wrapper.find(CarouselItem).prop("data-testid")).toEqual(0);
  });

  it("loops", () => {
    loop = true;
    buildWrapper();
    act(() => {
      wrapper.find(Button).props().onClick();
    });
    wrapper.update();
    expect(wrapper.find(CarouselItem).prop("data-testid")).toEqual(1);
  });
});
