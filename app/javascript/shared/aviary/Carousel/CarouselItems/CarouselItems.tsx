import React, { FC } from "react";
import { animated, config, useTransition } from "react-spring";

import { useCarouselContext } from "../CarouselContext";

import * as styles from "./CarouselItems.styles";

const CarouselItems: FC = ({ children }) => {
  const {
    activeIndex,
    direction,
    itemsToShow,
    loop,
    itemsToScroll,
    carouselItemsCount,
  } = useCarouselContext();

  const items = React.Children.toArray(children);

  const getItemsToShow = () => {
    const lastIndex = Math.min(activeIndex + itemsToShow, items.length);
    let result = items.slice(activeIndex, lastIndex) as any[];
    if (result.length < itemsToShow) {
      if (loop) result = [...result, ...items.slice(0, itemsToShow - result.length)];
      result = [
        ...items.slice(activeIndex - itemsToScroll + result.length, activeIndex),
        ...result,
      ];
    }
    return result;
  };

  const from = direction === "right" ? "100%" : "-100%";
  const leave = direction === "right" ? "-100%" : "100%";

  return (
    <div css={styles.carouselItems({ itemsToShow, carouselItemsCount })}>
      {useTransition(getItemsToShow(), item => item.key + activeIndex, {
        from: { transform: `translate3d(${from}, 0, 0)` },
        enter: { transform: "translate3d(0, 0, 0)" },
        leave: { transform: `translate3d(${leave}, 0, 0)`, position: "absolute" },
        config: config.gentle,
      }).map(({ item, key, props }) => (
        <animated.div
          key={key}
          style={props}
          css={styles.animatedDiv({ itemsToShow, carouselItemsCount })}
        >
          {item}
        </animated.div>
      ))}
    </div>
  );
};

export { CarouselItems };
