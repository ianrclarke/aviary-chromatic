import { mount } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { Carousel } from "../Carousel";
import { useCarouselContext } from "../CarouselContext";
import { CarouselItem } from "../CarouselItem";
import { CarouselInternals } from "../types";

import { CarouselItems } from "./CarouselItems";

// react-spring is mocked at "__mocks__/react-spring.tsx"

describe("CarouselItems", () => {
  let wrapper;
  let itemsToShow = 1;
  let itemsToScroll = 1;
  const carouselItemsCount = 4;
  let carouselContextData: CarouselInternals;

  const DumbComponent = () => {
    carouselContextData = useCarouselContext();
    return null;
  };

  const buildWrapper = () => {
    wrapper = mount(
      <Carousel itemsToShow={itemsToShow} itemsToScroll={itemsToScroll}>
        <CarouselItems>
          {[...new Array(carouselItemsCount)].map((_, i) => (
            <CarouselItem key={i} data-testid={i} />
          ))}
        </CarouselItems>
        <DumbComponent />
      </Carousel>
    );
  };

  beforeEach(() => {
    itemsToShow = 1;
    itemsToScroll = 1;
    buildWrapper();
  });

  baseTests(() => wrapper);

  it("when activeIndex is 0, it renders the first carousel item", () => {
    expect(wrapper.find(CarouselItem).prop("data-testid")).toEqual(0);
  });

  it("when activeIndex is 1, it renders the second carousel item", () => {
    act(() => {
      carouselContextData.setActiveIndex(1);
    });
    wrapper.update();
    expect(wrapper.find(CarouselItem).prop("data-testid")).toEqual(1);
  });

  it("shows 2 items when itemsToShow is set to 2", () => {
    itemsToShow = 2;
    buildWrapper();
    expect(wrapper.find(CarouselItem)).toHaveLength(2);
  });

  it("shows only one more item when itemsToScroll is set to 1 and itemsToShow is set to 2", () => {
    itemsToShow = 2;
    itemsToScroll = 1;

    buildWrapper();
    expect(wrapper.find(CarouselItem).at(0).prop("data-testid")).toEqual(0);
    expect(wrapper.find(CarouselItem).at(1).prop("data-testid")).toEqual(1);

    act(() => {
      carouselContextData.setActiveIndex(1);
    });
    wrapper.update();
    expect(wrapper.find(CarouselItem).at(0).prop("data-testid")).toEqual(1);
    expect(wrapper.find(CarouselItem).at(1).prop("data-testid")).toEqual(2);
  });
});
