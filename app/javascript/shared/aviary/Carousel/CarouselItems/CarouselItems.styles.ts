import { css } from "@emotion/core";

export const carouselItems = ({ carouselItemsCount, itemsToShow }) => css`
  width: 100%;
  position: relative;
  overflow: hidden;
  display: flex;
  justify-content: ${carouselItemsCount < itemsToShow ? "flex-start" : "space-between"};
  align-items: stretch;
`;

export const animatedDiv = ({ itemsToShow, carouselItemsCount }) => css`
  width: calc(100% / ${itemsToShow});
  display: inline-flex;
  justify-content: center;
  align-items: center;
  flex-basis: calc(
    (100% - ${itemsToShow - 1}rem) / ${itemsToShow}
  ); /* to have 1rem gap between each item */

  &:nth-of-type(2) {
    margin: 0 ${carouselItemsCount < itemsToShow ? "1rem" : "0"};
  }
`;
