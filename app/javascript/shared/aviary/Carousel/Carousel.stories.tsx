import React from "react";

import { boolean, number } from "@storybook/addon-knobs";

import { Carousel } from "./Carousel";
import { CarouselItem } from "./CarouselItem";
import { CarouselItems } from "./CarouselItems";
import { CarouselNext } from "./CarouselNext";
import { CarouselPrev } from "./CarouselPrev";
import { CarouselRadio } from "./CarouselRadio";

import { colors } from "@styles";

const storyNotes = `
#### Notes
-- This Carousel is made out of **Several Composable Components**.
Check the story source to better understand how this tool is assembled! Composability gives us special advantages.
For instance, **CarouselRadio can be placed above or below the carousel items**. You can even exclude it entirely!

-- CarouselRadio **is not designed** to be used alongside CarouselNext/CarouselPrev, it should be either radios or buttons not both.
`;

const itemStyle = {
  fontSize: "2rem",
  color: colors.white,
  padding: "1rem",
};

const buttonsWrapperStyle = {
  display: "flex",
  justifyContent: "space-between",
  marginTop: "15px",
};

const colorArray = [
  colors.primary,
  colors.dark,
  colors.grey,
  colors.blue,
  colors.warning,
  colors.purple,
  colors.orangeLight,
  colors.greenLight,
  colors.redLight,
  colors.purpleLight,
  colors.blueLight,
];

export default {
  title: "Aviary|Carousel",
  component: Carousel,
  parameters: { notes: storyNotes },
};

export const WithRadioButtons = () => {
  const numberOfItems = number("Number of items", 5);
  const itemsToShow = number("itemsToShow", 2);

  return (
    <Carousel itemsToShow={itemsToShow}>
      <CarouselItems>
        {[...new Array(numberOfItems)].map((_, i) => (
          <CarouselItem key={i}>
            <div
              style={{
                backgroundColor: colorArray[i],
                ...itemStyle,
              }}
            >
              Slide {i}
            </div>
          </CarouselItem>
        ))}
      </CarouselItems>
      <CarouselRadio />
    </Carousel>
  );
};

export const WithNextPrevButtons = () => {
  // Defining the knobs with const helps with ordering them properly in Storybook
  const numberOfItems = number("Number of items", 5);
  const itemsToShow = number("itemsToShow", 2);
  const itemsToScroll = number("itemsToScroll", 2);
  const loop = boolean("loop", true);

  return (
    <Carousel loop={loop} itemsToShow={itemsToShow} itemsToScroll={itemsToScroll}>
      <CarouselItems>
        {[...new Array(numberOfItems)].map((_, i) => (
          <CarouselItem key={i}>
            <div
              style={{
                backgroundColor: colorArray[i],
                ...itemStyle,
              }}
            >
              Slide {i}
            </div>
          </CarouselItem>
        ))}
      </CarouselItems>
      <div style={buttonsWrapperStyle}>
        <CarouselPrev>Back</CarouselPrev>
        <CarouselNext>Next</CarouselNext>
      </div>
    </Carousel>
  );
};
