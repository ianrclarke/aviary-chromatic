import { createContext, useContext } from "react";

import { CarouselInternals } from "../types.d";

const CarouselContext = createContext<CarouselInternals>(null);
const useCarouselContext = (): CarouselInternals => useContext(CarouselContext);

export { CarouselContext, useCarouselContext };
