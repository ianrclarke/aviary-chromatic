import { mount } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { Carousel } from "./Carousel";
import { useCarouselContext } from "./CarouselContext";
import { CarouselItem } from "./CarouselItem";
import { CarouselItems } from "./CarouselItems";
import { CarouselInternals } from "./types.d";

let carouselContextData: CarouselInternals;
const carouselItemsCount = 2;
let mockOnChange;

describe("Carousel", () => {
  let wrapper;

  const DumbComponent = () => {
    carouselContextData = useCarouselContext();
    return null;
  };

  const buildWrapper = () => {
    return mount(
      <Carousel onChange={mockOnChange}>
        <CarouselItems>
          {[...new Array(carouselItemsCount)].map((_, i) => (
            <CarouselItem key={i} />
          ))}
        </CarouselItems>
        <DumbComponent />
      </Carousel>
    );
  };

  beforeEach(() => {
    mockOnChange = jest.fn();
    wrapper = buildWrapper();
  });

  baseTests(() => wrapper);

  it("The carousel items count", () => {
    expect(carouselContextData.carouselItemsCount).toEqual(carouselItemsCount);
  });

  it("calls onChange when setActiveIndex is called", () => {
    act(() => {
      carouselContextData.setActiveIndex(1);
    });
    wrapper.update();
    expect(mockOnChange).toHaveBeenCalled();
  });
});
