import { mount } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { Radio } from "@shared/aviary";
import { baseTests } from "@testing/baseTests";

import { Carousel } from "../Carousel";
import { useCarouselContext } from "../CarouselContext";
import { CarouselItem } from "../CarouselItem";
import { CarouselItems } from "../CarouselItems";

import { CarouselRadio } from "./CarouselRadio";

describe("CarouselRadio", () => {
  let carouselContextData;
  let wrapper;
  const carouselItemsCount = 2;

  const DumbComponent = () => {
    carouselContextData = useCarouselContext();
    return null;
  };

  const buildWrapper = () => {
    wrapper = mount(
      <Carousel>
        <CarouselItems>
          {[...new Array(carouselItemsCount)].map((_, i) => (
            <CarouselItem key={i} data-testid={i} />
          ))}
        </CarouselItems>
        <CarouselRadio />
        <DumbComponent />
      </Carousel>
    );
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => wrapper);

  it("renders a Radio buttons for each CarouselItem", () => {
    expect(wrapper.find(Radio).length).toEqual(carouselItemsCount);
  });

  it("when clicking on the second Radio button, it renders the second CarouselItem", () => {
    act(() => {
      wrapper.find(Radio).at(1).props().onChange();
    });
    wrapper.update();
    expect(wrapper.find(CarouselItem).prop("data-testid")).toEqual(1);
  });

  it("changes the direction of the carousel properly", () => {
    act(() => {
      wrapper.find(Radio).at(1).props().onChange();
    });
    wrapper.update();
    expect(carouselContextData.direction).toEqual("right");

    act(() => {
      wrapper.find(Radio).at(0).props().onChange();
    });
    wrapper.update();
    expect(carouselContextData.direction).toEqual("left");
  });
});
