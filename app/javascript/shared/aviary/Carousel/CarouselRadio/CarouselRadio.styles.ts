import { css } from "@emotion/core";

export const carouselRadio = css`
  display: flex;
  margin-top: 1rem;
  justify-content: center;
`;

export const radio = css`
  &&& {
    + span {
      padding-left: 0;
      width: 1.125rem;
      margin: 0 0.375rem;
    }
  }
`;
