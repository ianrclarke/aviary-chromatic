import React, { FC } from "react";

import { Radio } from "@shared/aviary/Radio";

import { useCarouselContext } from "../CarouselContext";

import * as styles from "./CarouselRadio.styles";

const CarouselRadio: FC = () => {
  // The "itemsToScroll" Carousel prop doesn't affect how "CarouselRadio" changes pages
  // "CarouselRadio" always turn full pages that depends only on the "itemsToShow" Carousel prop
  const {
    activeIndex,
    setActiveIndex,
    carouselItemsCount,
    setDirection,
    itemsToShow,
    itemsToScroll,
  } = useCarouselContext();

  const handleChange = i => {
    const newActiveIndex = i * itemsToShow;
    setActiveIndex(newActiveIndex);
    setDirection(newActiveIndex > activeIndex ? "right" : "left");
  };

  const radiosCount = Math.ceil(carouselItemsCount / itemsToShow);

  if (radiosCount <= 1) return null;
  return (
    <div css={styles.carouselRadio}>
      {[...new Array(radiosCount)].map((_, i) => (
        <Radio
          css={styles.radio}
          key={i}
          checked={activeIndex === i * itemsToScroll}
          onChange={() => handleChange(i)}
        />
      ))}
    </div>
  );
};

export { CarouselRadio };
