import { ButtonProps } from "@shared/aviary/Button";

const DIRECTION_ENUM = {
  right: "right",
  left: "left",
};

type DirectionType = keyof typeof DIRECTION_ENUM;

interface CarouselInternals {
  activeIndex: number;
  setActiveIndex: (activeIndex: number) => void;
  carouselItemsCount: number;
  loop?: boolean;
  direction: DirectionType;
  setDirection: (direction: DirectionType) => void;
  itemsToShow?: number;
  itemsToScroll?: number;
}

interface CarouselButtonProps extends ButtonProps {
  onChange: (activeIndex: number) => void;
}

export { CarouselInternals, DirectionType, CarouselButtonProps };
