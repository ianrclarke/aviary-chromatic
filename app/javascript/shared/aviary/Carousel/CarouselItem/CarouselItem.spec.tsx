import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { CarouselItem } from "./CarouselItem";

describe("CarouselItem", () => {
  baseTests(() => mount(<CarouselItem />));
});
