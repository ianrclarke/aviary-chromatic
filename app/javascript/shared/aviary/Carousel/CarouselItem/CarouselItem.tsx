import React, { FC } from "react";

import * as styles from "./CarouselItem.styles";

const CarouselItem: FC = ({ children }) => {
  return <div css={styles.carouselItem}>{children}</div>;
};

export { CarouselItem };
