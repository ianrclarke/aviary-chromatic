import { css } from "@emotion/core";

export const carousel = css`
  width: 100%;
`;

export const carouselContainer = css`
  width: 100%;
`;
