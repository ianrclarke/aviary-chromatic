import React, { FC, useMemo, useState } from "react";

import { CarouselContext } from "./CarouselContext";
import { CarouselItems } from "./CarouselItems";
import { DirectionType } from "./types.d";

import * as styles from "./Carousel.styles";

const CarouselItemsType = (<CarouselItems />).type;

interface Props {
  /**
   * Determines the starting CarouselItem when loaded.
   *
   * @default 0
   */
  initialIndex?: number;

  /**
   * Determines the if the Carousel should loop or not
   *
   * @default false;
   */
  loop?: boolean;

  /**
   * Number of items to show at once
   *
   * @default 1
   */
  itemsToShow?: number;

  /**
   * Number of items to scroll
   *
   * @default 1
   */
  itemsToScroll?: number;
  /**
   * Function to trigger when the carousel moves forward of back
   */
  onChange?: (activeIndex: number) => void;
}

const Carousel: FC<Props> = ({
  initialIndex,
  loop,
  children,
  itemsToShow,
  itemsToScroll,
  onChange,
}) => {
  const [activeIndex, setActiveIndex] = useState(initialIndex);
  const [direction, setDirection] = useState<DirectionType>("right");

  const calculateCarouselItemsCount = () => {
    let count = 0;
    React.Children.forEach(children, child => {
      if (React.isValidElement(child) && child.type === CarouselItemsType) {
        count = React.Children.count(child.props.children);
      }
    });
    return count;
  };

  const carouselItemsCount = useMemo(calculateCarouselItemsCount, [children]);

  const onSetActiveIndex = (targetIndex: number) => {
    onChange(targetIndex);
    setActiveIndex(targetIndex);
  };

  return (
    <CarouselContext.Provider
      value={{
        activeIndex,
        setActiveIndex: onSetActiveIndex,
        carouselItemsCount,
        loop,
        direction,
        setDirection,
        itemsToShow,
        itemsToScroll: itemsToScroll || itemsToShow,
      }}
    >
      <div css={styles.carousel}>
        <div css={styles.carouselContainer}>{children}</div>
      </div>
    </CarouselContext.Provider>
  );
};

Carousel.defaultProps = {
  initialIndex: 0,
  loop: false,
  itemsToShow: 1,
  onChange: () => {
    return null;
  },
};

export { Carousel };
