const monthSelector = {
  January: "January",
  February: "February",
  March: "March",
  April: "April",
  May: "May",
  June: "June",
  July: "July",
  August: "August",
  September: "September",
  October: "October",
  November: "November",
  December: "December",
  SelectDate: "select date, the current date selected is {{month}} {{year}}",
  SelectDateYear: "select date, the current date selected is {{year}}",
  SelectAllDates: "select date",
  previousYear: "previous year",
  nextYear: "next year",
  previousMonth: "previous month",
  nextMonth: "next month",
  AllDates: "All dates",
  Clear: "Clear",
};

const attachment = {
  removeAttachment: "Remove attachment: {{name}}",
  attachmentName: "Attachment: {{name}}",
  showMore: "Show more",
  showLess: "Show less",
};

const searchBar = {
  ClearSearch: "Clear search",
  Search: "Search",
};

const dropdown = {
  GoBack: "Go back",
};

export const aviary = {
  dropdown,
  monthSelector,
  searchBar,
  attachment,

  A11yMenu: "Accessible navigation",
  SkipToMainContent: "Skip to main content",

  // Breadcrumb
  collapsedBreadcrumb: "Collapsed breadcrumb",

  // Toasts
  CloseNotification: "Close notification",
};
