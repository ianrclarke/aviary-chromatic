import { css } from "@emotion/core";

import { colors } from "@styles";

export const table = css`
  background-color: ${colors.white};
  color: ${colors.dark};

  td,
  th {
    border: 1px solid ${colors.greyLight};
    border-width: 0 0 1px;
    padding: 0.5rem 0.75rem;
    vertical-align: middle;
  }
  th {
    text-align: left;
  }
  thead {
    td,
    th {
      border-width: 0 0 2px;
      color: ${colors.dark};
    }
  }
  tfoot {
    td,
    th {
      border-width: 2px 0 0;
      color: ${colors.dark};
    }
  }
  tbody tr:last-child {
    td,
    th {
      border-bottom-width: 0;
    }
  }
`;

export const fullWidth = css`
  width: 100%;
`;
