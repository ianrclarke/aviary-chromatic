import React, { FC, HTMLProps } from "react";

import * as styles from "./Table.styles";

interface Props extends HTMLProps<HTMLTableElement> {
  isFullWidth?: boolean;
}

const Table: FC<Props> = ({ isFullWidth, children, ...rest }) => {
  const tableStyles = [styles.table, isFullWidth && styles.fullWidth];
  return (
    <table css={tableStyles} {...rest}>
      {children}
    </table>
  );
};

export { Table };
