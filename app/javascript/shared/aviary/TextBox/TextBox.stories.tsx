import React from "react";

import { TextBox } from "./TextBox";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=383%3A628",
};

export default {
  title: "Aviary|TextBox",
  component: TextBox,
  parameters: { design: componentDesign },
};

export const Default = () => <TextBox />;
