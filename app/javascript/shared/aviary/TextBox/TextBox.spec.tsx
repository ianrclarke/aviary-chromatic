import { mount, ReactWrapper } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { TextBox } from "./TextBox";

describe("TextBox", () => {
  let wrapper: ReactWrapper;
  beforeEach(() => {
    wrapper = mount(<TextBox />);
  });

  baseTests(() => wrapper);
});
