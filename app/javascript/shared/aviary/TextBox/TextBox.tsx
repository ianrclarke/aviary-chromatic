import React, { FC, HTMLProps } from "react";

import { inputTextBoxMaker } from "@styles/emotion-styles/helpers";

const TextBox: FC<HTMLProps<HTMLInputElement>> = props => (
  <input css={inputTextBoxMaker} {...props} />
);

export { TextBox };
