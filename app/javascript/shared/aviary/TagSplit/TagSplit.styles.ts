import { css } from "@emotion/core";

import { colors, dimensions, helpers, utilities, typography, animations } from "@styles";

export const buttonGroup = css`
  &:hover {
    button {
      border-color: ${colors.primary};
      box-shadow: 1px 1px 2px 0 ${helpers.hexToRgba(colors.grey, 0.25)};
    }
  }
`;

export const button = {
  base: css`
    color: ${colors.primary};
    background-color: ${colors.white};
    border-radius: ${dimensions.pillBorderRadius} 0 0 ${dimensions.pillBorderRadius};
    border-color: ${colors.light};
    padding: 0;
    align-items: center;
    &:hover,
    &:focus {
      background-color: ${colors.white};
      color: ${colors.primary};
    }
    @media only screen and (max-width: ${dimensions.phoneMax}) {
      font-size: ${typography.smallLabel};
      height: 1.5rem;
    }
  `,
  noThumbnail: css`
    &:hover,
    &:focus {
      color: ${colors.white};
      background-color: ${colors.primaryHover};
      svg path {
        fill: ${colors.white};
      }
    }
    &:focus + button,
    &:hover + button {
      background-color: ${colors.primaryHover};
      svg path {
        fill: ${colors.white};
      }
      &::before {
        background-color: ${helpers.hexToRgba(colors.separator, 0.25)};
      }
    }
  `,
  active: css`
    color: ${colors.white};
    background-color: ${colors.primary};
    border-color: ${colors.primary};
    box-shadow: 1px 1px 2px 0 ${helpers.hexToRgba(colors.grey, 0.25)};
  `,
};

export const icon = {
  base: css`
    path {
      fill: ${colors.primary};
    }
    left: 0.75rem;
    position: absolute;
    ${utilities.transition}
  `,
  active: css`
    path {
      fill: ${colors.white};
    }
    transform: rotate(90deg);
  `,
};

export const iconButton = {
  base: css`
    display: inline-flex;
    width: 2rem;
    border-radius: 0 100px 100px 0;
    background-color: ${colors.white};
    border-color: ${colors.light};
    svg {
      height: 1rem;
      @media only screen and (max-width: ${dimensions.phoneMax}) {
        height: 0.75rem;
      }
      path {
        fill: ${colors.light};
      }
    }
    &:hover,
    &:focus {
      background-color: ${colors.white};
      svg path {
        fill: ${colors.grey};
      }
    }

    @media only screen and (max-width: ${dimensions.phoneMax}) {
      font-size: ${typography.small};
      height: 1.5rem;
      width: 1.375rem;
    }
  `,
  noThumbnail: css`
    &:hover,
    &:focus {
      background-color: ${colors.primaryHover};
      svg path {
        fill: ${colors.white};
      }
    }
  `,
  active: css`
    background-color: ${colors.primary};
    border-color: ${colors.primary};
    box-shadow: 1px 1px 2px 0 ${helpers.hexToRgba(colors.grey, 0.25)};
    svg path {
      fill: ${colors.white};
    }
  `,
};

export const leftLatch = {
  base: css`
    &:before {
      height: 1.625rem;
      background-color: ${colors.separator};

      @media only screen and (max-width: ${dimensions.phoneMax}) {
        height: 0.875rem;
      }
    }
  `,
  active: css`
    &:before {
      background-color: ${helpers.hexToRgba(colors.separator, 0.25)};
    }
  `,
};

export const thumbnail = {
  base: css`
    height: calc(2.25rem - 0.125rem);
    width: calc(2.25rem - 0.125rem);
    border-radius: ${dimensions.circleRadius};
    @media only screen and (max-width: ${dimensions.phoneMax}) {
      display: none;
    }
  `,
  isLoading: css`
    ${animations.skeletonPulse};
  `,
  isLoaded: (url: string) => css`
    background: center / contain no-repeat url("${url}");
  `,
};

export const label = css`
  margin-left: 1.25rem;
  padding: calc(0.375rem - 0.0625rem) 0.75rem;
  @media only screen and (max-width: ${dimensions.phoneMax}) {
    margin-top: -0.125rem;
    padding-top: 0;
    padding-bottom: 0;
  }
`;
