import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { TagSplit } from "./TagSplit";

describe("TagSplit", () => {
  let wrapper;

  const buildWrapper = () => {
    wrapper = mount(<TagSplit label="This tag is applied" ariaLabel="Remove tag" />);
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => wrapper);
});
