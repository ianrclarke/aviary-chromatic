import React from "react";

import { boolean, number, text } from "@storybook/addon-knobs";

import { TagSplit } from "./TagSplit";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=960%3A66",
};

export default {
  title: "Aviary|TagSplit",
  component: TagSplit,
  parameters: { design: componentDesign },
};

export const Default = () => (
  <TagSplit
    maxLabelLength={number("Maximum label length", 20)}
    label={text("Tag label", "This tag is applied")}
    isActive={boolean("isActive", false)}
    isClosing={boolean("isClosing", false)}
    ariaLabel="Remove tag"
  />
);

export const Thumbnail = () => (
  <TagSplit
    maxLabelLength={number("Maximum label length", 20)}
    thumbnailUrl="https://i.picsum.photos/id/401/200/300.jpg"
    label={text("Tag label", "This tag is applied")}
    isClosing={boolean("isClosing", false)}
    ariaLabel="Remove tag"
  />
);
