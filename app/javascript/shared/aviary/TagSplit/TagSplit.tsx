import { faTimes } from "@fortawesome/pro-light-svg-icons";
import { faCaretRight } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC } from "react";

import { Button, ButtonGroup, IconButton } from "@shared/aviary";
import { useBackgroundImageLoading } from "@shared/hooks";

import * as styles from "./TagSplit.styles";

interface Props {
  label: string;
  thumbnailUrl?: string;
  ariaLabel: string;
  handleClick?: () => void;
  handleClose?: () => void;
  isActive?: boolean;
  maxLabelLength?: number;
  isClosing?: boolean;
}
const DEFAULT_LABEL_LENGTH = 16;

const TagSplit: FC<Props> = ({
  label,
  thumbnailUrl,
  ariaLabel,
  handleClick,
  handleClose,
  isActive,
  maxLabelLength,
  isClosing,
  ...rest
}) => {
  const isThumbnail = !!thumbnailUrl;

  const { isImageLoading } = useBackgroundImageLoading(thumbnailUrl);

  const buildIconStyles = () => [
    styles.iconButton.base,
    isActive && styles.iconButton.active,
    !isThumbnail && styles.button.noThumbnail,
    styles.leftLatch.base,
    isActive && styles.leftLatch.active,
  ];

  const getImageCSS = () => {
    if (isImageLoading) return styles.thumbnail.isLoading;
    else return styles.thumbnail.isLoaded(thumbnailUrl);
  };

  const renderIcon = () => {
    if (isThumbnail) {
      return <div css={[getImageCSS(), styles.thumbnail.base]} />;
    }
    return (
      <FontAwesomeIcon
        css={[styles.icon.base, isActive && styles.icon.active]}
        icon={faCaretRight}
      />
    );
  };

  const handleButtonClick = () => {
    if (isThumbnail) {
      window.open(thumbnailUrl, "_newtab");
    } else {
      handleClick();
    }
  };

  return (
    <ButtonGroup {...rest} css={styles.buttonGroup}>
      <Button
        title={label}
        isSize="small"
        css={[
          styles.button.base,
          !isThumbnail && styles.button.noThumbnail,
          isActive && styles.button.active,
        ]}
        onClick={handleButtonClick}
      >
        {renderIcon()}
        <p css={styles.label}>
          {label.length < maxLabelLength ? label : `${label.substr(0, maxLabelLength - 3)}...`}
        </p>
      </Button>
      <IconButton
        isLoading={isClosing}
        isSize="small"
        css={buildIconStyles()}
        onClick={handleClose}
        aria-label={ariaLabel}
      >
        <FontAwesomeIcon size={"2x"} icon={faTimes} />
      </IconButton>
    </ButtonGroup>
  );
};

TagSplit.defaultProps = { isActive: false, maxLabelLength: DEFAULT_LABEL_LENGTH };

export { TagSplit };
