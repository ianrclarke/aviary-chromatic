import React, { useState } from "react";

import { boolean, number, select } from "@storybook/addon-knobs";
import i18n from "i18next";
import { I18nextProvider } from "react-i18next";
import { MONTHS as MONTH_KEYS } from "./Months";
import { MonthSelector } from "./MonthSelector";

const localI18n = i18n.createInstance();
localI18n.init({
  ns: ["common"],
  defaultNS: "common",
  fallbackNS: "common",
  lng: "en",
  fallbackLng: "en",
  resources: {
    en: {
      common: {
        January: "January",
        February: "February",
        March: "March",
        April: "April",
        May: "May",
        June: "June",
        July: "July",
        August: "August",
        September: "September",
        October: "October",
        November: "November",
        December: "December",
        SelectDate: "select date, the current date selected is {{month}} {{year}}",
        SelectDateYear: "select date, the current date selected is {{month}} {{year}}",
        SelectAllDates: "select date",
        previousYear: "previous year",
        nextYear: "next year",
        previousMonth: "previous month",
        nextMonth: "next month",
        AllDates: "All dates",
        Clear: "Clear",
      },
    },
  },
});

const MONTHS = MONTH_KEYS.reduce((obj, month, index) => {
  return { ...obj, [month]: index };
}, {});

export default {
  title: "Aviary|MonthSelector",
  component: MonthSelector,
};

export const Default = () => {
  const [month, setMonth] = useState<number>(undefined);
  const [year, setYear] = useState<number>(undefined);

  return (
    <I18nextProvider i18n={localI18n}>
      <MonthSelector
        maxYear={number("maxYear", 2020)}
        minYear={number("minYear", 2007)}
        minMonth={select("minMonth", MONTHS, 0)}
        maxMonth={select("maxMonth", MONTHS, 11)}
        month={month}
        year={year}
        setMonth={setMonth}
        setYear={setYear}
        disabled={boolean("disabled", false)}
        isFullWidth={boolean("isFullWidth", false)}
      />
    </I18nextProvider>
  );
};
