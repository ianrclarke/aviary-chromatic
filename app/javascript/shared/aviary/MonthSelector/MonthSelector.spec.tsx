import { mount } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { DropdownItem, DropdownTrigger } from "@shared/aviary/Dropdown";
import { baseTests } from "@testing/baseTests";

import { MonthSelector } from "./MonthSelector";
import { MONTHS } from "./Months";

describe("MonthSelector", () => {
  let wrapper;
  let setMonth;
  let setYear;
  let month;
  let year;
  let minMonth;
  let minYear;
  let maxMonth;
  let maxYear;
  let mockDisabled;

  beforeEach(() => {
    setMonth = jest.fn();
    setYear = jest.fn();
    month = 1;
    year = 2020;
    minMonth = 3;
    minYear = 2007;
    maxMonth = 10;
    maxYear = 2020;
    mockDisabled = false;
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = mount(
      <MonthSelector
        month={month}
        setMonth={setMonth}
        year={year}
        setYear={setYear}
        minMonth={minMonth}
        maxMonth={maxMonth}
        minYear={minYear}
        maxYear={maxYear}
        disabled={mockDisabled}
      />
    );
  };

  const openMenu = () => {
    act(() => {
      wrapper.find(DropdownTrigger).find("div").simulate("click");
    });
    wrapper.update();
  };

  baseTests(() => wrapper);

  describe("Change date", () => {
    it("Sets the dropdown trigger to the current month", () => {
      const text = wrapper.find(DropdownTrigger).text();
      expect(text).toEqual(`${MONTHS[month]} ${year}`);
    });

    it("selects the month", () => {
      openMenu();
      wrapper.find(DropdownItem).at(1).props().onSelect();
      expect(setMonth).toHaveBeenCalledWith(1);
    });

    it("goes to next month when clicking the(>) button", () => {
      const nextMonthBtn = wrapper.find('[data-testid="increase-month"]');
      nextMonthBtn.props().onClick();
      expect(setMonth).toHaveBeenCalledWith(month + 1);
    });

    it("goes to the previous month when clicking the (<) button", () => {
      const prevMonthBtn = wrapper.find('[data-testid="decrease-month"]');
      prevMonthBtn.props().onClick();
      expect(setMonth).toHaveBeenCalledWith(month - 1);
    });

    it("decreases the year", () => {
      year = maxYear;
      buildWrapper();

      openMenu();

      act(() => {
        wrapper.find('[data-testid="decrease-year"]').props().onClick();
      });
      wrapper.update();

      act(() => {
        wrapper.find('[data-testid="select-year"]').props().onClick();
      });
      wrapper.update();

      expect(setYear).toHaveBeenCalledWith(year - 1);
    });

    it("increases the year", () => {
      year = minYear;
      buildWrapper();

      openMenu();

      act(() => {
        wrapper.find('[data-testid="increase-year"]').props().onClick();
      });
      wrapper.update();

      act(() => {
        wrapper.find('[data-testid="select-year"]').props().onClick();
      });
      wrapper.update();

      expect(setYear).toHaveBeenCalledWith(year + 1);
    });

    it("goes from December to January and increases the year", () => {
      month = 11;
      buildWrapper();

      wrapper.find('[data-testid="increase-month"]').props().onClick();
      expect(setMonth).toHaveBeenCalledWith(0);
      expect(setYear).toHaveBeenCalledWith(year + 1);
    });

    it("goes from January to December and decreases the year", () => {
      month = 0;
      buildWrapper();

      wrapper.find('[data-testid="decrease-month"]').props().onClick();
      expect(setMonth).toHaveBeenCalledWith(11);
      expect(setYear).toHaveBeenCalledWith(year - 1);
    });

    it("clears the date", () => {
      const prevMonthBtn = wrapper.find('[data-testid="decrease-month"]');
      prevMonthBtn.props().onClick();
      openMenu();
      act(() => {
        wrapper.find('[data-testid="clear-button"]').props().onClick();
      });
      expect(setMonth).toHaveBeenCalledWith(undefined);
      expect(setYear).toHaveBeenCalledWith(undefined);
    });

    it("selects the year as the date", () => {
      openMenu();
      act(() => {
        wrapper.find('[data-testid="select-year"]').props().onClick();
      });
      expect(setYear).toHaveBeenCalledWith(year);
    });
  });

  describe("disabled state", () => {
    it("does not incremement the year if max year is equal to the current year", () => {
      year = maxYear;
      buildWrapper();

      openMenu();

      const incrementYear = () => wrapper.find('[data-testid="increase-year"]');

      act(() => {
        incrementYear().props().onClick();
      });
      wrapper.update();
      expect(incrementYear().props().disabled).toBe(true);
    });

    it("does not decrement the year if the year is equal to minYear", () => {
      minYear = year;
      buildWrapper();

      openMenu();

      const decrementYear = () => wrapper.find('[data-testid="decrease-year"]');

      act(() => {
        decrementYear().props().onClick();
      });
      wrapper.update();

      expect(decrementYear().props().disabled).toBe(true);
    });

    it("does not incremement the month if max year is equal to the current year and max month is month", () => {
      year = maxYear;
      month = maxMonth;
      buildWrapper();

      const incrementMonth = wrapper.find('[data-testid="increase-month"]');
      expect(incrementMonth.props().disabled).toBe(true);
    });

    it("does not decrement the month if the year is equal to minYear and min month is month", () => {
      year = minYear;
      month = minMonth;
      buildWrapper();

      const decrementMonth = wrapper.find('[data-testid="decrease-month"]');

      expect(decrementMonth.props().disabled).toBe(true);
    });

    it("all buttons are disabled when disabled prop is true", () => {
      mockDisabled = true;
      buildWrapper();

      const decrementMonth = wrapper.find('[data-testid="decrease-month"]');
      const incrementMonth = wrapper.find('[data-testid="increase-month"]');

      expect(decrementMonth.props().disabled).toBe(true);
      expect(incrementMonth.props().disabled).toBe(true);
    });
  });

  describe("months shown", () => {
    it("shows all the months by default", () => {
      openMenu();
      expect(wrapper.find(DropdownItem)).toHaveLength(12);
    });

    it("disables selecting a month past the maxMonth when year is maxYear", () => {
      year = maxYear;
      buildWrapper();

      openMenu();
      wrapper.find(DropdownItem).forEach(dropdownItem => {
        const { disabled, ["data-testid"]: dropdownMonth } = dropdownItem.props();

        expect(disabled).toEqual(MONTHS.indexOf(dropdownMonth) > maxMonth);
      });
    });

    it("disables selecting a month past the maxMonth when year is undefined", () => {
      year = undefined;
      buildWrapper();

      openMenu();
      wrapper.find(DropdownItem).forEach(dropdownItem => {
        const { disabled, ["data-testid"]: dropdownMonth } = dropdownItem.props();

        expect(disabled).toEqual(MONTHS.indexOf(dropdownMonth) > maxMonth);
      });
    });

    it("disables selecting a month past the minMonth when year is minYear", () => {
      year = minYear;
      buildWrapper();

      openMenu();
      wrapper.find(DropdownItem).forEach(dropdownItem => {
        const { disabled, ["data-testid"]: dropdownMonth } = dropdownItem.props();

        expect(disabled).toEqual(MONTHS.indexOf(dropdownMonth) < minMonth);
      });
    });

    it("disables selecting a month outside the range of the minMonth and maxMonth when minYear and maxYear are the current year", () => {
      maxYear = year;
      minYear = year;
      buildWrapper();

      openMenu();
      wrapper.find(DropdownItem).forEach(dropdownItem => {
        const { disabled, ["data-testid"]: dropdownMonth } = dropdownItem.props();

        expect(disabled).toEqual(
          MONTHS.indexOf(dropdownMonth) < minMonth || MONTHS.indexOf(dropdownMonth) > maxMonth
        );
      });
    });
  });
});
