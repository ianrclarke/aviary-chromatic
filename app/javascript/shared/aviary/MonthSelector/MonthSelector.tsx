import { faChevronLeft, faChevronRight } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC, HTMLProps, useState } from "react";
import { useTranslation } from "react-i18next";

import {
  Dropdown,
  DropdownContent,
  DropdownHeader,
  DropdownItem,
  DropdownTrigger,
} from "@shared/aviary/Dropdown";

import { MONTHS } from "./Months";

import * as styles from "./MonthSelector.styles";

interface Props extends HTMLProps<HTMLDivElement> {
  month: number;
  setMonth: (value: number) => void;
  year: number;
  setYear: (value: number) => void;
  maxMonth?: number;
  maxYear?: number;
  minYear?: number;
  minMonth?: number;
  disabled?: boolean;
  isFullWidth?: boolean;
}

const NOW = new Date();
const MAX_YEAR = NOW.getFullYear();
const MAX_MONTH = NOW.getMonth();
const MIN_YEAR = 2007;
const MIN_MONTH = 0;
const defaultProps: Partial<Props> = {
  maxMonth: MAX_MONTH,
  maxYear: MAX_YEAR,
  minMonth: MIN_MONTH,
  minYear: MIN_YEAR,
};

const MonthSelector: FC<Props> = ({
  month,
  setMonth,
  year,
  setYear,
  maxYear,
  maxMonth,
  minYear,
  minMonth,
  disabled,
  isFullWidth,
  ...rest
}) => {
  const { t } = useTranslation("aviary:monthSelector");

  const [isDropdownOpen, setIsDropdownOpen] = useState<boolean>(false);
  const [buttonYear, setButtonYear] = useState<number>(NOW.getFullYear());
  const atMaxYear = () => year >= maxYear;
  const atButtonMaxYear = () => buttonYear >= maxYear;
  const atMaxDate = () => !isMonthSelectorFalsy(month) && atMaxYear() && month >= maxMonth;
  const atMinYear = () => !isMonthSelectorFalsy(year) && year <= minYear;
  const atButtonMinYear = () => buttonYear <= minYear;
  const atMinDate = () => !isMonthSelectorFalsy(month) && atMinYear() && month <= minMonth;

  const handleDropdownToggle = (isOpen: boolean): void => {
    if (isOpen) {
      setButtonYear(year || NOW.getFullYear());
    }
    setIsDropdownOpen(isOpen);
  };

  const increaseYear = () => {
    setYear(year + 1);

    if (month > maxMonth) {
      setMonth(maxMonth);
    }
  };

  const decreaseYear = () => {
    if (isMonthSelectorFalsy(year)) {
      year = NOW.getFullYear();
    }

    setYear(year - 1);

    if (month < minMonth) {
      setMonth(minMonth);
    }
  };

  const increaseMonth = () => {
    if (isMonthSelectorFalsy(month)) {
      setMonth(NOW.getMonth());
      setYear(NOW.getFullYear());
    } else if (month === 11) {
      increaseYear();
      setMonth(0);
    } else {
      setMonth(month + 1);
    }
  };

  const decreaseMonth = () => {
    if (isMonthSelectorFalsy(month)) {
      month = NOW.getMonth();
      year = NOW.getFullYear();
    }

    if (month === 0) {
      decreaseYear();
      setMonth(11);
    } else {
      setMonth(month - 1);
    }
  };

  const selectedDate = () => {
    if (isMonthSelectorFalsy(year)) {
      return t("aviary:monthSelector.AllDates");
    } else if (!isMonthSelectorFalsy(year) && isMonthSelectorFalsy(month)) {
      return <>{year}</>;
    } else {
      return (
        <>
          {t(`${MONTHS[month]}`) + " "}
          {year}
        </>
      );
    }
  };

  const setNewMonth = monthKey => {
    setYear(buttonYear);
    setMonth(getMonthIndex(monthKey));
  };

  const conditionalStyles = element => {
    return [element.base, isFullWidth && element.fullWidth];
  };

  const getMonthIndex = monthKey => MONTHS.findIndex(monthStr => monthStr === monthKey);
  const isMonthPastMaxDateBoundary = (monthIndex: number): boolean =>
    atButtonMaxYear() && monthIndex > maxMonth;
  const isMonthPastMinDateBoundary = (monthIndex: number): boolean =>
    atButtonMinYear() && monthIndex < minMonth;

  const isMonthOutsideLimits = (monthKey: string): boolean => {
    const monthIndex = getMonthIndex(monthKey);
    return isMonthPastMaxDateBoundary(monthIndex) || isMonthPastMinDateBoundary(monthIndex);
  };

  const isMonthSelectorFalsy = dateInput => {
    if (dateInput === null || dateInput === undefined) {
      return true;
    }
  };

  const increaseButtonYear = () => {
    setButtonYear(buttonYear + 1);
  };

  const decreaseButtonYear = () => {
    setButtonYear(buttonYear - 1);
  };

  const showButtonYear = () => {
    return buttonYear;
  };

  const setDateToYear = () => {
    setMonth(undefined);
    setYear(buttonYear);
    handleDropdownToggle(false);
  };

  const clear = () => {
    setYear(undefined);
    setMonth(undefined);
    setButtonYear(NOW.getFullYear());
    handleDropdownToggle(false);
  };

  const removeFocusOnClick = e => e.preventDefault();

  const getSelectDateLabel = () => {
    if (month === undefined && year === undefined) {
      return t(`aviary:monthSelector.SelectAllDates`);
    }

    if (month === undefined) {
      return t(`aviary:monthSelector.SelectDateYear`, { year });
    }

    return t(`aviary:monthSelector.SelectDate`, { month: MONTHS[month], year });
  };

  return (
    <div css={conditionalStyles(styles.monthContainer)} {...rest}>
      <button
        aria-label={t(`aviary:monthSelector.previousMonth`)}
        data-testid="decrease-month"
        onClick={decreaseMonth}
        css={conditionalStyles(styles.monthChevron)}
        disabled={disabled || atMinDate()}
        onMouseDown={removeFocusOnClick}
      >
        <FontAwesomeIcon css={conditionalStyles(styles.icon)} size="sm" icon={faChevronLeft} />
      </button>
      <Dropdown
        css={conditionalStyles(styles.dropdown)}
        disabled={disabled}
        isOpen={isDropdownOpen}
        triggerCallback={handleDropdownToggle}
        isFullWidth={true}
        dropdownPlacement="bottom"
      >
        <DropdownTrigger
          aria-live="assertive"
          css={styles.dropdownTrigger}
          aria-label={getSelectDateLabel()}
          onMouseDown={removeFocusOnClick}
        >
          {selectedDate()}
        </DropdownTrigger>
        <DropdownContent css={styles.dropdownContent}>
          <DropdownHeader showDivider={false} css={styles.dropdownHeader}>
            <button
              aria-label={t(`aviary:monthSelector.previousYear`)}
              data-testid="decrease-year"
              onClick={decreaseButtonYear}
              disabled={atButtonMinYear()}
              css={conditionalStyles(styles.nextYearChevron)}
              onMouseDown={removeFocusOnClick}
            >
              <FontAwesomeIcon css={styles.icon} size="sm" icon={faChevronLeft} />
            </button>
            <button
              css={styles.setYearButton}
              onClick={setDateToYear}
              onMouseDown={removeFocusOnClick}
              data-testid="select-year"
            >
              {showButtonYear()}
            </button>
            <button
              aria-label={t(`aviary:monthSelector.nextYear`)}
              data-testid="increase-year"
              onClick={increaseButtonYear}
              disabled={atButtonMaxYear()}
              css={conditionalStyles(styles.prevYearChevron)}
              onMouseDown={removeFocusOnClick}
            >
              <FontAwesomeIcon css={styles.icon} size="sm" icon={faChevronRight} />
            </button>
          </DropdownHeader>
          <div css={styles.dropdownItemsContainer}>
            {MONTHS.map(monthKey => (
              <DropdownItem
                css={styles.dropdownItem}
                key={monthKey}
                data-testid={monthKey}
                onSelect={() => setNewMonth(monthKey)}
                disabled={isMonthOutsideLimits(monthKey)}
                onMouseDown={removeFocusOnClick}
              >
                {t(`${monthKey}`)}
              </DropdownItem>
            ))}
          </div>
          <div css={styles.clear}>
            <button
              css={styles.clearButton}
              onClick={clear}
              onMouseDown={removeFocusOnClick}
              data-testid="clear-button"
            >
              {t(`Clear`)}
            </button>
          </div>
        </DropdownContent>
      </Dropdown>
      <button
        css={conditionalStyles(styles.monthChevron)}
        aria-label={t(`aviary:monthSelector.nextMonth`)}
        data-testid="increase-month"
        onClick={increaseMonth}
        disabled={disabled || atMaxDate()}
        onMouseDown={removeFocusOnClick}
      >
        <FontAwesomeIcon css={conditionalStyles(styles.icon)} size="sm" icon={faChevronRight} />
      </button>
    </div>
  );
};

MonthSelector.defaultProps = defaultProps;

export { MonthSelector };
