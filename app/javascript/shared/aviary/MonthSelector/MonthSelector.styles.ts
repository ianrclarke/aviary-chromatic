import { css } from "@emotion/core";

import { colors, dimensions, helpers } from "@styles";

export const monthContainer = {
  base: css`
    background-color: white;
    border: 1px solid ${colors.lightGreen};
    border-radius: 4px;
    display: inline-flex;
    color: ${colors.grey};
    position: relative;
    &:focus-within {
      box-shadow: 0 1px 10px 0 ${helpers.hexToRgba(colors.grey, 0.25)};
    }
  `,

  fullWidth: css`
    width: 100%;
    display: flex;
    justify-content: space-between;
  `,
};

export const dropdown = {
  base: css`
    position: initial;
    padding: 0.5rem 0;
  `,
  fullWidth: css`
    position: initial;
  `,
};

export const monthChevron = {
  base: css`
    border: none;
    padding: 0.5rem 1rem;
    cursor: pointer;
    background: transparent;
    &:hover {
      background-color: ${colors.green};
      path {
        fill: ${colors.white};
      }
    }
    &:disabled {
      cursor: not-allowed;
      &:hover {
        background-color: ${colors.white};
        path {
          fill: ${colors.green};
        }
      }
    }
  `,

  fullWidth: css`
    width: 15%;
  `,
};

export const dropdownTrigger = css`
  cursor: pointer;
  padding: 0 1rem;
  border-right: 1px solid ${helpers.hexToRgba(colors.lightGreen, 0.53)};
  border-left: 1px solid ${helpers.hexToRgba(colors.lightGreen, 0.53)};
  min-width: 9.4375rem;
  text-align: center;
`;

export const dropdownContent = css`
  width: 100%;
  margin-top: 0.75rem;
  min-height: 17.625rem;
  &&& {
    overflow-y: hidden;
  }
`;

export const dropdownHeader = css`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0.5rem 0;
  font-weight: 600;
  &&& {
    border-bottom: 1px solid ${helpers.hexToRgba(colors.grey, 0.25)};
  }
`;

export const dropdownItemsContainer = css`
  overflow: scroll;
  height: 12.325rem;
  margin-right: 0.5rem;
`;

export const dropdownItem = css`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0.3rem 0;
  &:first-of-type {
    margin-top: 0.5rem;
  }
`;

export const icon = {
  base: css`
    display: flex;
    align-items: center;
    path {
      fill: ${colors.green};
    }
  `,

  fullWidth: css`
    display: inline-block;
  `,
};

export const nextYearChevron = {
  base: css`
    border: none;
    padding-right: 1.3125rem;
    cursor: pointer;
    height: inherit;
    background: transparent;
    color: ${colors.grey};

    &:hover {
      color: ${colors.primaryHover};
    }

    &:disabled {
      cursor: not-allowed;
      color: ${colors.background};

      &:hover {
        background-color: ${colors.white};
      }
    }
  `,

  fullWidth: css`
    width: 20%;
  `,
};

export const prevYearChevron = {
  base: css`
    border: none;
    padding-left: 1.3125rem;
    cursor: pointer;
    height: inherit;
    background: transparent;
    color: ${colors.grey};

    &:hover {
      color: ${colors.primaryHover};
    }

    &:disabled {
      cursor: not-allowed;
      color: ${colors.background};

      &:hover {
        background-color: ${colors.white};
      }
    }
  `,

  fullWidth: css`
    width: 20%;
  `,
};

export const setYearButton = css`
  font-size: 0.95rem;
  background-color: ${colors.white};
  color: ${colors.primary};
  border: 1px solid ${colors.primary};
  border-radius: ${dimensions.borderRadius};
  cursor: pointer;

  &:hover {
    background-color: ${colors.primary};
    color: ${colors.white};
  }

  &:active {
    background-color: ${colors.primaryActive};
  }
`;

export const clear = css`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0.5rem 0;
  font-weight: 600;
  &&& {
    border-top: 1px solid ${helpers.hexToRgba(colors.grey, 0.25)};
  }
`;

export const clearButton = css`
  border: 0;
  font-size: 1rem;
  cursor: pointer;
  color: ${colors.danger};
`;
