import React, { FC, HTMLProps } from "react";

import * as styles from "./Separator.styles";

const Separator: FC<HTMLProps<HTMLHRElement>> = props => {
  return <hr css={styles.separator} {...props} />;
};

export { Separator };
