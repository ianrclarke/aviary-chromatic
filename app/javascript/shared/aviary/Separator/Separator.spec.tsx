import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Separator } from "./Separator";

describe("Separator component", () => {
  let wrapper;
  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = mount(<Separator />);
  };

  baseTests(() => wrapper);
});
