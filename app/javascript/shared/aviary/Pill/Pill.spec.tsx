import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Pill } from "./Pill";

describe("Pill", () => {
  let wrapper;
  let label;

  const buildWrapper = () => {
    wrapper = mount(<Pill isColor="primary">{label}</Pill>);
  };

  beforeEach(() => {
    label = "Active";
    buildWrapper();
  });

  baseTests(() => wrapper);
});
