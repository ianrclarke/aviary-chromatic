import { css } from "@emotion/core";

import { dimensions, helpers, typography } from "@styles";
import { ColorProfile } from "@styles/emotion-styles/colorProfiles/colorProfile";

export const base = css`
  ${helpers.fontSizeNormalizer(16)};
  border-radius: ${dimensions.pillBorderRadius};
  text-align: center;
  font-weight: ${typography.bold};
  padding: 0 0.5rem;
  min-width: 2rem;
`;

export const size = {
  small: css`
    ${helpers.fontSizeNormalizer(14)}
    padding: 0.125rem 0.5rem;
  `,
  large: css`
    ${helpers.fontSizeNormalizer(20)}
    padding: 0.125rem 0.5rem;
  `,
};

export const profileStyles = (profile: ColorProfile) => css`
  color: ${profile.baseColor};
  background-color: ${profile.extraLightColor};
  border-color: transparent;
`;
