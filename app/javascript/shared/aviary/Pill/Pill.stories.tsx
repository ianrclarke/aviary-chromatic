import React from "react";

import { select, text } from "@storybook/addon-knobs";

import { AVIARY_COLORS } from "@shared/aviary/aviaryColors";
import { Size } from "@shared/types/sizes.d";

import { PillContainer } from "./PillContainer";

import { Pill, PillSizes } from "./Pill";

export default {
  title: "Aviary/Pill",
  component: Pill,
};

export const Default = () => (
  <Pill
    isColor={select<keyof typeof AVIARY_COLORS>("isColor", AVIARY_COLORS, "primary")}
    isSize={select<PillSizes>("isSize", [null, Size.small, Size.large], null)}
  >
    {text("Label", "Available")}
  </Pill>
);

export const WithPillContainer = () => (
  <PillContainer>
    <Pill isColor={select<keyof typeof AVIARY_COLORS>("isColor", AVIARY_COLORS, "primary")}>
      Customize me!
    </Pill>
    <Pill isColor="dark">Dark</Pill>
    <Pill isColor="warning">Warning</Pill>
    <Pill isColor="danger">Danger</Pill>
    <Pill isColor="info">Info</Pill>
    <Pill isColor="purple">Purple</Pill>
  </PillContainer>
);
