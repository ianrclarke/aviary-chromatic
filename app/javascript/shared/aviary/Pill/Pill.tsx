import React, { FC, HTMLProps } from "react";

import { AviaryColorProps } from "@shared/aviary/aviaryColors";
import { Size } from "@shared/types/sizes.d";
import { profiles } from "@styles";

import * as styles from "./Pill.styles";

export type PillSizes = Size.small | Size.large;

interface Props extends AviaryColorProps, HTMLProps<HTMLDivElement> {
  /**
   * Determines the size of the pill
   *
   * @default none
   */
  isSize?: PillSizes;
}

const Pill: FC<Props> = ({ isColor, isSize, children, ...rest }) => {
  const stringToProfile = {
    primary: profiles.primaryProfile,
    dark: profiles.darkProfile,
    danger: profiles.dangerProfile,
    warning: profiles.warningProfile,
    info: profiles.infoProfile,
    purple: profiles.purpleProfile,
  };

  const pillStyles = [
    styles.base,
    styles.profileStyles(stringToProfile[isColor]),
    isSize && styles.size[isSize],
  ];

  return (
    <div css={pillStyles} {...rest}>
      {children}
    </div>
  );
};

Pill.defaultProps = {
  color: "primary",
};

export { Pill };
