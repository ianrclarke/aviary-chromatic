import React, { FC } from "react";

import * as styles from "./PillContainer.styles";

interface Props {
  isCentered?: boolean;
}

const PillContainer: FC<Props> = ({ isCentered, children, ...rest }) => {
  const pillContainerStyles = [styles.pillContainer, isCentered && styles.centered];
  return (
    <div css={pillContainerStyles} {...rest}>
      {children}
    </div>
  );
};

export { PillContainer };
