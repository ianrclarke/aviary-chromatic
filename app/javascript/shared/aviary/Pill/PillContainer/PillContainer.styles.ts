import { css } from "@emotion/core";

export const pillContainer = css`
  display: flex;
  align-content: center;

  & > div {
    flex-grow: 0;
    flex-shrink: 0;
  }
  & > div:not(:last-of-type) {
    margin-right: 0.5rem;
  }
`;

export const centered = css`
  justify-content: center;
`;
