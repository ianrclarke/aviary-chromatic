import { shallow } from "enzyme";
import React from "react";

import { Button } from "@shared/aviary/Button";
import { baseTests } from "@testing/baseTests";

import { ButtonGroup } from "./ButtonGroup";

describe("ButtonGroup tests", () => {
  let wrapper;

  const buildWrapper = () => {
    wrapper = shallow(
      <ButtonGroup>
        <Button>test1</Button>
        <Button>test2</Button>
      </ButtonGroup>
    );
  };

  beforeEach(() => {
    buildWrapper();
  });

  it("renders the passed children", () => {
    expect(wrapper.find("Button")).toHaveLength(2);
  });

  baseTests(() => wrapper);
});
