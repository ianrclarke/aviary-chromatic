import React, { Children, cloneElement, FC, HTMLAttributes } from "react";

interface Props extends HTMLAttributes<HTMLDivElement> {
  children: React.FunctionComponentElement<any>[];
}

const ButtonGroup: FC<Props> = ({ children, ...rest }: Props) => {
  const renderChildren = () => {
    return Children.map(children, (child, index) => {
      if (React.isValidElement(child)) {
        if (index === 0) {
          return cloneElement(child, {
            ...child.props,
            latch: "right",
          });
        } else if (index === children.length - 1) {
          return cloneElement(child, {
            ...child.props,
            latch: "left",
          });
        } else {
          return cloneElement(child, {
            ...child.props,
            latch: "middle",
          });
        }
      }
    });
  };

  return <div {...rest}>{renderChildren()}</div>;
};

export { ButtonGroup };
