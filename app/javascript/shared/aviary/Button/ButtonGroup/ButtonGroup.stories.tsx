import React from "react";

import { action } from "@storybook/addon-actions";
import { boolean, select, withKnobs } from "@storybook/addon-knobs";

import { bulmaColorProps } from "@shared/types/bulmaColors.d";
import { Button } from "../Button";
import { ButtonGroup } from "./ButtonGroup";

export default {
  title: "Aviary|ButtonGroup",
  component: ButtonGroup,
  subcomponents: { Button },
  decorators: [withKnobs],
};

export const Default = () => (
  <ButtonGroup>
    <Button
      onClick={action("You got the onClick for the first button! Awesome job!")}
      isOutlined={boolean("isOutlined (Button #1)", false)}
      isColor={select(
        "isColor (Button #1) ",
        Object.values(bulmaColorProps),
        bulmaColorProps.primary
      )}
    >
      Button!
    </Button>
    <Button
      onClick={action(
        "Did you just activate the onClick for the second button? You rapscallion, you ;)"
      )}
      isOutlined={boolean("isOutlined (Button #2)", false)}
      isColor={select(
        "isColor (Button #2) ",
        Object.values(bulmaColorProps),
        bulmaColorProps.primary
      )}
    >
      Another!
    </Button>
    <Button
      onClick={action("Aw jeez man you nailed the onClick for this button! sick!")}
      isOutlined={boolean("isOutlined (Button #3)", false)}
      isColor={select(
        "isColor (Button #3) ",
        Object.values(bulmaColorProps),
        bulmaColorProps.primary
      )}
    >
      A third!
    </Button>
    <Button
      onClick={action("onClick for the fourth button, huh? I like your style, friend")}
      isOutlined={boolean("isOutlined (Button #4)", false)}
      isColor={select(
        "isColor (Button #4) ",
        Object.values(bulmaColorProps),
        bulmaColorProps.danger
      )}
    >
      Numero quatro!
    </Button>
  </ButtonGroup>
);
