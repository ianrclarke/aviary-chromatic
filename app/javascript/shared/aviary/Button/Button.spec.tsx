import { faSearch } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Button } from "./Button";

let button;

describe("Button", () => {
  let mockDisabled;
  let mockTo;
  let mockHref;

  const buildWrapper = () => {
    button = shallow(
      <Button isColor="primary" disabled={mockDisabled} to={mockTo} href={mockHref}>
        Hello there
      </Button>
    );
  };

  beforeEach(() => {
    mockDisabled = false;
    mockHref = undefined;
    mockTo = undefined;
    buildWrapper();
  });

  it("renders properly with text", () => {
    buildWrapper();
    expect(button.contains("Hello there")).toBeTruthy();
  });

  it("renders with a FontAwsome icon", () => {
    const buttonIcon = shallow(
      <Button isColor="primary">
        Hello there <FontAwesomeIcon icon={faSearch} />
      </Button>
    );
    expect(buttonIcon.contains(<FontAwesomeIcon icon={faSearch} />)).toBe(true);
  });

  it("renders as button element under normal circumstances", () => {
    expect(button.exists("button")).toEqual(true);
  });

  it("renders as button element when disabled is true and there is an href prop", () => {
    mockDisabled = true;
    mockHref = "test";
    buildWrapper();

    expect(button.exists("button")).toEqual(true);
  });

  it("renders as button element when disabled is true and there is a to prop", () => {
    mockDisabled = true;
    mockTo = "test";
    buildWrapper();

    expect(button.exists("button")).toEqual(true);
  });

  it("renders as anchor element when there is an href prop", () => {
    mockHref = "test";
    buildWrapper();

    expect(button.exists("a")).toEqual(true);
  });

  it("renders as Link component when there is a to prop", () => {
    mockTo = "test";
    buildWrapper();

    expect(button.exists("Link")).toEqual(true);
  });

  baseTests(() => button);

  it("renders when an href prop is passed", () => {
    const hrefButton = shallow(
      <Button href="https://google.com">
        Hello there <FontAwesomeIcon icon={faSearch} />
      </Button>
    );
    expect(hrefButton.exists("FontAwesomeIcon")).toEqual(true);
  });

  it("renders when a to prop is passed", () => {
    const LinkButton = shallow(
      <Button to="/p/test">
        Hello there <FontAwesomeIcon icon={faSearch} />
      </Button>
    );
    expect(LinkButton.exists("FontAwesomeIcon")).toEqual(true);
  });
});
