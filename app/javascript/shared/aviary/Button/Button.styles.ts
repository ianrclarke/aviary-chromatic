import { css } from "@emotion/core";

import { animations, baseStyles, colors, dimensions, helpers, timing, utilities } from "@styles";
import { ColorProfile } from "@styles/emotion-styles/colorProfiles/colorProfile";

export const button = {
  noStyle: css`
    appearance: none;
    border: none;
    background-color: transparent;
  `,
  base: css`
    ${baseStyles.controlBase}
    ${utilities.transition}
    justify-content: center;

    path {
      transition: color ${timing.moderatelySlow} ${animations.easeOutCirc};
    }

    cursor: pointer;
    color: ${colors.primary};
    border-color: ${colors.primary};
    font-weight: 600;
    white-space: nowrap;
    height: 2.75rem;

    padding-top: calc(0.375rem - 1px);
    padding-bottom: calc(0.375rem - 1px);
    padding-left: 1rem;
    padding-right: 1rem;

    &:hover {
      box-shadow: 0 1px 2px 0 ${helpers.hexToRgba(colors.dark, 0.25)};
    }

    ${helpers.loadingSpinner(colors.white)}

    &[disabled] {
      font-weight: 500;
      opacity: 0.9;
      user-select: none;
      box-shadow: none;
    }
  `,
  xsmall: css`
    height: 1.5rem;
    padding: 0 0.5rem;
  `,
  small: css`
    height: 2.25rem;
    padding-left: 0.75rem;
    padding-right: 0.75rem;
  `,
  medium: css`
    height: 3.5rem;
    padding-left: 2.625rem;
    padding-right: 2.625rem;
  `,
  fullwidth: css`
    width: 100%;
  `,
};

export const circular = css`
  border-radius: 50%;
`;

/*
 * Conditional Color application functions
 * -- Uses ColorProfiles
 */
export const normalStyles = (profile: ColorProfile) => css`
  color: ${profile.baseText};
  background-color: ${profile.baseColor};
  border-color: ${profile.baseColor};

  path:not(.fa-spinner-third) {
    color: ${profile.baseSvg || profile.baseText};
  }

  &:hover {
    color: ${profile.hoverText};
    background-color: ${profile.hoverColor};
    border-color: ${profile.baseColor};
  }

  &:active,
  &:focus {
    background-color: ${profile.activeColor};
    border-color: ${profile.activeColor};
  }

  &[disabled] {
    color: ${colors.lightGreen};
    background-color: ${colors.lightGrey};
    border-color: ${colors.greyLight};

    path {
      color: ${profile.baseSvg || colors.lightGreen};
    }

    .fa-spinner-third path {
      fill: ${profile.baseColor};
    }

    &:hover,
    &:active {
      background-color: ${colors.lightGrey};
      border-color: ${colors.greyLight};
    }
  }

  &:after {
    border-width: 2px;
    border-style: solid;
    border-color: ${profile.baseText};
    border-right-color: transparent;
    border-top-color: transparent;
  }
`;

export const outlinesStyles = (profile: ColorProfile) => css`
  color: ${profile.outlineText || profile.baseColor};
  background-color: ${profile.baseText};
  border-color: ${profile.outlineBorderColor || profile.baseColor};

  path:not(.fa-spinner-third) {
    color: ${profile.baseColor};
  }

  &:hover {
    color: ${profile.outlineText || profile.baseColor};
    background-color: ${colors.white};
    border-color: ${profile.outlineBorderColor || profile.baseColor};
  }

  &:active,
  &:focus {
    background-color: ${profile.outlineActiveColor};
    border-color: ${profile.outlineActiveBorderColor};
  }

  &:after {
    border: 2px solid ${profile.baseColor};
    border-right-color: transparent;
    border-top-color: transparent;
  }
`;

export const textTypeStyle = (profile: ColorProfile) => css`
  color: ${profile.baseColor};
  background-color: transparent;
  border-color: transparent;

  path:not(.fa-spinner-third) {
    color: ${profile.baseColor};
  }

  &[disabled] {
    &,
    &:hover,
    &:focus {
      background: none;
      border: none;
      opacity: 0.5;
    }
  }

  &:hover {
    box-shadow: none;
    color: ${profile.hoverColor};
    background-color: ${colors.white};
    border-color: ${colors.white};
  }

  &:active,
  &:focus {
    color: ${profile.activeColor};
    background-color: ${colors.lightGrey};
    border-color: ${colors.lightGrey};
    box-shadow: 0 0 0 0.05rem ${helpers.hexToRgba(profile.activeColor, 0.15)};
  }
`;

export const loadingStyles = css`
  color: transparent !important;
  pointer-events: none;
  user-select: none;

  path:not(.fa-spinner-third) {
    color: transparent !important;
  }

  &[disabled] {
    path:not(.fa-spinner-third) {
      color: transparent !important;
    }
  }
`;

/*
 * Loader Icon Styles
 */
export const loader = css`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const loaderIcon = {
  base: css`
    animation-duration: ${timing.slow};
    display: none;
  `,
  loading: css`
    display: block;
  `,
};

export const normalLoader = (profile: ColorProfile) => css`
  path {
    fill: ${profile.baseText};
  }
`;

export const textTypeLoader = (profile: ColorProfile) => css`
  path {
    fill: ${profile.baseColor};
  }
`;

export const outlineLoader = (profile: ColorProfile) => css`
  path {
    fill: ${profile.baseColor};
  }
`;

export const baseIconStyles = css`
  width: 2.75rem;
  height: 2.75rem;
  padding: 0 !important;

  display: flex;
  justify-content: center;
  align-items: center;
`;

export const iconStyles = {
  xsmall: css`
    height: 1.5rem;
    width: 1.5rem;
  `,
  small: css`
    height: 2.25rem;
    width: 2.25rem;
  `,
  medium: css`
    height: 3.5rem;
    width: 3.5rem;
  `,
};

/*
 * Latch Styling, utilized with button groups
 */
const notLeftLatch = css`
  border-right: none;
  &:after {
    position: absolute;
    right: 0;
    content: "";
    background-color: ${colors.splitButtonSeparator};
    height: 1.5rem;
    width: 1px;
    margin-right: -1px;
  }
`;
const notRightLatch = css`
  border-left: none;
  &:before {
    position: absolute;
    left: 0;
    content: "";
    background-color: ${colors.splitButtonSeparator};
    height: 100%;
    width: 1px;
    margin-left: -1px;
  }
`;

const notRightLatchDisabled = css`
  &:before {
    background-color: ${colors.splitButtonDisabledSeparator};
    width: 1px;
  }
`;

export const latch = {
  left: css`
    ${notRightLatch};
    border-radius: 0 ${dimensions.borderRadius} ${dimensions.borderRadius} 0;
  `,
  middle: css`
    ${notLeftLatch}
    ${notRightLatch}
    border-radius: 0;
  `,
  right: css`
    ${notLeftLatch};
    border-radius: ${dimensions.borderRadius} 0 0 ${dimensions.borderRadius};
  `,
  disabled: css`
    ${notRightLatchDisabled}
  `,
};
