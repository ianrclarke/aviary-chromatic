import { faSpinnerThird } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC, HTMLProps, Ref } from "react";
import { Link, LinkProps } from "react-router-dom";

import { BulmaColorProps, bulmaColors } from "@shared/types/bulmaColors";
import { profiles } from "@styles";

import * as styles from "./Button.styles";

type BasicLinkProps = Partial<Pick<LinkProps, "to" | "component" | "replace" | "innerRef">>;

const defaultColor: bulmaColors = "primary";

const defaultProps = {
  isColor: defaultColor,
};

interface ButtonProps<T = HTMLButtonElement> extends BulmaColorProps, BasicLinkProps, HTMLProps<T> {
  innerRef?: Ref<any>;
  isCircular?: boolean;
  isFullWidth?: boolean;
  isIcon?: boolean;
  isLoading?: boolean;
  isOutlined?: boolean;
  isSize?: "xsmall" | "small" | "medium" | "large";
  isStatic?: boolean;
  isText?: boolean;
  latch?: "left" | "right" | "middle";
  noStyle?: boolean;
  "data-testid"?: string;
}

const Button: FC<ButtonProps<HTMLButtonElement | HTMLAnchorElement>> = ({
  children,
  component,
  disabled,
  href,
  innerRef,
  isCircular,
  isColor,
  isFullWidth,
  isIcon,
  isLoading,
  isOutlined,
  isSize,
  isText,
  latch,
  noStyle,
  replace,
  to,
  type,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  "data-testid": dataTestId,
  ...rest
}: ButtonProps<HTMLButtonElement | HTMLAnchorElement>) => {
  const stringToProfile = {
    primary: profiles.primaryProfile,
    secondary: profiles.darkProfile,
    specialty: profiles.specialtyProfile,
    danger: profiles.dangerProfile,
    warning: profiles.warningProfile,
    info: profiles.infoProfile,
    trio: profiles.trioProfile,
    important: profiles.purpleProfile,
  };

  const buttonBuilder = () => {
    if (noStyle) return styles.button.noStyle;
    return [
      isLoading && styles.loadingStyles,
      styles.button.base,
      styles.normalStyles(stringToProfile[isColor]),
      styles.latch[latch],
      disabled && styles.latch.disabled,
      isFullWidth && styles.button.fullwidth,
      isCircular && styles.circular,
      isText && styles.textTypeStyle(stringToProfile[isColor]),
      isOutlined && styles.outlinesStyles(stringToProfile[isColor]),
      isIcon && styles.baseIconStyles,
      isIcon && styles.iconStyles[isSize],
      styles.button[isSize],
    ];
  };

  const loaderBuilder = () => {
    return (
      !noStyle && [
        styles.loaderIcon.base,
        styles.normalLoader(stringToProfile[isColor]),
        isLoading && styles.loaderIcon.loading,
        isOutlined && styles.outlineLoader(stringToProfile[isColor]),
        isText && styles.textTypeLoader(stringToProfile[isColor]),
      ]
    );
  };

  const renderLoader = () => {
    if (!noStyle) {
      return (
        <div css={styles.loader}>
          <FontAwesomeIcon css={loaderBuilder()} icon={faSpinnerThird} spin={true} />
        </div>
      );
    }
  };

  const getValidButtonType = () => {
    if (type === "submit" || type === "reset") {
      return type;
    } else {
      return "button";
    }
  };

  // This is a nasty hack to avoid the component taking outline/focus ring when user clicks on it
  const handleMouseDown = e => {
    e.preventDefault();
    if (rest.onMouseDown) {
      rest.onMouseDown(e);
    }
  };

  const button = () => (
    <button
      css={buttonBuilder()}
      disabled={disabled}
      {...(rest as HTMLProps<HTMLButtonElement>)}
      ref={innerRef}
      type={getValidButtonType()}
      onMouseDown={handleMouseDown}
    >
      {renderLoader()}
      {children}
    </button>
  );

  const anchor = () => (
    <a
      css={buttonBuilder()}
      role="button"
      href={href}
      {...(rest as HTMLProps<HTMLAnchorElement>)}
      ref={innerRef}
    >
      {renderLoader()}
      {children}
    </a>
  );

  const routerLink = () => (
    <Link
      css={buttonBuilder()}
      to={to}
      component={component}
      replace={replace}
      {...rest}
      ref={innerRef}
    >
      {renderLoader()}
      {children}
    </Link>
  );

  const getElement = () => {
    if (!disabled) {
      if (to) {
        return routerLink();
      } else if (href) {
        return anchor();
      }
    }
    return button();
  };

  return getElement();
};

Button.defaultProps = defaultProps;

export { Button, ButtonProps };
