import React from "react";

import { boolean, select, text } from "@storybook/addon-knobs";
import { bulmaColorProps } from "@shared/types/bulmaColors.d";
import { action } from "@storybook/addon-actions";
import { Button } from "./Button";

const componentNotes = `#### Notes\n\b1. Passing an \`href\` prop will change the html element from \`<button>\` to \`<anchor>\`. Additionally, you can pass a \`to\` prop to make the buttton behave as a react router \`<Link />\`.
  \n\b2. \`isLoading\` should not be used in conjunction with \`noStyle\`.`;

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=383%3A655",
};

export default {
  title: "Aviary|Button",
  component: Button,
  parameters: { notes: componentNotes, design: componentDesign },
};

export const Default = () => (
  <Button
    isColor={select<any>("color", Object.keys(bulmaColorProps), bulmaColorProps.primary)}
    isOutlined={boolean("isOutlined", false)}
    disabled={boolean("disabled", false)}
    isLoading={boolean("isLoading", false)}
    isText={boolean("isText", false)}
    isSize={select("isSize", ["xsmall", "small", "medium"], null)}
    isFullWidth={boolean("isFullWidth", false)}
    href={text("href", "")}
    to={text("to", "")}
    onClick={action("clicked or enter/space pressed")}
    noStyle={boolean("noStyle", false)}
  >
    {text("Button text", "Das ein button")}
  </Button>
);
