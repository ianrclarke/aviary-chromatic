import React, { FC } from "react";

import { Button, ButtonProps } from "@shared/aviary/Button";

interface IconButtonProps extends ButtonProps {
  "aria-label": string;
}

const IconButton: FC<IconButtonProps> = ({ children, ...rest }) => {
  return (
    <Button isIcon={true} {...rest}>
      {children}
    </Button>
  );
};

export { IconButton, IconButtonProps };
