import React from "react";

import { faTimes } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { boolean, select, text } from "@storybook/addon-knobs";

import { bulmaColorProps } from "@shared/types/bulmaColors.d";
import { action } from "@storybook/addon-actions";
import { IconButton } from "./IconButton";

const notes = `
#### Notes
Passing an \`href\` prop will change the html element from \`<button>\` to \`<anchor>\`. Additionally, you can pass a \`to\` prop to make the buttton behave as a react router \`<Link />\`.
`;

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=383%3A655",
};

export default {
  title: "Aviary|Button/IconButton",
  component: IconButton,
  parameters: { notes, design: componentDesign },
};

export const Default = () => (
  <IconButton
    aria-label={text("Accessibility Label", "Tester")}
    isColor={select<any>("color", Object.keys(bulmaColorProps), bulmaColorProps.primary)}
    isOutlined={boolean("isOutlined", false)}
    isLoading={boolean("isLoading", false)}
    isCircular={boolean("isCircular", false)}
    isSize={select("isSize", ["xsmall", "small", "medium"], null)}
    disabled={boolean("disabled", false)}
    href={text("href", "")}
    to={text("to", "")}
    onClick={action("clicked or enter/space pressed")}
  >
    <FontAwesomeIcon size={"2x"} icon={faTimes} />
  </IconButton>
);
