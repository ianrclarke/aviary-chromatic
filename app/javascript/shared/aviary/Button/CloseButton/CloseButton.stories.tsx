import React from "react";

import { boolean, select } from "@storybook/addon-knobs";
import { Message } from "@shared/aviary/Message";
import { CloseButton } from "./CloseButton";
import { Size } from "@shared/types/sizes.d";

import { bulmaColorProps } from "@shared/types/bulmaColors.d";

import * as styles from "./CloseButton.styles";

export default {
  title: "Aviary|Close",
  component: CloseButton,
};

export const Default = () => (
  <CloseButton
    isCircular={boolean("isCircled", false)}
    isColor={select<any>("color", Object.keys(bulmaColorProps), bulmaColorProps.secondary)}
    isSize={select("isSize", ["small", "medium", "large"], null)}
    aria-label="Tester close"
  />
);

export const Variants = () => (
  <div css={styles.storyLayout}>
    <div css={styles.storyRow}>
      <CloseButton isSize={Size.xsmall} aria-label="Small" />
      <CloseButton isSize={Size.small} aria-label="Medium" />
      <CloseButton isSize={Size.medium} aria-label="Large" />
    </div>
    <div css={styles.storyRow}>
      <CloseButton isText={false} isSize={Size.xsmall} aria-label="Small circular" />
      <CloseButton isText={false} isSize={Size.small} aria-label="Medium circular" />
      <CloseButton isText={false} isSize={Size.medium} aria-label="Large circular" />
    </div>
    <div css={styles.storyRow}>
      <CloseButton isColor="danger" isText={false} isSize={Size.small} aria-label="Danger color" />
      <CloseButton
        isColor="primary"
        isText={false}
        isSize={Size.small}
        aria-label="Primary color"
      />
      <CloseButton isColor="info" isText={false} isSize={Size.small} aria-label="Info color" />
      <CloseButton
        isColor="warning"
        isText={false}
        isSize={Size.small}
        aria-label="Warning color"
      />
    </div>
    <div css={styles.storyRow}>
      <Message>
        <CloseButton
          absolutePosition="topRight"
          isColor="primary"
          isSize={Size.xsmall}
          aria-label="Message close"
        />
        This is a message with a nice close button
      </Message>
    </div>
  </div>
);
