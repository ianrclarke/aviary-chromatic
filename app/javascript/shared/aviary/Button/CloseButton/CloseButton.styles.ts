import { css } from "@emotion/core";

export const position = {
  topRight: css`
    position: absolute;
    top: 0.25rem;
    right: 0.25rem;
  `,
};

export const storyRow = css`
  display: flex;
  align-items: center;
  justify-content: center;

  & > button {
    margin-right: 1rem;
  }
`;

export const storyLayout = css`
  display: flex;
  flex-direction: column;

  & > div {
    margin-bottom: 1rem;
  }
`;
