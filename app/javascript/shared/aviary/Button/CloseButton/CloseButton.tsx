import { faTimes } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC } from "react";

import { bulmaColors } from "@shared/types/bulmaColors";
import { Size } from "@shared/types/sizes.d";

import { IconButtonProps, IconButton } from "../IconButton";

import * as styles from "./CloseButton.styles";

const defaultColor: bulmaColors = "secondary";

interface Props extends Omit<IconButtonProps, "type"> {
  /**
   * If the placement of the close button should be absolutely positioned
   *
   */
  absolutePosition?: "topRight";
}

const CloseButton: FC<Props> = ({ isText, isSize, absolutePosition, ...rest }) => {
  const iconSizer = () => {
    if (!isSize) return "1x";
    switch (isSize) {
      case Size.xsmall:
        return "lg";
      case Size.small:
        return "2x";
      case Size.medium:
        return "3x";
      default:
        break;
    }
  };

  const closeStyles = [absolutePosition && styles.position[absolutePosition]];
  return (
    <IconButton css={closeStyles} isCircular={true} isSize={isSize} {...rest} isText={isText}>
      <FontAwesomeIcon icon={faTimes} size={iconSizer()} />
    </IconButton>
  );
};

CloseButton.defaultProps = {
  isColor: defaultColor,
  isSize: Size.small,
  isText: true,
};

export { CloseButton };
