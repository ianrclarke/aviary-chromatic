const POPPER_POSITIONS = {
  "top-start": "top-start",
  top: "top",
  "top-end": "top-end",
  "right-start": "right-start",
  right: "right",
  "right-end": "right-end",
  "bottom-start": "bottom-start",
  bottom: "bottom",
  "bottom-end": "bottom-end",
  "left-start": "left-start",
  left: "left",
  "left-end": "left-end",
};

type PopperPositionType = keyof typeof POPPER_POSITIONS;

export { POPPER_POSITIONS, PopperPositionType };
