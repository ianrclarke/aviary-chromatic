import { css } from "@emotion/core";

import { animations } from "@styles";

export const skeletonButton = css`
  ${animations.skeletonPulse};
  width: 8rem;
  height: 2.75rem;
  border-radius: 4px;
`;

export const size = {
  xsmall: css`
    width: 4rem;
    height: 1.5rem;
  `,
  small: css`
    width: 4rem;
    height: 2.25rem;
  `,
  medium: css`
    width: 6rem;
    height: 3.5rem;
  `,
};

export const width = {
  tiny: css`
    width: 4rem;
  `,
  full: css`
    width: 100%;
  `,
};

export const pulled = {
  right: css`
    float: right;
  `,
  left: css`
    float: left;
  `,
};
