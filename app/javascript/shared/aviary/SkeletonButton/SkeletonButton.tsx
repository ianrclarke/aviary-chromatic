import React from "react";

import * as styles from "./SkeletonButton.styles";

interface Props {
  size?: "xsmall" | "small" | "medium";
  width?: "tiny" | "full";
  pulled?: "right" | "left";
}

const SkeletonButton: React.FC<Props> = ({ size, width, pulled, ...rest }) => {
  return (
    <div
      css={[styles.skeletonButton, styles.size[size], styles.width[width], styles.pulled[pulled]]}
      {...rest}
    />
  );
};

export { SkeletonButton };
