import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { SkeletonButton } from "./SkeletonButton";

describe("SkeletonButton tests", () => {
  let wrapper;
  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = shallow(<SkeletonButton />);
  };

  baseTests(() => wrapper);
});
