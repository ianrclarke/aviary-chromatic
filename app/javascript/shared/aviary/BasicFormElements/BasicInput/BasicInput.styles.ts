import { css } from "@emotion/core";

import { utilities, colors, helpers, dimensions, baseStyles } from "@styles";
import { ColorProfile } from "@styles/emotion-styles/colorProfiles/colorProfile";

export const input = css`
  ${utilities.transition};
  ${helpers.fontSizeNormalizer(16)};
  color: ${colors.dark};
  border: 1px solid ${colors.greyLight};
  border-radius: ${dimensions.borderRadius};

  &::placeholder {
    ${utilities.transition};
    color: ${colors.grey};
  }

  &:active,
  &:focus {
    ${baseStyles.cardShadow};
    outline: none;
    border-color: ${colors.green};

    &::placeholder {
      color: ${colors.greyLight};
    }
  }
`;

export const sizes = {
  small: css`
    padding: 0.5rem 0.75rem;
  `,
  medium: css`
    padding: 0.75rem 1rem;
  `,
  large: css`
    padding: 1rem 1.5rem;
  `,
};

export const colorStyles = (profile: ColorProfile) => css`
  &:active,
  &:focus {
    border-color: ${profile.baseColor};
  }
`;
