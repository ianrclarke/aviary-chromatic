import React from "react";

import { select, text } from "@storybook/addon-knobs";
import { AVIARY_COLORS } from "@shared/aviary/aviaryColors";

import { BasicInput } from "./BasicInput";

export default {
  title: "Aviary|BasicFormElements/BasicInput",
  component: BasicInput,
};

export const Default = () => (
  <BasicInput
    type="text"
    isSize={select("isSize", ["small", "medium", "large"], "medium")}
    isColor={select<any>("isColor", Object.keys(AVIARY_COLORS), null)}
    placeholder={text("placeholder", "This is a starbucks basic input")}
  />
);
