import React, { FC, HTMLProps } from "react";

import { profiles } from "@styles";

import { AviaryColorProps } from "../../aviaryColors";

import * as styles from "./BasicInput.styles";

interface Props extends AviaryColorProps, HTMLProps<HTMLInputElement> {
  isSize?: "small" | "medium" | "large";
  handleChange?: any;
}

const BasicInput: FC<Props> = ({ isColor, isSize, ...rest }) => {
  const stringToProfile = {
    primary: profiles.primaryProfile,
    dark: profiles.darkProfile,
    danger: profiles.dangerProfile,
    warning: profiles.warningProfile,
    info: profiles.infoProfile,
    purple: profiles.purpleProfile,
    light: profiles.infoProfile,
  };

  const css = [
    styles.input,
    styles.sizes[isSize],
    isColor && styles.colorStyles(stringToProfile[isColor]),
  ];
  return <input css={css} {...rest} />;
};

BasicInput.defaultProps = {
  isSize: "medium",
};

export { BasicInput };
