import classNames from "classnames";
import React, { Ref } from "react";

import "./Radio.scss";

interface Props extends React.HTMLProps<HTMLInputElement> {
  displayColor?: "primary" | "dark" | "grey" | "blue" | "text";
  innerRef?: Ref<any>;
}

const Radio: React.FC<Props> = ({
  id,
  displayColor,
  innerRef,
  hidden,
  children,
  className,
  ...rest
}) => {
  const buildClassNames = () => {
    return classNames("custom-radio__input radio", className, {
      [`color-${displayColor}`]: displayColor,
    });
  };

  const buildWrapperClass = () => {
    return classNames("custom-radio radio", { ["is-hidden"]: hidden });
  };

  return (
    <label className={buildWrapperClass()}>
      <input id={id} type="radio" className={buildClassNames()} ref={innerRef} {...rest} />
      <span />
      <div className="custom-radio__content">{children}</div>
    </label>
  );
};

export { Radio };
