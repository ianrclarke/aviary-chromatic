import React from "react";

import { Radio } from "./Radio";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=383%3A628",
};

export default {
  title: "Aviary|Radio",
  component: Radio,
  parameters: { design: componentDesign },
};

export const Default = () => <Radio />;
