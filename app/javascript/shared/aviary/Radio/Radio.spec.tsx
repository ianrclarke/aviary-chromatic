import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Radio } from "./Radio";

describe("custom Radio inputs", () => {
  let radio;

  beforeEach(() => {
    radio = shallow(<Radio name="test" id="tester" />);
  });

  baseTests(() => radio);
});
