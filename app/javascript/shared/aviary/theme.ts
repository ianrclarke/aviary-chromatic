import { createMuiTheme } from "@material-ui/core";

import { colors, typography } from "@styles";

const baseTheme = createMuiTheme({
  palette: {
    primary: {
      main: colors.primary,
    },
    error: {
      main: colors.danger,
    },
    text: {
      primary: colors.dark,
    },
  },
  typography: {
    fontFamily: typography.familySansSerif,
  },
});

export { baseTheme };
