import React from "react";

import { Column, Columns } from "@shared/aviary";

import { Container } from "./Container";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=499%3A438",
};

export default {
  title: "Aviary|Container",
  component: Container,
  parameters: { design: componentDesign },
};

export const Default = () => (
  <Container>
    <Columns>
      <Column>
        <p>Half width, containerized column</p>
      </Column>
      <Column>
        <p>Half width, containerized column</p>
      </Column>
    </Columns>
  </Container>
);
