import { mount } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Container } from "./Container";

describe("Container component", () => {
  let wrapper;
  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = mount(
      <Container>
        <p>This is some containerized text!</p>
      </Container>
    );
  };

  baseTests(() => wrapper);
});
