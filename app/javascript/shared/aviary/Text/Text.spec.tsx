import { mount, ReactWrapper } from "enzyme";
import React from "react";

import { Text } from "./Text";

describe("Text", () => {
  describe.each(["normal", "short", "long"])("%s", type => {
    let wrapper: ReactWrapper;
    let text: string;
    beforeEach(() => {
      text = "Some Text";
      wrapper = mount(<Text type={type as any}>{text}</Text>);
    });

    it("renders a p", () => {
      expect(wrapper.exists("p")).toBe(true);
      expect(wrapper.children()).toHaveLength(1);
    });

    it("contains the correct text", () => {
      expect(wrapper.text()).toEqual(text);
    });
  });
});
