import React, { FC, HTMLProps } from "react";

import * as styles from "./Text.styles";

interface Props extends HTMLProps<HTMLParagraphElement> {
  type?: "normal" | "short" | "long";
  isSize?: "xsmall" | "small";
}

const Text: FC<Props> = ({ type, isSize, children, ...rest }) => {
  const textStyles = [styles.text[type], isSize && styles.size[isSize]];
  return (
    <p css={textStyles} {...rest}>
      {children}
    </p>
  );
};

Text.defaultProps = { type: "normal" };

export { Text };
