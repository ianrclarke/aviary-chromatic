import React from "react";

import { Text } from "./Text";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=1475%3A542",
};

export default {
  title: "Aviary|Text",
  component: Text,
  parameters: { design: componentDesign },
};

export const Default = () => (
  <Text>
    <b>Title</b>
    <br />
    another line
    <br />
    followed by a line
  </Text>
);

export const Short = () => (
  <Text type="short">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
    laboris nisi ut aliquip ex ea
  </Text>
);

export const Long = () => (
  <Text type="long">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
    voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum ad minim veniam.
  </Text>
);
