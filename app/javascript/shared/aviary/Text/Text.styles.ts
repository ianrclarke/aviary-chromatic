import { css } from "@emotion/core";

import { colors } from "@styles";

const base = css`
  color: ${colors.grey};
  font-weight: 400;
`;

export const text = {
  normal: css`
    ${base}
    font-size: 1rem;
    line-height: 1.5rem;
  `,
  short: css`
    ${base}
    font-size: 1.125rem;
    line-height: 1.875rem;
  `,
  long: css`
    ${base}
    font-size: 1rem;
    line-height: 1.625rem;
  `,
};

export const size = {
  xsmall: css`
    font-size: 0.75rem;
  `,
  small: css`
    font-size: 0.875rem;
  `,
};
