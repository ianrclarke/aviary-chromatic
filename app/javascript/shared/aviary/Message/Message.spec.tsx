import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Message } from "./Message";

describe("Message", () => {
  let wrapper;

  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = shallow(<Message isColor="primary">This is a very nice message component</Message>);
  };

  baseTests(() => wrapper);
});
