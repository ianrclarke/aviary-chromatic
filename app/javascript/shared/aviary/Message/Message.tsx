import React, { FC } from "react";

import * as styles from "./Message.styles";

interface Props {
  /**
   * Sets the color of the message
   *
   * @default none
   */
  isColor?: "primary" | "info" | "danger" | "warning";
}

const Message: FC<Props> = ({ isColor, children, ...rest }) => {
  const messageStyles = [styles.base, isColor && styles.color[isColor]];
  return (
    <div css={messageStyles} {...rest}>
      {children}
    </div>
  );
};

export { Message };
