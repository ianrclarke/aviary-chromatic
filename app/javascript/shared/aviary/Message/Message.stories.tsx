import { select, text } from "@storybook/addon-knobs";
import React from "react";

import { Message } from "./Message";

export default {
  title: "Aviary|Message",
  component: Message,
};

export const Default = () => (
  <Message isColor={select<any>("color", ["primary", "info", "danger"], "primary")}>
    {text("Message contents", "This is some message content in here guys, it looks really cool.")}
  </Message>
);
