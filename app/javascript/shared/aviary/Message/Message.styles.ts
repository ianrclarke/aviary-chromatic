import { css } from "@emotion/core";

import { colors, dimensions, helpers } from "@styles";

export const base = css`
  display: block;
  position: relative;
  padding: 1.25rem 2rem 1.25rem 1.5rem;
  background-color: ${colors.greenExtraLight};
  border-color: ${colors.green};
  border-radius: ${dimensions.borderRadius};
  border-style: solid;
  border-width: 0 0 0 4px;
  color: ${colors.dark};
  box-shadow: 0px 2px 10px ${helpers.hexToRgba(colors.grey, 0.25)};

  &:not(:last-child) {
    margin-bottom: 1rem;
  }
`;

export const color = {
  success: css`
    border-color: ${colors.green};
    background-color: ${colors.greenExtraLight};
  `,
  info: css`
    border-color: ${colors.blue};
    background-color: ${colors.blueExtraLight};
  `,
  danger: css`
    border-color: ${colors.danger};
    background-color: ${colors.redExtraLight};
  `,
  warning: css`
    border-color: ${colors.warning};
    background-color: ${colors.orangeExtraLight};
  `,
};
