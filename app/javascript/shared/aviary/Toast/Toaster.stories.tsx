import React from "react";

import { css } from "@emotion/core";

import { Text, Title } from "@shared/aviary";

import { Toaster } from "./Toaster/Toaster";
import { ToastStoryButtons } from "./ToastStoryButtons";

export default {
  title: "Aviary|Toaster",
  component: Toaster,
};

export const Default = () => (
  <div
    css={css`
      display: flex;
      height: 500px;
      background-color: white;
      padding: 2rem;
    `}
  >
    <Toaster>
      <div>
        <Title>Welcome to Toasts</Title>
        <Text>
          You can make a Toast appear from any component using "useToast()". It's almost as if,
          like, it's magic maaaan
        </Text>
        <ToastStoryButtons />
      </div>
    </Toaster>
  </div>
);
