const TOAST_TYPES = {
  normal: "normal",
  error: "error",
  success: "success",
};

export type ToastType = keyof typeof TOAST_TYPES;
