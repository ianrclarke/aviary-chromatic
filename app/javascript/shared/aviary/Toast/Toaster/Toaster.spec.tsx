import { mount } from "enzyme";
import React, { FC } from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { useToast } from "../useToast";

import { Toaster } from "./Toaster";

jest.useFakeTimers();

describe("Toaster", () => {
  let toastInternals;
  let wrapper;

  const FakeToastTrigger: FC = () => {
    toastInternals = useToast();
    return null;
  };

  const buildWrapper = () => {
    wrapper = mount(
      <Toaster>
        <FakeToastTrigger />
      </Toaster>
    );
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("functional", () => {
    describe("creating toasts", () => {
      it("makes a normal toast and displays it", () => {
        const message = "This is just a normal toast here";
        const type = "normal";
        act(() => {
          toastInternals.makeToast(type, message);
        });
        wrapper.update();
        expect(wrapper.find("Toast").length).toEqual(1);
        expect(wrapper.find("Toast").contains(message)).toBeTruthy();
        expect(wrapper.find("Toast").prop("type")).toEqual(type);
      });
      it("makes an error toast and displays it", () => {
        const message = "This is an error toast";
        const type = "error";
        act(() => {
          toastInternals.makeToast(type, message);
        });
        wrapper.update();
        expect(wrapper.find("Toast").contains(message)).toBeTruthy();
        expect(wrapper.find("Toast").prop("type")).toEqual(type);
      });
      it("makes an error toast and displays it", () => {
        const message = "This is an error toast";
        const type = "error";
        act(() => {
          toastInternals.makeToast(type, message);
        });
        wrapper.update();
        expect(wrapper.find("Toast").contains(message)).toBeTruthy();
        expect(wrapper.find("Toast").prop("type")).toEqual(type);
      });
    });
    describe("dismissing toasts", () => {
      it("removes the toast when you click on the close button", () => {
        const message = "This is just a normal toast here";
        const type = "normal";
        act(() => {
          toastInternals.makeToast(type, message);
        });
        wrapper.update();
        act(() => {
          const dismiss = wrapper.find("Toast").prop("onDismiss");
          dismiss({ stopPropagation: jest.fn() });
        });
        wrapper.update();
        expect(wrapper.find("Toast").length).toEqual(0);
      });
      it("removes the toast when the timeout has passed", () => {
        const message = "This is just a normal toast here";
        const type = "normal";
        act(() => {
          toastInternals.makeToast(type, message);
        });
        wrapper.update();
        act(() => {
          jest.advanceTimersByTime(5001);
        });
        wrapper.update();
        expect(wrapper.find("Toast").length).toEqual(0);
      });
    });
  });
});
