import { faTimes } from "@fortawesome/pro-light-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { FC } from "react";
import { useTranslation } from "react-i18next";

import { ToastType } from "./toastTypes";

import * as styles from "./Toast.styles";

interface Props {
  /**
   * Sets the color of the alert
   *
   * @default normal
   */
  type: ToastType;
  /**
   * Determines if the Toast wil auto-dismiss after the timeout period
   *
   * @default true
   */
  autoDismiss: boolean;
  /**
   * The time the toast stays on screen if autoDismiss is true
   *
   * @default 5000
   */
  autoDismissTimeout?: number;
  /**
   * Callback for the close button
   *
   */
  onDismiss?: () => void;
  /**
   * Callback mouseEnter, used to pause the autoDismissal timer
   *
   */
  onMouseEnter?: () => void;
  /**
   * Callback mouseEnter, used to restart the autoDismissal timer
   *
   */
  onMouseLeave?: () => void;
  /**
   * Remaining time left in the timer
   *
   */
  timeRemaining?: number;
}

const Toast: FC<Props> = props => {
  const {
    type,
    autoDismiss,
    autoDismissTimeout,
    timeRemaining,
    children,
    onDismiss,
    ...rest
  } = props;
  const { t } = useTranslation("aviary");

  const styleBuilder = [styles.content, autoDismiss && styles.content, styles.types[type]];
  const timerBuilder = [styles.timer, styles.timerTypes[type]];
  const progressBuilder = [
    styles.timeDown(autoDismissTimeout, timeRemaining),
    styles.progressTypes[type],
  ];

  return (
    <div css={styles.base} {...rest} aria-live="polite">
      <div css={timerBuilder}>
        <div css={progressBuilder}>&nbsp;</div>
      </div>
      <div css={styleBuilder}>
        {children}
        <button
          css={styles.close}
          onClick={onDismiss}
          data-testid="toast-close"
          aria-label={t("CloseNotification")}
        >
          <FontAwesomeIcon icon={faTimes} size="lg" />
        </button>
      </div>
    </div>
  );
};

export { Toast, Props as ToastProps };
