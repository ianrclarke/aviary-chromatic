import { useContext } from "react";

import { ToastContext, ToastData } from "./ToastContext";

const useToast = (): ToastData => {
  return useContext(ToastContext);
};

export { useToast };
