import { mount } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { ToastController } from "./ToastController";

jest.useFakeTimers();

describe("ToastController", () => {
  let wrapper;
  let isAutodismiss;
  let mockAutoDismissTimeout;
  let mockType;
  let mockOnDismiss;
  let mockMouseEnter;
  let mockMouseLeave;

  const buildWrapper = () => {
    wrapper = mount(
      <ToastController
        type={mockType}
        autoDismiss={isAutodismiss}
        autoDismissTimeout={mockAutoDismissTimeout}
        onDismiss={mockOnDismiss}
        onMouseEnter={mockMouseEnter}
        onMouseLeave={mockMouseLeave}
      />
    );
  };

  beforeEach(() => {
    mockMouseLeave = jest.fn();
    mockMouseEnter = jest.fn();
    mockOnDismiss = jest.fn();
    mockType = "normal";
    isAutodismiss = true;
    mockAutoDismissTimeout = 5000;
    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("Functional", () => {
    it("calls mouseEnter when toast is moused over", () => {
      act(() => {
        const mouseEnter = wrapper.find("Toast").prop("onMouseEnter");
        mouseEnter();
      });
      wrapper.update();
      expect(mockMouseEnter).toHaveBeenCalled();
    });
    it("calls mouseLeave when toast is moused left", () => {
      act(() => {
        const mouseLeave = wrapper.find("Toast").prop("onMouseLeave");
        mouseLeave();
      });
      wrapper.update();
      expect(mockMouseLeave).toHaveBeenCalled();
    });
    it("counts down the timer for autoDismiss", () => {
      act(() => {
        jest.advanceTimersByTime(1000);
      });
      wrapper.update();
      let timeRemaining;
      act(() => {
        timeRemaining = wrapper.find("Toast").prop("timeRemaining");
      });
      wrapper.update();
      expect(timeRemaining).toEqual(4000);
    });
  });
});
