import { css } from "@emotion/core";
import React, { FC, ReactChild } from "react";

import { Button, ButtonGroup } from "@shared/aviary";

import { ToastOptions } from "./ToastContext";
import { ToastType } from "./toastTypes";

import { useToast } from "./index";

export const buttons = css`
  display: flex;
  margin-top: 1rem;
  & > button {
    margin-right: 0.5rem;
  }
`;

const ToastStoryButtons: FC = () => {
  const { makeToast, removeToast } = useToast();
  const toastID = "alert-item";

  const popError = () => {
    makeToast("error", "A fake thing fake failed.");
  };

  const popSuccess = () => {
    makeToast("success", "The real fake thing was successful");
  };

  const popNormal = () => {
    makeToast("normal", "Just a normal toast message here");
  };

  const popOptions = (status: ToastType, content: ReactChild, options?: ToastOptions) => e => {
    e.stopPropagation();
    makeToast(status, content, options);
  };

  const closeToast = () => {
    removeToast(toastID);
  };

  const onCloseThing = () => {
    alert("I AM A CALLBACK");
  };

  const fancyContent = () => (
    <div>
      Read this message please, what do you do?
      <br />
      <div css={buttons}>
        <Button isSize="small" onClick={closeToast} isColor="primary">
          Ignore this
        </Button>
        <Button isSize="small" isColor="danger" isOutlined={true}>
          Try again
        </Button>
      </div>
    </div>
  );

  return (
    <div>
      <ButtonGroup>
        <Button onClick={popNormal}>Make an normal toast</Button>
        <Button onClick={popError}>Make an error toast</Button>
        <Button onClick={popSuccess}>Make a success toast</Button>
      </ButtonGroup>
      <br />
      <br />
      <ButtonGroup>
        <Button onClick={popOptions("normal", "Non-auto toast is here", { autoDismiss: false })}>
          Make a non-autodismiss toast
        </Button>
        <Button
          onClick={popOptions(
            "success",
            "You were successful and making this a really long message!",
            { autoDismissTimeout: 10000 }
          )}
        >
          Super long auto-dismiss
        </Button>
        <Button onClick={popOptions("normal", fancyContent(), { toastId: toastID })}>
          Fancy content one with buttons
        </Button>
        <Button
          onClick={popOptions("normal", fancyContent(), {
            toastId: toastID,
            onClose: onCloseThing,
          })}
        >
          Callback
        </Button>
      </ButtonGroup>
    </div>
  );
};

export { ToastStoryButtons };
