import { mount } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { Toast } from "./Toast";

describe("Toast", () => {
  let wrapper;
  let isAutodismiss;
  let mockType;
  let mockOnDismiss;

  const buildWrapper = () => {
    wrapper = mount(
      <Toast type={mockType} autoDismiss={isAutodismiss} onDismiss={mockOnDismiss} />
    );
  };

  beforeEach(() => {
    mockOnDismiss = jest.fn();
    mockType = "normal";
    isAutodismiss = true;
    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("Functional", () => {
    it("calls onDismiss when you click the close ", () => {
      isAutodismiss = true;
      buildWrapper();
      act(() => {
        wrapper.find('[data-testid="toast-close"]').simulate("click");
      });
      wrapper.update();
      expect(mockOnDismiss).toHaveBeenCalled();
    });
  });
});
