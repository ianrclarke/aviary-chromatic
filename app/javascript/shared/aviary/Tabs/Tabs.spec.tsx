import { mount, ReactWrapper } from "enzyme";
import * as React from "react";
import { act } from "react-dom/test-utils";

import { baseTests } from "@testing/baseTests";

import { TabContent } from "./TabContent";
import { TabHeader } from "./TabHeader";

import { Tabs } from ".";

describe("Tabs", () => {
  let wrapper: ReactWrapper;
  let mockOnChange;

  const DumbComponent = ({ children }) => <div>{children}</div>;

  const buildWrapper = () => {
    wrapper = mount(
      <Tabs onChange={mockOnChange}>
        <TabHeader tabId="testTab1">
          <DumbComponent>trigger 1</DumbComponent>
        </TabHeader>
        <TabContent tabId="testTab1">
          <DumbComponent>content 1</DumbComponent>
        </TabContent>
        <TabHeader tabId="testTab2">
          <DumbComponent>trigger 2</DumbComponent>
        </TabHeader>
        <TabContent tabId="testTab2">
          <DumbComponent>content 2</DumbComponent>
        </TabContent>
      </Tabs>
    );
  };

  beforeEach(() => {
    mockOnChange = jest.fn();
    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("functionality", () => {
    const toggleTab = targetTabId => {
      act(() => {
        wrapper.find(`TabHeader[tabId="${targetTabId}"]`).find("button").simulate("click");
      });
      wrapper.update();
    };

    it("content is visible when tab is toggled on and hidden when tab is toggled off", () => {
      toggleTab("testTab1");
      expect(wrapper.find(TabContent).children().length).toBeGreaterThanOrEqual(1);
      toggleTab("testTab1");
      expect(wrapper.find(TabContent).children().length).toEqual(0);
    });

    it("opens tab 1 then closes tab 1 and opens tab 2 when tab 2 header is clicked", () => {
      toggleTab("testTab1");
      expect(wrapper.find('TabContent[tabId="testTab1"]').children().length).toBeGreaterThanOrEqual(
        1
      );
      expect(wrapper.find('TabContent[tabId="testTab2"]').children().length).toEqual(0);
      toggleTab("testTab2");
      expect(wrapper.find('TabContent[tabId="testTab1"]').children().length).toEqual(0);
      expect(wrapper.find('TabContent[tabId="testTab2"]').children().length).toBeGreaterThanOrEqual(
        1
      );
    });
    it("calls onChange with the right props when tab is changed", () => {
      toggleTab("testTab1");
      expect(mockOnChange).toHaveBeenCalledWith(null);
      toggleTab("testTab1");
      expect(mockOnChange).toHaveBeenCalledWith("testTab1");
    });
    it("only passes props to TabHeader or TabContent components", () => {
      wrapper = mount(
        <Tabs onChange={mockOnChange}>
          <TabHeader tabId="testTab1">
            <DumbComponent>trigger 1</DumbComponent>
          </TabHeader>
          <TabContent tabId="testTab1">
            <DumbComponent>content 1</DumbComponent>
          </TabContent>
          <>
            <TabHeader tabId="testTab2">
              <DumbComponent>trigger 2</DumbComponent>
            </TabHeader>
            <TabContent tabId="testTab2">
              <DumbComponent>content 2</DumbComponent>
            </TabContent>
          </>
        </Tabs>
      );
      expect(wrapper.find(TabHeader).first().props().onTriggerClicked).toBeDefined();
      expect(wrapper.find(TabContent).first().props().onTriggerClicked).toBeDefined();
      expect(wrapper.find(TabHeader).last().props().onTriggerClicked).toBeDefined();
      expect(wrapper.find(TabContent).last().props().onTriggerClicked).toBeDefined();
    });
  });
});
