import React from "react";

import { css } from "@emotion/core";
import { TabContent, TabHeader, Tabs, TabText } from "@shared/aviary";

import { ANIMATIONS } from "@shared/aviary/Tabs/types.d";
import { select } from "@storybook/addon-knobs";
import { colors } from "@styles";

const wrapperStyle = css`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;

const tabHeaderStyle = css`
  order: 1;
  flex: 1;
  background: white;
  padding: 2rem;
  border: ${colors.separator} 1px solid;
`;

const tabContentStyle = css`
  order: 2;
  width: 100%;
`;

const innerTabContentStyle = css`
  background: white;
  padding: 2rem;
  border: ${colors.separator} 1px solid;
`;

export default {
  title: "Aviary|Tabs",
  component: Tabs,
};

export const Default = () => (
  <Tabs css={wrapperStyle}>
    <TabHeader tabId="testTab1" css={tabHeaderStyle}>
      <TabText>Padawan</TabText>
    </TabHeader>
    <TabHeader tabId="testTab2" css={tabHeaderStyle}>
      <TabText>Jedi Grand Master</TabText>
    </TabHeader>
    <TabContent
      tabId="testTab1"
      css={tabContentStyle}
      animationType={select<any>("Padawan animation", ANIMATIONS, "slideDown")}
    >
      <div css={innerTabContentStyle}>
        Lucas ipsum dolor sit amet skywalker organa organa greedo kenobi skywalker dantooine kit
        baba wampa. Lando moff mara wicket ben mon ewok binks. Solo tatooine endor solo padmé
        skywalker antilles mandalorians hutt. Mara solo hutt wedge darth skywalker sebulba chewbacca
        darth. Lando wookiee boba watto qui-gonn antilles organa. Wedge coruscant mon mandalorians
        sidious wicket sith lando darth. Solo solo solo mon moff. Mace kamino zabrak dooku padmé
        palpatine palpatine mace thrawn. Skywalker kenobi jabba wookiee chewbacca bespin.
      </div>
    </TabContent>
    <TabContent
      tabId="testTab2"
      css={tabContentStyle}
      animationType={select<any>("Jedi animation", ANIMATIONS, "slideDown")}
    >
      <div css={innerTabContentStyle}>
        Lucas ipsum dolor sit amet chadra-fan zabrak elrood plo frozarns dantari ahsoka lobot md-5
        fosh. Arcona kushiban nautolan lama breha mon spar ralter. Hoojib rodian tion biggs bren
        frozarns. Joelle ti bria gunray boltrunians jabba kuat boltrunians. Deliah sulorine codru-ji
        rahn. Porkins falleen ortolan t88 skywalker cracken. Jaina thennqora umbaran wampa. Bothan
        hissa san fey'lya organa leia chazrach evazan falleen. Endor kendal terentatek ponda.
        Tyranus vao wicket verpine trioculus dooku wicket.
      </div>
    </TabContent>
  </Tabs>
);
