const ANIMATIONS = {
  slideDown: "slideDown",
  slideUp: "slideUp",
  slideRight: "slideRight",
  slideLeft: "slideLeft",
  fadeIn: "fadeIn",
};

type AnimationTypes = keyof typeof ANIMATIONS;

export { ANIMATIONS, AnimationTypes };
