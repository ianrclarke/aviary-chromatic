import React, { DetailedHTMLProps, FC, HtmlHTMLAttributes } from "react";
import { animated, config, useTransition } from "react-spring";

import { AnimationTypes } from "@shared/aviary/Tabs";

import * as styles from "./TabContent.styles";

interface Props extends DetailedHTMLProps<HtmlHTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  tabId: string;
  openTabId?: string;
  onTriggerClicked?: (targetTabId) => void;
  animationType?: AnimationTypes;
}

const enter = { transform: "translate3d(0, 0, 0)" };

const animations = {
  slideDown: {
    from: { transform: "translate3d(0, -100%, 0)" },
    enter,
  },
  slideUp: {
    from: { transform: "translate3d(0, 100%, 0)" },
    enter,
  },
  slideRight: {
    from: { transform: "translate3d(-100%, 0, 0)" },
    enter,
  },
  slideLeft: {
    from: { transform: "translate3d(100%, 0, 0)" },
    enter,
  },
  fadeIn: {
    from: { opacity: 0 },
    enter: { opacity: 1 },
  },
};

const TabContent: FC<Props> = ({
  animationType,
  tabId,
  openTabId,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onTriggerClicked,
  children,
  ...props
}) => {
  const animation = useTransition(children, null, {
    ...animations[animationType],
    config: {
      ...config.gentle,
      clamp: true,
      friction: 22,
      velocity: 15,
    },
  }).map(({ item, key, props: animatonProps }) => (
    <animated.div key={key} style={animatonProps} css={styles.animatedDiv}>
      {item}
    </animated.div>
  ));

  if (tabId !== openTabId) return null;

  const renderContent = () => {
    if (animationType) return animation;
    else return children;
  };

  return (
    <div css={styles.tabContent} {...props}>
      {renderContent()}
    </div>
  );
};

TabContent.defaultProps = {
  animationType: null,
};

export { TabContent };
