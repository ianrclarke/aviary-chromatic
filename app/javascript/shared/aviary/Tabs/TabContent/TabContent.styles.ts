import { css } from "@emotion/core";

export const tabContent = css`
  overflow: hidden;
`;

export const animatedDiv = css`
  position: relative;
`;
