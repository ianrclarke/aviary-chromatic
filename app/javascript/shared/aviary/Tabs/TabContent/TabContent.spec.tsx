import { shallow } from "enzyme";
import * as React from "react";

import { baseTests } from "@testing/baseTests";

import { TabContent } from "./TabContent";

describe("Tabs", () => {
  let wrapper;

  let mockOpenTabId;

  const buildWrapper = () => {
    wrapper = shallow(
      <TabContent tabId="testTab" openTabId={mockOpenTabId}>
        <p>content</p>
      </TabContent>
    );
  };

  beforeEach(() => {
    mockOpenTabId = null;
    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("functionality", () => {
    it("content is hidden when tab is not selected", () => {
      expect(wrapper.prop("children")).toBeUndefined();
    });
    it("content is visible when tab is selected", () => {
      mockOpenTabId = "testTab";
      buildWrapper();
      expect(wrapper.prop("children")).toBeDefined();
    });
  });
});
