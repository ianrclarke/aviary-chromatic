export { Tabs } from "./Tabs";
export { TabHeader } from "./TabHeader";
export { TabContent } from "./TabContent";
export { TabText } from "./TabText";
export type { AnimationTypes } from "./types.d";
