import { css } from "@emotion/core";

import { animations, typography, colors, timing } from "@styles/index";

export const tabText = css`
  transition: color ${timing.defaultTiming} ${animations.easeInOutQuad};
  font-weight: ${typography.semiBold};
  &:hover {
    color: ${colors.primary};
  }
`;

export const tabActiveButton = css`
  color: ${colors.primary};
  text-decoration: underline;
  text-decoration-color: ${colors.primary};
`;
