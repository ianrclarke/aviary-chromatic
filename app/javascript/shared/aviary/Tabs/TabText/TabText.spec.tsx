import { mount } from "enzyme";
import * as React from "react";

import { baseTests } from "@testing/baseTests";

import { TabText } from "./TabText";

describe("TabText", () => {
  let wrapper;

  const buildWrapper = () => {
    wrapper = mount(<TabText>trigger</TabText>);
  };

  beforeEach(() => {
    buildWrapper();
  });

  baseTests(() => wrapper);
});
