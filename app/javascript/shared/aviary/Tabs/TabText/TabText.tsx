import React, { FC, HTMLProps } from "react";

import { Text } from "@shared/aviary/Text";

import * as styles from "./TabText.styles";

interface Props extends HTMLProps<HTMLParagraphElement> {
  isOpen?: boolean;
  type?: "normal" | "short" | "long";
}

const TabText: FC<Props> = ({ isOpen, children, type, ...rest }) => {
  const conditionalButtonStyles = () => {
    return [styles.tabText, isOpen && styles.tabActiveButton];
  };

  return (
    <Text css={conditionalButtonStyles()} type={type} {...rest}>
      {children}
    </Text>
  );
};

export { TabText };
