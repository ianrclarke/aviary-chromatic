import React, {
  ButtonHTMLAttributes,
  cloneElement,
  DetailedHTMLProps,
  FC,
  ReactElement,
} from "react";

import * as styles from "./TabHeader.styles";

interface Props
  extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  tabId: string;
  openTabId?: string;
  onTriggerClicked?: (targetTabId) => void;
}

const TabHeader: FC<Props> = ({ tabId, openTabId, onTriggerClicked, children, ...props }) => {
  const isOpen = tabId === openTabId;

  const cloneItem = (child: ReactElement) => {
    return cloneElement(child, {
      isOpen,
      ...child.props,
    });
  };

  const renderChildren = () => {
    return React.Children.map(children, child => {
      if (React.isValidElement(child)) {
        return cloneItem(child);
      }
      return child;
    });
  };

  return (
    <button onClick={() => onTriggerClicked(tabId)} {...props} css={styles.button} tabIndex={-1}>
      {renderChildren()}
    </button>
  );
};

export { TabHeader };
