import { mount, ReactWrapper } from "enzyme";
import * as React from "react";

import { baseTests } from "@testing/baseTests";

import { TabHeader } from "./TabHeader";

describe("TabHeader", () => {
  let wrapper: ReactWrapper;

  let mockOnTriggerClicked;

  const buildWrapper = () => {
    wrapper = mount(
      <TabHeader tabId="testTab" onTriggerClicked={mockOnTriggerClicked}>
        trigger
      </TabHeader>
    );
  };

  beforeEach(() => {
    mockOnTriggerClicked = jest.fn();
    buildWrapper();
  });

  baseTests(() => wrapper);

  describe("functionality", () => {
    it("calls onTriggerClicked when TabHeader button clicked", () => {
      wrapper.find("button").simulate("click");
      expect(mockOnTriggerClicked).toHaveBeenCalled();
    });
  });
});
