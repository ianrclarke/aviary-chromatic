import { css } from "@emotion/core";

export const button = css`
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  appearance: none;
  position: relative;
`;
