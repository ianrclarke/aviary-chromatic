import React, { cloneElement, FC, ReactElement, useState } from "react";

import * as styles from "./Tabs.styles";

interface Props {
  initialTabId?: string;
  isUncloseable?: boolean;
  onChange?: (openTabId: string) => void;
}

const Tabs: FC<Props> = ({ initialTabId, isUncloseable, onChange, children, ...rest }) => {
  const [openTabId, setOpenTabId] = useState(initialTabId);

  const onTriggerClicked = targetTabId => {
    if (openTabId !== targetTabId) {
      setOpenTabId(targetTabId);
    } else if (!isUncloseable) {
      setOpenTabId(null);
    }
    onChange(openTabId);
  };

  const cloneItem = (child: ReactElement) => {
    if (child.type === React.Fragment || child.type === "Symbol(react.fragment)") {
      return child.props.children.map(item => cloneItem(item));
    }
    return cloneElement(child, {
      openTabId,
      onTriggerClicked,
      ...child.props,
    });
  };

  const renderChildren = () => {
    return React.Children.map(children, child => {
      if (React.isValidElement(child)) {
        return cloneItem(child);
      }
      return child;
    });
  };

  return (
    <div css={styles.tabs} {...rest}>
      {renderChildren()}
    </div>
  );
};

Tabs.defaultProps = {
  initialTabId: null,
  onChange: () => {
    return null;
  },
};

export { Tabs };
