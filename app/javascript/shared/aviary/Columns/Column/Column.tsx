import React, { FC, HTMLProps } from "react";

import { Sizes, Width } from "../grid";

import * as styles from "./Column.styles";

type WidthOptions = Sizes | Width;

interface ColumnProps extends HTMLProps<HTMLDivElement> {
  columnWidth?: WidthOptions;
  columnOffset?: WidthOptions;
  isPaddingless?: boolean;
  textAlign?: "left" | "center " | "right";
  "data-testid"?: string;
  preserveMobileColumns?: boolean;
}

const Column: FC<ColumnProps> = ({
  columnWidth,
  columnOffset,
  isPaddingless,
  textAlign,
  children,
  preserveMobileColumns,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  "data-testid": dataTestId,
  ...rest
}) => {
  const isSizes = (gridType: WidthOptions): gridType is Sizes => {
    return typeof gridType === "number";
  };

  const isWidth = (gridType: WidthOptions): gridType is Width => {
    return gridType === "full" || gridType === "narrow";
  };

  const makeWidth = () => {
    if (isSizes(columnWidth) && preserveMobileColumns) {
      return styles.columnWidths(columnWidth);
    }
    if (isSizes(columnWidth)) {
      return styles.sizes(columnWidth);
    }
    if (isWidth(columnWidth)) {
      return styles.widthOptions[columnWidth];
    }
  };

  const makeOffset = () => {
    if (isSizes(columnOffset)) {
      return styles.offsetSize(columnOffset);
    }
  };
  const columnBuilder = [
    styles.column,
    columnWidth && makeWidth(),
    columnOffset && makeOffset(),
    isPaddingless && styles.paddingless,
    textAlign && styles.textAlignment[textAlign],
  ];
  return (
    <div css={columnBuilder} {...rest}>
      {children}
    </div>
  );
};

export { Column };
