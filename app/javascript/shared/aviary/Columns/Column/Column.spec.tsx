import { shallow } from "enzyme";
import React from "react";

import { baseTests } from "@testing/baseTests";

import { Column } from "./Column";

describe("Column", () => {
  let wrapper;

  beforeEach(() => {
    buildWrapper();
  });

  const buildWrapper = () => {
    wrapper = shallow(
      <Column>
        <p>This is a column</p>
      </Column>
    );
  };

  baseTests(() => wrapper);
});
