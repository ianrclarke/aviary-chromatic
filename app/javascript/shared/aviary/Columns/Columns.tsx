import React, { FC, HTMLProps } from "react";

import * as styles from "./Columns.styles";
interface ColumnsProps extends HTMLProps<HTMLDivElement> {
  isMultiline?: boolean;
  isMarginless?: boolean;
  isCentered?: boolean;
}

const Columns: FC<ColumnsProps> = ({
  isMultiline,
  isMarginless,
  isCentered,
  children,
  ...rest
}) => {
  const columnsStyles = [
    styles.columns,
    isMultiline && styles.multiline,
    isMarginless && styles.marginless,
    isCentered && styles.centered,
  ];
  return (
    <div css={columnsStyles} {...rest}>
      {children}
    </div>
  );
};

export { Columns };
