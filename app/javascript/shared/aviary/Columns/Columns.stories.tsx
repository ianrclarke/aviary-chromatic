import React from "react";

import { select } from "@storybook/addon-knobs";
import { Column, Columns } from "@shared/aviary/Columns";

import { Container } from "@shared/aviary";

import { Sizes, SIZES } from "./grid";

import * as styles from "./Story.styles";

const componentDesign = {
  type: "figma",
  url: "https://www.figma.com/file/IzkK9ncJusumIi1LqsPcPH/Design-System?node-id=499%3A438",
};

export default {
  title: "Aviary|Columns",
  component: Columns,
  parameters: { design: componentDesign },
};

export const Default = () => (
  <div style={{ width: "100%" }}>
    <Container>
      <Columns>
        <Column columnWidth={select<Sizes>("gridWidth", SIZES as any, null)}>
          <div css={styles.exampleDiv}>
            <p>Customize width with "gridWidth"</p>
          </div>
        </Column>
        <Column columnOffset={select<Sizes>("offset", SIZES as any, null)}>
          <div css={styles.exampleDiv}>
            <p>Customize offset via "offset" </p>
          </div>
        </Column>
      </Columns>
    </Container>
    <Container>
      <Columns>
        <Column columnWidth={1}>
          <div css={styles.exampleDiv}>
            <p>GridWith 1</p>
          </div>
        </Column>
        <Column>
          <div css={styles.exampleDiv}>
            <p>Auto column</p>
          </div>
        </Column>
      </Columns>
      <Columns>
        <Column columnWidth={2}>
          <div css={styles.exampleDiv}>
            <p>GridWith 2</p>
          </div>
        </Column>
        <Column>
          <div css={styles.exampleDiv}>
            <p>Auto column</p>
          </div>
        </Column>
      </Columns>
      <Columns>
        <Column columnWidth={3}>
          <div css={styles.exampleDiv}>
            <p>GridWith 3</p>
          </div>
        </Column>
        <Column>
          <div css={styles.exampleDiv}>
            <p>Auto column</p>
          </div>
        </Column>
      </Columns>
      <Columns>
        <Column columnWidth={4}>
          <div css={styles.exampleDiv}>
            <p>GridWith 4</p>
          </div>
        </Column>
        <Column>
          <div css={styles.exampleDiv}>
            <p>Auto column</p>
          </div>
        </Column>
      </Columns>
      <Columns>
        <Column columnWidth={5}>
          <div css={styles.exampleDiv}>
            <p>GridWith 5</p>
          </div>
        </Column>
        <Column>
          <div css={styles.exampleDiv}>
            <p>Auto column</p>
          </div>
        </Column>
      </Columns>
      <Columns>
        <Column columnWidth={4}>
          <div css={styles.exampleDiv}>
            <p>GridWith 4</p>
          </div>
        </Column>
        <Column columnOffset={4}>
          <div css={styles.exampleDiv}>
            <p>OffsetWidth 4 column</p>
          </div>
        </Column>
      </Columns>
      <Columns>
        <Column columnOffset={4} columnWidth={4}>
          <div css={styles.exampleDiv}>
            <p>OffsetWidth 4 column, gridWidth 4</p>
          </div>
        </Column>
      </Columns>
    </Container>
  </div>
);
