export const SIZES = {
  1: 1,
  2: 2,
  3: 3,
  4: 4,
  5: 5,
  6: 6,
  7: 7,
  8: 8,
  9: 9,
  10: 10,
  11: 11,
  12: 12,
};
export type Sizes = keyof typeof SIZES;

export type Width = "full" | "narrow";
export type AllSizes = Sizes | Width;
export interface SizeObject {
  mobile?: AllSizes;
  tablet?: AllSizes;
  touch?: AllSizes;
  desktop?: AllSizes;
  widescreen?: AllSizes;
  fullhd?: AllSizes;
  default?: AllSizes;
}
export type Platforms = "mobile" | "tablet" | "desktop";
export interface HorizontalSize {
  isSize?: AllSizes | SizeObject;
}
export interface OffsetObject {
  mobile?: Sizes;
  tablet?: Sizes;
  touch?: Sizes;
  desktop?: Sizes;
  widescreen?: Sizes;
  fullhd?: Sizes;
  default?: Sizes;
}
export interface Offset {
  isOffset?: Sizes | OffsetObject;
}
