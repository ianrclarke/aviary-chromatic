import { css } from "@emotion/core";

import { colors, dimensions } from "@styles";

export const exampleDiv = css`
  background-color: ${colors.dark};
  color: ${colors.white};
  width: 100%;
  padding: 0.5rem;
  border-radius: ${dimensions.borderRadius};
`;
