import { createContext } from "react";

const FlippersContext = createContext<string[]>([]);

export { FlippersContext };
