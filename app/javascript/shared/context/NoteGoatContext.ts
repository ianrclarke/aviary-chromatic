import { createContext } from "react";

interface NoteGoatData {
  noteGoatIsOpen: boolean;
  setNoteGoatIsOpen: (open: boolean) => void;
  message: string;
  setMessage: (message: string) => void;
  status: string;
  setStatus: (status: string) => void;
  setFlashMessage: (status: string, message: string) => void;
}

const NoteGoatContext = createContext<NoteGoatData>(null);

export { NoteGoatContext, NoteGoatData };
