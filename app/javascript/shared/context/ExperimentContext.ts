import { createContext } from "react";

interface ExperimentContextData {
  e2e: boolean;
  roleId: string;
  userId: string;
  storeId: string;
}

const ExperimentContext = createContext<ExperimentContextData>(null);

export { ExperimentContext, ExperimentContextData };
