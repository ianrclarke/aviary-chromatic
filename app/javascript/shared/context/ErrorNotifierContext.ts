import { createContext } from "react";

interface ErrorNotice {
  error: any;
  context?: {};
  environment?: any;
  params?: any;
  session?: any;
}

interface ErrorNotifierData {
  notify: (notice: ErrorNotice) => void;
}

const ErrorNotifierContext = createContext<ErrorNotifierData>({ notify: () => null });

export { ErrorNotifierContext, ErrorNotifierData };
