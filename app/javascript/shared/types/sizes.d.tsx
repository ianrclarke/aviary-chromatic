enum Size {
  thumbnail = "thumbnail",
  xsmall = "xsmall",
  small = "small",
  medium = "medium",
  large = "large",
  xlarge = "xlarge",
}

export { Size };
