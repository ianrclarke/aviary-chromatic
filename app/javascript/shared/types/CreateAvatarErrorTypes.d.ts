interface AvatarErrorsType {
  message: string;
  fields: {
    input?: string[];
    filter?: string[];
  };
}
export { AvatarErrorsType };
