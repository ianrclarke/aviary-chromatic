export interface StripeErrorType {
  code: string;
  message: string;
  type: string;
}

export interface StripeDefaultInputType {
  complete: boolean;
  elementType: string;
  empty: boolean;
  error: StripeErrorType;
  value: string;
}

export interface StripeNumberInputType {
  brand: string;
  complete: boolean;
  elementType: string;
  empty: boolean;
  error: StripeErrorType;
  value: string;
}
