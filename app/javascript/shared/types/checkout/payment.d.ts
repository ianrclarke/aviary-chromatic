export interface CreditCard {
  ccType: string;
  firstName: string;
  lastName: string;
  id: string;
  lastDigits: string;
  month: string;
  year: string;
}
