export type bulmaColors =
  | "primary"
  | "info"
  | "success"
  | "warning"
  | "danger"
  | "secondary"
  | "purple"
  | "trio"
  | "specialty"
  | "important";

export enum bulmaColorProps {
  primary = "primary",
  secondary = "secondary",
  danger = "danger",
  info = "info",
  warning = "warning",
  trio = "trio",
  specialty = "specialty",
  important = "important",
}

export interface BulmaColorProps {
  isColor?: bulmaColors;
}
