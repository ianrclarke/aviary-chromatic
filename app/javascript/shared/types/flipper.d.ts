// Keep this in sync with services/patient_side/active_flippers.rb
export type PatientFlipper =
  | "patient_communications_josh"
  | "patient_unsubscribe_josh"
  | "account_page_v2_josejr"
  | "store_promotions_patient_luis"
  | "backorder_modal_broughy"
  | "shipping_delay_banner_broughy"
  | "wellness_categories_aidan";

// Keep this in sync with services/practitioner_side/active_flippers.rb
export type PractitionerFlipper =
  | "practitioner_v2_recommendations_page_trevor"
  | "practitioner_v2_wholesale_aidan"
  | "student_workflow"
  | "rx_v2_josh"
  | "patient_promotions_hossam"
  | "practitioner_catalog_redesign_dysnomia"
  | "share_n_save"
  | "wellness_categories_aidan"
  | "epic_ehr_bifrost";

export type Flipper = PatientFlipper | PractitionerFlipper;
