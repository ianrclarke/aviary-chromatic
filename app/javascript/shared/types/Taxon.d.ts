export interface Taxon {
  id: number;
  name: string;
  parent_id: number;
  subtype_name: string;
  parent_name: string;
}
