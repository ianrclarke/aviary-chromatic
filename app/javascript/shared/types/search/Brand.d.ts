export interface Brand {
  id: number;
  name: string;
  prefix: string;
  products_count: string;
  contact_details: object[];
}
