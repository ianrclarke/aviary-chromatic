export interface SearchSuggestion {
  id: number;
  name: string;
  type: "Brand" | "Filter" | "Query" | "Type" | "Category";
  parent: string;
}

export interface SearchResult {
  item: SearchSuggestion;
  matches: SearchResultMatch[];
  score: number;
}

export type Index = number[];

export interface SearchResultMatch {
  indices: Index[];
  key: string;
}

export interface SuggestionTagReplacement {
  type: "primary" | "purple" | "info" | "warning" | "danger" | "dark";
  name: string;
}
