export interface SearchBaseResult {
  _index: string;
  _id: string;
  _score: number;
  _type: string;
}

export interface SearchCatalogResult extends SearchBaseResult {
  _source: {
    name: string;
    brand_name: string;
    np_product_id: string;
    product_id: number;
    upc: array;
    np_variant_id: array;
    sku: array;
    status: array;
  };
}

export interface SearchStoreResult extends SearchBaseResult {
  _source: {
    store_id: number;
    blacklist_ids: any;
  };
}

export interface SearchPatientResult extends SearchBaseResult {
  _source: {
    patient_id: number;
    first_name: string;
    last_name: string;
    email: string;
    store_id: number;
    archived: boolean;
    practitioner_ids: any;
    full_name: string;
    email_prefix: string;
  };
}

export interface SearchOmniProductResult extends SearchBaseResult {
  _source: {
    name: string;
    sku: string;
    product_id: number;
    brand_name: string;
  };
}

export interface SearchOmniPractitionerResult extends SearchBaseResult {
  _source: {
    practitioner_full_name: string;
    practitioner_user_id: number;
    practitioner_email: string;
    practitioner_store_id: number;
    practitioner_id: number;
  };
}

export interface SearchOmniPatientResult extends SearchBaseResult {
  _source: {
    full_name: string;
    email: string;
    store_id: number;
    patient_id: number;
    patient_user_id: number;
  };
}

export interface SearchOmniBrandResult extends SearchBaseResult {
  _source: {
    b_name: string;
    b_id: number;
    b_slug: string;
  };
}

export interface SearchOmniStoreResult extends SearchBaseResult {
  _source: {
    store_name: string;
    store_id: number;
  };
}

export interface SearchOmniClerkResult extends SearchBaseResult {
  _source: {
    clerk_full_name: string;
    clerk_email: string;
    clerk_store_id: number;
    clerk_id: number;
    clerk_user_id: number;
  };
}

export interface SearchOmniOrderResult extends SearchBaseResult {
  _source: {
    order_id: number;
    order_shipment_number: string;
    order_number: string;
    order_shipment_id: number;
    order_store_id: number;
  };
}
