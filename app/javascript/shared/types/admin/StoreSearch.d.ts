export interface StoreSearchResult {
  id: number;
  name: string;
}
