declare module "*.png" {
  const png: string;
  export default png;
}

declare module "*.jpg" {
  const jpg: string;
  export default jpg;
}

declare module "*.gif" {
  const src: string;
  export default src;
}

declare module "*.svg" {
  import React, { FC, SVGProps } from "react";
  const ReactComponent: FC<SVGProps<SVGSVGElement>>;
  const src: string;
  export default src;
  export { ReactComponent };
}
