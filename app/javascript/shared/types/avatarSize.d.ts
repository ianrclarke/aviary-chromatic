enum AvatarSize {
  SMALL = "SMALL_SIZE",
  LARGE = "LARGE_SIZE",
}

export { AvatarSize };
