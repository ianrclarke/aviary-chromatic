/*
 * Primary
 * Buttons, text links, display icons
 */

export const greenery = "#88b04b"; // Used for buttons, primary text, text links, display icons
export const greeneryLight = "#9ABC66"; // Hover on greenery buttons
export const greeneryActive = "#7A9E43"; // Active color for greenery
export const greeneryExtraLight = "#F8FAF5"; // Backgrounds to greenery icons

export const berry = "#1B7ABA"; // Used for tags, text links, display icons
export const berryLight = "#3D8EC4"; // Hover on berry buttons
export const berryExtraLight = "#F5F7FA"; // Backgrounds to berry icons, disabled buttons

export const jay = "#284CA9";

export const weirdBlueForFacesFeedback = "#9FB8C8";
/*
 * Opacity colours
 */

export const green25opacity = "#E1EBD2";
export const green5opacity = "#F9FBF6";

export const blue25opacity = "#C6DEEE";
export const blue5opacity = "#F4F8FC";

export const orange25opacity = "#FDE6C6";
export const orange5opacity = "#FFFAF4";

export const red25opacity = "#F8D6DB";
export const red5opacity = "#FEF7F8";

export const purple25opacity = "#DDCFD9";
export const purple5opacity = "#F8F5F7";

/*
 * Type
 * All text, with the exception of links
 */

export const orion = "#404f5e"; // Used for headers, primary text, form inputs

export const pigeon = "#5e778e"; // Used for secondary text, form labels + icons
export const pigeonLight = "#C8D0D8"; // Used for form outlines, tag outlines
export const pigeonExtraLight = "#EBF0F5"; // Used for separator lines, base background
/*
 * System
 * Used sparingly for tags, highlights
 */

export const citrus = "#F89C1C";
export const coral = "#EF5C6E";
export const coralLight = "#F8B9C1";
export const grape = "#784067";
export const glacier = "#3EC5F3";
export const silt = "#ABBDB1";
export const highlighter = "#FBF8E5";
export const gull = "#FBFCFD";

/*
 * Shades
 */

export const white = "#ffffff";
export const black = "#000000";

/*
 * Social media icon colors
 */

export const facebookBlue = "#3C5A99";
export const twitterBlue = "#1DA1F2";
export const linkedInBlue = "#2867B2";

/*
 * The rest of the page is not part of the current Product Colors style guide.
 * Please clear with design before using for new products
 */
export const legacy = {
  fullscriptBlue: "#3498db",
  blueGreyLight: "#f8fafb",
  blueGrey: "#7793a9",
  border: "#d6ddd8",
  grapeExtraLight: "#e6d1e0",
  sycamore: "#779b42",
  greenOlive: "#69922b",
  nightHawkGray: "#363636",
  babyBlue: "#d1e2f3",
  orionExtraLight: "#98a8b9",
  orionDark: "#364351",
  orionExtraDark: "#2e3944",
};
