import * as colors from "@styles/emotion-styles/colors";

import { ColorProfile } from "./colorProfile";

export const primaryProfile: ColorProfile = {
  baseColor: colors.primary,
  baseText: colors.white,

  hoverColor: colors.primaryHover,
  hoverText: colors.white,

  activeColor: colors.primaryActive,
  outlineActiveColor: colors.greenExtraLight,

  extraLightColor: colors.greenExtraLight,
};
