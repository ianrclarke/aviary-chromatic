export interface ColorProfile {
  baseColor: string;
  baseText: string;
  baseSvg?: string;

  outlineText?: string;
  outlineSvg?: string;

  hoverColor: string;
  hoverText: string;
  hoverSvg?: string;

  activeColor?: string;

  outlineBorderColor?: string;
  outlineActiveBorderColor?: string;
  outlineActiveColor?: string;

  extraLightColor?: string;
}
