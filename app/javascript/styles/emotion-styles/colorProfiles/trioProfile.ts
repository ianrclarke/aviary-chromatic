import * as colors from "@styles/emotion-styles/colors";

import { ColorProfile } from "./colorProfile";

export const trioProfile: ColorProfile = {
  baseColor: colors.lightGreen,
  baseText: colors.white,

  outlineText: colors.grey,
  outlineSvg: colors.primary,

  hoverColor: colors.primary,
  hoverText: colors.white,

  activeColor: colors.primaryActive,
  outlineActiveColor: colors.greenExtraLight,
};
