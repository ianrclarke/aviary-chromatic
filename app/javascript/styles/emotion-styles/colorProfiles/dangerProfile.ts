import * as colors from "@styles/emotion-styles/colors";

import { ColorProfile } from "./colorProfile";

export const dangerProfile: ColorProfile = {
  baseColor: colors.danger,
  baseText: colors.white,

  hoverColor: colors.danger,
  hoverText: colors.white,

  activeColor: colors.danger,
  outlineActiveColor: colors.lightRed,

  extraLightColor: colors.redExtraLight,
};
