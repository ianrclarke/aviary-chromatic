import * as colors from "@styles/emotion-styles/colors";

import { ColorProfile } from "./colorProfile";

export const infoProfile: ColorProfile = {
  baseColor: colors.blue,
  baseText: colors.white,

  hoverColor: colors.blueLight,
  hoverText: colors.white,

  activeColor: colors.blueLight,
  outlineActiveColor: colors.blueExtraLight,

  extraLightColor: colors.blueExtraLight,
};
