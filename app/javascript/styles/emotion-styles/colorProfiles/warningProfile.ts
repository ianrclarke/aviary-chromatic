import * as colors from "@styles/emotion-styles/colors";

import { ColorProfile } from "./colorProfile";

export const warningProfile: ColorProfile = {
  baseColor: colors.warning,
  baseText: colors.white,

  hoverColor: colors.warning,
  hoverText: colors.white,

  activeColor: colors.warning,

  extraLightColor: colors.orangeExtraLight,
};
