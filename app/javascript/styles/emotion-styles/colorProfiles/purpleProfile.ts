import * as colors from "@styles/emotion-styles/colors";

import { ColorProfile } from "./colorProfile";

export const purpleProfile: ColorProfile = {
  baseColor: colors.purple,
  baseText: colors.white,

  hoverColor: colors.purple,
  hoverText: colors.white,

  activeColor: colors.purple,
  outlineActiveColor: colors.purpleLight,

  extraLightColor: colors.purpleExtraLight,
};
