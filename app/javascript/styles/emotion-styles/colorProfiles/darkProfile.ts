import * as colors from "@styles/emotion-styles/colors";

import { ColorProfile } from "./colorProfile";

export const darkProfile: ColorProfile = {
  baseColor: colors.dark,
  baseText: colors.white,

  hoverColor: colors.dark,
  hoverText: colors.white,

  activeColor: colors.dark,
  outlineActiveColor: colors.darkExtraLight,

  extraLightColor: colors.greyExtraLight,
};
