import * as colors from "@styles/emotion-styles/colors";

import { ColorProfile } from "./colorProfile";

export const specialtyProfile: ColorProfile = {
  baseColor: colors.grey,
  baseText: colors.white,

  hoverColor: colors.grey,
  hoverText: colors.white,

  activeColor: colors.grey,

  outlineBorderColor: colors.light,
  outlineActiveBorderColor: colors.green,
  outlineActiveColor: colors.white,
};
