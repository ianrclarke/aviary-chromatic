export const h1 = "2.75rem"; // bulma size-1
export const h2 = "2.125rem"; // bulma size-2
export const h3 = "1.625rem"; // bulma size-3
export const h4 = "1.375rem"; // bulma size-4
export const h5 = "1.125rem"; // bulma size-5
