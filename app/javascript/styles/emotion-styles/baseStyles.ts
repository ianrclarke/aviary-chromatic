import { css } from "@emotion/core";

import { hexToRgba } from "@styles/emotion-styles/helpers";

import * as colors from "./colors";
import { grey } from "./colors";
import { modalBorderRadius } from "./dimensions";

export const controlBase = css`
  appearance: none;
  box-shadow: none;

  display: inline-flex;
  position: relative;
  align-items: center;
  justify-content: flex-start;
  line-height: 1.5;
  vertical-align: top;

  color: ${colors.dark};
  border: 1px solid transparent;
  border-radius: 4px;
  font-size: 1rem;

  &[disabled],
  fieldset[disabled] & {
    cursor: not-allowed;
  }
`;

export const cardShadow = css`
  box-shadow: 0px 1px 2px ${hexToRgba(grey, 0.25)};
`;

export const modalStyles = css`
  border-radius: ${modalBorderRadius};
  box-shadow: 0 2px 4px 0 ${hexToRgba(colors.dark, 0.2)};
`;

export const footerShadow = css`
  box-shadow: 0px -3px 8px ${hexToRgba(grey, 0.25)};
`;

export const popoverShadow = css`
  box-shadow: 0px 3px 8px ${hexToRgba(colors.grey, 0.25)};
`;
