import { hexToRgba } from "./helpers/hexToRgba";
import * as palette from "./palette";

// Brand colors
export const green = palette.greenery;
export const purple = palette.grape;
export const red = palette.coral;
export const pink = palette.coral;
export const blue = palette.berry;
export const orange = palette.citrus;
export const dark = palette.orion;
export const grey = palette.pigeon;
export const darkBlue = palette.jay;

// Color Spectrums
export const greyLight = palette.pigeonLight;
export const greyExtraLight = palette.berryExtraLight;

export const blueLight = palette.berryLight;
export const blueExtraLight = palette.berryExtraLight;

export const greenLight = palette.green25opacity;
export const greenExtraLight = palette.green5opacity;

export const orangeLight = palette.orange25opacity;
export const orangeExtraLight = palette.orange5opacity;

export const redLight = palette.red25opacity;
export const redExtraLight = palette.red5opacity;

export const purpleLight = palette.purple25opacity;
export const purpleExtraLight = palette.purple5opacity;

// Naming
export const primary = green;
export const warning = orange;
export const danger = orange;
export const success = green;
export const light = palette.pigeonLight;
export const info = palette.legacy.fullscriptBlue;
export const darkExtraLight = palette.legacy.orionExtraLight;
export const darkDark = palette.legacy.orionDark;
export const darkExtraDark = palette.legacy.orionExtraDark;
export const white = palette.white;
export const black = palette.black;
export const lightGrey = palette.berryExtraLight;
export const lightGreen = palette.silt;
export const lightRed = palette.coralLight;

export const greyBlue = palette.weirdBlueForFacesFeedback;

// Hover states, legacy
export const linkHover = palette.legacy.nightHawkGray;

export const primaryHover = palette.greeneryLight;
export const primaryActive = palette.greeneryActive;

// Background
export const background = palette.pigeonExtraLight;
export const backgroundLight = palette.berryExtraLight;
export const backgroundExtraLight = palette.gull;
export const backgroundWhite = palette.white;
export const backgroundHighlighter = palette.highlighter;
export const backgroundDanger = hexToRgba(danger, 0.9);
export const backgroundPurple = hexToRgba(purple, 0.9);
export const backgroundModal = hexToRgba(palette.black, 0.2);
export const backgroundWarningLight = hexToRgba(warning, 0.05);

// Banners
export const successBanner = palette.greeneryExtraLight;

// Borders and separators
export const separator = palette.pigeonExtraLight;
export const splitButtonSeparator = hexToRgba(palette.white, 0.75);
export const splitButtonDisabledSeparator = hexToRgba(palette.pigeonLight, 0.5);

export const disabledColorOnBlack = hexToRgba(palette.white, 0.5);

// Sidebar
export const sidebarLink = palette.legacy.babyBlue;

// Social media icon colors
export const facebookBlue = palette.facebookBlue;
export const twitterBlue = palette.twitterBlue;
export const linkedInBlue = palette.linkedInBlue;

export const legacy = {
  border: palette.legacy.border,
  blue: palette.legacy.fullscriptBlue,
  light: palette.pigeonExtraLight,
  blueGrey: palette.legacy.blueGrey,
  blueGreyLight: palette.legacy.blueGreyLight,
};
