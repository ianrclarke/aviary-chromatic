export const h1 = "2.75rem"; // bulma size-1
export const h2 = "2.125rem"; // bulma size-2
export const h3 = "1.625rem"; // bulma size-3
export const h4 = "1.375rem"; // bulma size-4
export const h5 = "1.125rem"; // bulma size-5
export const smallLabel = "0.875rem";
export const subtitle = "1.125rem"; // legacy sizes
export const small = "0.75rem"; // legacy sizes
export const paragraphShort = "1.125rem";
export const paragraphLong = "1rem";

// Font weights
export const bold = "900";
export const semiBold = "600";
export const strong = "500";
export const regular = "400";
export const light = "300";

// Font families
export const familySansSerif = `sofia-pro, Apple-system, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell",
  "Fira Sans", "Droid Sans", "Helvetica Neue", "Helvetica", "Arial", sans-serif`;

export const headerFont = familySansSerif;
export const bodyFont = familySansSerif;
