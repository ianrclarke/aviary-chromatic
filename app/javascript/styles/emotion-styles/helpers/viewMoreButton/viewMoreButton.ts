import { css } from "@emotion/core";

import { white } from "@styles/emotion-styles/colors";

export const viewMoreButton = (color: string) => css`
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${color};
  border-radius: 4px;
  border: 1px solid ${color};
  margin-bottom: 0.5rem;
  margin-right: 0.5rem;
  width: 100%;
  height: 100%;
  padding: 1rem 1.5rem;

  &:hover {
    border: 1px solid ${color};
    background-color: ${color};
    color: ${white};
  }
`;
