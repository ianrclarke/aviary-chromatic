import { SerializedStyles } from "@emotion/core";

const { css } = jest.requireActual("@emotion/core");

import { stylesHelper } from "./stylesHelper";

describe("styles", () => {
  let baseStyle: SerializedStyles;
  let flexedStyle: SerializedStyles;
  let redStyle: SerializedStyles;
  let blueStyle: SerializedStyles;
  let greenStyle: SerializedStyles;
  let sizeStyle;

  beforeEach(() => {
    baseStyle = css`
      width: 100%;
    `;
    flexedStyle = css`
      display: flex;
    `;
    redStyle = css`
      color: red;
    `;
    blueStyle = css`
      color: blue;
    `;
    greenStyle = css`
      color: green;
    `;

    sizeStyle = {
      big: css`
        font-size: 5rem;
      `,
      med: css`
        font-size: 3rem;
      `,
      small: css`
        font-size: 1rem;
      `,
    };
  });

  describe("Single css", () => {
    it("returns an array of the element", () => {
      expect(stylesHelper(baseStyle)).toEqual([baseStyle]);
    });
  });

  describe("css boolean pair", () => {
    it("returns the css array if boolean is true", () => {
      expect(stylesHelper([baseStyle, true])).toEqual([baseStyle]);
    });

    it("returns an empty array if boolean is false", () => {
      expect(stylesHelper([baseStyle, false])).toEqual([]);
    });
  });

  describe("list of css boolean pair", () => {
    it("returns all css values that are true", () => {
      expect(
        stylesHelper([
          [baseStyle, true],
          [redStyle, false],
          [blueStyle, false],
          [greenStyle, true],
        ])
      ).toEqual([baseStyle, greenStyle]);
    });
  });

  describe("css with bracket notation", () => {
    const mockProp = "big";
    const nullProp = "sandwich";

    it("returns the css by using bracket notation", () => {
      expect(stylesHelper(sizeStyle[mockProp])).toEqual([sizeStyle[mockProp]]);
    });

    it("referring a non-existent property has no negative effects", () => {
      expect(stylesHelper(baseStyle, sizeStyle[nullProp])).toEqual([baseStyle]);
    });
  });

  describe("all combinded", () => {
    it("works for a real life exampe", () => {
      const flexed = true;
      const color = "red";

      expect(
        stylesHelper(baseStyle, [flexedStyle, flexed], [[redStyle, color === "red"]])
      ).toEqual([baseStyle, flexedStyle, redStyle]);
    });
  });
});
