import { css, SerializedStyles } from "@emotion/core";

export const focusSelector = (styles: SerializedStyles) => {
  return css`
    &:active,
    &.is-active {
      ${styles}
    }
  `;
};
