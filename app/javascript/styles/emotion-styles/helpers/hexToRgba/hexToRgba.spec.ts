import { hexToRgba } from "./hexToRgba";

const noAlphaSample = {
  hex: "#f31277",
  opacity: 0.5,
  rgba: "rgba(243, 18, 119, 0.5)",
};

const alphaSample = {
  hex: "#32f31277",
  rgba: "rgba(243, 18, 119, 0.5)",
};

describe("hexToRgba", () => {
  it("correctly returns the rgba for eight digit hex values", () => {
    expect(hexToRgba(alphaSample.hex)).toEqual(alphaSample.rgba);
  });

  it("correctly returns the rgba for six digit hex values", () => {
    expect(hexToRgba(noAlphaSample.hex, noAlphaSample.opacity)).toEqual(noAlphaSample.rgba);
  });

  it("returns null for incorrect hex values", () => {
    expect(hexToRgba("0")).toBeNull();
  });
});
