import { css } from "@emotion/core";

import { background, grey, light, lightGreen, primary } from "@styles/emotion-styles/colors";
import { boxShadowMaker } from "@styles/emotion-styles/helpers/boxShadowMaker";

export const inputTextBoxMaker = css`
  ${boxShadowMaker};
  height: 2.25rem;
  width: 100%;
  padding-left: 0.625rem;
  padding-right: 1rem;
  font-size: 1rem;
  line-height: 1.625rem;
  color: ${grey};
  caret-color: ${primary};

  ::placeholder {
    color: ${light};
  }

  &[disabled],
  fieldset[disabled] & {
    background-color: ${background};
    color: ${lightGreen};
    cursor: not-allowed;
  }

  /* States */
  &:focus,
  &:focus-within,
  &.is-focused,
  &:active,
  &.is-active {
    outline: none;
    border-color: ${primary};
  }
`;
