import { css } from "@emotion/core";

import { hoverBoxShadow } from "@styles/emotion-styles/animations";
import { greyLight, white } from "@styles/emotion-styles/colors";
import { borderRadius } from "@styles/emotion-styles/dimensions";

export const boxShadowMaker = css`
  background: ${white};
  border: 1px solid ${greyLight};
  box-sizing: border-box;
  border-radius: ${borderRadius};

  :hover,
  :active,
  :focus-within,
  :focus {
    ${hoverBoxShadow}
  }
`;
