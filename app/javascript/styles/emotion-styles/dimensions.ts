// Thumbs
export const thumbDimensions = "50px";
export const thumbDimensionsSmall = "35px";
export const thumbDimensionsLarge = "65px";

// Border
export const borderRadius = "4px";
export const circleRadius = "50%";
export const modalBorderRadius = "5px";
export const roundedCardRadius = "14px";
export const pillBorderRadius = "80px";
export const accountCard = "5px 5px 0 0";
export const semiRoundedCardRadius = "10px";

export const containerMargin = "2rem";
export const containerMarginMobile = "1rem";

const GAP = 32;

export const breakpoints = {
  phoneMini: { min: 0, max: 319 },
  phoneSmall: { min: 320, max: 374 },
  phone: { min: 375, max: 599 },
  phoneLarge: { min: 600, max: 768 },
  tablet: { min: 769, max: 959 + 2 * GAP },
  desktop: { min: 960 + 2 * GAP, max: 1151 + 2 * GAP },
  widescreen: { min: 1152 + 2 * GAP, max: 1343 + 2 * GAP },
  fullHD: { min: 1344 + 2 * GAP, max: Number.MAX_SAFE_INTEGER },
};

// Bulma default breakpoints
export const gap = `${GAP}px`;

export const maxContainer = `${breakpoints.fullHD.min - 64}px`;

// New method of breakpoints, all values are INCLUSIVE and never cross into another "named" breakpoint.
// EX: "tablet" or "tabletMax" only ever reference values that are valid tablet sizes ( ie: between 769px and 1023px)
// The OLD method was wrong, and crossed boundaries by saying "tabletPlus", which was actually 1px ABOVE
// what a tablet could be, which caused TOO much confusion

// Phone mini
export const phoneMini = `${breakpoints.phoneMini.min}px`;
export const phoneMiniMax = `${breakpoints.phoneMini.max}px`;

// Phone small 320px (iPhone SE, 5S)
export const phoneSmall = `${breakpoints.phoneSmall.min}px`;
export const phoneSmallMax = `${breakpoints.phoneSmall.max}px`;

// Phone 375px (iPhone 6S, 7, 8 etc)
export const phone = `${breakpoints.phone.min}px`;
export const phoneMax = `${breakpoints.phone.max}px`;

// Phone large 600px
export const phoneLarge = `${breakpoints.phoneLarge.min}px`;
export const phoneLargeMax = `${breakpoints.phoneLarge.max}px`;

// Tablet 769px
export const tablet = `${breakpoints.tablet.min}px`;
export const tabletMax = `${breakpoints.tablet.max}px`;

// Desktop 1024px
export const desktop = `${breakpoints.desktop.min}px`;
export const desktopMax = `${breakpoints.desktop.max}px`;

// Widescreen 1216px
export const widescreen = `${breakpoints.widescreen.min}px`;
export const widescreenMax = `${breakpoints.widescreen.max}px`;

// FullHD 1408px+
export const fullHD = `${breakpoints.fullHD.min}px`;
export const fullHDMax = `${breakpoints.fullHD.max}px`;
