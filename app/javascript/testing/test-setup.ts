/* eslint-disable  */
// this file is loaded by jest.config before any test that is run. You can put top-level code that applies to all jest tests here.
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import React from "react";
React.useLayoutEffect = React.useEffect;

configure({ adapter: new Adapter() });

require("jest-fetch-mock").enableMocks();

declare global {
  namespace jest {
    interface Matchers<R, T> {
      toContainStringOnce(b: string): R;
    }
  }
}

const { location, scrollTo } = window;

expect.extend({
  toContainStringOnce(received: string[], expected: string) {
    const pass = received.filter(value => value === expected).length === 1;
    if (pass) {
      return {
        message: () => `expected [${received}] to not contain string "${expected}" once`,
        pass: true,
      };
    }
    return {
      message: () => `expected [${received}] to contain string "${expected}" once`,
      pass: false,
    };
  },
});

let isConsoleError;
let isConsoleWarning;
let originalError;
let originalWarn;

beforeAll(() => {
  originalError = global.console.error;
  originalWarn = global.console.warn;
});

beforeEach(() => {
  jest.resetModules();
  isConsoleError = false;
  isConsoleWarning = false;

  jest.spyOn(global.console, "error").mockImplementation((...args) => {
    isConsoleError = true;
    originalError(...args);
  });

  jest.spyOn(global.console, "warn").mockImplementation((...args) => {
    isConsoleWarning = true;
    originalWarn(...args);
  });

  delete window.location;
  delete window.scrollTo;
  // @ts-ignore
  window.location = {
    assign: jest.fn(),
  };
  window.scrollTo = jest.fn();
});

afterEach(() => {
  if (isConsoleError) {
    throw new Error("Console errors are not allowed");
  }
  if (isConsoleWarning) {
    throw new Error("Console warnings are not allowed");
  }
  window.location = location;
  window.scrollTo = scrollTo;
});
