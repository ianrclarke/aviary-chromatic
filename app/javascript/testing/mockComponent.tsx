import React, { FC } from "react";

const mockComponent: FC = ({ children }) => {
  return <>{children}</>;
};

export { mockComponent };
