import { ReactWrapper, ShallowWrapper } from "enzyme";

export const baseTests = (getWrapper: () => ReactWrapper | ShallowWrapper) => {
  describe("Base Tests", () => {
    it("wrapper is defined", () => {
      const wrapper = getWrapper();
      expect(wrapper).not.toBeUndefined();
      expect(wrapper).not.toBeNull();
    });
  });
};
