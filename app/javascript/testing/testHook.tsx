import { mount } from "enzyme";
import React, { FC } from "react";
import { act } from "react-dom/test-utils";

interface Props {
  hook: () => void;
}
const TestHook: FC<Props> = ({ hook }) => {
  hook();
  return null;
};

let hookWrapper;

const testHook = (hook: () => void) => {
  act(() => {
    hookWrapper = mount(<TestHook hook={hook} />);
  });
};

export { testHook, hookWrapper };
