import glob from "glob";

/**
 * writes a list of files matching the glob pattern to stdout
 * run only the subset of files in each Jest jobs.
 */

const JOB_COUNT = process.env.CI_NODE_TOTAL || 6;
//Gitlab CI_NODE_INDEX starting from 1 by default, so set it to start from 0
const JOB_INDEX = process.env.CI_NODE_INDEX - 1 || 0;

function getFiles() {
  const allFiles = glob.sync("app/javascript/**/*.spec.ts*");

  const filesPerJob = Math.ceil(allFiles.length / JOB_COUNT);
  const startIndex = filesPerJob * JOB_INDEX;
  return allFiles.slice(startIndex, startIndex + filesPerJob);
}

const files = getFiles();

process.stdout.write(files.join(" "));
