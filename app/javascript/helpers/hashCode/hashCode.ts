/* eslint-disable no-bitwise */
const hashCode = (value: string) => {
  let hash = 0;
  for (let i = 0; i < value.length; i++) {
    hash = (hash << 5) - hash + value.charCodeAt(i);
    hash &= hash;
  }
  return hash;
};
/* eslint-enable no-bitwise */

export { hashCode };
