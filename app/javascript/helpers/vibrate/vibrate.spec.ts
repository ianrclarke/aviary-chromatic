/* eslint-disable @typescript-eslint/unbound-method */
import { vibrate } from "./vibrate";

describe("vibrate", () => {
  it("calls window vibrate if supported", () => {
    window.navigator.vibrate = jest.fn();
    vibrate();
    expect(window.navigator.vibrate).toHaveBeenCalledWith(100);
  });

  it("does nothing is window vibrate does not exist", () => {
    delete window.navigator.vibrate;
    vibrate();
  });
});
