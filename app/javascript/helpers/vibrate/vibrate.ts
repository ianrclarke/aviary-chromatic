const vibrate = (pattern: number | number[] = 100) => {
  if (window && window.navigator && window.navigator.vibrate) {
    window.navigator.vibrate(pattern);
  }
};

export { vibrate };
