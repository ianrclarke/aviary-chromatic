import { variantIsAvailable } from "./variantIsAvailable";

describe("variantIsAvailable", () => {
  it("is true if in stock", () => {
    expect(variantIsAvailable("In Stock", "Canada")).toEqual(true);
  });

  it("is true if back order and not Canada", () => {
    expect(variantIsAvailable("Backordered", "USA")).toEqual(true);
  });

  it("is false if back order and Canada", () => {
    expect(variantIsAvailable("Backordered", "Canada")).toEqual(false);
  });

  it("is false for a lot of things", () => {
    expect(variantIsAvailable("Bad stuff", "USA")).toEqual(false);
  });
});
