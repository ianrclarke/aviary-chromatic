const variantIsAvailable = (availabilityStatus, country) => {
  return (
    availabilityStatus === "In Stock" ||
    (availabilityStatus === "Backordered" && country !== "Canada")
  );
};

export { variantIsAvailable };
