export const getColourForStatus = (status: string) => {
  switch (status.toLowerCase()) {
    case "in stock":
      return "info";
    case "out of stock":
    case "backordered":
      return "warning";
    case "unavailable":
    case "n/a":
      return "danger";
    default:
      return "warning";
  }
};
