import { createAddressFragment } from "@shared/data/fragments/mocks";

import { isCalifornia } from "./isCalifornia";

describe("isCalifornia", () => {
  let addressMock;
  it("returns true if address is in california", () => {
    addressMock = createAddressFragment("Hollywood", {
      state: "California",
    });
    expect(isCalifornia(addressMock)).toEqual(true);
  });
  it("returns false if not in california", () => {
    addressMock = createAddressFragment("Hollywood");
    expect(isCalifornia(addressMock)).toEqual(false);
  });
});
