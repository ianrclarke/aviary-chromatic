import { AddressFragment } from "@shared/data/fragments";

export const isCalifornia = (shippingAddress: AddressFragment) => {
  return shippingAddress.state.toLowerCase() === "california";
};
