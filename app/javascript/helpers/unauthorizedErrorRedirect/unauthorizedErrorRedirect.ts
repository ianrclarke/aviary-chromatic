import { ServerError, ServerParseError } from "apollo-link-http-common";

type NetworkError = Error | ServerError | ServerParseError;

const isServerError = (error: NetworkError): error is ServerError => {
  return !!(error as ServerError).result;
};

const isServerParseError = (error: NetworkError): error is ServerParseError => {
  return !!(error as ServerParseError).response;
};

const is403 = (error: NetworkError): boolean => {
  return isServerError(error) || (isServerParseError(error) && error.statusCode === 403);
};

const isDevelopment = (): boolean => {
  return process.env.NODE_ENV === "development";
};

const unauthorizedErrorRedirect = (error: NetworkError) => {
  if (!!error && is403(error) && !isDevelopment()) {
    window.location.assign("/");
  }
};

export { unauthorizedErrorRedirect };
