/* eslint-disable @typescript-eslint/unbound-method */
import { ServerError, ServerParseError } from "apollo-link-http-common";

import "isomorphic-fetch";
import { unauthorizedErrorRedirect } from "./unauthorizedErrorRedirect";

describe("unauthorizedErrorRedirect", () => {
  beforeEach(() => {
    window.location.assign = jest.fn();
  });

  it("redirects to / when receiving a 403 ServerError", () => {
    const error = {
      response: new Response(),
      statusCode: 403,
      result: { foo: "bar" },
      message: "",
      name: "",
      stack: "",
    } as ServerError;

    unauthorizedErrorRedirect(error);
    expect(window.location.assign).toHaveBeenCalledWith("/");
  });

  it("redirects to / when receiving a 403 ServerParseError", () => {
    const error = {
      response: new Response(""),
      statusCode: 403,
      bodyText: "",
    } as ServerParseError;

    unauthorizedErrorRedirect(error);
    expect(window.location.assign).toHaveBeenCalledWith("/");
  });

  it("does not redirect if the environment is development", () => {
    const error = {
      response: new Response(""),
      statusCode: 403,
      bodyText: "",
    } as ServerParseError;
    process.env.NODE_ENV = "development";

    unauthorizedErrorRedirect(error);
    expect(window.location.assign).not.toHaveBeenCalledWith("/");
  });

  it("does not redirect if the error is a generic Error", () => {
    const error = new Error("bla!");
    unauthorizedErrorRedirect(error);
    expect(window.location.assign).not.toHaveBeenCalled();
  });

  it("does not redirect if error is undefined", () => {
    unauthorizedErrorRedirect(undefined);
    expect(window.location.assign).not.toHaveBeenCalled();
  });
});
