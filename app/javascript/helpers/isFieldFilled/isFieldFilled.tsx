export const isFieldFilled = item => {
  return item !== "" && item !== null && typeof item !== "undefined";
};
