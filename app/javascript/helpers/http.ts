let csrfName;
export const getCSRFName = () => {
  if (!csrfName) {
    const element = document.querySelector("meta[name='csrf-param']");
    if (element) {
      csrfName = element.getAttribute("content");
    }
  }
  return csrfName;
};

let csrfToken;
export const getCSRFToken = () => {
  if (!csrfToken) {
    const element = document.querySelector("meta[name='csrf-token']");
    if (element) {
      csrfToken = element.getAttribute("content");
    }
  }
  return csrfToken;
};

export const fetchPost = (url, params) => {
  return fetch(url, {
    method: "POST",
    mode: "cors",
    cache: "default",
    credentials: "include",
    body: JSON.stringify(params),
    headers: {
      "Content-Type": "application/json",
      "X-CSRF-TOKEN": getCSRFToken(),
    },
    redirect: "manual",
  });
};

export const fetchPostWithJson = (url, params) => {
  return fetchPost(url, params).then(response => response.json());
};

export const fetchGet = url => {
  return fetch(url, {
    method: "GET",
    mode: "cors",
    cache: "default",
    credentials: "include",
    headers: {
      "Content-Type": "application/json",
      Auth: "213455",
    },
  }).then(response => response.json());
};

export const fetchPatch = (url, params) => {
  return fetch(url, {
    method: "PATCH",
    mode: "cors",
    cache: "default",
    credentials: "include",
    body: JSON.stringify(params),
    headers: {
      "Content-Type": "application/json",
      "X-CSRF-TOKEN": getCSRFToken(),
    },
  });
};

export const fetchPut = (url, params) => {
  return fetch(url, {
    method: "PUT",
    mode: "cors",
    cache: "default",
    credentials: "include",
    body: JSON.stringify(params),
    headers: {
      "Content-Type": "application/json",
      "X-CSRF-TOKEN": getCSRFToken(),
    },
  });
};

export const fetchDelete = (url, params) => {
  return fetch(url, {
    method: "DELETE",
    mode: "cors",
    cache: "default",
    credentials: "include",
    body: JSON.stringify(params),
    headers: {
      "Content-Type": "application/json",
      "X-CSRF-TOKEN": getCSRFToken(),
    },
  });
};
