import { act } from "react-dom/test-utils";

import { wait } from "./wait";

// eslint-disable-next-line @typescript-eslint/require-await
const actWait = async (amount = 0) =>
  act(async () => {
    await wait(amount);
  });
export { wait, actWait };
