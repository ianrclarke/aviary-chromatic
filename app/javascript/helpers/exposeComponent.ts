/* eslint-disable no-underscore-dangle */
import React from "react";
import ReactDOM from "react-dom";

(window as any).React = React;
(window as any).ReactDOM = ReactDOM;
(window as any).__COMPONENTS = (window as any).__COMPONENTS || {};

export const exposeComponent = (component, name: string) => {
  (window as any).__COMPONENTS[name] = component;
};
