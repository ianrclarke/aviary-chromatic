import { getLocalData, removeLocalData, setLocalData } from "@helpers/accessLocalStorage";

describe("accessLocalStorage", () => {
  it("returns an empty value when nothing is set yet", () => {
    expect(getLocalData("invalidKey")).toEqual(null);
  });

  describe("set/get different data types", () => {
    it("returns an object when set", () => {
      const objectData = { firstName: "FirstName", lastName: "LastName" };
      setLocalData("objectData", objectData);
      expect(getLocalData("objectData")).toEqual(objectData);
      removeLocalData("objectData");
    });

    it("returns string when set", () => {
      const sampleMessage = "Sample Message";
      setLocalData("sampleMessage", sampleMessage);
      expect(getLocalData("sampleMessage")).toEqual(sampleMessage);
      removeLocalData("sampleMessage");
    });

    it("return boolean when set", () => {
      const sampleBoolean = true;
      setLocalData("sampleBoolean", sampleBoolean);
      expect(getLocalData("sampleBoolean")).toEqual(sampleBoolean);
      removeLocalData("sampleBoolean");
    });

    it("return a number when set", () => {
      const sampleNumber = 999;
      setLocalData("sampleNumber", sampleNumber);
      expect(getLocalData("sampleNumber")).toEqual(sampleNumber);
      removeLocalData("sampleNumber");
    });
  });

  it("returns an empty value when the previously set data has been removed", () => {
    setLocalData("randomData", 123123);
    expect(getLocalData("randomData")).toEqual(123123);
    removeLocalData("randomData");
    expect(getLocalData("randomData")).toEqual(null);
  });
});
