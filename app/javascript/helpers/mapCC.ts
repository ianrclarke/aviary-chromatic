export const mapCC = ccType => {
  switch (ccType) {
    case ccType === "MasterCard":
      return "mastercard";
    case ccType === "Visa":
      return "visa";
    case ccType === "American Express":
      return "amex";
    case ccType === "Discover":
      return "discover";
    case ccType === "Diners Club":
      return "dinersclub";
    case ccType === "JCB":
      return "jcb";
  }
};
