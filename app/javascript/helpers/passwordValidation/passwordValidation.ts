export interface ValidPassword {
  isValid: boolean;
  isValidLength: boolean;
  isValidLowercase?: boolean;
  isValidNumeric?: boolean;
  isValidPasswords: boolean;
  isValidSpecialCharacter?: boolean;
  isValidUppercase?: boolean;
}

export const empty = (stringToCheck: string): boolean => {
  return stringToCheck === undefined || stringToCheck === null || stringToCheck.length === 0;
};

export const isValid = (
  password1: string,
  password2: string,
  passwordLength = 6,
  isAdvancedSecurity = false
): ValidPassword => {
  const isValidLength = validateLength(password1, passwordLength);
  const isValidPasswords = validatePasswords(password1, password2);
  if (!isAdvancedSecurity) {
    return {
      isValid: isValidLength && isValidPasswords,
      isValidLength,
      isValidPasswords,
    };
  } else {
    const isValidLowercase = validateLowercase(password1);
    const isValidNumeric = validateNumeric(password1);
    const isValidSpecialCharacter = validateSpecialCharacter(password1);
    const isValidUppercase = validateUppercase(password1);
    return {
      isValid:
        isValidLength &&
        isValidPasswords &&
        isValidLowercase &&
        isValidNumeric &&
        isValidSpecialCharacter &&
        isValidUppercase,
      isValidLength,
      isValidPasswords,
      isValidLowercase,
      isValidNumeric,
      isValidSpecialCharacter,
      isValidUppercase,
    };
  }
};

const validateLength = (password: string, requiredLength: number): boolean => {
  if (empty(password) || requiredLength === null) {
    return false;
  }
  return password.length >= requiredLength;
};

const validateLowercase = (password: string): boolean => {
  if (empty(password)) {
    return false;
  }
  return password.match(/[a-z]/g) !== null;
};

const validateNumeric = (password: string): boolean => {
  if (empty(password)) {
    return false;
  }
  return /\d/.exec(password) !== null;
};

const validatePasswords = (password1: string, password2: string): boolean => {
  if (empty(password1) || empty(password2)) {
    return false;
  }
  return password1 === password2;
};

const validateSpecialCharacter = (password: string): boolean => {
  if (empty(password)) {
    return false;
  }
  return /[^a-zA-z\d\s]/.exec(password) !== null;
};

const validateUppercase = (password: string): boolean => {
  if (empty(password)) {
    return false;
  }
  return /[A-Z]/g.exec(password) !== null;
};
