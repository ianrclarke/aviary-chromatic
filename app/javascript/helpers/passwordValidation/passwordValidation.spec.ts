import { empty, isValid } from "./passwordValidation";

describe("password validation", () => {
  describe("empty", () => {
    it("should return true when the string is null", () => {
      expect(empty(null)).toBe(true);
    });

    it("should return true when the string is blank", () => {
      expect(empty("")).toBe(true);
    });

    it("should return false when the string is undefined", () => {
      expect(empty(undefined)).toBe(true);
    });

    it("should return false when the string is not null nor blank", () => {
      expect(empty("asdf")).toBe(false);
    });
  });

  describe("isValid", () => {
    describe("is advanced security", () => {
      it("should return an object with isValid as false and isValidLength as false when the passwords are under 8 characters long", () => {
        expect(isValid("asdfQ1$", "asdfQ1$", 8, true)).toMatchObject({
          isValid: false,
          isValidLength: false,
        });
      });

      it("should return an object with isValid as false and isValidPasswords as false when the passwords are not equal", () => {
        expect(isValid("asdfQW12$", "zxcvAS12$", 8, true)).toMatchObject({
          isValid: false,
          isValidPasswords: false,
        });
      });

      it("should return an object with isValid as false and isValidNumeric as false when the passwords do not include 1 or more numbers", () => {
        expect(isValid("asdfQWER$", "asdfQWER$", 8, true)).toMatchObject({
          isValid: false,
          isValidNumeric: false,
        });
      });

      it("should return an object with isValid as false and isValidLowercase as false when the passwords do not include 1 or more lowercase", () => {
        expect(isValid("ASDFQW12$", "ASDFQW12$", 8, true)).toMatchObject({
          isValid: false,
          isValidLowercase: false,
        });
      });

      it("should return an object with isValid as false and isValidUppercase as false when the passwords do not include 1 or more uppercase", () => {
        expect(isValid("asdfqw12$", "asdfqw12$", 8, true)).toMatchObject({
          isValid: false,
          isValidUppercase: false,
        });
      });

      it("should return an object with isValid as false and isValidSpecialCharacter as false when the passwords do not include 1 or more special characters", () => {
        expect(isValid("asdfQW123", "asdfQW123", 8, true)).toMatchObject({
          isValid: false,
          isValidSpecialCharacter: false,
        });
      });

      it("should return an object with isValid as true when the passwords are valid", () => {
        expect(isValid("asdfQW12$", "asdfQW12$", 8, true)).toMatchObject({
          isValid: true,
        });
      });
    });

    describe("is not advanced security", () => {
      it("should return an object with isValid as false and isValidLength as false when the passwords are under 6 characters long", () => {
        expect(isValid("asdf", "asdf")).toMatchObject({
          isValid: false,
          isValidLength: false,
        });
      });

      it("should return an object with isValid as false and isValidPasswords as false when the passwords are not equal", () => {
        expect(isValid("asdfasdf", "asdfqwer")).toMatchObject({
          isValid: false,
          isValidPasswords: false,
        });
      });

      it("should return an object with isValid as true when the passwords are valid", () => {
        expect(isValid("asdfas", "asdfas")).toMatchObject({
          isValid: true,
        });
      });
    });
  });
});
