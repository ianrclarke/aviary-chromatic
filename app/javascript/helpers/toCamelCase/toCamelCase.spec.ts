import { toCamelCase } from "@helpers/toCamelCase";

describe("toCamelCase", () => {
  it("returns null when we pass in null", () => {
    const expected = null;

    const actual = toCamelCase(null);

    expect(actual).toEqual(expected);
  });

  it("returns a camel cased string when we pass in a string that contains spaces", () => {
    const expected = "thisIsNotTheWay";

    const actual = toCamelCase("This is not the way");

    expect(actual).toEqual(expected);
  });

  it("returns a string when we pass in a string without any spaces", () => {
    const expected = "something";

    const actual = toCamelCase("something");

    expect(actual).toEqual(expected);
  });

  it("returns an empty string when we pass in an empty string", () => {
    const expected = "";

    const actual = toCamelCase("");

    expect(actual).toEqual(expected);
  });
});
