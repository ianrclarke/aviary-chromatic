// WARNING: This function may not work for all languages

const toCamelCase = (label: string): string => {
  if (label === null) {
    return null;
  }

  const elements = label.toLowerCase().split(" ");
  return elements
    .map((element, index) => {
      if (index === 0) {
        return element;
      }
      const firstCharacter = element[0].toUpperCase();
      return firstCharacter + element.substring(1);
    })
    .join("");
};

export { toCamelCase };
