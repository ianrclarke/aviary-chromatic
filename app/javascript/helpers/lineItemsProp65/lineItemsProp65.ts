export const lineItemsProp65 = lineItems => {
  return lineItems.some(lineItem => lineItem.variant.product.proposition65);
};
