import { createLineItemShard } from "@shared/data/shards/mocks";

import { lineItemsProp65 } from "./lineItemsProp65";

describe("lineItemsProp65", () => {
  let lineItems;
  it("returns true if a lineItem is prop 65", () => {
    lineItems = [
      createLineItemShard("item 1", {
        VariantShardOptions: {
          productFragmentOptions: {
            proposition65: true,
          },
        },
      }),
      createLineItemShard("item 2", {
        VariantShardOptions: {
          productFragmentOptions: {
            proposition65: false,
          },
        },
      }),
    ];

    expect(lineItemsProp65(lineItems)).toEqual(true);
  });
  it("returns false if no lineItem is prop 65", () => {
    lineItems = [
      createLineItemShard("item 1", {
        VariantShardOptions: {
          productFragmentOptions: {
            proposition65: false,
          },
        },
      }),
      createLineItemShard("item 2", {
        VariantShardOptions: {
          productFragmentOptions: {
            proposition65: false,
          },
        },
      }),
    ];
    expect(lineItemsProp65(lineItems)).toEqual(false);
  });
});
