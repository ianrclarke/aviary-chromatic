export const isNullOrUndefined = item => {
  return typeof item === "undefined" || item === null;
};
