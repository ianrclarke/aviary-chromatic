export const lineItemsInvalid = lineItems => {
  return lineItems.some(lineItem => !lineItem.variant.validForSale);
};
