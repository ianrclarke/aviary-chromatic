import { createLineItemShard } from "@shared/data/shards/mocks";

import { lineItemsInvalid } from "./lineItemsInvalid";

describe("lineItemsInvalid", () => {
  let lineItems;
  it("returns true if a lineItem is not Valid for sale", () => {
    lineItems = [
      createLineItemShard("item 1", {
        VariantShardOptions: {
          variantFragmentOptions: { validForSale: false },
        },
      }),
      createLineItemShard("item 2", {
        VariantShardOptions: {
          variantFragmentOptions: { validForSale: false },
        },
      }),
    ];

    expect(lineItemsInvalid(lineItems)).toEqual(true);
  });
  it("returns false if no lineItem is invalid", () => {
    lineItems = [createLineItemShard("item 1"), createLineItemShard("item 2")];

    expect(lineItemsInvalid(lineItems)).toEqual(false);
  });
});
