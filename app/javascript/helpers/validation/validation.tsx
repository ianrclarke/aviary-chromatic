const allFormFieldsFilled = formData => {
  return Object.values(formData).every(value => {
    return value !== "" && value !== null && value !== undefined;
  });
};

export { allFormFieldsFilled };
