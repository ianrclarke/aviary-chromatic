import { allFormFieldsFilled } from "./validation";

describe("validation", () => {
  it("returns false if any field has no value", () => {
    const formData = {
      thing: null,
      thing2: "hello",
    };
    expect(allFormFieldsFilled(formData)).toEqual(false);
  });
  it("returns true if all fields have a value", () => {
    const formData = {
      thing: "hello",
      thing2: "hello",
    };
    expect(allFormFieldsFilled(formData)).toEqual(true);
  });
});
