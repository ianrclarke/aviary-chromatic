import { render } from "react-dom";

export const renderComponents = (components: object) => {
  const componentAttr = "data-component";
  const nodeSelector = `[${componentAttr}]`;
  const nodesToRender = Array.from(document.querySelectorAll(nodeSelector));

  if (nodesToRender.length === 0) {
    // eslint-disable-next-line no-console
    return console.warn("renderComponents() found no elements using the selector " + nodeSelector);
  }

  nodesToRender.forEach(node => {
    const componentName = node.attributes["data-component"].value;
    const Component = components[componentName];

    if (!Component) {
      // eslint-disable-next-line no-console
      return console.warn(`renderComponents() was not passed a component for ${componentName}`);
    }

    const props = node.attributes["data-component-props"].value;

    render(Component(props ? JSON.parse(props) : undefined), node);
  });
};
