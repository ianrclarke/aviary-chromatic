export const lineItemsHeatsensitive = lineItems => {
  return lineItems.some(lineItem => lineItem.variant.product.heatSensitive);
};
