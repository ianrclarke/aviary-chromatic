import { createLineItemShard } from "@shared/data/shards/mocks";

import { lineItemsHeatsensitive } from "./lineItemsHeatsensitive";

describe("lineItemsHeatsensitive", () => {
  let lineItems;
  it("returns true if a lineItem is heat sensitive", () => {
    lineItems = [
      createLineItemShard("item 1", {
        VariantShardOptions: {
          productFragmentOptions: {
            heatSensitive: true,
          },
        },
      }),
      createLineItemShard("item 2", {
        VariantShardOptions: {
          productFragmentOptions: {
            heatSensitive: false,
          },
        },
      }),
    ];

    expect(lineItemsHeatsensitive(lineItems)).toEqual(true);
  });
  it("returns false if no lineItem is heat sensitive", () => {
    lineItems = [
      createLineItemShard("item 1", {
        VariantShardOptions: {
          productFragmentOptions: {
            heatSensitive: false,
          },
        },
      }),
      createLineItemShard("item 2", {
        VariantShardOptions: {
          productFragmentOptions: {
            heatSensitive: false,
          },
        },
      }),
    ];
    expect(lineItemsHeatsensitive(lineItems)).toEqual(false);
  });
});
