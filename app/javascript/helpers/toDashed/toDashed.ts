const toDashed = (label: string): string => {
  if (label === null) {
    return null;
  }

  const lowerCased = label.toLowerCase();
  const splitUp = lowerCased.split(" ");
  return splitUp.join("-");
};

export { toDashed };
