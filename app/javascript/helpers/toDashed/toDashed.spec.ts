import { toDashed } from "@helpers/toDashed";

describe("toDashed", () => {
  it("returns null when we pass in null", () => {
    const expected = null;

    const actual = toDashed(null);

    expect(actual).toEqual(expected);
  });

  it("returns the dashed string when there are spaces", () => {
    const expected = "this-is-not-the-way";

    const actual = toDashed("This is not the way");

    expect(actual).toEqual(expected);
  });

  it("returns the string when there are no strings", () => {
    const expected = "string";

    const actual = toDashed("string");

    expect(actual).toEqual(expected);
  });

  it("returns an empty string when an empty string is passed in", () => {
    const expected = "";

    const actual = toDashed("");

    expect(actual).toEqual(expected);
  });
});
