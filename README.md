# Fullscript

[![Dependency Status](https://gemnasium.com/badges/d437fe9e2b262cc5fc0cd2d5be4d6796.svg)](https://gemnasium.com/1edf247f441f85cb6393aa811faeb469) [![build status](https://git.fullscript.io/developers/hw-admin/badges/staging/build.svg)](https://git.fullscript.io/developers/hw-admin/pipelines) [![Coverage Status](https://coveralls.io/repos/HealthWave/hw_admin/badge.svg?branch=staging&service=github&t=WV3C4c)](https://coveralls.io/github/HealthWave/hw_admin?branch=staging)

## Dev Setup

Make sure to setup spring & bootsnap for a speedy local environment.

[Instructions here](https://paper.dropbox.com/doc/Make-hw_admin-boot-up-fast-with-bootsnap-and-spring-STwuPZzO3pCMSGo0mTc2v)

Download a fresh copy of the [daily database scrub](https://devops-files.fullscript.io/staging_db/). You can import the database using the instructions in `restore_mysql_dump.md`.

Create the default accounts for admin, practitioners, clerks, and patients

```bash
$ rake dev_bootstrap:setup
Connected to Elastic version 1.7.6.
Cleaning data from whitelisted tables...Done!
Seeding users...
Seeding roles...
Seeding admins...
Seeding stores...
Seeding designations...
Seeding emerson_doctor_types...
Seeding practitioner_types...
Seeding practitioners...
Seeding patients...
Seeding clerks...
Seeding landing_pages...
Seeding certifications...
Seeding merchant_data...
Seeding api_partners...
Seeding partner_stores...
Setting up store payment method...Done!
Flipping the flippers...
************************************************************************
All done! Don't forget to reindex Elastic ( rake es:dev:rebuild ).

$ rake es:dev:rebuild
Connected to Elastic version 1.7.6.
alias => index is dev_catalog => dev_catalog-2018-04-06
Catalog template pushed. Takes effect only on new Indexes
created index: dev_catalog-2018-04-06
Products | ......This is slowwww: [██████......................................................................................................] 25.6/s 6% 00:00:59, ETA: 00:14:10
```

Celebrate!

Documentation regarding Webpacker and React-Rails can be found [here](https://paper.dropbox.com/doc/Webpacker-and-React-Rails-s9i63nCXTlYcqFYL4ztF3)

### Staging Environments

[Documentation HERE](https://git.fullscript.io/developers/hw-admin/issues/8058)

## Staging or Scrubbed Databases (Now with seeds!)

If you need a copy of the (cleaned/scrubbed) current staging database they will be generated daily here: [https://devops-files.fullscript.io/staging_db/](https://devops-files.fullscript.io/staging_db/).

In the folder above you will find the raw database scrubbed of all EPHI and their seeded versions.
```
staging-ca-06-12-18-seeded.sql.gz                  12-Jun-2018 04:01             5304908
staging-ca-06-12-18.sql.gz                         12-Jun-2018 04:00             5458215
staging-us-06-12-18-seeded.sql.gz                  12-Jun-2018 03:05            76542018
staging-us-06-12-18.sql.gz                         12-Jun-2018 03:02            77140793
```

The scrubs are created by a process in the [fs-hauser](https://git.fullscript.io/devops/fs-hauser) repository with a daily cron task. See [scripts/create_staging_db.sh](https://git.fullscript.io/devops/fs-hauser/blob/master/scripts/create_staging_db.sh).
The seeded versions are created with the rake task `dev:setup:staging` in hw-admin. This is the same process used when we create review environments.

That site is only accessible either from the office or on the VPN. If you need some retention, or there is a process missing to keep that DB up to date please let the DevOps team know.

### Making a Payout

1.  Create an Order and Ship it
2.  use ranchit script to 'ssh' into the environment
3.  If you don't know the Store ID run

```
> rake dev:stores
```

4.  Take Store ID and plug into this rake task:

```
> rake dev:make_payout[STORE_ID]
```

done. Payout created and Stripe Transfer created.

If something prevents this from working, ie. perhaps Order not shipped?
then the rake task will thrown an exception with a somewhat useful message.

## Server Configuration

* environment

  * App Server: Passenger 3
  * Stack: stable-v2
  * Runtime: Ruby 1.9.3
  * RubyGems: 1.8.17
  * Database: MySQL 5.0.x

* instances
  * Application Master
    * High CPU Medium (64 bit)
    * 30 GB
  * Master Database
    * High CPU Medium (64 bit)
    * 50 GB
  * Utility Instance
    * Name: resque
    * High CPU Medium (64 bit)
    * 30 GB

## Installation Notes

* create a directory on both the app master and utility at /data/shared/config
* add all YML configuration to the /data/shared/config directory
* upload and apply custom recipes (https://github.com/HealthWave/hw-ey-recipes/tree/us)
* install wkhtmltopdf (64-bit instructions => https://github.com/pdfkit/pdfkit/wiki/Engine-Yard)
  * note that this needs to be installed on the utility instance as well


### ERP events setup AKA. https://paper.dropbox.com/doc/ERP-events-for-devs-NMzNcntn28oBN6F4fPmoC

* Install PostgreSQL `brew install postgresql`
* Note: To run postgres use `brew services start postgres`
* Create a PostgreSQL DB for development environment `createdb erp_event_db`
* Create a PostgreSQL DB for test environment `createdb erp_event_test_db`
* Add PostgreSQL development info to `_env.rb` file `ENV['FS_EVENT_EMITTER_DEVELOPMENT_DB'] = "postgres://user@localhost/erp_event_db"`
* Add PostgreSQL test `_env.rb` file `ENV['FS_EVENT_EMITTER_TEST_DB'] = "postgres://user@localhost/erp_event_test_db"`
* Run migrations for ERP Event Emitter development DB `FS_EVENT_EMITTER=true bundle exec rake db:migrate`
* Run migrations for ERP Event Emitter test `FS_EVENT_EMITTER=true RAILS_ENV=test bundle exec rake db:migrate`  

## Dependencies

* Airbrake http://status.airbrake.io/ https://twitter.com/airbrake
* Amazon S3 http://status.aws.amazon.com/
* DNSimple http://dnsimplestatus.com/ https://twitter.com/dnsimple
* Emerson IN-APP
* Intercom https://twitter.com/intercomstatus
* Mandrill http://status.mandrillapp.com/ https://twitter.com/mandrillapp
* Mailchimp http://status.mailchimp.com/ https://twitter.com/mailchimpstatus
* Google http://www.google.com/appsstatus#hl=en&v=status
* Geonames
* Apps
* Slack https://status.slack.com/
* Zendesk https://twitter.com/zendeskops

## Stripe API Version

The version of the Stripe api used when the app is running is now stored in the Database.
The old env var `STRIPE_API_VERSION` is still used, but only to bootstrap the version value into the Database

To change the version active, one needs the new `Stripe api version manager` Role. Adding the role makes a new admin page available.

See here https://us.fullscript.com/a/stripe_api_version

A list of available version is present to choose. The list is hardcoded and requires a code change and a deploy to be changed.

Changing the version on the new admin page is not sufficient for the change to take effect. Depending on the Environment involved you need to cause a "restart" in different ways.

See also: https://paper.dropbox.com/doc/Stripe-API-Version--AlZ9AdViRlDvpDp~DwRAMgP0Ag-RvxFnKrOmB3cxZZ4gHdpF#:uid=070792630849732073483377&h2=DEVS-LOOK-HERE

### Development Env

Just restart your dev environment to see the new Api verison take effect

### Staging/Review Envs

Options to Restart:
    - deploy a change to the enviroment; or
    - run this rx command: `rx st env -b <your branch>` (see devops about the particulars of that command)

* Note: Seeds will run using the `STRIPE_API_VERSION` set for all staging enviroments, not your custom setting. This may mean that Seed Stores in your enviroment were not created with the API version you wanted. Just make new stores in that envrioment.

### The Production envs

Doing a deploy is the only way to make an API version change take effect.

