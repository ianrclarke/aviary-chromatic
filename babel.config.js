module.exports = function (api) {
  api.cache(true);
  const presets = [
    ["@babel/preset-env", { useBuiltIns: "entry", corejs: 3 }], // TODO explore bugfixes: true
    "@babel/preset-react", //TODO once supported by react, add runtime: "automatic"
    "@babel/typescript",
    ["@emotion/babel-preset-css-prop", { sourceMap: false }],
  ];
  const plugins = [
    ["@babel/proposal-decorators", { legacy: true }],
    ["@babel/proposal-class-properties", { loose: true }],
    ["@babel/transform-runtime", { helpers: false, useESModules: true, corejs: false }],
    ["@babel/transform-regenerator", { async: false }],
    "lodash",
  ];

  return {
    presets,
    plugins,
  };
};
