#!/usr/bin/env rake
# Add your own tasks in files placed in lib/tasks ending in .rake,
# for example lib/tasks/capistrano.rake, and they will automatically be available to Rake.

require_relative 'config/application'
require 'benchmark'
require 'resque/tasks'
require 'resque/scheduler/tasks'
require 'sitemap_generator/tasks'

HwAdmin::Application.load_tasks

#NOTE: wrapper around rake tasks to benchmark and save execution time
module Rake
  class Task
    NAMESPACES_TO_MEASURE = %w{alerts recur emerson catalogue materialized_views payouts stripe shipping propack inventory}

    def execute_with_timestamps(*args)
      task_namespace = name.split(':').first

      if NAMESPACES_TO_MEASURE.include?(task_namespace)
      	persistent_benchmark = JobStatus.find_or_initialize_by(task_name: name)
      	tic = Time.now
        bench = Benchmark.measure do
          execute_without_timestamps(*args)
        end
        toc = Time.now
        persistent_benchmark.update_attributes(:start_at => tic, :end_at => toc,
          :real => bench.real, :utime => bench.utime, :stime => bench.stime,
          :cutime => bench.cutime, :cstime => bench.cstime)
        persistent_benchmark.save
      else
      	execute_without_timestamps(*args)
      end
    end

    alias :execute_without_timestamps :execute
    alias :execute :execute_with_timestamps
  end
end

Knapsack.load_tasks if defined?(Knapsack)
task "db:schema:dump": "strong_migrations:alphabetize_columns"
