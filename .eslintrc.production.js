module.exports = {
  extends: [".eslintrc.js"],
  rules: {
    "no-console": "error",
    "no-debugger": "error",
    "import/no-cycle": "error",
    "import/no-self-import": "error",
  },
};
