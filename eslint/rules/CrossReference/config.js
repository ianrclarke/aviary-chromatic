const fs = require("fs");
const path = require("path");

function getModules(dir) {
  return fs
    .readdirSync(path.resolve(__dirname, "../../../app/javascript", dir))
    .map(name => path.join(dir, name))
    .map(name => `@${name}`);
}

module.exports = [
  {
    directory: "",
    modules: getModules(""),
    sharedModules: [
      "@aviary",
      "@legacyComponents",
      "@helpers",
      "@locales",
      "@shared",
      "@styles",
      "@testing",
    ],
    whitelistDirectories: ["graphql_validation", "legacy", "packs", "playground"],
  },
  {
    directory: "checkout",
    modules: getModules("checkout"),
    sharedModules: ["@checkout/shared"],
    whitelistDirectories: [],
  },
  {
    directory: "clinic",
    modules: getModules("clinic"),
    sharedModules: ["@clinic/shared", "@clinic/data"],
    whitelistDirectories: [],
  },
];
