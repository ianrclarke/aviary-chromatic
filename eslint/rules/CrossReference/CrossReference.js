const path = require("path");
const CONFIG = require("./config.js")

module.exports = {
    meta: {
        type: "problem",
        docs: {
            description: "Prevent imports across applications",
            category: "no-cross-imports",
            recommended: false
        },
        fixable: null,
        schema: []
    },

    create: function(context) {
        function getModule(filePath, {directory}) {
          const basePath = path.join("app/javascript", directory);
          const jsIndex = filePath.indexOf(basePath);
          if (jsIndex === -1) {
            return null;
          }
          const name = filePath
            .substring(jsIndex)
            .replace("app/javascript", "")
            .replace(directory, "")
            .split("/")
            .filter(val => val)[0];

          return path.join(directory, name);
        }

        function getImportModule(importPath, config) {
          const jsModule = getModule(importPath, config);
          if (jsModule) {
            return jsModule;
          }

          return (
            config.modules.find(moduleName => importPath.startsWith(`${moduleName}/`)) ||
            config.modules.find(moduleName => importPath.startsWith(moduleName))
          );
        }

        function isValidConfig(node, config) {
          const fileModule = getModule(context.getFilename(), config);
          if (!fileModule) return true;

          const whitelisted = config.whitelistDirectories.includes(fileModule);
          if (whitelisted) return true;

          const importModule = getImportModule(node.source.value, config)
          if (!importModule ) return true;

          const shared = config.sharedModules.includes(importModule);
          if (shared ) return true;

          if (importModule === fileModule || importModule === `@${fileModule}`) return true;

          return false;
        }


        function isValid(node) {
          return CONFIG.every(config=> isValidConfig(node, config));
        }

        return {
            ImportDeclaration(node) {
              if (!isValid(node)){
                context.report({node, message: `Imported module ${node.source.value} breaks crossReference rule`})
              }
            }
        };
    }
};
