const CrossReference = require("./CrossReference/CrossReference.js");

module.exports = {
  rules: {
    "cross-reference": CrossReference,
  },
};
