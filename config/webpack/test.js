process.env.NODE_ENV = process.env.NODE_ENV || "test";

const { environment } = require("@rails/webpacker");

// Plugins

const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const tsCheckerPlugin = new ForkTsCheckerWebpackPlugin({
  eslint: false,
  async: false,
  checkSyntacticErrors: false,
});
environment.plugins.append("ForkTsChecker", tsCheckerPlugin);

const WebpackAssetsManifest = require("webpack-assets-manifest");
const manifestPlugin = new WebpackAssetsManifest({
  entrypoints: true,
  writeToDisk: true,
  publicPath: true,
});
environment.plugins.append("Manifest", manifestPlugin);

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// Loaders
environment.loaders.prepend("babel", {
  test: /\.(js|jsx|ts|tsx)?(\.erb)?$/,
  exclude: /node_modules/,
  loader: "babel-loader",
  options: {
    plugins: ["react-hot-loader/babel"],
  },
});

environment.loaders.insert(
  "sass",
  {
    test: /\.(scss|sass)$/,
    exclude: /node_modules/,
    use: [
      { loader: MiniCssExtractPlugin.loader },
      { loader: "css-loader" },
      { loader: "sass-loader" },
    ],
  },
  { after: "babel" }
);

environment.loaders.append("svgr", {
  test: /\.svg$/,
  issuer: {
    test: /\.(js|jsx|ts|tsx)$/,
  },
  use: [
    "@svgr/webpack",
    {
      loader: "url-loader",
      options: {
        esModule: false,
      },
    },
  ],
});

environment.loaders.append("svg", {
  test: /\.svg$/,
  use: ["url-loader"],
});

// Webpack Config
environment.config.merge({
  mode: "development",
  devtool: process.env.WEBPACKER_DEVTOOL || "eval-source-map",
  resolve: {
    alias: {
      "@admin": "admin",
      "@aviary": "aviary",
      "@checkout": "checkout",
      "@clerk": "clinic/clerk",
      "@ehr": "clinic/ehr",
      "@clinic": "clinic",
      "@helpers": "helpers",
      "@legacyComponents": "legacyComponents",
      "@api": "api",
      "@locales": "locales",
      "@oauth": "oauth",
      "@patient": "patient",
      "@shared": "shared",
      "@styles": "styles",
      "@unsubscribe": "unsubscribe",
    },
  },
  output: {
    pathinfo: false,
    publicPath: "/packs-test/",
    filename: "js/[name].js",
  },
  stats: true,
  optimization: {
    minimize: false,
    minimizer: [],
    splitChunks: { chunks: "all" },
  },
});

console.log("Webpacker running in TEST mode");
console.log("NODE_ENV is: " + process.env.NODE_ENV);

module.exports = environment.toWebpackConfig();
