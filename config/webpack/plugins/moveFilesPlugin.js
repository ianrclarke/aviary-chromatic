const path = require("path");
const fs = require("fs");

/**
 * MoveFilesPlugin - Moves file(s) from one directory to another
 *
 * @param {string} fromDir - directory to move file(s) from
 * @param {string} toDir -  directory to move file(s) to
 * @param {RegExp} test - regex to find matching file(s) to move
 */
class MoveFilesPlugin {
  constructor({ fromDir, toDir, test }) {
    const basePath = path.resolve(__dirname, "..", "..", "..");
    console.log("Base Path: ", basePath);
    this.fromDir = basePath + fromDir;
    this.toDir = basePath + toDir;
    this.test = test;
  }

  createToDir() {
    // creates destination directory in case it doesn't already exist
    try {
      fs.mkdirSync(this.toDir, { recursive: true });
    } catch (e) {
      if (e.code !== "EEXIST") {
        throw e;
      }
    }
  }

  apply(compiler) {
    compiler.hooks.afterEmit.tap("MoveFiles", () => {
      const files = fs.readdirSync(this.fromDir).filter(fileName => fileName.match(this.test));
      // Also moves from one dir to another if they are different
      // Overrides anything in the destination directory if something already exists

      this.createToDir();

      // fromDir and toDir need a closing slash
      files.forEach(file => {
        fs.renameSync(`${this.fromDir}${file}`, `${this.toDir}${file}`);
      });
    });
  }
}

module.exports = MoveFilesPlugin;
