process.env.NODE_ENV = process.env.NODE_ENV || "production";

const { environment } = require("@rails/webpacker");

environment.plugins.delete("Compression Brotli"); //disable .br file output
// Plugins

// const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
// const bundleAnalyzerPlugin = new BundleAnalyzerPlugin({
//   openAnalyzer: false,
//   analyzerMode: "static",
// });
// environment.plugins.append("BundleAnalyzer", bundleAnalyzerPlugin);

const WorkboxPlugin = require("workbox-webpack-plugin");
const injectManifestPlugin = new WorkboxPlugin.InjectManifest({
  swSrc: "public/service-worker.js.tmpl",
  swDest: "../service-worker.js",
  exclude: [/^.*\.map\.?.*?$/],
});
environment.plugins.append("InjectManifestPlugin", injectManifestPlugin);

const MoveFilesPlugin = require("./plugins/moveFilesPlugin");
const moveFilesPlugin = new MoveFilesPlugin({
  fromDir: "/public/packs/js/",
  toDir: "/public/sourcemaps/js/",
  test: /^.*\.map\.?.*?$/,
});
environment.plugins.append("MoveFilesPlugin", moveFilesPlugin);

const WebpackAssetsManifest = require("webpack-assets-manifest");
const manifestAssetPlugin = new WebpackAssetsManifest({
  entrypoints: true,
  writeToDisk: true,
  publicPath: true,
  customize: entry => {
    // remove sourcemaps from non entrypoints
    if (entry && entry.value && entry.value.toLowerCase().match(/^.*\.map\.?.*?$/)) {
      return false;
    }
  },
  transform: assets => {
    // Remove sourcemaps from entrypoints
    Object.keys(assets.entrypoints).forEach(entrypointKey => {
      delete assets.entrypoints[entrypointKey]["js.map"];
    });
  },
});
environment.plugins.append("Manifest", manifestAssetPlugin);

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const miniCssPlugin = new MiniCssExtractPlugin({
  filename: "css/[name].css",
  chunkFilename: "css/[id].css",
  // moduleFilename: ({ name }) => `${name.replace("/js/", "/css/")}.css`,
});
environment.plugins.append("MiniCssExtract", miniCssPlugin);

const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const tsCheckerPlugin = new ForkTsCheckerWebpackPlugin({
  eslint: false,
  async: false,
  checkSyntacticErrors: true,
});
environment.plugins.append("ForkTsChecker", tsCheckerPlugin);

const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const optimizeCSSAssets = new OptimizeCSSAssetsPlugin();

const TerserPlugin = require("terser-webpack-plugin");
const terserPlugin = new TerserPlugin({
  cache: true,
  parallel: true,
  extractComments: false,
  terserOptions: {
    ecma: 8,
    warnings: false,
    parse: {},
    compress: { collapse_vars: true },
    mangle: true,
    module: true,
    output: { comments: false },
    toplevel: false,
    nameCache: null,
    ie8: false,
    keep_classnames: undefined,
    keep_fnames: false,
    safari10: false,
  },
});

// Loaders
environment.loaders.prepend("babel", {
  test: /\.(js|jsx|ts|tsx)?(\.erb)?$/,
  exclude: /node_modules/,
  loader: "babel-loader",
  options: {
    plugins: ["react-hot-loader/babel"],
  },
});

environment.loaders.insert(
  "sass",
  {
    test: /\.(scss|sass)$/,
    exclude: /node_modules/,
    use: [
      { loader: MiniCssExtractPlugin.loader },
      { loader: "css-loader" },
      { loader: "postcss-loader" },
      { loader: "sass-loader" },
    ],
  },
  { after: "babel" }
);

environment.loaders.append("svgr", {
  test: /\.svg$/,
  issuer: {
    exclude: {
      test: /\.styles.(js|ts)$/,
    },
    test: /\.(js|jsx|ts|tsx)$/,
  },
  use: [
    "@svgr/webpack",
    {
      loader: "url-loader",
      options: {
        esModule: false,
      },
    },
  ],
});

environment.loaders.append("svg", {
  test: /\.svg$/,
  use: [
    {
      loader: "url-loader",
      options: {
        limit: 8000, // Convert images < 8kb to base64 strings
        name: "images/[hash]-[name].[ext]",
      },
    },
  ],
});

// Webpack Config
environment.config.merge({
  mode: "production",
  devtool: process.env.WEBPACKER_DEVTOOL || "none",
  entry: "app/javascript/packs",
  output: {
    publicPath: (process.env.WEBPACKER_ASSET_HOST || "") + "/packs/",
    filename: "js/[name].[contenthash].js",
    chunkFilename: "js/[name].[chunkhash].js",
    sourcePrefix: "\t",
  },
  stats: true,
  target: "web",
  resolve: {
    alias: {
      "@admin": "admin",
      "@aviary": "aviary",
      "@checkout": "checkout",
      "@clerk": "clinic/clerk",
      "@ehr": "clinic/ehr",
      "@clinic": "clinic",
      "@helpers": "helpers",
      "@legacyComponents": "legacyComponents",
      "@api": "api",
      "@locales": "locales",
      "@oauth": "oauth",
      "@patient": "patient",
      "@shared": "shared",
      "@styles": "styles",
      "@unsubscribe": "unsubscribe",
    },
  },
  // performance: {
  //   maxAssetSize: 200000, // int (in bytes),
  //   maxEntrypointSize: 400000, // int (in bytes)
  // },
  optimization: {
    runtimeChunk: "single", //extract runtime boilerplate into a single chunk
    moduleIds: "hashed",
    providedExports: true,
    usedExports: true,
    concatenateModules: true,
    sideEffects: true,
    minimize: true,
    minimizer: [terserPlugin, optimizeCSSAssets],
    splitChunks: {
      chunks: "all",
      minSize: 0,
      maxSize: 0,
      minChunks: 2,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: "~",
      name: true,
      cacheGroups: {
        vendors: false,
        default: false,
        vendor: {
          name: "vendor",
          chunks: "all",
          test: /node_modules/,
          minChunks: 3,
        },
        common: {
          chunks: "initial",
          minChunks: 2,
        },
      },
    },
  },
});

console.log("Webpacker running in PRODUCTION mode");
console.log("NODE_ENV is: " + process.env.NODE_ENV);

module.exports = environment.toWebpackConfig();
