process.env.NODE_ENV = process.env.NODE_ENV || "development";

const path = require("path");
const { environment } = require("@rails/webpacker");

// Delete preconfigured plugins and loaders
environment.plugins.delete("CaseSensitivePaths");
environment.plugins.delete("MiniCssExtract");
environment.plugins.delete("Environment");
environment.plugins.delete("Manifest");

environment.loaders.delete("css");
environment.loaders.delete("babel");
environment.loaders.delete("sass");
environment.loaders.delete("moduleCss");
environment.loaders.delete("moduleSass");

const APP_JAVASCRIPT = path.resolve(__dirname, "..", "..", "app", "javascript");
// Plugins

// const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
// const bundleAnalyzerPlugin = new BundleAnalyzerPlugin({
//   openAnalyzer: false,
//   analyzerMode: "static",
// });
// environment.plugins.append("BundleAnalyzer", bundleAnalyzerPlugin);

const WebpackAssetsManifest = require("webpack-assets-manifest");
const manifestPlugin = new WebpackAssetsManifest({
  entrypoints: true,
  writeToDisk: true,
  publicPath: true,
});
environment.plugins.append("Manifest", manifestPlugin);

const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const tsCheckerPlugin = new ForkTsCheckerWebpackPlugin({
  tsconfig: path.resolve(__dirname, "..", "..", "tsconfig.local.json"),
  eslint: false,
  async: false,
  checkSyntacticErrors: false,
});
environment.plugins.append("ForkTsChecker", tsCheckerPlugin);

const SpeedMeasurePlugin = require("speed-measure-webpack-plugin");
const speedMeasurePlugin = new SpeedMeasurePlugin();

// Loaders
environment.loaders.insert("babel", {
  test: /\.(ts|tsx)$/,
  include: APP_JAVASCRIPT,
  exclude: [/node_modules/, /\.spec\.(ts|tsx)$/, /\.stories\.(ts|tsx)$/],
  loader: "babel-loader",
  options: {
    plugins: ["react-hot-loader/babel"],
  },
});

environment.loaders.insert(
  "reactHotReload",
  {
    test: /\.(js|jsx)$/,
    use: "react-hot-loader/webpack",
    include: /node_modules\/react-dom/,
  },
  { after: "babel" }
);

environment.loaders.insert("sass", {
  test: /\.(scss|sass)$/,
  include: [APP_JAVASCRIPT],
  use: [
    { loader: "style-loader" },
    {
      loader: "css-loader",
      options: {
        sourceMap: false,
      },
    },
    {
      loader: "sass-loader",
      options: {
        sourceMap: false,
      },
    },
  ],
});

environment.loaders.insert("css", {
  test: /\.(css)$/,
  include: [path.resolve(__dirname, "..", "..", "node_modules", "react-image-crop")],
  use: [
    { loader: "style-loader" },
    {
      loader: "css-loader",
      options: {
        sourceMap: false,
      },
    },
  ],
});

environment.loaders.append("svgr", {
  test: /\.svg$/,
  include: APP_JAVASCRIPT,
  issuer: {
    exclude: {
      test: /\.styles.(js|ts)$/,
    },
    test: /\.(js|jsx|ts|tsx)$/,
  },
  use: [
    "@svgr/webpack",
    {
      loader: "url-loader",
      options: {
        esModule: false,
      },
    },
  ],
});

environment.loaders.append("svg", {
  test: /\.svg$/,
  include: APP_JAVASCRIPT,
  use: [
    {
      loader: "url-loader",
      options: {
        limit: 8000, // Convert images < 8kb to base64 strings
        name: "images/[hash]-[name].[ext]",
      },
    },
  ],
});

environment.loaders.insert(
  "cache",
  {
    use: "cache-loader",
  },
  { after: "file" }
);

// Webpack Config
environment.config.merge({
  mode: "development",
  devtool: process.env.WEBPACKER_DEVTOOL || "eval-source-map",
  devServer: {
    hot: true,
  },
  resolve: {
    alias: {
      "@admin": "admin",
      "@aviary": "aviary",
      "@checkout": "checkout",
      "@clerk": "clinic/clerk",
      "@ehr": "clinic/ehr",
      "@clinic": "clinic",
      "@helpers": "helpers",
      "@legacyComponents": "legacyComponents",
      "@api": "api",
      "@locales": "locales",
      "@oauth": "oauth",
      "@patient": "patient",
      "@shared": "shared",
      "@styles": "styles",
      "@unsubscribe": "unsubscribe",
    },
  },
  output: {
    pathinfo: false,
    publicPath: "/packs/",
    filename: "js/[name].js",
    chunkFilename: "js/[name].js",
  },
  stats: {
    children: false,
    modules: false,
  },
  optimization: {
    providedExports: false,
    usedExports: false,
    concatenateModules: false,
    sideEffects: false,
    minimize: false,
    minimizer: [],
    runtimeChunk: "single",
    splitChunks: { chunks: "all" },
  },
  watchOptions: {
    ignored: ["**/node_modules/**", "**/*.spec.*", "**/*.stories.*"],
  },
});

console.log("Webpacker running in DEVELOPMENT mode");
console.log("NODE_ENV is: " + process.env.NODE_ENV);

module.exports = speedMeasurePlugin.wrap(environment.toWebpackConfig());
