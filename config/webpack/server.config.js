const path = require("path");
const webpack = require("webpack");

const entryPoint = path.resolve("app/javascript/server/components");
const outputPath = path.resolve("tmp/server");
const aliasPath = alias => path.resolve("app/javascript", alias);

module.exports = {
  context: __dirname,
  mode: "development",
  watch: true,
  entry: {
    app: entryPoint,
  },
  module: {
    rules: [
      {
        test: /\.svg$/,
        issuer: {
          exclude: {
            test: /\.styles.(js|ts)$/,
          },
          test: /\.(js|jsx|ts|tsx)$/,
        },
        use: [
          "@svgr/webpack",
          {
            loader: "url-loader",
            options: {
              esModule: false,
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 8000, // Convert images < 8kb to base64 strings
              name: "images/[hash]-[name].[ext]",
            },
          },
        ],
      },
      {
        test: /\.(png|jpeg)$/,
        use: [{ loader: "url-loader" }],
      },
      {
        test: /\.(scss|sass)$/,
        use: [
          {
            loader: "css-loader",
          },
          { loader: "postcss-loader" },
          { loader: "sass-loader" },
        ],
      },
      {
        exclude: /node_modules/,
        loader: "babel-loader",
        test: /\.(js|jsx|ts|tsx)?(\.erb)?$/,
        options: {
          plugins: ["react-hot-loader/babel"],
        },
      },
    ],
  },
  target: "node",
  output: {
    filename: "components-bundle.js",
    libraryTarget: "commonjs2",
    path: outputPath,
  },
  resolve: {
    alias: {
      "@admin": aliasPath("admin"),
      "@aviary": aliasPath("aviary"),
      "@checkout": aliasPath("checkout"),
      "@clerk": aliasPath("clinic/clerk"),
      "@ehr": aliasPath("clinic/ehr"),
      "@clinic": aliasPath("clinic"),
      "@helpers": aliasPath("helpers"),
      "@legacyComponents": aliasPath("legacyComponents"),
      "@api": aliasPath("api"),
      "@locales": aliasPath("locales"),
      "@oauth": aliasPath("oauth"),
      "@patient": aliasPath("patient"),
      "@shared": aliasPath("shared"),
      "@styles": aliasPath("styles"),
      "@unsubscribe": aliasPath("unsubscribe"),
    },
    extensions: [".ts", ".tsx", ".js", ".jsx"],
  },
};
