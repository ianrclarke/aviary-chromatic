import { addDecorator, addParameters } from "@storybook/react";
import { withKnobs } from "@storybook/addon-knobs";

import StoryRouter from "storybook-react-router";

import "../../app/javascript/styles/framework.scss";

addDecorator(withKnobs);

addDecorator(StoryRouter());

import yourTheme from "../yourTheme";

addParameters({
  options: {
    theme: yourTheme,
  },
});
