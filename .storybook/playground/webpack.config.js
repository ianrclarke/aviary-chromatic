module.exports = async ({ config, mode }) => {
  config.resolve.modules = ["app/javascript", "node_modules"];

  config.resolve.alias = {
    "@aviary": "aviary",
    "@checkout": "checkout",
    "@clerk": "clinic/clerk",
    "@ehr": "clinic/ehr",
    "@clinic": "clinic",
    "@helpers": "helpers",
    "@legacyComponents": "legacyComponents",
    "@api": "api",
    "@locales": "locales",
    "@oauth": "oauth",
    "@patient": "patient",
    "@shared": "shared",
    "@styles": "styles",
    "@unsubscribe": "unsubscribe",
  };

  config.resolve.extensions.push(
    ".tsx",
    ".ts",
    ".mjs",
    ".json",
    ".sass",
    ".scss",
    ".css",
    ".png",
    ".gif",
    ".jpeg",
    ".jpg"
  );

  config.module.rules.push({
    test: /\.(scss|sass)$/,
    exclude: /node_modules/,
    use: ["style-loader", "css-loader", "sass-loader"],
  });

  return config;
};
