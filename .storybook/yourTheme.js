import { create } from "@storybook/theming";
import { colors } from "../app/javascript/styles/index";

export default create({
  base: "light",

  colorPrimary: colors.primary,
  colorSecondary: colors.primary,

  // UI
  appBg: colors.background,
  appContentBg: colors.white,
  appBorderColor: colors.background,
  appBorderRadius: 4,

  // Typography
  fontBase: '"Open Sans", sans-serif',
  fontCode: "monospace",

  // Text colors
  textColor: colors.dark,
  textInverseColor: "rgba(255,255,255,0.9)",

  // Toolbar default and active colors
  barTextColor: colors.white,
  barSelectedColor: colors.primary,
  barBg: colors.dark,

  // Form colors
  inputBg: "white",
  inputBorder: "silver",
  inputTextColor: "black",
  inputBorderRadius: 4,

  brandTitle: "Fullscript Aviary UI Kit",
  brandUrl: "https://brand.fullscript.com",
  brandImage: "https://fullscript.com/wp-content/uploads/2018/03/fullscript-logo.svg",
});
