import { withA11y } from "@storybook/addon-a11y";
import { withKnobs } from "@storybook/addon-knobs";
import { addDecorator, addParameters } from "@storybook/react";
import { withDesign } from "storybook-addon-designs";
import { DocsPage, DocsContainer } from "@storybook/addon-docs/blocks";
import StoryRouter from "storybook-react-router";

import "../app/javascript/styles/framework.scss";

addDecorator(withKnobs);
addDecorator(withA11y);
addDecorator(withDesign);

addDecorator(StoryRouter());

import yourTheme from "./yourTheme";
import React from "react";

addDecorator(storyFn => (
  <div
    style={{
      display: "flex",
      justifyContent: "center",
      paddingTop: "2rem",
    }}
  >
    {storyFn()}
  </div>
));

addParameters({
  options: {
    theme: yourTheme,
  },
  docs: {
    container: DocsContainer,
    page: DocsPage,
  },
});
