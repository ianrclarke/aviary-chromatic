module.exports = {
  addons: [
    {
      name: "@storybook/preset-typescript",
      options: {
        forkTsCheckerWebpackPluginOptions: {
          colors: false,
        },
        transpileManager: false,
      },
    },
    "@storybook/addon-a11y",
    "@storybook/addon-actions",
    "@storybook/addon-docs",
    "@storybook/addon-knobs",
    "@storybook/addon-links",
    "storybook-addon-designs",
    "@storybook/addon-notes",
  ],
  stories: ["../app/javascript/**/*.stories.tsx"],
};
